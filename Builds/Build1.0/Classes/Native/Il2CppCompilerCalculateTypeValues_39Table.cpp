﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Facebook.Unity.CallbackManager
struct CallbackManager_t2446104503;
// Facebook.Unity.Canvas.ICanvasFacebookCallbackHandler
struct ICanvasFacebookCallbackHandler_t2278759577;
// Facebook.Unity.Canvas.ICanvasJSWrapper
struct ICanvasJSWrapper_t2851222927;
// Facebook.Unity.Example.GraphRequest
struct GraphRequest_t4047451309;
// Facebook.Unity.HideUnityDelegate
struct HideUnityDelegate_t1353799728;
// Facebook.Unity.IFacebookImplementation
struct IFacebookImplementation_t2218504286;
// Facebook.Unity.InitDelegate
struct InitDelegate_t3081360126;
// Facebook.Unity.ResultContainer
struct ResultContainer_t4150301447;
// Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>
struct Callback_1_t3507265931;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_t3662770472;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t2690840144;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Void
struct Void_t1185182177;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;




#ifndef U3CMODULEU3E_T692745549_H
#define U3CMODULEU3E_T692745549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745549 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745549_H
#ifndef U3CMODULEU3E_T692745551_H
#define U3CMODULEU3E_T692745551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745551 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745551_H
#ifndef U3CMODULEU3E_T692745550_H
#define U3CMODULEU3E_T692745550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745550 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745550_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ANDROIDWRAPPER_T4285589061_H
#define ANDROIDWRAPPER_T4285589061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Android.AndroidWrapper
struct  AndroidWrapper_t4285589061  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDWRAPPER_T4285589061_H
#ifndef U3CU3EC_T4183747061_H
#define U3CU3EC_T4183747061_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Canvas.CanvasFacebook/<>c
struct  U3CU3Ec_t4183747061  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t4183747061_StaticFields
{
public:
	// Facebook.Unity.Canvas.CanvasFacebook/<>c Facebook.Unity.Canvas.CanvasFacebook/<>c::<>9
	U3CU3Ec_t4183747061 * ___U3CU3E9_0;
	// Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer> Facebook.Unity.Canvas.CanvasFacebook/<>c::<>9__40_0
	Callback_1_t3507265931 * ___U3CU3E9__40_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4183747061_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4183747061 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4183747061 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4183747061 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__40_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4183747061_StaticFields, ___U3CU3E9__40_0_1)); }
	inline Callback_1_t3507265931 * get_U3CU3E9__40_0_1() const { return ___U3CU3E9__40_0_1; }
	inline Callback_1_t3507265931 ** get_address_of_U3CU3E9__40_0_1() { return &___U3CU3E9__40_0_1; }
	inline void set_U3CU3E9__40_0_1(Callback_1_t3507265931 * value)
	{
		___U3CU3E9__40_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__40_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T4183747061_H
#ifndef U3CU3EC__DISPLAYCLASS47_0_T3013172403_H
#define U3CU3EC__DISPLAYCLASS47_0_T3013172403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Canvas.CanvasFacebook/<>c__DisplayClass47_0
struct  U3CU3Ec__DisplayClass47_0_t3013172403  : public RuntimeObject
{
public:
	// Facebook.Unity.ResultContainer Facebook.Unity.Canvas.CanvasFacebook/<>c__DisplayClass47_0::result
	ResultContainer_t4150301447 * ___result_0;
	// Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer> Facebook.Unity.Canvas.CanvasFacebook/<>c__DisplayClass47_0::callback
	Callback_1_t3507265931 * ___callback_1;

public:
	inline static int32_t get_offset_of_result_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass47_0_t3013172403, ___result_0)); }
	inline ResultContainer_t4150301447 * get_result_0() const { return ___result_0; }
	inline ResultContainer_t4150301447 ** get_address_of_result_0() { return &___result_0; }
	inline void set_result_0(ResultContainer_t4150301447 * value)
	{
		___result_0 = value;
		Il2CppCodeGenWriteBarrier((&___result_0), value);
	}

	inline static int32_t get_offset_of_callback_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass47_0_t3013172403, ___callback_1)); }
	inline Callback_1_t3507265931 * get_callback_1() const { return ___callback_1; }
	inline Callback_1_t3507265931 ** get_address_of_callback_1() { return &___callback_1; }
	inline void set_callback_1(Callback_1_t3507265931 * value)
	{
		___callback_1 = value;
		Il2CppCodeGenWriteBarrier((&___callback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS47_0_T3013172403_H
#ifndef U3CTAKESCREENSHOTU3EC__ITERATOR0_T3544038915_H
#define U3CTAKESCREENSHOTU3EC__ITERATOR0_T3544038915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0
struct  U3CTakeScreenshotU3Ec__Iterator0_t3544038915  : public RuntimeObject
{
public:
	// System.Int32 Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::<width>__0
	int32_t ___U3CwidthU3E__0_0;
	// System.Int32 Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::<height>__0
	int32_t ___U3CheightU3E__0_1;
	// UnityEngine.Texture2D Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::<tex>__0
	Texture2D_t3840446185 * ___U3CtexU3E__0_2;
	// System.Byte[] Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::<screenshot>__0
	ByteU5BU5D_t4116647657* ___U3CscreenshotU3E__0_3;
	// UnityEngine.WWWForm Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::<wwwForm>__0
	WWWForm_t4064702195 * ___U3CwwwFormU3E__0_4;
	// Facebook.Unity.Example.GraphRequest Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::$this
	GraphRequest_t4047451309 * ___U24this_5;
	// System.Object Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CwidthU3E__0_0() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U3CwidthU3E__0_0)); }
	inline int32_t get_U3CwidthU3E__0_0() const { return ___U3CwidthU3E__0_0; }
	inline int32_t* get_address_of_U3CwidthU3E__0_0() { return &___U3CwidthU3E__0_0; }
	inline void set_U3CwidthU3E__0_0(int32_t value)
	{
		___U3CwidthU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3E__0_1() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U3CheightU3E__0_1)); }
	inline int32_t get_U3CheightU3E__0_1() const { return ___U3CheightU3E__0_1; }
	inline int32_t* get_address_of_U3CheightU3E__0_1() { return &___U3CheightU3E__0_1; }
	inline void set_U3CheightU3E__0_1(int32_t value)
	{
		___U3CheightU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtexU3E__0_2() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U3CtexU3E__0_2)); }
	inline Texture2D_t3840446185 * get_U3CtexU3E__0_2() const { return ___U3CtexU3E__0_2; }
	inline Texture2D_t3840446185 ** get_address_of_U3CtexU3E__0_2() { return &___U3CtexU3E__0_2; }
	inline void set_U3CtexU3E__0_2(Texture2D_t3840446185 * value)
	{
		___U3CtexU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtexU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CscreenshotU3E__0_3() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U3CscreenshotU3E__0_3)); }
	inline ByteU5BU5D_t4116647657* get_U3CscreenshotU3E__0_3() const { return ___U3CscreenshotU3E__0_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CscreenshotU3E__0_3() { return &___U3CscreenshotU3E__0_3; }
	inline void set_U3CscreenshotU3E__0_3(ByteU5BU5D_t4116647657* value)
	{
		___U3CscreenshotU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscreenshotU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwFormU3E__0_4() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U3CwwwFormU3E__0_4)); }
	inline WWWForm_t4064702195 * get_U3CwwwFormU3E__0_4() const { return ___U3CwwwFormU3E__0_4; }
	inline WWWForm_t4064702195 ** get_address_of_U3CwwwFormU3E__0_4() { return &___U3CwwwFormU3E__0_4; }
	inline void set_U3CwwwFormU3E__0_4(WWWForm_t4064702195 * value)
	{
		___U3CwwwFormU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwFormU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U24this_5)); }
	inline GraphRequest_t4047451309 * get_U24this_5() const { return ___U24this_5; }
	inline GraphRequest_t4047451309 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(GraphRequest_t4047451309 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTAKESCREENSHOTU3EC__ITERATOR0_T3544038915_H
#ifndef FACEBOOKBASE_T1615169142_H
#define FACEBOOKBASE_T1615169142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookBase
struct  FacebookBase_t1615169142  : public RuntimeObject
{
public:
	// Facebook.Unity.InitDelegate Facebook.Unity.FacebookBase::onInitCompleteDelegate
	InitDelegate_t3081360126 * ___onInitCompleteDelegate_0;
	// System.Boolean Facebook.Unity.FacebookBase::<Initialized>k__BackingField
	bool ___U3CInitializedU3Ek__BackingField_1;
	// Facebook.Unity.CallbackManager Facebook.Unity.FacebookBase::<CallbackManager>k__BackingField
	CallbackManager_t2446104503 * ___U3CCallbackManagerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_onInitCompleteDelegate_0() { return static_cast<int32_t>(offsetof(FacebookBase_t1615169142, ___onInitCompleteDelegate_0)); }
	inline InitDelegate_t3081360126 * get_onInitCompleteDelegate_0() const { return ___onInitCompleteDelegate_0; }
	inline InitDelegate_t3081360126 ** get_address_of_onInitCompleteDelegate_0() { return &___onInitCompleteDelegate_0; }
	inline void set_onInitCompleteDelegate_0(InitDelegate_t3081360126 * value)
	{
		___onInitCompleteDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___onInitCompleteDelegate_0), value);
	}

	inline static int32_t get_offset_of_U3CInitializedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FacebookBase_t1615169142, ___U3CInitializedU3Ek__BackingField_1)); }
	inline bool get_U3CInitializedU3Ek__BackingField_1() const { return ___U3CInitializedU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CInitializedU3Ek__BackingField_1() { return &___U3CInitializedU3Ek__BackingField_1; }
	inline void set_U3CInitializedU3Ek__BackingField_1(bool value)
	{
		___U3CInitializedU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CCallbackManagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FacebookBase_t1615169142, ___U3CCallbackManagerU3Ek__BackingField_2)); }
	inline CallbackManager_t2446104503 * get_U3CCallbackManagerU3Ek__BackingField_2() const { return ___U3CCallbackManagerU3Ek__BackingField_2; }
	inline CallbackManager_t2446104503 ** get_address_of_U3CCallbackManagerU3Ek__BackingField_2() { return &___U3CCallbackManagerU3Ek__BackingField_2; }
	inline void set_U3CCallbackManagerU3Ek__BackingField_2(CallbackManager_t2446104503 * value)
	{
		___U3CCallbackManagerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackManagerU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKBASE_T1615169142_H
#ifndef IOSWRAPPER_T3090931871_H
#define IOSWRAPPER_T3090931871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.IOS.IOSWrapper
struct  IOSWrapper_t3090931871  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSWRAPPER_T3090931871_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef CANVASFACEBOOK_T419876522_H
#define CANVASFACEBOOK_T419876522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Canvas.CanvasFacebook
struct  CanvasFacebook_t419876522  : public FacebookBase_t1615169142
{
public:
	// System.String Facebook.Unity.Canvas.CanvasFacebook::appId
	String_t* ___appId_3;
	// System.String Facebook.Unity.Canvas.CanvasFacebook::appLinkUrl
	String_t* ___appLinkUrl_4;
	// Facebook.Unity.Canvas.ICanvasJSWrapper Facebook.Unity.Canvas.CanvasFacebook::canvasJSWrapper
	RuntimeObject* ___canvasJSWrapper_5;
	// Facebook.Unity.HideUnityDelegate Facebook.Unity.Canvas.CanvasFacebook::onHideUnityDelegate
	HideUnityDelegate_t1353799728 * ___onHideUnityDelegate_6;
	// System.Boolean Facebook.Unity.Canvas.CanvasFacebook::<LimitEventUsage>k__BackingField
	bool ___U3CLimitEventUsageU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_appId_3() { return static_cast<int32_t>(offsetof(CanvasFacebook_t419876522, ___appId_3)); }
	inline String_t* get_appId_3() const { return ___appId_3; }
	inline String_t** get_address_of_appId_3() { return &___appId_3; }
	inline void set_appId_3(String_t* value)
	{
		___appId_3 = value;
		Il2CppCodeGenWriteBarrier((&___appId_3), value);
	}

	inline static int32_t get_offset_of_appLinkUrl_4() { return static_cast<int32_t>(offsetof(CanvasFacebook_t419876522, ___appLinkUrl_4)); }
	inline String_t* get_appLinkUrl_4() const { return ___appLinkUrl_4; }
	inline String_t** get_address_of_appLinkUrl_4() { return &___appLinkUrl_4; }
	inline void set_appLinkUrl_4(String_t* value)
	{
		___appLinkUrl_4 = value;
		Il2CppCodeGenWriteBarrier((&___appLinkUrl_4), value);
	}

	inline static int32_t get_offset_of_canvasJSWrapper_5() { return static_cast<int32_t>(offsetof(CanvasFacebook_t419876522, ___canvasJSWrapper_5)); }
	inline RuntimeObject* get_canvasJSWrapper_5() const { return ___canvasJSWrapper_5; }
	inline RuntimeObject** get_address_of_canvasJSWrapper_5() { return &___canvasJSWrapper_5; }
	inline void set_canvasJSWrapper_5(RuntimeObject* value)
	{
		___canvasJSWrapper_5 = value;
		Il2CppCodeGenWriteBarrier((&___canvasJSWrapper_5), value);
	}

	inline static int32_t get_offset_of_onHideUnityDelegate_6() { return static_cast<int32_t>(offsetof(CanvasFacebook_t419876522, ___onHideUnityDelegate_6)); }
	inline HideUnityDelegate_t1353799728 * get_onHideUnityDelegate_6() const { return ___onHideUnityDelegate_6; }
	inline HideUnityDelegate_t1353799728 ** get_address_of_onHideUnityDelegate_6() { return &___onHideUnityDelegate_6; }
	inline void set_onHideUnityDelegate_6(HideUnityDelegate_t1353799728 * value)
	{
		___onHideUnityDelegate_6 = value;
		Il2CppCodeGenWriteBarrier((&___onHideUnityDelegate_6), value);
	}

	inline static int32_t get_offset_of_U3CLimitEventUsageU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CanvasFacebook_t419876522, ___U3CLimitEventUsageU3Ek__BackingField_7)); }
	inline bool get_U3CLimitEventUsageU3Ek__BackingField_7() const { return ___U3CLimitEventUsageU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CLimitEventUsageU3Ek__BackingField_7() { return &___U3CLimitEventUsageU3Ek__BackingField_7; }
	inline void set_U3CLimitEventUsageU3Ek__BackingField_7(bool value)
	{
		___U3CLimitEventUsageU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASFACEBOOK_T419876522_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T3119828856_H
#define NULLABLE_1_T3119828856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Single>
struct  Nullable_1_t3119828856 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3119828856, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3119828856, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3119828856_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef SHAREDIALOGMODE_T1679970535_H
#define SHAREDIALOGMODE_T1679970535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ShareDialogMode
struct  ShareDialogMode_t1679970535 
{
public:
	// System.Int32 Facebook.Unity.ShareDialogMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShareDialogMode_t1679970535, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREDIALOGMODE_T1679970535_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef JSBRIDGE_T2057182915_H
#define JSBRIDGE_T2057182915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Canvas.JsBridge
struct  JsBridge_t2057182915  : public MonoBehaviour_t3962482529
{
public:
	// Facebook.Unity.Canvas.ICanvasFacebookCallbackHandler Facebook.Unity.Canvas.JsBridge::facebook
	RuntimeObject* ___facebook_4;

public:
	inline static int32_t get_offset_of_facebook_4() { return static_cast<int32_t>(offsetof(JsBridge_t2057182915, ___facebook_4)); }
	inline RuntimeObject* get_facebook_4() const { return ___facebook_4; }
	inline RuntimeObject** get_address_of_facebook_4() { return &___facebook_4; }
	inline void set_facebook_4(RuntimeObject* value)
	{
		___facebook_4 = value;
		Il2CppCodeGenWriteBarrier((&___facebook_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSBRIDGE_T2057182915_H
#ifndef CONSOLEBASE_T1023975560_H
#define CONSOLEBASE_T1023975560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.ConsoleBase
struct  ConsoleBase_t1023975560  : public MonoBehaviour_t3962482529
{
public:
	// System.String Facebook.Unity.Example.ConsoleBase::status
	String_t* ___status_6;
	// System.String Facebook.Unity.Example.ConsoleBase::lastResponse
	String_t* ___lastResponse_7;
	// UnityEngine.Vector2 Facebook.Unity.Example.ConsoleBase::scrollPosition
	Vector2_t2156229523  ___scrollPosition_8;
	// System.Nullable`1<System.Single> Facebook.Unity.Example.ConsoleBase::scaleFactor
	Nullable_1_t3119828856  ___scaleFactor_9;
	// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::textStyle
	GUIStyle_t3956901511 * ___textStyle_10;
	// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::buttonStyle
	GUIStyle_t3956901511 * ___buttonStyle_11;
	// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::textInputStyle
	GUIStyle_t3956901511 * ___textInputStyle_12;
	// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::labelStyle
	GUIStyle_t3956901511 * ___labelStyle_13;
	// UnityEngine.Texture2D Facebook.Unity.Example.ConsoleBase::<LastResponseTexture>k__BackingField
	Texture2D_t3840446185 * ___U3CLastResponseTextureU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_status_6() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___status_6)); }
	inline String_t* get_status_6() const { return ___status_6; }
	inline String_t** get_address_of_status_6() { return &___status_6; }
	inline void set_status_6(String_t* value)
	{
		___status_6 = value;
		Il2CppCodeGenWriteBarrier((&___status_6), value);
	}

	inline static int32_t get_offset_of_lastResponse_7() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___lastResponse_7)); }
	inline String_t* get_lastResponse_7() const { return ___lastResponse_7; }
	inline String_t** get_address_of_lastResponse_7() { return &___lastResponse_7; }
	inline void set_lastResponse_7(String_t* value)
	{
		___lastResponse_7 = value;
		Il2CppCodeGenWriteBarrier((&___lastResponse_7), value);
	}

	inline static int32_t get_offset_of_scrollPosition_8() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___scrollPosition_8)); }
	inline Vector2_t2156229523  get_scrollPosition_8() const { return ___scrollPosition_8; }
	inline Vector2_t2156229523 * get_address_of_scrollPosition_8() { return &___scrollPosition_8; }
	inline void set_scrollPosition_8(Vector2_t2156229523  value)
	{
		___scrollPosition_8 = value;
	}

	inline static int32_t get_offset_of_scaleFactor_9() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___scaleFactor_9)); }
	inline Nullable_1_t3119828856  get_scaleFactor_9() const { return ___scaleFactor_9; }
	inline Nullable_1_t3119828856 * get_address_of_scaleFactor_9() { return &___scaleFactor_9; }
	inline void set_scaleFactor_9(Nullable_1_t3119828856  value)
	{
		___scaleFactor_9 = value;
	}

	inline static int32_t get_offset_of_textStyle_10() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___textStyle_10)); }
	inline GUIStyle_t3956901511 * get_textStyle_10() const { return ___textStyle_10; }
	inline GUIStyle_t3956901511 ** get_address_of_textStyle_10() { return &___textStyle_10; }
	inline void set_textStyle_10(GUIStyle_t3956901511 * value)
	{
		___textStyle_10 = value;
		Il2CppCodeGenWriteBarrier((&___textStyle_10), value);
	}

	inline static int32_t get_offset_of_buttonStyle_11() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___buttonStyle_11)); }
	inline GUIStyle_t3956901511 * get_buttonStyle_11() const { return ___buttonStyle_11; }
	inline GUIStyle_t3956901511 ** get_address_of_buttonStyle_11() { return &___buttonStyle_11; }
	inline void set_buttonStyle_11(GUIStyle_t3956901511 * value)
	{
		___buttonStyle_11 = value;
		Il2CppCodeGenWriteBarrier((&___buttonStyle_11), value);
	}

	inline static int32_t get_offset_of_textInputStyle_12() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___textInputStyle_12)); }
	inline GUIStyle_t3956901511 * get_textInputStyle_12() const { return ___textInputStyle_12; }
	inline GUIStyle_t3956901511 ** get_address_of_textInputStyle_12() { return &___textInputStyle_12; }
	inline void set_textInputStyle_12(GUIStyle_t3956901511 * value)
	{
		___textInputStyle_12 = value;
		Il2CppCodeGenWriteBarrier((&___textInputStyle_12), value);
	}

	inline static int32_t get_offset_of_labelStyle_13() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___labelStyle_13)); }
	inline GUIStyle_t3956901511 * get_labelStyle_13() const { return ___labelStyle_13; }
	inline GUIStyle_t3956901511 ** get_address_of_labelStyle_13() { return &___labelStyle_13; }
	inline void set_labelStyle_13(GUIStyle_t3956901511 * value)
	{
		___labelStyle_13 = value;
		Il2CppCodeGenWriteBarrier((&___labelStyle_13), value);
	}

	inline static int32_t get_offset_of_U3CLastResponseTextureU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___U3CLastResponseTextureU3Ek__BackingField_14)); }
	inline Texture2D_t3840446185 * get_U3CLastResponseTextureU3Ek__BackingField_14() const { return ___U3CLastResponseTextureU3Ek__BackingField_14; }
	inline Texture2D_t3840446185 ** get_address_of_U3CLastResponseTextureU3Ek__BackingField_14() { return &___U3CLastResponseTextureU3Ek__BackingField_14; }
	inline void set_U3CLastResponseTextureU3Ek__BackingField_14(Texture2D_t3840446185 * value)
	{
		___U3CLastResponseTextureU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastResponseTextureU3Ek__BackingField_14), value);
	}
};

struct ConsoleBase_t1023975560_StaticFields
{
public:
	// System.Collections.Generic.Stack`1<System.String> Facebook.Unity.Example.ConsoleBase::menuStack
	Stack_1_t2690840144 * ___menuStack_5;

public:
	inline static int32_t get_offset_of_menuStack_5() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560_StaticFields, ___menuStack_5)); }
	inline Stack_1_t2690840144 * get_menuStack_5() const { return ___menuStack_5; }
	inline Stack_1_t2690840144 ** get_address_of_menuStack_5() { return &___menuStack_5; }
	inline void set_menuStack_5(Stack_1_t2690840144 * value)
	{
		___menuStack_5 = value;
		Il2CppCodeGenWriteBarrier((&___menuStack_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSOLEBASE_T1023975560_H
#ifndef COMPILEDFACEBOOKLOADER_T350999866_H
#define COMPILEDFACEBOOKLOADER_T350999866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FB/CompiledFacebookLoader
struct  CompiledFacebookLoader_t350999866  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPILEDFACEBOOKLOADER_T350999866_H
#ifndef FACEBOOKGAMEOBJECT_T3770693708_H
#define FACEBOOKGAMEOBJECT_T3770693708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookGameObject
struct  FacebookGameObject_t3770693708  : public MonoBehaviour_t3962482529
{
public:
	// Facebook.Unity.IFacebookImplementation Facebook.Unity.FacebookGameObject::<Facebook>k__BackingField
	RuntimeObject* ___U3CFacebookU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CFacebookU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FacebookGameObject_t3770693708, ___U3CFacebookU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CFacebookU3Ek__BackingField_4() const { return ___U3CFacebookU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CFacebookU3Ek__BackingField_4() { return &___U3CFacebookU3Ek__BackingField_4; }
	inline void set_U3CFacebookU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CFacebookU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFacebookU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKGAMEOBJECT_T3770693708_H
#ifndef CANVASFACEBOOKGAMEOBJECT_T1048256264_H
#define CANVASFACEBOOKGAMEOBJECT_T1048256264_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Canvas.CanvasFacebookGameObject
struct  CanvasFacebookGameObject_t1048256264  : public FacebookGameObject_t3770693708
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASFACEBOOKGAMEOBJECT_T1048256264_H
#ifndef CANVASFACEBOOKLOADER_T4289078786_H
#define CANVASFACEBOOKLOADER_T4289078786_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Canvas.CanvasFacebookLoader
struct  CanvasFacebookLoader_t4289078786  : public CompiledFacebookLoader_t350999866
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASFACEBOOKLOADER_T4289078786_H
#ifndef LOGVIEW_T1067263371_H
#define LOGVIEW_T1067263371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.LogView
struct  LogView_t1067263371  : public ConsoleBase_t1023975560
{
public:

public:
};

struct LogView_t1067263371_StaticFields
{
public:
	// System.String Facebook.Unity.Example.LogView::datePatt
	String_t* ___datePatt_15;
	// System.Collections.Generic.IList`1<System.String> Facebook.Unity.Example.LogView::events
	RuntimeObject* ___events_16;

public:
	inline static int32_t get_offset_of_datePatt_15() { return static_cast<int32_t>(offsetof(LogView_t1067263371_StaticFields, ___datePatt_15)); }
	inline String_t* get_datePatt_15() const { return ___datePatt_15; }
	inline String_t** get_address_of_datePatt_15() { return &___datePatt_15; }
	inline void set_datePatt_15(String_t* value)
	{
		___datePatt_15 = value;
		Il2CppCodeGenWriteBarrier((&___datePatt_15), value);
	}

	inline static int32_t get_offset_of_events_16() { return static_cast<int32_t>(offsetof(LogView_t1067263371_StaticFields, ___events_16)); }
	inline RuntimeObject* get_events_16() const { return ___events_16; }
	inline RuntimeObject** get_address_of_events_16() { return &___events_16; }
	inline void set_events_16(RuntimeObject* value)
	{
		___events_16 = value;
		Il2CppCodeGenWriteBarrier((&___events_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGVIEW_T1067263371_H
#ifndef MENUBASE_T1079888489_H
#define MENUBASE_T1079888489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.MenuBase
struct  MenuBase_t1079888489  : public ConsoleBase_t1023975560
{
public:

public:
};

struct MenuBase_t1079888489_StaticFields
{
public:
	// Facebook.Unity.ShareDialogMode Facebook.Unity.Example.MenuBase::shareDialogMode
	int32_t ___shareDialogMode_15;

public:
	inline static int32_t get_offset_of_shareDialogMode_15() { return static_cast<int32_t>(offsetof(MenuBase_t1079888489_StaticFields, ___shareDialogMode_15)); }
	inline int32_t get_shareDialogMode_15() const { return ___shareDialogMode_15; }
	inline int32_t* get_address_of_shareDialogMode_15() { return &___shareDialogMode_15; }
	inline void set_shareDialogMode_15(int32_t value)
	{
		___shareDialogMode_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUBASE_T1079888489_H
#ifndef ANDROIDFACEBOOKLOADER_T2958816769_H
#define ANDROIDFACEBOOKLOADER_T2958816769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.Android.AndroidFacebookLoader
struct  AndroidFacebookLoader_t2958816769  : public CompiledFacebookLoader_t350999866
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDFACEBOOKLOADER_T2958816769_H
#ifndef ACCESSTOKENMENU_T4028641200_H
#define ACCESSTOKENMENU_T4028641200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AccessTokenMenu
struct  AccessTokenMenu_t4028641200  : public MenuBase_t1079888489
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESSTOKENMENU_T4028641200_H
#ifndef APPEVENTS_T3645639549_H
#define APPEVENTS_T3645639549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AppEvents
struct  AppEvents_t3645639549  : public MenuBase_t1079888489
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPEVENTS_T3645639549_H
#ifndef APPLINKS_T2028121612_H
#define APPLINKS_T2028121612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AppLinks
struct  AppLinks_t2028121612  : public MenuBase_t1079888489
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLINKS_T2028121612_H
#ifndef APPREQUESTS_T2419817778_H
#define APPREQUESTS_T2419817778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AppRequests
struct  AppRequests_t2419817778  : public MenuBase_t1079888489
{
public:
	// System.String Facebook.Unity.Example.AppRequests::requestMessage
	String_t* ___requestMessage_16;
	// System.String Facebook.Unity.Example.AppRequests::requestTo
	String_t* ___requestTo_17;
	// System.String Facebook.Unity.Example.AppRequests::requestFilter
	String_t* ___requestFilter_18;
	// System.String Facebook.Unity.Example.AppRequests::requestExcludes
	String_t* ___requestExcludes_19;
	// System.String Facebook.Unity.Example.AppRequests::requestMax
	String_t* ___requestMax_20;
	// System.String Facebook.Unity.Example.AppRequests::requestData
	String_t* ___requestData_21;
	// System.String Facebook.Unity.Example.AppRequests::requestTitle
	String_t* ___requestTitle_22;
	// System.String Facebook.Unity.Example.AppRequests::requestObjectID
	String_t* ___requestObjectID_23;
	// System.Int32 Facebook.Unity.Example.AppRequests::selectedAction
	int32_t ___selectedAction_24;
	// System.String[] Facebook.Unity.Example.AppRequests::actionTypeStrings
	StringU5BU5D_t1281789340* ___actionTypeStrings_25;

public:
	inline static int32_t get_offset_of_requestMessage_16() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestMessage_16)); }
	inline String_t* get_requestMessage_16() const { return ___requestMessage_16; }
	inline String_t** get_address_of_requestMessage_16() { return &___requestMessage_16; }
	inline void set_requestMessage_16(String_t* value)
	{
		___requestMessage_16 = value;
		Il2CppCodeGenWriteBarrier((&___requestMessage_16), value);
	}

	inline static int32_t get_offset_of_requestTo_17() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestTo_17)); }
	inline String_t* get_requestTo_17() const { return ___requestTo_17; }
	inline String_t** get_address_of_requestTo_17() { return &___requestTo_17; }
	inline void set_requestTo_17(String_t* value)
	{
		___requestTo_17 = value;
		Il2CppCodeGenWriteBarrier((&___requestTo_17), value);
	}

	inline static int32_t get_offset_of_requestFilter_18() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestFilter_18)); }
	inline String_t* get_requestFilter_18() const { return ___requestFilter_18; }
	inline String_t** get_address_of_requestFilter_18() { return &___requestFilter_18; }
	inline void set_requestFilter_18(String_t* value)
	{
		___requestFilter_18 = value;
		Il2CppCodeGenWriteBarrier((&___requestFilter_18), value);
	}

	inline static int32_t get_offset_of_requestExcludes_19() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestExcludes_19)); }
	inline String_t* get_requestExcludes_19() const { return ___requestExcludes_19; }
	inline String_t** get_address_of_requestExcludes_19() { return &___requestExcludes_19; }
	inline void set_requestExcludes_19(String_t* value)
	{
		___requestExcludes_19 = value;
		Il2CppCodeGenWriteBarrier((&___requestExcludes_19), value);
	}

	inline static int32_t get_offset_of_requestMax_20() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestMax_20)); }
	inline String_t* get_requestMax_20() const { return ___requestMax_20; }
	inline String_t** get_address_of_requestMax_20() { return &___requestMax_20; }
	inline void set_requestMax_20(String_t* value)
	{
		___requestMax_20 = value;
		Il2CppCodeGenWriteBarrier((&___requestMax_20), value);
	}

	inline static int32_t get_offset_of_requestData_21() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestData_21)); }
	inline String_t* get_requestData_21() const { return ___requestData_21; }
	inline String_t** get_address_of_requestData_21() { return &___requestData_21; }
	inline void set_requestData_21(String_t* value)
	{
		___requestData_21 = value;
		Il2CppCodeGenWriteBarrier((&___requestData_21), value);
	}

	inline static int32_t get_offset_of_requestTitle_22() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestTitle_22)); }
	inline String_t* get_requestTitle_22() const { return ___requestTitle_22; }
	inline String_t** get_address_of_requestTitle_22() { return &___requestTitle_22; }
	inline void set_requestTitle_22(String_t* value)
	{
		___requestTitle_22 = value;
		Il2CppCodeGenWriteBarrier((&___requestTitle_22), value);
	}

	inline static int32_t get_offset_of_requestObjectID_23() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestObjectID_23)); }
	inline String_t* get_requestObjectID_23() const { return ___requestObjectID_23; }
	inline String_t** get_address_of_requestObjectID_23() { return &___requestObjectID_23; }
	inline void set_requestObjectID_23(String_t* value)
	{
		___requestObjectID_23 = value;
		Il2CppCodeGenWriteBarrier((&___requestObjectID_23), value);
	}

	inline static int32_t get_offset_of_selectedAction_24() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___selectedAction_24)); }
	inline int32_t get_selectedAction_24() const { return ___selectedAction_24; }
	inline int32_t* get_address_of_selectedAction_24() { return &___selectedAction_24; }
	inline void set_selectedAction_24(int32_t value)
	{
		___selectedAction_24 = value;
	}

	inline static int32_t get_offset_of_actionTypeStrings_25() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___actionTypeStrings_25)); }
	inline StringU5BU5D_t1281789340* get_actionTypeStrings_25() const { return ___actionTypeStrings_25; }
	inline StringU5BU5D_t1281789340** get_address_of_actionTypeStrings_25() { return &___actionTypeStrings_25; }
	inline void set_actionTypeStrings_25(StringU5BU5D_t1281789340* value)
	{
		___actionTypeStrings_25 = value;
		Il2CppCodeGenWriteBarrier((&___actionTypeStrings_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPREQUESTS_T2419817778_H
#ifndef DIALOGSHARE_T2334043085_H
#define DIALOGSHARE_T2334043085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.DialogShare
struct  DialogShare_t2334043085  : public MenuBase_t1079888489
{
public:
	// System.String Facebook.Unity.Example.DialogShare::shareLink
	String_t* ___shareLink_16;
	// System.String Facebook.Unity.Example.DialogShare::shareTitle
	String_t* ___shareTitle_17;
	// System.String Facebook.Unity.Example.DialogShare::shareDescription
	String_t* ___shareDescription_18;
	// System.String Facebook.Unity.Example.DialogShare::shareImage
	String_t* ___shareImage_19;
	// System.String Facebook.Unity.Example.DialogShare::feedTo
	String_t* ___feedTo_20;
	// System.String Facebook.Unity.Example.DialogShare::feedLink
	String_t* ___feedLink_21;
	// System.String Facebook.Unity.Example.DialogShare::feedTitle
	String_t* ___feedTitle_22;
	// System.String Facebook.Unity.Example.DialogShare::feedCaption
	String_t* ___feedCaption_23;
	// System.String Facebook.Unity.Example.DialogShare::feedDescription
	String_t* ___feedDescription_24;
	// System.String Facebook.Unity.Example.DialogShare::feedImage
	String_t* ___feedImage_25;
	// System.String Facebook.Unity.Example.DialogShare::feedMediaSource
	String_t* ___feedMediaSource_26;

public:
	inline static int32_t get_offset_of_shareLink_16() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___shareLink_16)); }
	inline String_t* get_shareLink_16() const { return ___shareLink_16; }
	inline String_t** get_address_of_shareLink_16() { return &___shareLink_16; }
	inline void set_shareLink_16(String_t* value)
	{
		___shareLink_16 = value;
		Il2CppCodeGenWriteBarrier((&___shareLink_16), value);
	}

	inline static int32_t get_offset_of_shareTitle_17() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___shareTitle_17)); }
	inline String_t* get_shareTitle_17() const { return ___shareTitle_17; }
	inline String_t** get_address_of_shareTitle_17() { return &___shareTitle_17; }
	inline void set_shareTitle_17(String_t* value)
	{
		___shareTitle_17 = value;
		Il2CppCodeGenWriteBarrier((&___shareTitle_17), value);
	}

	inline static int32_t get_offset_of_shareDescription_18() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___shareDescription_18)); }
	inline String_t* get_shareDescription_18() const { return ___shareDescription_18; }
	inline String_t** get_address_of_shareDescription_18() { return &___shareDescription_18; }
	inline void set_shareDescription_18(String_t* value)
	{
		___shareDescription_18 = value;
		Il2CppCodeGenWriteBarrier((&___shareDescription_18), value);
	}

	inline static int32_t get_offset_of_shareImage_19() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___shareImage_19)); }
	inline String_t* get_shareImage_19() const { return ___shareImage_19; }
	inline String_t** get_address_of_shareImage_19() { return &___shareImage_19; }
	inline void set_shareImage_19(String_t* value)
	{
		___shareImage_19 = value;
		Il2CppCodeGenWriteBarrier((&___shareImage_19), value);
	}

	inline static int32_t get_offset_of_feedTo_20() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___feedTo_20)); }
	inline String_t* get_feedTo_20() const { return ___feedTo_20; }
	inline String_t** get_address_of_feedTo_20() { return &___feedTo_20; }
	inline void set_feedTo_20(String_t* value)
	{
		___feedTo_20 = value;
		Il2CppCodeGenWriteBarrier((&___feedTo_20), value);
	}

	inline static int32_t get_offset_of_feedLink_21() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___feedLink_21)); }
	inline String_t* get_feedLink_21() const { return ___feedLink_21; }
	inline String_t** get_address_of_feedLink_21() { return &___feedLink_21; }
	inline void set_feedLink_21(String_t* value)
	{
		___feedLink_21 = value;
		Il2CppCodeGenWriteBarrier((&___feedLink_21), value);
	}

	inline static int32_t get_offset_of_feedTitle_22() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___feedTitle_22)); }
	inline String_t* get_feedTitle_22() const { return ___feedTitle_22; }
	inline String_t** get_address_of_feedTitle_22() { return &___feedTitle_22; }
	inline void set_feedTitle_22(String_t* value)
	{
		___feedTitle_22 = value;
		Il2CppCodeGenWriteBarrier((&___feedTitle_22), value);
	}

	inline static int32_t get_offset_of_feedCaption_23() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___feedCaption_23)); }
	inline String_t* get_feedCaption_23() const { return ___feedCaption_23; }
	inline String_t** get_address_of_feedCaption_23() { return &___feedCaption_23; }
	inline void set_feedCaption_23(String_t* value)
	{
		___feedCaption_23 = value;
		Il2CppCodeGenWriteBarrier((&___feedCaption_23), value);
	}

	inline static int32_t get_offset_of_feedDescription_24() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___feedDescription_24)); }
	inline String_t* get_feedDescription_24() const { return ___feedDescription_24; }
	inline String_t** get_address_of_feedDescription_24() { return &___feedDescription_24; }
	inline void set_feedDescription_24(String_t* value)
	{
		___feedDescription_24 = value;
		Il2CppCodeGenWriteBarrier((&___feedDescription_24), value);
	}

	inline static int32_t get_offset_of_feedImage_25() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___feedImage_25)); }
	inline String_t* get_feedImage_25() const { return ___feedImage_25; }
	inline String_t** get_address_of_feedImage_25() { return &___feedImage_25; }
	inline void set_feedImage_25(String_t* value)
	{
		___feedImage_25 = value;
		Il2CppCodeGenWriteBarrier((&___feedImage_25), value);
	}

	inline static int32_t get_offset_of_feedMediaSource_26() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___feedMediaSource_26)); }
	inline String_t* get_feedMediaSource_26() const { return ___feedMediaSource_26; }
	inline String_t** get_address_of_feedMediaSource_26() { return &___feedMediaSource_26; }
	inline void set_feedMediaSource_26(String_t* value)
	{
		___feedMediaSource_26 = value;
		Il2CppCodeGenWriteBarrier((&___feedMediaSource_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGSHARE_T2334043085_H
#ifndef GRAPHREQUEST_T4047451309_H
#define GRAPHREQUEST_T4047451309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.GraphRequest
struct  GraphRequest_t4047451309  : public MenuBase_t1079888489
{
public:
	// System.String Facebook.Unity.Example.GraphRequest::apiQuery
	String_t* ___apiQuery_16;
	// UnityEngine.Texture2D Facebook.Unity.Example.GraphRequest::profilePic
	Texture2D_t3840446185 * ___profilePic_17;

public:
	inline static int32_t get_offset_of_apiQuery_16() { return static_cast<int32_t>(offsetof(GraphRequest_t4047451309, ___apiQuery_16)); }
	inline String_t* get_apiQuery_16() const { return ___apiQuery_16; }
	inline String_t** get_address_of_apiQuery_16() { return &___apiQuery_16; }
	inline void set_apiQuery_16(String_t* value)
	{
		___apiQuery_16 = value;
		Il2CppCodeGenWriteBarrier((&___apiQuery_16), value);
	}

	inline static int32_t get_offset_of_profilePic_17() { return static_cast<int32_t>(offsetof(GraphRequest_t4047451309, ___profilePic_17)); }
	inline Texture2D_t3840446185 * get_profilePic_17() const { return ___profilePic_17; }
	inline Texture2D_t3840446185 ** get_address_of_profilePic_17() { return &___profilePic_17; }
	inline void set_profilePic_17(Texture2D_t3840446185 * value)
	{
		___profilePic_17 = value;
		Il2CppCodeGenWriteBarrier((&___profilePic_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHREQUEST_T4047451309_H
#ifndef MAINMENU_T1823058806_H
#define MAINMENU_T1823058806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.MainMenu
struct  MainMenu_t1823058806  : public MenuBase_t1079888489
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINMENU_T1823058806_H
#ifndef PAY_T1264260185_H
#define PAY_T1264260185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.Pay
struct  Pay_t1264260185  : public MenuBase_t1079888489
{
public:
	// System.String Facebook.Unity.Example.Pay::payProduct
	String_t* ___payProduct_16;

public:
	inline static int32_t get_offset_of_payProduct_16() { return static_cast<int32_t>(offsetof(Pay_t1264260185, ___payProduct_16)); }
	inline String_t* get_payProduct_16() const { return ___payProduct_16; }
	inline String_t** get_address_of_payProduct_16() { return &___payProduct_16; }
	inline void set_payProduct_16(String_t* value)
	{
		___payProduct_16 = value;
		Il2CppCodeGenWriteBarrier((&___payProduct_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAY_T1264260185_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3900 = { sizeof (AndroidFacebookLoader_t2958816769), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3901 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3902 = { sizeof (CanvasFacebook_t419876522), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3902[5] = 
{
	CanvasFacebook_t419876522::get_offset_of_appId_3(),
	CanvasFacebook_t419876522::get_offset_of_appLinkUrl_4(),
	CanvasFacebook_t419876522::get_offset_of_canvasJSWrapper_5(),
	CanvasFacebook_t419876522::get_offset_of_onHideUnityDelegate_6(),
	CanvasFacebook_t419876522::get_offset_of_U3CLimitEventUsageU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3903 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3903[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3904 = { sizeof (U3CU3Ec_t4183747061), -1, sizeof(U3CU3Ec_t4183747061_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3904[2] = 
{
	U3CU3Ec_t4183747061_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4183747061_StaticFields::get_offset_of_U3CU3E9__40_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3905 = { sizeof (U3CU3Ec__DisplayClass47_0_t3013172403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3905[2] = 
{
	U3CU3Ec__DisplayClass47_0_t3013172403::get_offset_of_result_0(),
	U3CU3Ec__DisplayClass47_0_t3013172403::get_offset_of_callback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3906 = { sizeof (CanvasFacebookGameObject_t1048256264), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3907 = { sizeof (CanvasFacebookLoader_t4289078786), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3908 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3909 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3910 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3911 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3912 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3913 = { sizeof (JsBridge_t2057182915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3913[1] = 
{
	JsBridge_t2057182915::get_offset_of_facebook_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3914 = { sizeof (U3CModuleU3E_t692745549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3915 = { sizeof (AndroidWrapper_t4285589061), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3916 = { sizeof (U3CModuleU3E_t692745550), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3917 = { sizeof (IOSWrapper_t3090931871), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3918 = { sizeof (U3CModuleU3E_t692745551), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3919 = { sizeof (ConsoleBase_t1023975560), -1, sizeof(ConsoleBase_t1023975560_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3919[11] = 
{
	0,
	ConsoleBase_t1023975560_StaticFields::get_offset_of_menuStack_5(),
	ConsoleBase_t1023975560::get_offset_of_status_6(),
	ConsoleBase_t1023975560::get_offset_of_lastResponse_7(),
	ConsoleBase_t1023975560::get_offset_of_scrollPosition_8(),
	ConsoleBase_t1023975560::get_offset_of_scaleFactor_9(),
	ConsoleBase_t1023975560::get_offset_of_textStyle_10(),
	ConsoleBase_t1023975560::get_offset_of_buttonStyle_11(),
	ConsoleBase_t1023975560::get_offset_of_textInputStyle_12(),
	ConsoleBase_t1023975560::get_offset_of_labelStyle_13(),
	ConsoleBase_t1023975560::get_offset_of_U3CLastResponseTextureU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3920 = { sizeof (LogView_t1067263371), -1, sizeof(LogView_t1067263371_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3920[2] = 
{
	LogView_t1067263371_StaticFields::get_offset_of_datePatt_15(),
	LogView_t1067263371_StaticFields::get_offset_of_events_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3921 = { sizeof (MenuBase_t1079888489), -1, sizeof(MenuBase_t1079888489_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3921[1] = 
{
	MenuBase_t1079888489_StaticFields::get_offset_of_shareDialogMode_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3922 = { sizeof (AccessTokenMenu_t4028641200), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3923 = { sizeof (AppEvents_t3645639549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3924 = { sizeof (AppLinks_t2028121612), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3925 = { sizeof (AppRequests_t2419817778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3925[10] = 
{
	AppRequests_t2419817778::get_offset_of_requestMessage_16(),
	AppRequests_t2419817778::get_offset_of_requestTo_17(),
	AppRequests_t2419817778::get_offset_of_requestFilter_18(),
	AppRequests_t2419817778::get_offset_of_requestExcludes_19(),
	AppRequests_t2419817778::get_offset_of_requestMax_20(),
	AppRequests_t2419817778::get_offset_of_requestData_21(),
	AppRequests_t2419817778::get_offset_of_requestTitle_22(),
	AppRequests_t2419817778::get_offset_of_requestObjectID_23(),
	AppRequests_t2419817778::get_offset_of_selectedAction_24(),
	AppRequests_t2419817778::get_offset_of_actionTypeStrings_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3926 = { sizeof (DialogShare_t2334043085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3926[11] = 
{
	DialogShare_t2334043085::get_offset_of_shareLink_16(),
	DialogShare_t2334043085::get_offset_of_shareTitle_17(),
	DialogShare_t2334043085::get_offset_of_shareDescription_18(),
	DialogShare_t2334043085::get_offset_of_shareImage_19(),
	DialogShare_t2334043085::get_offset_of_feedTo_20(),
	DialogShare_t2334043085::get_offset_of_feedLink_21(),
	DialogShare_t2334043085::get_offset_of_feedTitle_22(),
	DialogShare_t2334043085::get_offset_of_feedCaption_23(),
	DialogShare_t2334043085::get_offset_of_feedDescription_24(),
	DialogShare_t2334043085::get_offset_of_feedImage_25(),
	DialogShare_t2334043085::get_offset_of_feedMediaSource_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3927 = { sizeof (GraphRequest_t4047451309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3927[2] = 
{
	GraphRequest_t4047451309::get_offset_of_apiQuery_16(),
	GraphRequest_t4047451309::get_offset_of_profilePic_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3928 = { sizeof (U3CTakeScreenshotU3Ec__Iterator0_t3544038915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3928[9] = 
{
	U3CTakeScreenshotU3Ec__Iterator0_t3544038915::get_offset_of_U3CwidthU3E__0_0(),
	U3CTakeScreenshotU3Ec__Iterator0_t3544038915::get_offset_of_U3CheightU3E__0_1(),
	U3CTakeScreenshotU3Ec__Iterator0_t3544038915::get_offset_of_U3CtexU3E__0_2(),
	U3CTakeScreenshotU3Ec__Iterator0_t3544038915::get_offset_of_U3CscreenshotU3E__0_3(),
	U3CTakeScreenshotU3Ec__Iterator0_t3544038915::get_offset_of_U3CwwwFormU3E__0_4(),
	U3CTakeScreenshotU3Ec__Iterator0_t3544038915::get_offset_of_U24this_5(),
	U3CTakeScreenshotU3Ec__Iterator0_t3544038915::get_offset_of_U24current_6(),
	U3CTakeScreenshotU3Ec__Iterator0_t3544038915::get_offset_of_U24disposing_7(),
	U3CTakeScreenshotU3Ec__Iterator0_t3544038915::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3929 = { sizeof (MainMenu_t1823058806), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3930 = { sizeof (Pay_t1264260185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3930[1] = 
{
	Pay_t1264260185::get_offset_of_payProduct_16(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
