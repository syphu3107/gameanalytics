﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct GenericVirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_virtual_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct GenericInterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (const RuntimeMethod* method, RuntimeObject* obj)
	{
		VirtualInvokeData invokeData;
		il2cpp_codegen_get_generic_interface_invoke_data(method, obj, &invokeData);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// Facebook.Unity.Settings.FacebookSettings
struct FacebookSettings_t265492256;
// Facebook.Unity.Settings.FacebookSettings/<>c
struct U3CU3Ec_t2896466078;
// Facebook.Unity.Settings.FacebookSettings/OnChangeCallback
struct OnChangeCallback_t2605352606;
// Facebook.Unity.Settings.FacebookSettings/OnChangeCallback[]
struct OnChangeCallbackU5BU5D_t3225232683;
// Facebook.Unity.Settings.FacebookSettings/UrlSchemes
struct UrlSchemes_t603992841;
// Facebook.Unity.Settings.FacebookSettings/UrlSchemes[]
struct UrlSchemesU5BU5D_t3208731316;
// System.Action`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>
struct Action_1_t2777820201;
// System.Action`1<System.Object>
struct Action_1_t3252573759;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>
struct List_1_t4077427348;
// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/UrlSchemes>
struct List_1_t2076067583;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Delegate
struct Delegate_t1188392813;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Void
struct Void_t1185182177;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522;

extern RuntimeClass* Action_1_t2777820201_il2cpp_TypeInfo_var;
extern RuntimeClass* FacebookSettings_t265492256_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t2076067583_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t3319525431_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t4077427348_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CU3Ec_t2896466078_il2cpp_TypeInfo_var;
extern RuntimeClass* UrlSchemes_t603992841_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral1323011570;
extern String_t* _stringLiteral3452614544;
extern String_t* _stringLiteral565915282;
extern String_t* _stringLiteral786313173;
extern const RuntimeMethod* Action_1__ctor_m1305872465_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1685793073_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m3948897584_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m641913066_RuntimeMethod_var;
extern const RuntimeMethod* List_1_ForEach_m4089584929_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Remove_m1223406277_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m1023008772_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m3128781828_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m706204246_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_m883280456_RuntimeMethod_var;
extern const RuntimeMethod* ScriptableObject_CreateInstance_TisFacebookSettings_t265492256_m3878763949_RuntimeMethod_var;
extern const RuntimeMethod* U3CU3Ec_U3CSettingsChangedU3Eb__76_0_m52101905_RuntimeMethod_var;
extern const uint32_t FacebookSettings_RegisterChangeEventCallback_m3472570634_MetadataUsageId;
extern const uint32_t FacebookSettings_SettingsChanged_m1760271451_MetadataUsageId;
extern const uint32_t FacebookSettings_UnregisterChangeEventCallback_m1404630495_MetadataUsageId;
extern const uint32_t FacebookSettings__cctor_m2460564019_MetadataUsageId;
extern const uint32_t FacebookSettings__ctor_m1186197728_MetadataUsageId;
extern const uint32_t FacebookSettings_get_AdvertiserIDCollectionEnabled_m1524812109_MetadataUsageId;
extern const uint32_t FacebookSettings_get_AppId_m2041263324_MetadataUsageId;
extern const uint32_t FacebookSettings_get_AppIds_m2021279578_MetadataUsageId;
extern const uint32_t FacebookSettings_get_AppLabels_m375090046_MetadataUsageId;
extern const uint32_t FacebookSettings_get_AppLinkSchemes_m3466100027_MetadataUsageId;
extern const uint32_t FacebookSettings_get_AutoLogAppEventsEnabled_m3999834749_MetadataUsageId;
extern const uint32_t FacebookSettings_get_ChannelUrl_m1181329812_MetadataUsageId;
extern const uint32_t FacebookSettings_get_ClientToken_m1680362625_MetadataUsageId;
extern const uint32_t FacebookSettings_get_ClientTokens_m2548316486_MetadataUsageId;
extern const uint32_t FacebookSettings_get_Cookie_m3111427717_MetadataUsageId;
extern const uint32_t FacebookSettings_get_FrictionlessRequests_m2847432906_MetadataUsageId;
extern const uint32_t FacebookSettings_get_Instance_m3716855809_MetadataUsageId;
extern const uint32_t FacebookSettings_get_IosURLSuffix_m3664804883_MetadataUsageId;
extern const uint32_t FacebookSettings_get_IsValidAppId_m2110720990_MetadataUsageId;
extern const uint32_t FacebookSettings_get_Logging_m2591483607_MetadataUsageId;
extern const uint32_t FacebookSettings_get_NullableInstance_m3084529484_MetadataUsageId;
extern const uint32_t FacebookSettings_get_SelectedAppIndex_m3864493479_MetadataUsageId;
extern const uint32_t FacebookSettings_get_Status_m130576739_MetadataUsageId;
extern const uint32_t FacebookSettings_get_UploadAccessToken_m3072067980_MetadataUsageId;
extern const uint32_t FacebookSettings_get_Xfbml_m3743274674_MetadataUsageId;
extern const uint32_t FacebookSettings_set_AdvertiserIDCollectionEnabled_m1513918774_MetadataUsageId;
extern const uint32_t FacebookSettings_set_AppIds_m989131_MetadataUsageId;
extern const uint32_t FacebookSettings_set_AppLabels_m3173051710_MetadataUsageId;
extern const uint32_t FacebookSettings_set_AppLinkSchemes_m826429750_MetadataUsageId;
extern const uint32_t FacebookSettings_set_AutoLogAppEventsEnabled_m1125027621_MetadataUsageId;
extern const uint32_t FacebookSettings_set_ClientTokens_m2937685148_MetadataUsageId;
extern const uint32_t FacebookSettings_set_Cookie_m627350524_MetadataUsageId;
extern const uint32_t FacebookSettings_set_FrictionlessRequests_m698970453_MetadataUsageId;
extern const uint32_t FacebookSettings_set_IosURLSuffix_m2297605321_MetadataUsageId;
extern const uint32_t FacebookSettings_set_Logging_m2108563834_MetadataUsageId;
extern const uint32_t FacebookSettings_set_SelectedAppIndex_m2320561838_MetadataUsageId;
extern const uint32_t FacebookSettings_set_Status_m3621432817_MetadataUsageId;
extern const uint32_t FacebookSettings_set_UploadAccessToken_m3764926645_MetadataUsageId;
extern const uint32_t FacebookSettings_set_Xfbml_m1250939187_MetadataUsageId;
extern const uint32_t U3CU3Ec__cctor_m3195961849_MetadataUsageId;
extern const uint32_t UrlSchemes__ctor_m4194951824_MetadataUsageId;

struct DelegateU5BU5D_t1703627840;


#ifndef U3CMODULEU3E_T692745546_H
#define U3CMODULEU3E_T692745546_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745546 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745546_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CU3EC_T2896466078_H
#define U3CU3EC_T2896466078_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Settings.FacebookSettings/<>c
struct  U3CU3Ec_t2896466078  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2896466078_StaticFields
{
public:
	// Facebook.Unity.Settings.FacebookSettings/<>c Facebook.Unity.Settings.FacebookSettings/<>c::<>9
	U3CU3Ec_t2896466078 * ___U3CU3E9_0;
	// System.Action`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback> Facebook.Unity.Settings.FacebookSettings/<>c::<>9__76_0
	Action_1_t2777820201 * ___U3CU3E9__76_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2896466078_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2896466078 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2896466078 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2896466078 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__76_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2896466078_StaticFields, ___U3CU3E9__76_0_1)); }
	inline Action_1_t2777820201 * get_U3CU3E9__76_0_1() const { return ___U3CU3E9__76_0_1; }
	inline Action_1_t2777820201 ** get_address_of_U3CU3E9__76_0_1() { return &___U3CU3E9__76_0_1; }
	inline void set_U3CU3E9__76_0_1(Action_1_t2777820201 * value)
	{
		___U3CU3E9__76_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__76_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T2896466078_H
#ifndef URLSCHEMES_T603992841_H
#define URLSCHEMES_T603992841_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Settings.FacebookSettings/UrlSchemes
struct  UrlSchemes_t603992841  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings/UrlSchemes::list
	List_1_t3319525431 * ___list_0;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(UrlSchemes_t603992841, ___list_0)); }
	inline List_1_t3319525431 * get_list_0() const { return ___list_0; }
	inline List_1_t3319525431 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3319525431 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URLSCHEMES_T603992841_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LIST_1_T4077427348_H
#define LIST_1_T4077427348_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>
struct  List_1_t4077427348  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	OnChangeCallbackU5BU5D_t3225232683* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t4077427348, ____items_1)); }
	inline OnChangeCallbackU5BU5D_t3225232683* get__items_1() const { return ____items_1; }
	inline OnChangeCallbackU5BU5D_t3225232683** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(OnChangeCallbackU5BU5D_t3225232683* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t4077427348, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t4077427348, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t4077427348, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t4077427348_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	OnChangeCallbackU5BU5D_t3225232683* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t4077427348_StaticFields, ____emptyArray_5)); }
	inline OnChangeCallbackU5BU5D_t3225232683* get__emptyArray_5() const { return ____emptyArray_5; }
	inline OnChangeCallbackU5BU5D_t3225232683** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(OnChangeCallbackU5BU5D_t3225232683* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T4077427348_H
#ifndef LIST_1_T2076067583_H
#define LIST_1_T2076067583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/UrlSchemes>
struct  List_1_t2076067583  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	UrlSchemesU5BU5D_t3208731316* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t2076067583, ____items_1)); }
	inline UrlSchemesU5BU5D_t3208731316* get__items_1() const { return ____items_1; }
	inline UrlSchemesU5BU5D_t3208731316** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(UrlSchemesU5BU5D_t3208731316* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t2076067583, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t2076067583, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t2076067583, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t2076067583_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	UrlSchemesU5BU5D_t3208731316* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t2076067583_StaticFields, ____emptyArray_5)); }
	inline UrlSchemesU5BU5D_t3208731316* get__emptyArray_5() const { return ____emptyArray_5; }
	inline UrlSchemesU5BU5D_t3208731316** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(UrlSchemesU5BU5D_t3208731316* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T2076067583_H
#ifndef LIST_1_T3319525431_H
#define LIST_1_T3319525431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.String>
struct  List_1_t3319525431  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t1281789340* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____items_1)); }
	inline StringU5BU5D_t1281789340* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_t1281789340** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_t1281789340* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t3319525431_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	StringU5BU5D_t1281789340* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3319525431_StaticFields, ____emptyArray_5)); }
	inline StringU5BU5D_t1281789340* get__emptyArray_5() const { return ____emptyArray_5; }
	inline StringU5BU5D_t1281789340** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(StringU5BU5D_t1281789340* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3319525431_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef FACEBOOKSETTINGS_T265492256_H
#define FACEBOOKSETTINGS_T265492256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Settings.FacebookSettings
struct  FacebookSettings_t265492256  : public ScriptableObject_t2528358522
{
public:
	// System.Int32 Facebook.Unity.Settings.FacebookSettings::selectedAppIndex
	int32_t ___selectedAppIndex_9;
	// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::clientTokens
	List_1_t3319525431 * ___clientTokens_10;
	// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::appIds
	List_1_t3319525431 * ___appIds_11;
	// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::appLabels
	List_1_t3319525431 * ___appLabels_12;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::cookie
	bool ___cookie_13;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::logging
	bool ___logging_14;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::status
	bool ___status_15;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::xfbml
	bool ___xfbml_16;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::frictionlessRequests
	bool ___frictionlessRequests_17;
	// System.String Facebook.Unity.Settings.FacebookSettings::iosURLSuffix
	String_t* ___iosURLSuffix_18;
	// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/UrlSchemes> Facebook.Unity.Settings.FacebookSettings::appLinkSchemes
	List_1_t2076067583 * ___appLinkSchemes_19;
	// System.String Facebook.Unity.Settings.FacebookSettings::uploadAccessToken
	String_t* ___uploadAccessToken_20;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::autoLogAppEventsEnabled
	bool ___autoLogAppEventsEnabled_21;
	// System.Boolean Facebook.Unity.Settings.FacebookSettings::advertiserIDCollectionEnabled
	bool ___advertiserIDCollectionEnabled_22;

public:
	inline static int32_t get_offset_of_selectedAppIndex_9() { return static_cast<int32_t>(offsetof(FacebookSettings_t265492256, ___selectedAppIndex_9)); }
	inline int32_t get_selectedAppIndex_9() const { return ___selectedAppIndex_9; }
	inline int32_t* get_address_of_selectedAppIndex_9() { return &___selectedAppIndex_9; }
	inline void set_selectedAppIndex_9(int32_t value)
	{
		___selectedAppIndex_9 = value;
	}

	inline static int32_t get_offset_of_clientTokens_10() { return static_cast<int32_t>(offsetof(FacebookSettings_t265492256, ___clientTokens_10)); }
	inline List_1_t3319525431 * get_clientTokens_10() const { return ___clientTokens_10; }
	inline List_1_t3319525431 ** get_address_of_clientTokens_10() { return &___clientTokens_10; }
	inline void set_clientTokens_10(List_1_t3319525431 * value)
	{
		___clientTokens_10 = value;
		Il2CppCodeGenWriteBarrier((&___clientTokens_10), value);
	}

	inline static int32_t get_offset_of_appIds_11() { return static_cast<int32_t>(offsetof(FacebookSettings_t265492256, ___appIds_11)); }
	inline List_1_t3319525431 * get_appIds_11() const { return ___appIds_11; }
	inline List_1_t3319525431 ** get_address_of_appIds_11() { return &___appIds_11; }
	inline void set_appIds_11(List_1_t3319525431 * value)
	{
		___appIds_11 = value;
		Il2CppCodeGenWriteBarrier((&___appIds_11), value);
	}

	inline static int32_t get_offset_of_appLabels_12() { return static_cast<int32_t>(offsetof(FacebookSettings_t265492256, ___appLabels_12)); }
	inline List_1_t3319525431 * get_appLabels_12() const { return ___appLabels_12; }
	inline List_1_t3319525431 ** get_address_of_appLabels_12() { return &___appLabels_12; }
	inline void set_appLabels_12(List_1_t3319525431 * value)
	{
		___appLabels_12 = value;
		Il2CppCodeGenWriteBarrier((&___appLabels_12), value);
	}

	inline static int32_t get_offset_of_cookie_13() { return static_cast<int32_t>(offsetof(FacebookSettings_t265492256, ___cookie_13)); }
	inline bool get_cookie_13() const { return ___cookie_13; }
	inline bool* get_address_of_cookie_13() { return &___cookie_13; }
	inline void set_cookie_13(bool value)
	{
		___cookie_13 = value;
	}

	inline static int32_t get_offset_of_logging_14() { return static_cast<int32_t>(offsetof(FacebookSettings_t265492256, ___logging_14)); }
	inline bool get_logging_14() const { return ___logging_14; }
	inline bool* get_address_of_logging_14() { return &___logging_14; }
	inline void set_logging_14(bool value)
	{
		___logging_14 = value;
	}

	inline static int32_t get_offset_of_status_15() { return static_cast<int32_t>(offsetof(FacebookSettings_t265492256, ___status_15)); }
	inline bool get_status_15() const { return ___status_15; }
	inline bool* get_address_of_status_15() { return &___status_15; }
	inline void set_status_15(bool value)
	{
		___status_15 = value;
	}

	inline static int32_t get_offset_of_xfbml_16() { return static_cast<int32_t>(offsetof(FacebookSettings_t265492256, ___xfbml_16)); }
	inline bool get_xfbml_16() const { return ___xfbml_16; }
	inline bool* get_address_of_xfbml_16() { return &___xfbml_16; }
	inline void set_xfbml_16(bool value)
	{
		___xfbml_16 = value;
	}

	inline static int32_t get_offset_of_frictionlessRequests_17() { return static_cast<int32_t>(offsetof(FacebookSettings_t265492256, ___frictionlessRequests_17)); }
	inline bool get_frictionlessRequests_17() const { return ___frictionlessRequests_17; }
	inline bool* get_address_of_frictionlessRequests_17() { return &___frictionlessRequests_17; }
	inline void set_frictionlessRequests_17(bool value)
	{
		___frictionlessRequests_17 = value;
	}

	inline static int32_t get_offset_of_iosURLSuffix_18() { return static_cast<int32_t>(offsetof(FacebookSettings_t265492256, ___iosURLSuffix_18)); }
	inline String_t* get_iosURLSuffix_18() const { return ___iosURLSuffix_18; }
	inline String_t** get_address_of_iosURLSuffix_18() { return &___iosURLSuffix_18; }
	inline void set_iosURLSuffix_18(String_t* value)
	{
		___iosURLSuffix_18 = value;
		Il2CppCodeGenWriteBarrier((&___iosURLSuffix_18), value);
	}

	inline static int32_t get_offset_of_appLinkSchemes_19() { return static_cast<int32_t>(offsetof(FacebookSettings_t265492256, ___appLinkSchemes_19)); }
	inline List_1_t2076067583 * get_appLinkSchemes_19() const { return ___appLinkSchemes_19; }
	inline List_1_t2076067583 ** get_address_of_appLinkSchemes_19() { return &___appLinkSchemes_19; }
	inline void set_appLinkSchemes_19(List_1_t2076067583 * value)
	{
		___appLinkSchemes_19 = value;
		Il2CppCodeGenWriteBarrier((&___appLinkSchemes_19), value);
	}

	inline static int32_t get_offset_of_uploadAccessToken_20() { return static_cast<int32_t>(offsetof(FacebookSettings_t265492256, ___uploadAccessToken_20)); }
	inline String_t* get_uploadAccessToken_20() const { return ___uploadAccessToken_20; }
	inline String_t** get_address_of_uploadAccessToken_20() { return &___uploadAccessToken_20; }
	inline void set_uploadAccessToken_20(String_t* value)
	{
		___uploadAccessToken_20 = value;
		Il2CppCodeGenWriteBarrier((&___uploadAccessToken_20), value);
	}

	inline static int32_t get_offset_of_autoLogAppEventsEnabled_21() { return static_cast<int32_t>(offsetof(FacebookSettings_t265492256, ___autoLogAppEventsEnabled_21)); }
	inline bool get_autoLogAppEventsEnabled_21() const { return ___autoLogAppEventsEnabled_21; }
	inline bool* get_address_of_autoLogAppEventsEnabled_21() { return &___autoLogAppEventsEnabled_21; }
	inline void set_autoLogAppEventsEnabled_21(bool value)
	{
		___autoLogAppEventsEnabled_21 = value;
	}

	inline static int32_t get_offset_of_advertiserIDCollectionEnabled_22() { return static_cast<int32_t>(offsetof(FacebookSettings_t265492256, ___advertiserIDCollectionEnabled_22)); }
	inline bool get_advertiserIDCollectionEnabled_22() const { return ___advertiserIDCollectionEnabled_22; }
	inline bool* get_address_of_advertiserIDCollectionEnabled_22() { return &___advertiserIDCollectionEnabled_22; }
	inline void set_advertiserIDCollectionEnabled_22(bool value)
	{
		___advertiserIDCollectionEnabled_22 = value;
	}
};

struct FacebookSettings_t265492256_StaticFields
{
public:
	// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback> Facebook.Unity.Settings.FacebookSettings::onChangeCallbacks
	List_1_t4077427348 * ___onChangeCallbacks_7;
	// Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::instance
	FacebookSettings_t265492256 * ___instance_8;

public:
	inline static int32_t get_offset_of_onChangeCallbacks_7() { return static_cast<int32_t>(offsetof(FacebookSettings_t265492256_StaticFields, ___onChangeCallbacks_7)); }
	inline List_1_t4077427348 * get_onChangeCallbacks_7() const { return ___onChangeCallbacks_7; }
	inline List_1_t4077427348 ** get_address_of_onChangeCallbacks_7() { return &___onChangeCallbacks_7; }
	inline void set_onChangeCallbacks_7(List_1_t4077427348 * value)
	{
		___onChangeCallbacks_7 = value;
		Il2CppCodeGenWriteBarrier((&___onChangeCallbacks_7), value);
	}

	inline static int32_t get_offset_of_instance_8() { return static_cast<int32_t>(offsetof(FacebookSettings_t265492256_StaticFields, ___instance_8)); }
	inline FacebookSettings_t265492256 * get_instance_8() const { return ___instance_8; }
	inline FacebookSettings_t265492256 ** get_address_of_instance_8() { return &___instance_8; }
	inline void set_instance_8(FacebookSettings_t265492256 * value)
	{
		___instance_8 = value;
		Il2CppCodeGenWriteBarrier((&___instance_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKSETTINGS_T265492256_H
#ifndef ONCHANGECALLBACK_T2605352606_H
#define ONCHANGECALLBACK_T2605352606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Settings.FacebookSettings/OnChangeCallback
struct  OnChangeCallback_t2605352606  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCHANGECALLBACK_T2605352606_H
#ifndef ACTION_1_T2777820201_H
#define ACTION_1_T2777820201_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>
struct  Action_1_t2777820201  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T2777820201_H
#ifndef ASYNCCALLBACK_T3962456242_H
#define ASYNCCALLBACK_T3962456242_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.AsyncCallback
struct  AsyncCallback_t3962456242  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCCALLBACK_T3962456242_H
// System.Delegate[]
struct DelegateU5BU5D_t1703627840  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Delegate_t1188392813 * m_Items[1];

public:
	inline Delegate_t1188392813 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Delegate_t1188392813 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Delegate_t1188392813 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline Delegate_t1188392813 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Delegate_t1188392813 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Delegate_t1188392813 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};


// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_m1328026504_gshared (List_1_t257213610 * __this, int32_t p0, const RuntimeMethod* method);
// !!0 UnityEngine.ScriptableObject::CreateInstance<System.Object>()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * ScriptableObject_CreateInstance_TisRuntimeObject_m1552711675_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(!0)
extern "C" IL2CPP_METHOD_ATTR bool List_1_Remove_m2390619627_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Action_1__ctor_m118522912_gshared (Action_1_t3252573759 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::ForEach(System.Action`1<!0>)
extern "C" IL2CPP_METHOD_ATTR void List_1_ForEach_m3737504377_gshared (List_1_t257213610 * __this, Action_1_t3252573759 * p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);

// Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::get_Instance()
extern "C" IL2CPP_METHOD_ATTR FacebookSettings_t265492256 * FacebookSettings_get_Instance_m3716855809 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void Facebook.Unity.Settings.FacebookSettings::SettingsChanged()
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_SettingsChanged_m1760271451 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_AppIds()
extern "C" IL2CPP_METHOD_ATTR List_1_t3319525431 * FacebookSettings_get_AppIds_m2021279578 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int32 Facebook.Unity.Settings.FacebookSettings::get_SelectedAppIndex()
extern "C" IL2CPP_METHOD_ATTR int32_t FacebookSettings_get_SelectedAppIndex_m3864493479 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.String>::get_Item(System.Int32)
inline String_t* List_1_get_Item_m883280456 (List_1_t3319525431 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  String_t* (*) (List_1_t3319525431 *, int32_t, const RuntimeMethod*))List_1_get_Item_m1328026504_gshared)(__this, p0, method);
}
// System.String System.String::Trim()
extern "C" IL2CPP_METHOD_ATTR String_t* String_Trim_m923598732 (String_t* __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_ClientTokens()
extern "C" IL2CPP_METHOD_ATTR List_1_t3319525431 * FacebookSettings_get_ClientTokens_m2548316486 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String Facebook.Unity.Settings.FacebookSettings::get_AppId()
extern "C" IL2CPP_METHOD_ATTR String_t* FacebookSettings_get_AppId_m2041263324 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
extern "C" IL2CPP_METHOD_ATTR int32_t String_get_Length_m3847582255 (String_t* __this, const RuntimeMethod* method);
// System.Boolean System.String::Equals(System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_Equals_m2270643605 (String_t* __this, String_t* p0, const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Inequality_m215368492 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::get_NullableInstance()
extern "C" IL2CPP_METHOD_ATTR FacebookSettings_t265492256 * FacebookSettings_get_NullableInstance_m3084529484 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Equality_m1810815630 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// !!0 UnityEngine.ScriptableObject::CreateInstance<Facebook.Unity.Settings.FacebookSettings>()
inline FacebookSettings_t265492256 * ScriptableObject_CreateInstance_TisFacebookSettings_t265492256_m3878763949 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	return ((  FacebookSettings_t265492256 * (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))ScriptableObject_CreateInstance_TisRuntimeObject_m1552711675_gshared)(__this /* static, unused */, method);
}
// UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern "C" IL2CPP_METHOD_ATTR Object_t631007953 * Resources_Load_m3880010804 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>::Add(!0)
inline void List_1_Add_m641913066 (List_1_t4077427348 * __this, OnChangeCallback_t2605352606 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4077427348 *, OnChangeCallback_t2605352606 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.Boolean System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>::Remove(!0)
inline bool List_1_Remove_m1223406277 (List_1_t4077427348 * __this, OnChangeCallback_t2605352606 * p0, const RuntimeMethod* method)
{
	return ((  bool (*) (List_1_t4077427348 *, OnChangeCallback_t2605352606 *, const RuntimeMethod*))List_1_Remove_m2390619627_gshared)(__this, p0, method);
}
// System.Void System.Action`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m1305872465 (Action_1_t2777820201 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t2777820201 *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_m118522912_gshared)(__this, p0, p1, method);
}
// System.Void System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>::ForEach(System.Action`1<!0>)
inline void List_1_ForEach_m4089584929 (List_1_t4077427348 * __this, Action_1_t2777820201 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4077427348 *, Action_1_t2777820201 *, const RuntimeMethod*))List_1_ForEach_m3737504377_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
inline void List_1__ctor_m706204246 (List_1_t3319525431 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3319525431 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.String>::Add(!0)
inline void List_1_Add_m1685793073 (List_1_t3319525431 * __this, String_t* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3319525431 *, String_t*, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.Void System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/UrlSchemes>::.ctor()
inline void List_1__ctor_m1023008772 (List_1_t2076067583 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2076067583 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Void Facebook.Unity.Settings.FacebookSettings/UrlSchemes::.ctor(System.Collections.Generic.List`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR void UrlSchemes__ctor_m4194951824 (UrlSchemes_t603992841 * __this, List_1_t3319525431 * ___schemes0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/UrlSchemes>::Add(!0)
inline void List_1_Add_m3948897584 (List_1_t2076067583 * __this, UrlSchemes_t603992841 * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t2076067583 *, UrlSchemes_t603992841 *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.Void UnityEngine.ScriptableObject::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ScriptableObject__ctor_m1310743131 (ScriptableObject_t2528358522 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/OnChangeCallback>::.ctor()
inline void List_1__ctor_m3128781828 (List_1_t4077427348 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t4077427348 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Void Facebook.Unity.Settings.FacebookSettings/<>c::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m4285870876 (U3CU3Ec_t2896466078 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void Facebook.Unity.Settings.FacebookSettings/OnChangeCallback::Invoke()
extern "C" IL2CPP_METHOD_ATTR void OnChangeCallback_Invoke_m379538596 (OnChangeCallback_t2605352606 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 Facebook.Unity.Settings.FacebookSettings::get_SelectedAppIndex()
extern "C" IL2CPP_METHOD_ATTR int32_t FacebookSettings_get_SelectedAppIndex_m3864493479 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_SelectedAppIndex_m3864493479_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = L_0->get_selectedAppIndex_9();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_SelectedAppIndex(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_SelectedAppIndex_m2320561838 (RuntimeObject * __this /* static, unused */, int32_t ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_SelectedAppIndex_m2320561838_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = L_0->get_selectedAppIndex_9();
		int32_t L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_3 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_selectedAppIndex_9(L_4);
		FacebookSettings_SettingsChanged_m1760271451(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_AppIds()
extern "C" IL2CPP_METHOD_ATTR List_1_t3319525431 * FacebookSettings_get_AppIds_m2021279578 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_AppIds_m2021279578_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_t3319525431 * L_1 = L_0->get_appIds_11();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_AppIds(System.Collections.Generic.List`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_AppIds_m989131 (RuntimeObject * __this /* static, unused */, List_1_t3319525431 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_AppIds_m989131_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_t3319525431 * L_1 = L_0->get_appIds_11();
		List_1_t3319525431 * L_2 = ___value0;
		if ((((RuntimeObject*)(List_1_t3319525431 *)L_1) == ((RuntimeObject*)(List_1_t3319525431 *)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_3 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		List_1_t3319525431 * L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_appIds_11(L_4);
		FacebookSettings_SettingsChanged_m1760271451(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_AppLabels()
extern "C" IL2CPP_METHOD_ATTR List_1_t3319525431 * FacebookSettings_get_AppLabels_m375090046 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_AppLabels_m375090046_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_t3319525431 * L_1 = L_0->get_appLabels_12();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_AppLabels(System.Collections.Generic.List`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_AppLabels_m3173051710 (RuntimeObject * __this /* static, unused */, List_1_t3319525431 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_AppLabels_m3173051710_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_t3319525431 * L_1 = L_0->get_appLabels_12();
		List_1_t3319525431 * L_2 = ___value0;
		if ((((RuntimeObject*)(List_1_t3319525431 *)L_1) == ((RuntimeObject*)(List_1_t3319525431 *)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_3 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		List_1_t3319525431 * L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_appLabels_12(L_4);
		FacebookSettings_SettingsChanged_m1760271451(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings::get_ClientTokens()
extern "C" IL2CPP_METHOD_ATTR List_1_t3319525431 * FacebookSettings_get_ClientTokens_m2548316486 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_ClientTokens_m2548316486_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_t3319525431 * L_1 = L_0->get_clientTokens_10();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_ClientTokens(System.Collections.Generic.List`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_ClientTokens_m2937685148 (RuntimeObject * __this /* static, unused */, List_1_t3319525431 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_ClientTokens_m2937685148_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_t3319525431 * L_1 = L_0->get_clientTokens_10();
		List_1_t3319525431 * L_2 = ___value0;
		if ((((RuntimeObject*)(List_1_t3319525431 *)L_1) == ((RuntimeObject*)(List_1_t3319525431 *)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_3 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		List_1_t3319525431 * L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_clientTokens_10(L_4);
		FacebookSettings_SettingsChanged_m1760271451(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.String Facebook.Unity.Settings.FacebookSettings::get_AppId()
extern "C" IL2CPP_METHOD_ATTR String_t* FacebookSettings_get_AppId_m2041263324 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_AppId_m2041263324_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		List_1_t3319525431 * L_0 = FacebookSettings_get_AppIds_m2021279578(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = FacebookSettings_get_SelectedAppIndex_m3864493479(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_2 = List_1_get_Item_m883280456(L_0, L_1, /*hidden argument*/List_1_get_Item_m883280456_RuntimeMethod_var);
		NullCheck(L_2);
		String_t* L_3 = String_Trim_m923598732(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.String Facebook.Unity.Settings.FacebookSettings::get_ClientToken()
extern "C" IL2CPP_METHOD_ATTR String_t* FacebookSettings_get_ClientToken_m1680362625 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_ClientToken_m1680362625_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		List_1_t3319525431 * L_0 = FacebookSettings_get_ClientTokens_m2548316486(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = FacebookSettings_get_SelectedAppIndex_m3864493479(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_2 = List_1_get_Item_m883280456(L_0, L_1, /*hidden argument*/List_1_get_Item_m883280456_RuntimeMethod_var);
		NullCheck(L_2);
		String_t* L_3 = String_Trim_m923598732(L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_IsValidAppId()
extern "C" IL2CPP_METHOD_ATTR bool FacebookSettings_get_IsValidAppId_m2110720990 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_IsValidAppId_m2110720990_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		String_t* L_0 = FacebookSettings_get_AppId_m2041263324(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		String_t* L_1 = FacebookSettings_get_AppId_m2041263324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = String_get_Length_m3847582255(L_1, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0027;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		String_t* L_3 = FacebookSettings_get_AppId_m2041263324(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = String_Equals_m2270643605(L_3, _stringLiteral3452614544, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
	}

IL_0027:
	{
		return (bool)0;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Cookie()
extern "C" IL2CPP_METHOD_ATTR bool FacebookSettings_get_Cookie_m3111427717 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_Cookie_m3111427717_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_cookie_13();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_Cookie(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_Cookie_m627350524 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_Cookie_m627350524_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_cookie_13();
		bool L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_3 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_cookie_13(L_4);
		FacebookSettings_SettingsChanged_m1760271451(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Logging()
extern "C" IL2CPP_METHOD_ATTR bool FacebookSettings_get_Logging_m2591483607 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_Logging_m2591483607_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_logging_14();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_Logging(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_Logging_m2108563834 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_Logging_m2108563834_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_logging_14();
		bool L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_3 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_logging_14(L_4);
		FacebookSettings_SettingsChanged_m1760271451(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Status()
extern "C" IL2CPP_METHOD_ATTR bool FacebookSettings_get_Status_m130576739 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_Status_m130576739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_status_15();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_Status(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_Status_m3621432817 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_Status_m3621432817_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_status_15();
		bool L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_3 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_status_15(L_4);
		FacebookSettings_SettingsChanged_m1760271451(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_Xfbml()
extern "C" IL2CPP_METHOD_ATTR bool FacebookSettings_get_Xfbml_m3743274674 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_Xfbml_m3743274674_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_xfbml_16();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_Xfbml(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_Xfbml_m1250939187 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_Xfbml_m1250939187_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_xfbml_16();
		bool L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_3 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_xfbml_16(L_4);
		FacebookSettings_SettingsChanged_m1760271451(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.String Facebook.Unity.Settings.FacebookSettings::get_IosURLSuffix()
extern "C" IL2CPP_METHOD_ATTR String_t* FacebookSettings_get_IosURLSuffix_m3664804883 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_IosURLSuffix_m3664804883_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = L_0->get_iosURLSuffix_18();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_IosURLSuffix(System.String)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_IosURLSuffix_m2297605321 (RuntimeObject * __this /* static, unused */, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_IosURLSuffix_m2297605321_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = L_0->get_iosURLSuffix_18();
		String_t* L_2 = ___value0;
		bool L_3 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_4 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = ___value0;
		NullCheck(L_4);
		L_4->set_iosURLSuffix_18(L_5);
		FacebookSettings_SettingsChanged_m1760271451(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.String Facebook.Unity.Settings.FacebookSettings::get_ChannelUrl()
extern "C" IL2CPP_METHOD_ATTR String_t* FacebookSettings_get_ChannelUrl_m1181329812 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_ChannelUrl_m1181329812_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		return _stringLiteral1323011570;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_FrictionlessRequests()
extern "C" IL2CPP_METHOD_ATTR bool FacebookSettings_get_FrictionlessRequests_m2847432906 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_FrictionlessRequests_m2847432906_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_frictionlessRequests_17();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_FrictionlessRequests(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_FrictionlessRequests_m698970453 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_FrictionlessRequests_m698970453_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_frictionlessRequests_17();
		bool L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_3 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_frictionlessRequests_17(L_4);
		FacebookSettings_SettingsChanged_m1760271451(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/UrlSchemes> Facebook.Unity.Settings.FacebookSettings::get_AppLinkSchemes()
extern "C" IL2CPP_METHOD_ATTR List_1_t2076067583 * FacebookSettings_get_AppLinkSchemes_m3466100027 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_AppLinkSchemes_m3466100027_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_t2076067583 * L_1 = L_0->get_appLinkSchemes_19();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_AppLinkSchemes(System.Collections.Generic.List`1<Facebook.Unity.Settings.FacebookSettings/UrlSchemes>)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_AppLinkSchemes_m826429750 (RuntimeObject * __this /* static, unused */, List_1_t2076067583 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_AppLinkSchemes_m826429750_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		List_1_t2076067583 * L_1 = L_0->get_appLinkSchemes_19();
		List_1_t2076067583 * L_2 = ___value0;
		if ((((RuntimeObject*)(List_1_t2076067583 *)L_1) == ((RuntimeObject*)(List_1_t2076067583 *)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_3 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		List_1_t2076067583 * L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_appLinkSchemes_19(L_4);
		FacebookSettings_SettingsChanged_m1760271451(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.String Facebook.Unity.Settings.FacebookSettings::get_UploadAccessToken()
extern "C" IL2CPP_METHOD_ATTR String_t* FacebookSettings_get_UploadAccessToken_m3072067980 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_UploadAccessToken_m3072067980_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = L_0->get_uploadAccessToken_20();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_UploadAccessToken(System.String)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_UploadAccessToken_m3764926645 (RuntimeObject * __this /* static, unused */, String_t* ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_UploadAccessToken_m3764926645_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = L_0->get_uploadAccessToken_20();
		String_t* L_2 = ___value0;
		bool L_3 = String_op_Inequality_m215368492(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0022;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_4 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_5 = ___value0;
		NullCheck(L_4);
		L_4->set_uploadAccessToken_20(L_5);
		FacebookSettings_SettingsChanged_m1760271451(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_0022:
	{
		return;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_AutoLogAppEventsEnabled()
extern "C" IL2CPP_METHOD_ATTR bool FacebookSettings_get_AutoLogAppEventsEnabled_m3999834749 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_AutoLogAppEventsEnabled_m3999834749_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_autoLogAppEventsEnabled_21();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_AutoLogAppEventsEnabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_AutoLogAppEventsEnabled_m1125027621 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_AutoLogAppEventsEnabled_m1125027621_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_autoLogAppEventsEnabled_21();
		bool L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_3 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_autoLogAppEventsEnabled_21(L_4);
		FacebookSettings_SettingsChanged_m1760271451(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Boolean Facebook.Unity.Settings.FacebookSettings::get_AdvertiserIDCollectionEnabled()
extern "C" IL2CPP_METHOD_ATTR bool FacebookSettings_get_AdvertiserIDCollectionEnabled_m1524812109 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_AdvertiserIDCollectionEnabled_m1524812109_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_advertiserIDCollectionEnabled_22();
		return L_1;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::set_AdvertiserIDCollectionEnabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_set_AdvertiserIDCollectionEnabled_m1513918774 (RuntimeObject * __this /* static, unused */, bool ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_set_AdvertiserIDCollectionEnabled_m1513918774_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_1 = L_0->get_advertiserIDCollectionEnabled_22();
		bool L_2 = ___value0;
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_3 = FacebookSettings_get_Instance_m3716855809(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_4 = ___value0;
		NullCheck(L_3);
		L_3->set_advertiserIDCollectionEnabled_22(L_4);
		FacebookSettings_SettingsChanged_m1760271451(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::get_Instance()
extern "C" IL2CPP_METHOD_ATTR FacebookSettings_t265492256 * FacebookSettings_get_Instance_m3716855809 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_Instance_m3716855809_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = FacebookSettings_get_NullableInstance_m3084529484(NULL /*static, unused*/, /*hidden argument*/NULL);
		((FacebookSettings_t265492256_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t265492256_il2cpp_TypeInfo_var))->set_instance_8(L_0);
		FacebookSettings_t265492256 * L_1 = ((FacebookSettings_t265492256_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t265492256_il2cpp_TypeInfo_var))->get_instance_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_2 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_1, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0021;
		}
	}
	{
		FacebookSettings_t265492256 * L_3 = ScriptableObject_CreateInstance_TisFacebookSettings_t265492256_m3878763949(NULL /*static, unused*/, /*hidden argument*/ScriptableObject_CreateInstance_TisFacebookSettings_t265492256_m3878763949_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		((FacebookSettings_t265492256_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t265492256_il2cpp_TypeInfo_var))->set_instance_8(L_3);
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_4 = ((FacebookSettings_t265492256_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t265492256_il2cpp_TypeInfo_var))->get_instance_8();
		return L_4;
	}
}
// Facebook.Unity.Settings.FacebookSettings Facebook.Unity.Settings.FacebookSettings::get_NullableInstance()
extern "C" IL2CPP_METHOD_ATTR FacebookSettings_t265492256 * FacebookSettings_get_NullableInstance_m3084529484 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_get_NullableInstance_m3084529484_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_0 = ((FacebookSettings_t265492256_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t265492256_il2cpp_TypeInfo_var))->get_instance_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_m1810815630(NULL /*static, unused*/, L_0, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Object_t631007953 * L_2 = Resources_Load_m3880010804(NULL /*static, unused*/, _stringLiteral786313173, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		((FacebookSettings_t265492256_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t265492256_il2cpp_TypeInfo_var))->set_instance_8(((FacebookSettings_t265492256 *)IsInstClass((RuntimeObject*)L_2, FacebookSettings_t265492256_il2cpp_TypeInfo_var)));
	}

IL_0021:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		FacebookSettings_t265492256 * L_3 = ((FacebookSettings_t265492256_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t265492256_il2cpp_TypeInfo_var))->get_instance_8();
		return L_3;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::RegisterChangeEventCallback(Facebook.Unity.Settings.FacebookSettings/OnChangeCallback)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_RegisterChangeEventCallback_m3472570634 (RuntimeObject * __this /* static, unused */, OnChangeCallback_t2605352606 * ___callback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_RegisterChangeEventCallback_m3472570634_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		List_1_t4077427348 * L_0 = ((FacebookSettings_t265492256_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t265492256_il2cpp_TypeInfo_var))->get_onChangeCallbacks_7();
		OnChangeCallback_t2605352606 * L_1 = ___callback0;
		NullCheck(L_0);
		List_1_Add_m641913066(L_0, L_1, /*hidden argument*/List_1_Add_m641913066_RuntimeMethod_var);
		return;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::UnregisterChangeEventCallback(Facebook.Unity.Settings.FacebookSettings/OnChangeCallback)
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_UnregisterChangeEventCallback_m1404630495 (RuntimeObject * __this /* static, unused */, OnChangeCallback_t2605352606 * ___callback0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_UnregisterChangeEventCallback_m1404630495_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		List_1_t4077427348 * L_0 = ((FacebookSettings_t265492256_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t265492256_il2cpp_TypeInfo_var))->get_onChangeCallbacks_7();
		OnChangeCallback_t2605352606 * L_1 = ___callback0;
		NullCheck(L_0);
		List_1_Remove_m1223406277(L_0, L_1, /*hidden argument*/List_1_Remove_m1223406277_RuntimeMethod_var);
		return;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::SettingsChanged()
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings_SettingsChanged_m1760271451 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings_SettingsChanged_m1760271451_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Action_1_t2777820201 * G_B2_0 = NULL;
	List_1_t4077427348 * G_B2_1 = NULL;
	Action_1_t2777820201 * G_B1_0 = NULL;
	List_1_t4077427348 * G_B1_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(FacebookSettings_t265492256_il2cpp_TypeInfo_var);
		List_1_t4077427348 * L_0 = ((FacebookSettings_t265492256_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t265492256_il2cpp_TypeInfo_var))->get_onChangeCallbacks_7();
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t2896466078_il2cpp_TypeInfo_var);
		Action_1_t2777820201 * L_1 = ((U3CU3Ec_t2896466078_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2896466078_il2cpp_TypeInfo_var))->get_U3CU3E9__76_0_1();
		Action_1_t2777820201 * L_2 = L_1;
		G_B1_0 = L_2;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = L_0;
			goto IL_0024;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(U3CU3Ec_t2896466078_il2cpp_TypeInfo_var);
		U3CU3Ec_t2896466078 * L_3 = ((U3CU3Ec_t2896466078_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2896466078_il2cpp_TypeInfo_var))->get_U3CU3E9_0();
		intptr_t L_4 = (intptr_t)U3CU3Ec_U3CSettingsChangedU3Eb__76_0_m52101905_RuntimeMethod_var;
		Action_1_t2777820201 * L_5 = (Action_1_t2777820201 *)il2cpp_codegen_object_new(Action_1_t2777820201_il2cpp_TypeInfo_var);
		Action_1__ctor_m1305872465(L_5, L_3, (intptr_t)L_4, /*hidden argument*/Action_1__ctor_m1305872465_RuntimeMethod_var);
		Action_1_t2777820201 * L_6 = L_5;
		((U3CU3Ec_t2896466078_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2896466078_il2cpp_TypeInfo_var))->set_U3CU3E9__76_0_1(L_6);
		G_B2_0 = L_6;
		G_B2_1 = G_B1_1;
	}

IL_0024:
	{
		NullCheck(G_B2_1);
		List_1_ForEach_m4089584929(G_B2_1, G_B2_0, /*hidden argument*/List_1_ForEach_m4089584929_RuntimeMethod_var);
		return;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::.ctor()
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings__ctor_m1186197728 (FacebookSettings_t265492256 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings__ctor_m1186197728_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t3319525431 * L_0 = (List_1_t3319525431 *)il2cpp_codegen_object_new(List_1_t3319525431_il2cpp_TypeInfo_var);
		List_1__ctor_m706204246(L_0, /*hidden argument*/List_1__ctor_m706204246_RuntimeMethod_var);
		List_1_t3319525431 * L_1 = L_0;
		String_t* L_2 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		NullCheck(L_1);
		List_1_Add_m1685793073(L_1, L_2, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
		__this->set_clientTokens_10(L_1);
		List_1_t3319525431 * L_3 = (List_1_t3319525431 *)il2cpp_codegen_object_new(List_1_t3319525431_il2cpp_TypeInfo_var);
		List_1__ctor_m706204246(L_3, /*hidden argument*/List_1__ctor_m706204246_RuntimeMethod_var);
		List_1_t3319525431 * L_4 = L_3;
		NullCheck(L_4);
		List_1_Add_m1685793073(L_4, _stringLiteral3452614544, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
		__this->set_appIds_11(L_4);
		List_1_t3319525431 * L_5 = (List_1_t3319525431 *)il2cpp_codegen_object_new(List_1_t3319525431_il2cpp_TypeInfo_var);
		List_1__ctor_m706204246(L_5, /*hidden argument*/List_1__ctor_m706204246_RuntimeMethod_var);
		List_1_t3319525431 * L_6 = L_5;
		NullCheck(L_6);
		List_1_Add_m1685793073(L_6, _stringLiteral565915282, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
		__this->set_appLabels_12(L_6);
		__this->set_cookie_13((bool)1);
		__this->set_logging_14((bool)1);
		__this->set_status_15((bool)1);
		__this->set_frictionlessRequests_17((bool)1);
		String_t* L_7 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_iosURLSuffix_18(L_7);
		List_1_t2076067583 * L_8 = (List_1_t2076067583 *)il2cpp_codegen_object_new(List_1_t2076067583_il2cpp_TypeInfo_var);
		List_1__ctor_m1023008772(L_8, /*hidden argument*/List_1__ctor_m1023008772_RuntimeMethod_var);
		List_1_t2076067583 * L_9 = L_8;
		UrlSchemes_t603992841 * L_10 = (UrlSchemes_t603992841 *)il2cpp_codegen_object_new(UrlSchemes_t603992841_il2cpp_TypeInfo_var);
		UrlSchemes__ctor_m4194951824(L_10, (List_1_t3319525431 *)NULL, /*hidden argument*/NULL);
		NullCheck(L_9);
		List_1_Add_m3948897584(L_9, L_10, /*hidden argument*/List_1_Add_m3948897584_RuntimeMethod_var);
		__this->set_appLinkSchemes_19(L_9);
		String_t* L_11 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_uploadAccessToken_20(L_11);
		__this->set_autoLogAppEventsEnabled_21((bool)1);
		__this->set_advertiserIDCollectionEnabled_22((bool)1);
		ScriptableObject__ctor_m1310743131(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings::.cctor()
extern "C" IL2CPP_METHOD_ATTR void FacebookSettings__cctor_m2460564019 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FacebookSettings__cctor_m2460564019_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		List_1_t4077427348 * L_0 = (List_1_t4077427348 *)il2cpp_codegen_object_new(List_1_t4077427348_il2cpp_TypeInfo_var);
		List_1__ctor_m3128781828(L_0, /*hidden argument*/List_1__ctor_m3128781828_RuntimeMethod_var);
		((FacebookSettings_t265492256_StaticFields*)il2cpp_codegen_static_fields_for(FacebookSettings_t265492256_il2cpp_TypeInfo_var))->set_onChangeCallbacks_7(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.Unity.Settings.FacebookSettings/<>c::.cctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_m3195961849 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__cctor_m3195961849_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CU3Ec_t2896466078 * L_0 = (U3CU3Ec_t2896466078 *)il2cpp_codegen_object_new(U3CU3Ec_t2896466078_il2cpp_TypeInfo_var);
		U3CU3Ec__ctor_m4285870876(L_0, /*hidden argument*/NULL);
		((U3CU3Ec_t2896466078_StaticFields*)il2cpp_codegen_static_fields_for(U3CU3Ec_t2896466078_il2cpp_TypeInfo_var))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings/<>c::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m4285870876 (U3CU3Ec_t2896466078 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings/<>c::<SettingsChanged>b__76_0(Facebook.Unity.Settings.FacebookSettings/OnChangeCallback)
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec_U3CSettingsChangedU3Eb__76_0_m52101905 (U3CU3Ec_t2896466078 * __this, OnChangeCallback_t2605352606 * ___callback0, const RuntimeMethod* method)
{
	{
		OnChangeCallback_t2605352606 * L_0 = ___callback0;
		NullCheck(L_0);
		OnChangeCallback_Invoke_m379538596(L_0, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern "C"  void DelegatePInvokeWrapper_OnChangeCallback_t2605352606 (OnChangeCallback_t2605352606 * __this, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc)();
	PInvokeFunc il2cppPInvokeFunc = reinterpret_cast<PInvokeFunc>(il2cpp_codegen_get_method_pointer(((RuntimeDelegate*)__this)->method));

	// Native function invocation
	il2cppPInvokeFunc();

}
// System.Void Facebook.Unity.Settings.FacebookSettings/OnChangeCallback::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void OnChangeCallback__ctor_m2607534094 (OnChangeCallback_t2605352606 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	__this->set_method_ptr_0(il2cpp_codegen_get_method_pointer((RuntimeMethod*)___method1));
	__this->set_method_3(___method1);
	__this->set_m_target_2(___object0);
}
// System.Void Facebook.Unity.Settings.FacebookSettings/OnChangeCallback::Invoke()
extern "C" IL2CPP_METHOD_ATTR void OnChangeCallback_Invoke_m379538596 (OnChangeCallback_t2605352606 * __this, const RuntimeMethod* method)
{
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((RuntimeMethod*)(__this->get_method_3()));
	DelegateU5BU5D_t1703627840* delegatesToInvoke = __this->get_delegates_11();
	if (delegatesToInvoke != NULL)
	{
		il2cpp_array_size_t length = delegatesToInvoke->max_length;
		for (il2cpp_array_size_t i = 0; i < length; i++)
		{
			Delegate_t1188392813* currentDelegate = (delegatesToInvoke)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i));
			Il2CppMethodPointer targetMethodPointer = currentDelegate->get_method_ptr_0();
			RuntimeMethod* targetMethod = (RuntimeMethod*)(currentDelegate->get_method_3());
			RuntimeObject* targetThis = currentDelegate->get_m_target_2();
			il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
			bool ___methodIsStatic = MethodIsStatic(targetMethod);
			if (___methodIsStatic)
			{
				if (il2cpp_codegen_method_parameter_count(targetMethod) == 0)
				{
					// open
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, targetMethod);
					}
				}
				else
				{
					// closed
					{
						typedef void (*FunctionPointerType) (RuntimeObject *, void*, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(NULL, targetThis, targetMethod);
					}
				}
			}
			else
			{
				{
					// closed
					if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
					{
						if (il2cpp_codegen_method_is_generic_instance(targetMethod))
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								GenericInterfaceActionInvoker0::Invoke(targetMethod, targetThis);
							else
								GenericVirtActionInvoker0::Invoke(targetMethod, targetThis);
						}
						else
						{
							if (il2cpp_codegen_method_is_interface_method(targetMethod))
								InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
							else
								VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
						}
					}
					else
					{
						typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
						((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
					}
				}
			}
		}
	}
	else
	{
		Il2CppMethodPointer targetMethodPointer = __this->get_method_ptr_0();
		RuntimeMethod* targetMethod = (RuntimeMethod*)(__this->get_method_3());
		RuntimeObject* targetThis = __this->get_m_target_2();
		il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found(targetMethod);
		bool ___methodIsStatic = MethodIsStatic(targetMethod);
		if (___methodIsStatic)
		{
			if (il2cpp_codegen_method_parameter_count(targetMethod) == 0)
			{
				// open
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, targetMethod);
				}
			}
			else
			{
				// closed
				{
					typedef void (*FunctionPointerType) (RuntimeObject *, void*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(NULL, targetThis, targetMethod);
				}
			}
		}
		else
		{
			{
				// closed
				if (il2cpp_codegen_method_is_virtual(targetMethod) && !il2cpp_codegen_object_is_of_sealed_type(targetThis) && il2cpp_codegen_delegate_has_invoker((Il2CppDelegate*)__this))
				{
					if (il2cpp_codegen_method_is_generic_instance(targetMethod))
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							GenericInterfaceActionInvoker0::Invoke(targetMethod, targetThis);
						else
							GenericVirtActionInvoker0::Invoke(targetMethod, targetThis);
					}
					else
					{
						if (il2cpp_codegen_method_is_interface_method(targetMethod))
							InterfaceActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), il2cpp_codegen_method_get_declaring_type(targetMethod), targetThis);
						else
							VirtActionInvoker0::Invoke(il2cpp_codegen_method_get_slot(targetMethod), targetThis);
					}
				}
				else
				{
					typedef void (*FunctionPointerType) (void*, const RuntimeMethod*);
					((FunctionPointerType)targetMethodPointer)(targetThis, targetMethod);
				}
			}
		}
	}
}
// System.IAsyncResult Facebook.Unity.Settings.FacebookSettings/OnChangeCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* OnChangeCallback_BeginInvoke_m1095598358 (OnChangeCallback_t2605352606 * __this, AsyncCallback_t3962456242 * ___callback0, RuntimeObject * ___object1, const RuntimeMethod* method)
{
	void *__d_args[1] = {0};
	return (RuntimeObject*)il2cpp_codegen_delegate_begin_invoke((RuntimeDelegate*)__this, __d_args, (RuntimeDelegate*)___callback0, (RuntimeObject*)___object1);
}
// System.Void Facebook.Unity.Settings.FacebookSettings/OnChangeCallback::EndInvoke(System.IAsyncResult)
extern "C" IL2CPP_METHOD_ATTR void OnChangeCallback_EndInvoke_m3952752254 (OnChangeCallback_t2605352606 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	il2cpp_codegen_delegate_end_invoke((Il2CppAsyncResult*) ___result0, 0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.Unity.Settings.FacebookSettings/UrlSchemes::.ctor(System.Collections.Generic.List`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR void UrlSchemes__ctor_m4194951824 (UrlSchemes_t603992841 * __this, List_1_t3319525431 * ___schemes0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (UrlSchemes__ctor_m4194951824_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	UrlSchemes_t603992841 * G_B2_0 = NULL;
	UrlSchemes_t603992841 * G_B1_0 = NULL;
	List_1_t3319525431 * G_B3_0 = NULL;
	UrlSchemes_t603992841 * G_B3_1 = NULL;
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		List_1_t3319525431 * L_0 = ___schemes0;
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_000d;
		}
	}
	{
		List_1_t3319525431 * L_1 = ___schemes0;
		G_B3_0 = L_1;
		G_B3_1 = G_B1_0;
		goto IL_0012;
	}

IL_000d:
	{
		List_1_t3319525431 * L_2 = (List_1_t3319525431 *)il2cpp_codegen_object_new(List_1_t3319525431_il2cpp_TypeInfo_var);
		List_1__ctor_m706204246(L_2, /*hidden argument*/List_1__ctor_m706204246_RuntimeMethod_var);
		G_B3_0 = L_2;
		G_B3_1 = G_B2_0;
	}

IL_0012:
	{
		NullCheck(G_B3_1);
		G_B3_1->set_list_0(G_B3_0);
		return;
	}
}
// System.Collections.Generic.List`1<System.String> Facebook.Unity.Settings.FacebookSettings/UrlSchemes::get_Schemes()
extern "C" IL2CPP_METHOD_ATTR List_1_t3319525431 * UrlSchemes_get_Schemes_m4104936705 (UrlSchemes_t603992841 * __this, const RuntimeMethod* method)
{
	{
		List_1_t3319525431 * L_0 = __this->get_list_0();
		return L_0;
	}
}
// System.Void Facebook.Unity.Settings.FacebookSettings/UrlSchemes::set_Schemes(System.Collections.Generic.List`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR void UrlSchemes_set_Schemes_m1205686412 (UrlSchemes_t603992841 * __this, List_1_t3319525431 * ___value0, const RuntimeMethod* method)
{
	{
		List_1_t3319525431 * L_0 = ___value0;
		__this->set_list_0(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
