﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1, typename T2>
struct InterfaceActionInvoker2
{
	typedef void (*Action)(void*, T1, T2, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// Facebook.Unity.AccessToken
struct AccessToken_t2431487013;
// Facebook.Unity.Example.AccessTokenMenu
struct AccessTokenMenu_t4028641200;
// Facebook.Unity.Example.AppEvents
struct AppEvents_t3645639549;
// Facebook.Unity.Example.AppLinks
struct AppLinks_t2028121612;
// Facebook.Unity.Example.AppRequests
struct AppRequests_t2419817778;
// Facebook.Unity.Example.ConsoleBase
struct ConsoleBase_t1023975560;
// Facebook.Unity.Example.DialogShare
struct DialogShare_t2334043085;
// Facebook.Unity.Example.GraphRequest
struct GraphRequest_t4047451309;
// Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0
struct U3CTakeScreenshotU3Ec__Iterator0_t3544038915;
// Facebook.Unity.Example.LogView
struct LogView_t1067263371;
// Facebook.Unity.Example.MainMenu
struct MainMenu_t1823058806;
// Facebook.Unity.Example.MenuBase
struct MenuBase_t1079888489;
// Facebook.Unity.Example.Pay
struct Pay_t1264260185;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAccessTokenRefreshResult>
struct FacebookDelegate_1_t123177164;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>
struct FacebookDelegate_1_t3420330156;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>
struct FacebookDelegate_1_t1976622014;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>
struct FacebookDelegate_1_t138390958;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>
struct FacebookDelegate_1_t1669328765;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>
struct FacebookDelegate_1_t2649775846;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>
struct FacebookDelegate_1_t602110559;
// Facebook.Unity.FacebookDelegate`1<System.Object>
struct FacebookDelegate_1_t903090771;
// Facebook.Unity.HideUnityDelegate
struct HideUnityDelegate_t1353799728;
// Facebook.Unity.IAccessTokenRefreshResult
struct IAccessTokenRefreshResult_t2300192557;
// Facebook.Unity.IAppLinkResult
struct IAppLinkResult_t1302378253;
// Facebook.Unity.IAppRequestResult
struct IAppRequestResult_t4153637407;
// Facebook.Unity.IGraphResult
struct IGraphResult_t2315406351;
// Facebook.Unity.ILoginResult
struct ILoginResult_t3846344158;
// Facebook.Unity.IPayResult
struct IPayResult_t531823943;
// Facebook.Unity.IResult
struct IResult_t2818782343;
// Facebook.Unity.IShareResult
struct IShareResult_t2779125952;
// Facebook.Unity.InitDelegate
struct InitDelegate_t3081360126;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.Dictionary`2/Entry<System.String,System.Object>[]
struct EntryU5BU5D_t2447176574;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,System.Object>
struct KeyCollection_t3055037934;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,System.Object>
struct ValueCollection_t286439485;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t132545152;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle>
struct Dictionary_2_t3742157810;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t96558379;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t2059959053;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t827303578;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t3954782707;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_t3662770472;
// System.Collections.Generic.List`1<System.Byte[]>
struct List_1_t1293755103;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t257213610;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t3923495619;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t2690840144;
// System.Collections.IDictionary
struct IDictionary_t1363984059;
// System.Collections.IEnumerable
struct IEnumerable_t1941168011;
// System.Collections.IEnumerator
struct IEnumerator_t1853284238;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t1169129676;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// System.IntPtr[]
struct IntPtrU5BU5D_t4013366056;
// System.NotSupportedException
struct NotSupportedException_t1314879016;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.Binder
struct Binder_t2999457153;
// System.Reflection.MemberFilter
struct MemberFilter_t426314064;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t2481557153;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t3940880105;
// System.Uri
struct Uri_t100236324;
// System.Uri/UriInfo
struct UriInfo_t1092684687;
// System.UriParser
struct UriParser_t3890150400;
// System.Void
struct Void_t1185182177;
// UnityEngine.Coroutine
struct Coroutine_t3829159415;
// UnityEngine.Font
struct Font_t1956802104;
// UnityEngine.GUIContent
struct GUIContent_t3050628031;
// UnityEngine.GUILayoutOption
struct GUILayoutOption_t811797299;
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t2510215842;
// UnityEngine.GUISettings
struct GUISettings_t1774757634;
// UnityEngine.GUISkin
struct GUISkin_t1244372282;
// UnityEngine.GUISkin/SkinChangedDelegate
struct SkinChangedDelegate_t1143955295;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// UnityEngine.GUIStyleState
struct GUIStyleState_t1397964415;
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t2383250302;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t3962482529;
// UnityEngine.Object
struct Object_t631007953;
// UnityEngine.RectOffset
struct RectOffset_t1369453676;
// UnityEngine.Texture
struct Texture_t3661962703;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;
// UnityEngine.WaitForEndOfFrame
struct WaitForEndOfFrame_t1314943911;

extern RuntimeClass* Boolean_t97287965_il2cpp_TypeInfo_var;
extern RuntimeClass* CharU5BU5D_t3528271667_il2cpp_TypeInfo_var;
extern RuntimeClass* ConsoleBase_t1023975560_il2cpp_TypeInfo_var;
extern RuntimeClass* DateTime_t3738529785_il2cpp_TypeInfo_var;
extern RuntimeClass* Dictionary_2_t2865362463_il2cpp_TypeInfo_var;
extern RuntimeClass* Enum_t4135868527_il2cpp_TypeInfo_var;
extern RuntimeClass* FB_t2178373596_il2cpp_TypeInfo_var;
extern RuntimeClass* FacebookDelegate_1_t123177164_il2cpp_TypeInfo_var;
extern RuntimeClass* FacebookDelegate_1_t138390958_il2cpp_TypeInfo_var;
extern RuntimeClass* FacebookDelegate_1_t1669328765_il2cpp_TypeInfo_var;
extern RuntimeClass* FacebookDelegate_1_t1976622014_il2cpp_TypeInfo_var;
extern RuntimeClass* FacebookDelegate_1_t2649775846_il2cpp_TypeInfo_var;
extern RuntimeClass* FacebookDelegate_1_t3420330156_il2cpp_TypeInfo_var;
extern RuntimeClass* FacebookDelegate_1_t602110559_il2cpp_TypeInfo_var;
extern RuntimeClass* GUIContent_t3050628031_il2cpp_TypeInfo_var;
extern RuntimeClass* GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var;
extern RuntimeClass* GUIStyle_t3956901511_il2cpp_TypeInfo_var;
extern RuntimeClass* GUI_t1624858472_il2cpp_TypeInfo_var;
extern RuntimeClass* HideUnityDelegate_t1353799728_il2cpp_TypeInfo_var;
extern RuntimeClass* IDisposable_t3640265483_il2cpp_TypeInfo_var;
extern RuntimeClass* IEnumerator_t1853284238_il2cpp_TypeInfo_var;
extern RuntimeClass* IGraphResult_t2315406351_il2cpp_TypeInfo_var;
extern RuntimeClass* IList_1_t3662770472_il2cpp_TypeInfo_var;
extern RuntimeClass* IResult_t2818782343_il2cpp_TypeInfo_var;
extern RuntimeClass* InitDelegate_t3081360126_il2cpp_TypeInfo_var;
extern RuntimeClass* Input_t1431474628_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t257213610_il2cpp_TypeInfo_var;
extern RuntimeClass* List_1_t3319525431_il2cpp_TypeInfo_var;
extern RuntimeClass* LogView_t1067263371_il2cpp_TypeInfo_var;
extern RuntimeClass* Math_t1671470975_il2cpp_TypeInfo_var;
extern RuntimeClass* MenuBase_t1079888489_il2cpp_TypeInfo_var;
extern RuntimeClass* NotSupportedException_t1314879016_il2cpp_TypeInfo_var;
extern RuntimeClass* OGActionType_t397354381_il2cpp_TypeInfo_var;
extern RuntimeClass* Object_t631007953_il2cpp_TypeInfo_var;
extern RuntimeClass* RectOffset_t1369453676_il2cpp_TypeInfo_var;
extern RuntimeClass* ShareDialogMode_t1679970535_il2cpp_TypeInfo_var;
extern RuntimeClass* Stack_1_t2690840144_il2cpp_TypeInfo_var;
extern RuntimeClass* StringU5BU5D_t1281789340_il2cpp_TypeInfo_var;
extern RuntimeClass* String_t_il2cpp_TypeInfo_var;
extern RuntimeClass* Texture2D_t3840446185_il2cpp_TypeInfo_var;
extern RuntimeClass* Type_t_il2cpp_TypeInfo_var;
extern RuntimeClass* U3CTakeScreenshotU3Ec__Iterator0_t3544038915_il2cpp_TypeInfo_var;
extern RuntimeClass* Uri_t100236324_il2cpp_TypeInfo_var;
extern RuntimeClass* Vector2_t2156229523_il2cpp_TypeInfo_var;
extern RuntimeClass* WWWForm_t4064702195_il2cpp_TypeInfo_var;
extern RuntimeClass* WaitForEndOfFrame_t1314943911_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral116104668;
extern String_t* _stringLiteral1167841337;
extern String_t* _stringLiteral1207775305;
extern String_t* _stringLiteral1235538921;
extern String_t* _stringLiteral1236728777;
extern String_t* _stringLiteral1237965765;
extern String_t* _stringLiteral1259398883;
extern String_t* _stringLiteral1346698490;
extern String_t* _stringLiteral1357171336;
extern String_t* _stringLiteral135812671;
extern String_t* _stringLiteral1383326236;
extern String_t* _stringLiteral1405573206;
extern String_t* _stringLiteral1410652248;
extern String_t* _stringLiteral1458937931;
extern String_t* _stringLiteral1499488298;
extern String_t* _stringLiteral1535797236;
extern String_t* _stringLiteral1606485075;
extern String_t* _stringLiteral1674951175;
extern String_t* _stringLiteral1700118802;
extern String_t* _stringLiteral172829340;
extern String_t* _stringLiteral1769972441;
extern String_t* _stringLiteral1810187820;
extern String_t* _stringLiteral1833358689;
extern String_t* _stringLiteral1839852391;
extern String_t* _stringLiteral2072270356;
extern String_t* _stringLiteral2129832496;
extern String_t* _stringLiteral2394881543;
extern String_t* _stringLiteral2405134015;
extern String_t* _stringLiteral243311952;
extern String_t* _stringLiteral2455296183;
extern String_t* _stringLiteral2487819760;
extern String_t* _stringLiteral2500948695;
extern String_t* _stringLiteral2523608186;
extern String_t* _stringLiteral2552831472;
extern String_t* _stringLiteral2599999525;
extern String_t* _stringLiteral2600534157;
extern String_t* _stringLiteral2611099171;
extern String_t* _stringLiteral2644898952;
extern String_t* _stringLiteral2670760884;
extern String_t* _stringLiteral2686216175;
extern String_t* _stringLiteral2829152152;
extern String_t* _stringLiteral2833616318;
extern String_t* _stringLiteral2854633673;
extern String_t* _stringLiteral2862787243;
extern String_t* _stringLiteral2872441188;
extern String_t* _stringLiteral2896053709;
extern String_t* _stringLiteral2965269087;
extern String_t* _stringLiteral3024588073;
extern String_t* _stringLiteral3033606244;
extern String_t* _stringLiteral3039347034;
extern String_t* _stringLiteral3045473405;
extern String_t* _stringLiteral3070279316;
extern String_t* _stringLiteral3191340502;
extern String_t* _stringLiteral3247624226;
extern String_t* _stringLiteral3253941996;
extern String_t* _stringLiteral3329508946;
extern String_t* _stringLiteral3357641048;
extern String_t* _stringLiteral3357662094;
extern String_t* _stringLiteral3367156840;
extern String_t* _stringLiteral3369767408;
extern String_t* _stringLiteral3452614566;
extern String_t* _stringLiteral3454777324;
extern String_t* _stringLiteral3464325523;
extern String_t* _stringLiteral346560186;
extern String_t* _stringLiteral3497109170;
extern String_t* _stringLiteral3524439568;
extern String_t* _stringLiteral3613892036;
extern String_t* _stringLiteral3661832575;
extern String_t* _stringLiteral3787658585;
extern String_t* _stringLiteral379382575;
extern String_t* _stringLiteral3858022679;
extern String_t* _stringLiteral3863765171;
extern String_t* _stringLiteral3928422333;
extern String_t* _stringLiteral3963994475;
extern String_t* _stringLiteral3978312045;
extern String_t* _stringLiteral403140625;
extern String_t* _stringLiteral4052139789;
extern String_t* _stringLiteral408850744;
extern String_t* _stringLiteral4089792011;
extern String_t* _stringLiteral4147269477;
extern String_t* _stringLiteral415541884;
extern String_t* _stringLiteral4160356986;
extern String_t* _stringLiteral4170297164;
extern String_t* _stringLiteral4175710859;
extern String_t* _stringLiteral4249307141;
extern String_t* _stringLiteral435507638;
extern String_t* _stringLiteral450383487;
extern String_t* _stringLiteral454323450;
extern String_t* _stringLiteral475705030;
extern String_t* _stringLiteral565867462;
extern String_t* _stringLiteral568801581;
extern String_t* _stringLiteral606735498;
extern String_t* _stringLiteral626585464;
extern String_t* _stringLiteral640607579;
extern String_t* _stringLiteral677797808;
extern String_t* _stringLiteral733908546;
extern String_t* _stringLiteral80284664;
extern String_t* _stringLiteral809088009;
extern String_t* _stringLiteral820750591;
extern String_t* _stringLiteral820751583;
extern String_t* _stringLiteral828973377;
extern String_t* _stringLiteral841429450;
extern String_t* _stringLiteral917394094;
extern const RuntimeMethod* Array_Empty_TisGUILayoutOption_t811797299_m4167206926_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2_Add_m3724819337_RuntimeMethod_var;
extern const RuntimeMethod* Dictionary_2__ctor_m15304876_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_Any_TisString_t_m3986012742_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_OfType_TisRuntimeObject_m444860550_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_ToArray_TisString_t_m2924696684_RuntimeMethod_var;
extern const RuntimeMethod* Enumerable_ToList_TisRuntimeObject_m2366248129_RuntimeMethod_var;
extern const RuntimeMethod* FacebookDelegate_1__ctor_m1320861630_RuntimeMethod_var;
extern const RuntimeMethod* FacebookDelegate_1__ctor_m1454705827_RuntimeMethod_var;
extern const RuntimeMethod* FacebookDelegate_1__ctor_m2125150161_RuntimeMethod_var;
extern const RuntimeMethod* FacebookDelegate_1__ctor_m2272005295_RuntimeMethod_var;
extern const RuntimeMethod* FacebookDelegate_1__ctor_m3067526517_RuntimeMethod_var;
extern const RuntimeMethod* FacebookDelegate_1__ctor_m3417605933_RuntimeMethod_var;
extern const RuntimeMethod* FacebookDelegate_1__ctor_m411964393_RuntimeMethod_var;
extern const RuntimeMethod* GraphRequest_ProfilePhotoCallback_m2406981014_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m1685793073_RuntimeMethod_var;
extern const RuntimeMethod* List_1_Add_m3338814081_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m2321703786_RuntimeMethod_var;
extern const RuntimeMethod* List_1__ctor_m706204246_RuntimeMethod_var;
extern const RuntimeMethod* MainMenu_OnHideUnity_m3704770818_RuntimeMethod_var;
extern const RuntimeMethod* MainMenu_OnInitComplete_m4103279806_RuntimeMethod_var;
extern const RuntimeMethod* MenuBase_HandleResult_m3006125243_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1__ctor_m2532889892_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1__ctor_m2962682148_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1__ctor_m3964206222_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1_get_HasValue_m2149791491_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1_get_HasValue_m604914582_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1_get_Value_m1111532194_RuntimeMethod_var;
extern const RuntimeMethod* Nullable_1_get_Value_m4168550405_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Pop_m812987992_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1_Push_m559717952_RuntimeMethod_var;
extern const RuntimeMethod* Stack_1__ctor_m2088216795_RuntimeMethod_var;
extern const RuntimeMethod* U3CTakeScreenshotU3Ec__Iterator0_Reset_m262479894_RuntimeMethod_var;
extern const RuntimeType* AccessTokenMenu_t4028641200_0_0_0_var;
extern const RuntimeType* AppEvents_t3645639549_0_0_0_var;
extern const RuntimeType* AppLinks_t2028121612_0_0_0_var;
extern const RuntimeType* AppRequests_t2419817778_0_0_0_var;
extern const RuntimeType* DialogShare_t2334043085_0_0_0_var;
extern const RuntimeType* GraphRequest_t4047451309_0_0_0_var;
extern const RuntimeType* LogView_t1067263371_0_0_0_var;
extern const RuntimeType* Pay_t1264260185_0_0_0_var;
extern const RuntimeType* ShareDialogMode_t1679970535_0_0_0_var;
extern const uint32_t AccessTokenMenu_GetGui_m2805584269_MetadataUsageId;
extern const uint32_t AppEvents_GetGui_m1498937247_MetadataUsageId;
extern const uint32_t AppLinks_GetGui_m3812488917_MetadataUsageId;
extern const uint32_t AppRequests_GetGui_m3488322347_MetadataUsageId;
extern const uint32_t AppRequests_GetSelectedOGActionType_m1566487639_MetadataUsageId;
extern const uint32_t AppRequests__ctor_m3895077944_MetadataUsageId;
extern const uint32_t ConsoleBase_Button_m1005519490_MetadataUsageId;
extern const uint32_t ConsoleBase_GoBack_m3535004784_MetadataUsageId;
extern const uint32_t ConsoleBase_LabelAndTextField_m2312391562_MetadataUsageId;
extern const uint32_t ConsoleBase_SwitchMenu_m676980302_MetadataUsageId;
extern const uint32_t ConsoleBase__cctor_m1709056498_MetadataUsageId;
extern const uint32_t ConsoleBase__ctor_m3547073371_MetadataUsageId;
extern const uint32_t ConsoleBase_get_ButtonStyle_m1393828074_MetadataUsageId;
extern const uint32_t ConsoleBase_get_FontSize_m754853133_MetadataUsageId;
extern const uint32_t ConsoleBase_get_LabelStyle_m3856255500_MetadataUsageId;
extern const uint32_t ConsoleBase_get_MenuStack_m1135196678_MetadataUsageId;
extern const uint32_t ConsoleBase_get_ScaleFactor_m540546232_MetadataUsageId;
extern const uint32_t ConsoleBase_get_TextInputStyle_m927335950_MetadataUsageId;
extern const uint32_t ConsoleBase_get_TextStyle_m2535314378_MetadataUsageId;
extern const uint32_t ConsoleBase_set_MenuStack_m582115117_MetadataUsageId;
extern const uint32_t DialogShare_GetGui_m2888532382_MetadataUsageId;
extern const uint32_t DialogShare__ctor_m3148804403_MetadataUsageId;
extern const uint32_t GraphRequest_GetGui_m1928235709_MetadataUsageId;
extern const uint32_t GraphRequest_ProfilePhotoCallback_m2406981014_MetadataUsageId;
extern const uint32_t GraphRequest_TakeScreenshot_m1191488447_MetadataUsageId;
extern const uint32_t GraphRequest__ctor_m89463010_MetadataUsageId;
extern const uint32_t LogView_AddLog_m1637420510_MetadataUsageId;
extern const uint32_t LogView_OnGUI_m1199105350_MetadataUsageId;
extern const uint32_t LogView__cctor_m45639004_MetadataUsageId;
extern const uint32_t LogView__ctor_m298494391_MetadataUsageId;
extern const uint32_t MainMenu_CallFBLoginForPublish_m2374054384_MetadataUsageId;
extern const uint32_t MainMenu_CallFBLogin_m2730593439_MetadataUsageId;
extern const uint32_t MainMenu_CallFBLogout_m4130237067_MetadataUsageId;
extern const uint32_t MainMenu_GetGui_m3329753249_MetadataUsageId;
extern const uint32_t MainMenu_OnHideUnity_m3704770818_MetadataUsageId;
extern const uint32_t MainMenu_OnInitComplete_m4103279806_MetadataUsageId;
extern const uint32_t MenuBase_AddBackButton_m214045700_MetadataUsageId;
extern const uint32_t MenuBase_AddDialogModeButton_m2926670472_MetadataUsageId;
extern const uint32_t MenuBase_AddDialogModeButtons_m2182366232_MetadataUsageId;
extern const uint32_t MenuBase_AddLogButton_m1613724113_MetadataUsageId;
extern const uint32_t MenuBase_AddStatus_m319405204_MetadataUsageId;
extern const uint32_t MenuBase_HandleResult_m3006125243_MetadataUsageId;
extern const uint32_t MenuBase_OnGUI_m4282337643_MetadataUsageId;
extern const uint32_t MenuBase__ctor_m2160361082_MetadataUsageId;
extern const uint32_t Pay_CallFBPay_m2256746739_MetadataUsageId;
extern const uint32_t Pay_GetGui_m2631553089_MetadataUsageId;
extern const uint32_t Pay__ctor_m988135194_MetadataUsageId;
extern const uint32_t U3CTakeScreenshotU3Ec__Iterator0_MoveNext_m4252001862_MetadataUsageId;
extern const uint32_t U3CTakeScreenshotU3Ec__Iterator0_Reset_m262479894_MetadataUsageId;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct GUIStyleState_t1397964415_marshaled_com;
struct GUIStyleState_t1397964415_marshaled_pinvoke;
struct RectOffset_t1369453676_marshaled_com;

struct ByteU5BU5D_t4116647657;
struct CharU5BU5D_t3528271667;
struct StringU5BU5D_t1281789340;
struct GUILayoutOptionU5BU5D_t2510215842;


#ifndef U3CMODULEU3E_T692745551_H
#define U3CMODULEU3E_T692745551_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745551 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745551_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CTAKESCREENSHOTU3EC__ITERATOR0_T3544038915_H
#define U3CTAKESCREENSHOTU3EC__ITERATOR0_T3544038915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0
struct  U3CTakeScreenshotU3Ec__Iterator0_t3544038915  : public RuntimeObject
{
public:
	// System.Int32 Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::<width>__0
	int32_t ___U3CwidthU3E__0_0;
	// System.Int32 Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::<height>__0
	int32_t ___U3CheightU3E__0_1;
	// UnityEngine.Texture2D Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::<tex>__0
	Texture2D_t3840446185 * ___U3CtexU3E__0_2;
	// System.Byte[] Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::<screenshot>__0
	ByteU5BU5D_t4116647657* ___U3CscreenshotU3E__0_3;
	// UnityEngine.WWWForm Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::<wwwForm>__0
	WWWForm_t4064702195 * ___U3CwwwFormU3E__0_4;
	// Facebook.Unity.Example.GraphRequest Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::$this
	GraphRequest_t4047451309 * ___U24this_5;
	// System.Object Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CwidthU3E__0_0() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U3CwidthU3E__0_0)); }
	inline int32_t get_U3CwidthU3E__0_0() const { return ___U3CwidthU3E__0_0; }
	inline int32_t* get_address_of_U3CwidthU3E__0_0() { return &___U3CwidthU3E__0_0; }
	inline void set_U3CwidthU3E__0_0(int32_t value)
	{
		___U3CwidthU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3E__0_1() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U3CheightU3E__0_1)); }
	inline int32_t get_U3CheightU3E__0_1() const { return ___U3CheightU3E__0_1; }
	inline int32_t* get_address_of_U3CheightU3E__0_1() { return &___U3CheightU3E__0_1; }
	inline void set_U3CheightU3E__0_1(int32_t value)
	{
		___U3CheightU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtexU3E__0_2() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U3CtexU3E__0_2)); }
	inline Texture2D_t3840446185 * get_U3CtexU3E__0_2() const { return ___U3CtexU3E__0_2; }
	inline Texture2D_t3840446185 ** get_address_of_U3CtexU3E__0_2() { return &___U3CtexU3E__0_2; }
	inline void set_U3CtexU3E__0_2(Texture2D_t3840446185 * value)
	{
		___U3CtexU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtexU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CscreenshotU3E__0_3() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U3CscreenshotU3E__0_3)); }
	inline ByteU5BU5D_t4116647657* get_U3CscreenshotU3E__0_3() const { return ___U3CscreenshotU3E__0_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CscreenshotU3E__0_3() { return &___U3CscreenshotU3E__0_3; }
	inline void set_U3CscreenshotU3E__0_3(ByteU5BU5D_t4116647657* value)
	{
		___U3CscreenshotU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscreenshotU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwFormU3E__0_4() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U3CwwwFormU3E__0_4)); }
	inline WWWForm_t4064702195 * get_U3CwwwFormU3E__0_4() const { return ___U3CwwwFormU3E__0_4; }
	inline WWWForm_t4064702195 ** get_address_of_U3CwwwFormU3E__0_4() { return &___U3CwwwFormU3E__0_4; }
	inline void set_U3CwwwFormU3E__0_4(WWWForm_t4064702195 * value)
	{
		___U3CwwwFormU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwFormU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U24this_5)); }
	inline GraphRequest_t4047451309 * get_U24this_5() const { return ___U24this_5; }
	inline GraphRequest_t4047451309 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(GraphRequest_t4047451309 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTAKESCREENSHOTU3EC__ITERATOR0_T3544038915_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef DICTIONARY_2_T2865362463_H
#define DICTIONARY_2_T2865362463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct  Dictionary_2_t2865362463  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t385246372* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t2447176574* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t3055037934 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t286439485 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___buckets_0)); }
	inline Int32U5BU5D_t385246372* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t385246372** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t385246372* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((&___buckets_0), value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___entries_1)); }
	inline EntryU5BU5D_t2447176574* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t2447176574** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t2447176574* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((&___entries_1), value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_6), value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___keys_7)); }
	inline KeyCollection_t3055037934 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t3055037934 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t3055037934 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((&___keys_7), value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ___values_8)); }
	inline ValueCollection_t286439485 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t286439485 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t286439485 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((&___values_8), value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t2865362463, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICTIONARY_2_T2865362463_H
#ifndef LIST_1_T257213610_H
#define LIST_1_T257213610_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.Object>
struct  List_1_t257213610  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_t2843939325* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____items_1)); }
	inline ObjectU5BU5D_t2843939325* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_t2843939325** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_t2843939325* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t257213610, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t257213610_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_t2843939325* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t257213610_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_t2843939325* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_t2843939325** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_t2843939325* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T257213610_H
#ifndef LIST_1_T3319525431_H
#define LIST_1_T3319525431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<System.String>
struct  List_1_t3319525431  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	StringU5BU5D_t1281789340* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____items_1)); }
	inline StringU5BU5D_t1281789340* get__items_1() const { return ____items_1; }
	inline StringU5BU5D_t1281789340** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(StringU5BU5D_t1281789340* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3319525431, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t3319525431_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	StringU5BU5D_t1281789340* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3319525431_StaticFields, ____emptyArray_5)); }
	inline StringU5BU5D_t1281789340* get__emptyArray_5() const { return ____emptyArray_5; }
	inline StringU5BU5D_t1281789340** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(StringU5BU5D_t1281789340* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T3319525431_H
#ifndef STACK_1_T2690840144_H
#define STACK_1_T2690840144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Stack`1<System.String>
struct  Stack_1_t2690840144  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	StringU5BU5D_t1281789340* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;
	// System.Object System.Collections.Generic.Stack`1::_syncRoot
	RuntimeObject * ____syncRoot_3;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_t2690840144, ____array_0)); }
	inline StringU5BU5D_t1281789340* get__array_0() const { return ____array_0; }
	inline StringU5BU5D_t1281789340** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(StringU5BU5D_t1281789340* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((&____array_0), value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_t2690840144, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_t2690840144, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(Stack_1_t2690840144, ____syncRoot_3)); }
	inline RuntimeObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline RuntimeObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(RuntimeObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STACK_1_T2690840144_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4013366056* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t2481557153 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t2481557153 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t2481557153 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t1169129676* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t1169129676** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t1169129676* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4013366056* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4013366056** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4013366056* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t2481557153 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t1169129676* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef GUICONTENT_T3050628031_H
#define GUICONTENT_T3050628031_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIContent
struct  GUIContent_t3050628031  : public RuntimeObject
{
public:
	// System.String UnityEngine.GUIContent::m_Text
	String_t* ___m_Text_0;
	// UnityEngine.Texture UnityEngine.GUIContent::m_Image
	Texture_t3661962703 * ___m_Image_1;
	// System.String UnityEngine.GUIContent::m_Tooltip
	String_t* ___m_Tooltip_2;

public:
	inline static int32_t get_offset_of_m_Text_0() { return static_cast<int32_t>(offsetof(GUIContent_t3050628031, ___m_Text_0)); }
	inline String_t* get_m_Text_0() const { return ___m_Text_0; }
	inline String_t** get_address_of_m_Text_0() { return &___m_Text_0; }
	inline void set_m_Text_0(String_t* value)
	{
		___m_Text_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_0), value);
	}

	inline static int32_t get_offset_of_m_Image_1() { return static_cast<int32_t>(offsetof(GUIContent_t3050628031, ___m_Image_1)); }
	inline Texture_t3661962703 * get_m_Image_1() const { return ___m_Image_1; }
	inline Texture_t3661962703 ** get_address_of_m_Image_1() { return &___m_Image_1; }
	inline void set_m_Image_1(Texture_t3661962703 * value)
	{
		___m_Image_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Image_1), value);
	}

	inline static int32_t get_offset_of_m_Tooltip_2() { return static_cast<int32_t>(offsetof(GUIContent_t3050628031, ___m_Tooltip_2)); }
	inline String_t* get_m_Tooltip_2() const { return ___m_Tooltip_2; }
	inline String_t** get_address_of_m_Tooltip_2() { return &___m_Tooltip_2; }
	inline void set_m_Tooltip_2(String_t* value)
	{
		___m_Tooltip_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Tooltip_2), value);
	}
};

struct GUIContent_t3050628031_StaticFields
{
public:
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_Text
	GUIContent_t3050628031 * ___s_Text_3;
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_Image
	GUIContent_t3050628031 * ___s_Image_4;
	// UnityEngine.GUIContent UnityEngine.GUIContent::s_TextImage
	GUIContent_t3050628031 * ___s_TextImage_5;
	// UnityEngine.GUIContent UnityEngine.GUIContent::none
	GUIContent_t3050628031 * ___none_6;

public:
	inline static int32_t get_offset_of_s_Text_3() { return static_cast<int32_t>(offsetof(GUIContent_t3050628031_StaticFields, ___s_Text_3)); }
	inline GUIContent_t3050628031 * get_s_Text_3() const { return ___s_Text_3; }
	inline GUIContent_t3050628031 ** get_address_of_s_Text_3() { return &___s_Text_3; }
	inline void set_s_Text_3(GUIContent_t3050628031 * value)
	{
		___s_Text_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_Text_3), value);
	}

	inline static int32_t get_offset_of_s_Image_4() { return static_cast<int32_t>(offsetof(GUIContent_t3050628031_StaticFields, ___s_Image_4)); }
	inline GUIContent_t3050628031 * get_s_Image_4() const { return ___s_Image_4; }
	inline GUIContent_t3050628031 ** get_address_of_s_Image_4() { return &___s_Image_4; }
	inline void set_s_Image_4(GUIContent_t3050628031 * value)
	{
		___s_Image_4 = value;
		Il2CppCodeGenWriteBarrier((&___s_Image_4), value);
	}

	inline static int32_t get_offset_of_s_TextImage_5() { return static_cast<int32_t>(offsetof(GUIContent_t3050628031_StaticFields, ___s_TextImage_5)); }
	inline GUIContent_t3050628031 * get_s_TextImage_5() const { return ___s_TextImage_5; }
	inline GUIContent_t3050628031 ** get_address_of_s_TextImage_5() { return &___s_TextImage_5; }
	inline void set_s_TextImage_5(GUIContent_t3050628031 * value)
	{
		___s_TextImage_5 = value;
		Il2CppCodeGenWriteBarrier((&___s_TextImage_5), value);
	}

	inline static int32_t get_offset_of_none_6() { return static_cast<int32_t>(offsetof(GUIContent_t3050628031_StaticFields, ___none_6)); }
	inline GUIContent_t3050628031 * get_none_6() const { return ___none_6; }
	inline GUIContent_t3050628031 ** get_address_of_none_6() { return &___none_6; }
	inline void set_none_6(GUIContent_t3050628031 * value)
	{
		___none_6 = value;
		Il2CppCodeGenWriteBarrier((&___none_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.GUIContent
struct GUIContent_t3050628031_marshaled_pinvoke
{
	char* ___m_Text_0;
	Texture_t3661962703 * ___m_Image_1;
	char* ___m_Tooltip_2;
};
// Native definition for COM marshalling of UnityEngine.GUIContent
struct GUIContent_t3050628031_marshaled_com
{
	Il2CppChar* ___m_Text_0;
	Texture_t3661962703 * ___m_Image_1;
	Il2CppChar* ___m_Tooltip_2;
};
#endif // GUICONTENT_T3050628031_H
#ifndef WWWFORM_T4064702195_H
#define WWWFORM_T4064702195_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WWWForm
struct  WWWForm_t4064702195  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Byte[]> UnityEngine.WWWForm::formData
	List_1_t1293755103 * ___formData_0;
	// System.Collections.Generic.List`1<System.String> UnityEngine.WWWForm::fieldNames
	List_1_t3319525431 * ___fieldNames_1;
	// System.Collections.Generic.List`1<System.String> UnityEngine.WWWForm::fileNames
	List_1_t3319525431 * ___fileNames_2;
	// System.Collections.Generic.List`1<System.String> UnityEngine.WWWForm::types
	List_1_t3319525431 * ___types_3;
	// System.Byte[] UnityEngine.WWWForm::boundary
	ByteU5BU5D_t4116647657* ___boundary_4;
	// System.Boolean UnityEngine.WWWForm::containsFiles
	bool ___containsFiles_5;

public:
	inline static int32_t get_offset_of_formData_0() { return static_cast<int32_t>(offsetof(WWWForm_t4064702195, ___formData_0)); }
	inline List_1_t1293755103 * get_formData_0() const { return ___formData_0; }
	inline List_1_t1293755103 ** get_address_of_formData_0() { return &___formData_0; }
	inline void set_formData_0(List_1_t1293755103 * value)
	{
		___formData_0 = value;
		Il2CppCodeGenWriteBarrier((&___formData_0), value);
	}

	inline static int32_t get_offset_of_fieldNames_1() { return static_cast<int32_t>(offsetof(WWWForm_t4064702195, ___fieldNames_1)); }
	inline List_1_t3319525431 * get_fieldNames_1() const { return ___fieldNames_1; }
	inline List_1_t3319525431 ** get_address_of_fieldNames_1() { return &___fieldNames_1; }
	inline void set_fieldNames_1(List_1_t3319525431 * value)
	{
		___fieldNames_1 = value;
		Il2CppCodeGenWriteBarrier((&___fieldNames_1), value);
	}

	inline static int32_t get_offset_of_fileNames_2() { return static_cast<int32_t>(offsetof(WWWForm_t4064702195, ___fileNames_2)); }
	inline List_1_t3319525431 * get_fileNames_2() const { return ___fileNames_2; }
	inline List_1_t3319525431 ** get_address_of_fileNames_2() { return &___fileNames_2; }
	inline void set_fileNames_2(List_1_t3319525431 * value)
	{
		___fileNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___fileNames_2), value);
	}

	inline static int32_t get_offset_of_types_3() { return static_cast<int32_t>(offsetof(WWWForm_t4064702195, ___types_3)); }
	inline List_1_t3319525431 * get_types_3() const { return ___types_3; }
	inline List_1_t3319525431 ** get_address_of_types_3() { return &___types_3; }
	inline void set_types_3(List_1_t3319525431 * value)
	{
		___types_3 = value;
		Il2CppCodeGenWriteBarrier((&___types_3), value);
	}

	inline static int32_t get_offset_of_boundary_4() { return static_cast<int32_t>(offsetof(WWWForm_t4064702195, ___boundary_4)); }
	inline ByteU5BU5D_t4116647657* get_boundary_4() const { return ___boundary_4; }
	inline ByteU5BU5D_t4116647657** get_address_of_boundary_4() { return &___boundary_4; }
	inline void set_boundary_4(ByteU5BU5D_t4116647657* value)
	{
		___boundary_4 = value;
		Il2CppCodeGenWriteBarrier((&___boundary_4), value);
	}

	inline static int32_t get_offset_of_containsFiles_5() { return static_cast<int32_t>(offsetof(WWWForm_t4064702195, ___containsFiles_5)); }
	inline bool get_containsFiles_5() const { return ___containsFiles_5; }
	inline bool* get_address_of_containsFiles_5() { return &___containsFiles_5; }
	inline void set_containsFiles_5(bool value)
	{
		___containsFiles_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WWWFORM_T4064702195_H
#ifndef YIELDINSTRUCTION_T403091072_H
#define YIELDINSTRUCTION_T403091072_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.YieldInstruction
struct  YieldInstruction_t403091072  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_t403091072_marshaled_com
{
};
#endif // YIELDINSTRUCTION_T403091072_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef BYTE_T1134296376_H
#define BYTE_T1134296376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Byte
struct  Byte_t1134296376 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t1134296376, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTE_T1134296376_H
#ifndef CHAR_T3634460470_H
#define CHAR_T3634460470_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Char
struct  Char_t3634460470 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_t3634460470, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_t3634460470_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_t4116647657* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_t3634460470_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_t4116647657* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_t4116647657* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((&___categoryForLatin1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHAR_T3634460470_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t385246372* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t385246372* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t385246372* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t385246372** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t385246372* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_31)); }
	inline DateTime_t3738529785  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t3738529785 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t3738529785  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_32)); }
	inline DateTime_t3738529785  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t3738529785  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t594665363_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t594665363_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T378540539_H
#define NULLABLE_1_T378540539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t378540539 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t378540539, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T378540539_H
#ifndef NULLABLE_1_T3119828856_H
#define NULLABLE_1_T3119828856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Single>
struct  Nullable_1_t3119828856 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3119828856, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3119828856, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3119828856_H
#ifndef SINGLE_T1397266774_H
#define SINGLE_T1397266774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Single
struct  Single_t1397266774 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_t1397266774, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLE_T1397266774_H
#ifndef SYSTEMEXCEPTION_T176217640_H
#define SYSTEMEXCEPTION_T176217640_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t176217640  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T176217640_H
#ifndef UINT32_T2560061978_H
#define UINT32_T2560061978_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UInt32
struct  UInt32_t2560061978 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t2560061978, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UINT32_T2560061978_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef WAITFORENDOFFRAME_T1314943911_H
#define WAITFORENDOFFRAME_T1314943911_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.WaitForEndOfFrame
struct  WaitForEndOfFrame_t1314943911  : public YieldInstruction_t403091072
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WAITFORENDOFFRAME_T1314943911_H
#ifndef HTTPMETHOD_T313594167_H
#define HTTPMETHOD_T313594167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.HttpMethod
struct  HttpMethod_t313594167 
{
public:
	// System.Int32 Facebook.Unity.HttpMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HttpMethod_t313594167, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPMETHOD_T313594167_H
#ifndef OGACTIONTYPE_T397354381_H
#define OGACTIONTYPE_T397354381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.OGActionType
struct  OGActionType_t397354381 
{
public:
	// System.Int32 Facebook.Unity.OGActionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OGActionType_t397354381, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OGACTIONTYPE_T397354381_H
#ifndef SHAREDIALOGMODE_T1679970535_H
#define SHAREDIALOGMODE_T1679970535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ShareDialogMode
struct  ShareDialogMode_t1679970535 
{
public:
	// System.Int32 Facebook.Unity.ShareDialogMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShareDialogMode_t1679970535, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREDIALOGMODE_T1679970535_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef NOTSUPPORTEDEXCEPTION_T1314879016_H
#define NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.NotSupportedException
struct  NotSupportedException_t1314879016  : public SystemException_t176217640
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NOTSUPPORTEDEXCEPTION_T1314879016_H
#ifndef NULLABLE_1_T1166124571_H
#define NULLABLE_1_T1166124571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.DateTime>
struct  Nullable_1_t1166124571 
{
public:
	// T System.Nullable`1::value
	DateTime_t3738529785  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1166124571, ___value_0)); }
	inline DateTime_t3738529785  get_value_0() const { return ___value_0; }
	inline DateTime_t3738529785 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(DateTime_t3738529785  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1166124571, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1166124571_H
#ifndef BINDINGFLAGS_T2721792723_H
#define BINDINGFLAGS_T2721792723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_t2721792723 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_t2721792723, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_T2721792723_H
#ifndef RUNTIMETYPEHANDLE_T3027515415_H
#define RUNTIMETYPEHANDLE_T3027515415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t3027515415 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t3027515415, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T3027515415_H
#ifndef FLAGS_T2372798318_H
#define FLAGS_T2372798318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri/Flags
struct  Flags_t2372798318 
{
public:
	// System.UInt64 System.Uri/Flags::value__
	uint64_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Flags_t2372798318, ___value___2)); }
	inline uint64_t get_value___2() const { return ___value___2; }
	inline uint64_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint64_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLAGS_T2372798318_H
#ifndef URIFORMAT_T2031163398_H
#define URIFORMAT_T2031163398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriFormat
struct  UriFormat_t2031163398 
{
public:
	// System.Int32 System.UriFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriFormat_t2031163398, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIFORMAT_T2031163398_H
#ifndef URIIDNSCOPE_T1847433844_H
#define URIIDNSCOPE_T1847433844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriIdnScope
struct  UriIdnScope_t1847433844 
{
public:
	// System.Int32 System.UriIdnScope::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriIdnScope_t1847433844, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIIDNSCOPE_T1847433844_H
#ifndef URIKIND_T3816567336_H
#define URIKIND_T3816567336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.UriKind
struct  UriKind_t3816567336 
{
public:
	// System.Int32 System.UriKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UriKind_t3816567336, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URIKIND_T3816567336_H
#ifndef COROUTINE_T3829159415_H
#define COROUTINE_T3829159415_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Coroutine
struct  Coroutine_t3829159415  : public YieldInstruction_t403091072
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t3829159415, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_pinvoke : public YieldInstruction_t403091072_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t3829159415_marshaled_com : public YieldInstruction_t403091072_marshaled_com
{
	intptr_t ___m_Ptr_0;
};
#endif // COROUTINE_T3829159415_H
#ifndef TYPE_T3858932131_H
#define TYPE_T3858932131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUILayoutOption/Type
struct  Type_t3858932131 
{
public:
	// System.Int32 UnityEngine.GUILayoutOption/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_t3858932131, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T3858932131_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef RECTOFFSET_T1369453676_H
#define RECTOFFSET_T1369453676_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RectOffset
struct  RectOffset_t1369453676  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.RectOffset::m_Ptr
	intptr_t ___m_Ptr_0;
	// System.Object UnityEngine.RectOffset::m_SourceStyle
	RuntimeObject * ___m_SourceStyle_1;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(RectOffset_t1369453676, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_SourceStyle_1() { return static_cast<int32_t>(offsetof(RectOffset_t1369453676, ___m_SourceStyle_1)); }
	inline RuntimeObject * get_m_SourceStyle_1() const { return ___m_SourceStyle_1; }
	inline RuntimeObject ** get_address_of_m_SourceStyle_1() { return &___m_SourceStyle_1; }
	inline void set_m_SourceStyle_1(RuntimeObject * value)
	{
		___m_SourceStyle_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_SourceStyle_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.RectOffset
struct RectOffset_t1369453676_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
// Native definition for COM marshalling of UnityEngine.RectOffset
struct RectOffset_t1369453676_marshaled_com
{
	intptr_t ___m_Ptr_0;
	Il2CppIUnknown* ___m_SourceStyle_1;
};
#endif // RECTOFFSET_T1369453676_H
#ifndef SCREENORIENTATION_T1705519499_H
#define SCREENORIENTATION_T1705519499_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScreenOrientation
struct  ScreenOrientation_t1705519499 
{
public:
	// System.Int32 UnityEngine.ScreenOrientation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScreenOrientation_t1705519499, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENORIENTATION_T1705519499_H
#ifndef TEXTANCHOR_T2035777396_H
#define TEXTANCHOR_T2035777396_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextAnchor
struct  TextAnchor_t2035777396 
{
public:
	// System.Int32 UnityEngine.TextAnchor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextAnchor_t2035777396, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTANCHOR_T2035777396_H
#ifndef TEXTUREFORMAT_T2701165832_H
#define TEXTUREFORMAT_T2701165832_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureFormat
struct  TextureFormat_t2701165832 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_t2701165832, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREFORMAT_T2701165832_H
#ifndef TOUCHPHASE_T72348083_H
#define TOUCHPHASE_T72348083_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchPhase
struct  TouchPhase_t72348083 
{
public:
	// System.Int32 UnityEngine.TouchPhase::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchPhase_t72348083, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHPHASE_T72348083_H
#ifndef TOUCHTYPE_T2034578258_H
#define TOUCHTYPE_T2034578258_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TouchType
struct  TouchType_t2034578258 
{
public:
	// System.Int32 UnityEngine.TouchType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TouchType_t2034578258, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCHTYPE_T2034578258_H
#ifndef ACCESSTOKEN_T2431487013_H
#define ACCESSTOKEN_T2431487013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AccessToken
struct  AccessToken_t2431487013  : public RuntimeObject
{
public:
	// System.String Facebook.Unity.AccessToken::<TokenString>k__BackingField
	String_t* ___U3CTokenStringU3Ek__BackingField_1;
	// System.DateTime Facebook.Unity.AccessToken::<ExpirationTime>k__BackingField
	DateTime_t3738529785  ___U3CExpirationTimeU3Ek__BackingField_2;
	// System.Collections.Generic.IEnumerable`1<System.String> Facebook.Unity.AccessToken::<Permissions>k__BackingField
	RuntimeObject* ___U3CPermissionsU3Ek__BackingField_3;
	// System.String Facebook.Unity.AccessToken::<UserId>k__BackingField
	String_t* ___U3CUserIdU3Ek__BackingField_4;
	// System.Nullable`1<System.DateTime> Facebook.Unity.AccessToken::<LastRefresh>k__BackingField
	Nullable_1_t1166124571  ___U3CLastRefreshU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CTokenStringU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AccessToken_t2431487013, ___U3CTokenStringU3Ek__BackingField_1)); }
	inline String_t* get_U3CTokenStringU3Ek__BackingField_1() const { return ___U3CTokenStringU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CTokenStringU3Ek__BackingField_1() { return &___U3CTokenStringU3Ek__BackingField_1; }
	inline void set_U3CTokenStringU3Ek__BackingField_1(String_t* value)
	{
		___U3CTokenStringU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTokenStringU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CExpirationTimeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AccessToken_t2431487013, ___U3CExpirationTimeU3Ek__BackingField_2)); }
	inline DateTime_t3738529785  get_U3CExpirationTimeU3Ek__BackingField_2() const { return ___U3CExpirationTimeU3Ek__BackingField_2; }
	inline DateTime_t3738529785 * get_address_of_U3CExpirationTimeU3Ek__BackingField_2() { return &___U3CExpirationTimeU3Ek__BackingField_2; }
	inline void set_U3CExpirationTimeU3Ek__BackingField_2(DateTime_t3738529785  value)
	{
		___U3CExpirationTimeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CPermissionsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AccessToken_t2431487013, ___U3CPermissionsU3Ek__BackingField_3)); }
	inline RuntimeObject* get_U3CPermissionsU3Ek__BackingField_3() const { return ___U3CPermissionsU3Ek__BackingField_3; }
	inline RuntimeObject** get_address_of_U3CPermissionsU3Ek__BackingField_3() { return &___U3CPermissionsU3Ek__BackingField_3; }
	inline void set_U3CPermissionsU3Ek__BackingField_3(RuntimeObject* value)
	{
		___U3CPermissionsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPermissionsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CUserIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AccessToken_t2431487013, ___U3CUserIdU3Ek__BackingField_4)); }
	inline String_t* get_U3CUserIdU3Ek__BackingField_4() const { return ___U3CUserIdU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CUserIdU3Ek__BackingField_4() { return &___U3CUserIdU3Ek__BackingField_4; }
	inline void set_U3CUserIdU3Ek__BackingField_4(String_t* value)
	{
		___U3CUserIdU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserIdU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CLastRefreshU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AccessToken_t2431487013, ___U3CLastRefreshU3Ek__BackingField_5)); }
	inline Nullable_1_t1166124571  get_U3CLastRefreshU3Ek__BackingField_5() const { return ___U3CLastRefreshU3Ek__BackingField_5; }
	inline Nullable_1_t1166124571 * get_address_of_U3CLastRefreshU3Ek__BackingField_5() { return &___U3CLastRefreshU3Ek__BackingField_5; }
	inline void set_U3CLastRefreshU3Ek__BackingField_5(Nullable_1_t1166124571  value)
	{
		___U3CLastRefreshU3Ek__BackingField_5 = value;
	}
};

struct AccessToken_t2431487013_StaticFields
{
public:
	// Facebook.Unity.AccessToken Facebook.Unity.AccessToken::<CurrentAccessToken>k__BackingField
	AccessToken_t2431487013 * ___U3CCurrentAccessTokenU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CCurrentAccessTokenU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AccessToken_t2431487013_StaticFields, ___U3CCurrentAccessTokenU3Ek__BackingField_0)); }
	inline AccessToken_t2431487013 * get_U3CCurrentAccessTokenU3Ek__BackingField_0() const { return ___U3CCurrentAccessTokenU3Ek__BackingField_0; }
	inline AccessToken_t2431487013 ** get_address_of_U3CCurrentAccessTokenU3Ek__BackingField_0() { return &___U3CCurrentAccessTokenU3Ek__BackingField_0; }
	inline void set_U3CCurrentAccessTokenU3Ek__BackingField_0(AccessToken_t2431487013 * value)
	{
		___U3CCurrentAccessTokenU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentAccessTokenU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESSTOKEN_T2431487013_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef NULLABLE_1_T2119916463_H
#define NULLABLE_1_T2119916463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Facebook.Unity.OGActionType>
struct  Nullable_1_t2119916463 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t2119916463, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t2119916463, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T2119916463_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t3027515415  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t3027515415  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t3027515415 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t3027515415  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t426314064 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t426314064 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t426314064 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t3940880105* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2999457153 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t426314064 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t426314064 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t426314064 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_0), value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t426314064 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t426314064 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t426314064 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_1), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t426314064 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t426314064 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t426314064 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_2), value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_3), value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t3940880105* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t3940880105** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t3940880105* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_5), value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2999457153 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2999457153 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2999457153 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBinder_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef URI_T100236324_H
#define URI_T100236324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Uri
struct  Uri_t100236324  : public RuntimeObject
{
public:
	// System.String System.Uri::m_String
	String_t* ___m_String_16;
	// System.String System.Uri::m_originalUnicodeString
	String_t* ___m_originalUnicodeString_17;
	// System.UriParser System.Uri::m_Syntax
	UriParser_t3890150400 * ___m_Syntax_18;
	// System.String System.Uri::m_DnsSafeHost
	String_t* ___m_DnsSafeHost_19;
	// System.Uri/Flags System.Uri::m_Flags
	uint64_t ___m_Flags_20;
	// System.Uri/UriInfo System.Uri::m_Info
	UriInfo_t1092684687 * ___m_Info_21;
	// System.Boolean System.Uri::m_iriParsing
	bool ___m_iriParsing_22;

public:
	inline static int32_t get_offset_of_m_String_16() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_String_16)); }
	inline String_t* get_m_String_16() const { return ___m_String_16; }
	inline String_t** get_address_of_m_String_16() { return &___m_String_16; }
	inline void set_m_String_16(String_t* value)
	{
		___m_String_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_String_16), value);
	}

	inline static int32_t get_offset_of_m_originalUnicodeString_17() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_originalUnicodeString_17)); }
	inline String_t* get_m_originalUnicodeString_17() const { return ___m_originalUnicodeString_17; }
	inline String_t** get_address_of_m_originalUnicodeString_17() { return &___m_originalUnicodeString_17; }
	inline void set_m_originalUnicodeString_17(String_t* value)
	{
		___m_originalUnicodeString_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_originalUnicodeString_17), value);
	}

	inline static int32_t get_offset_of_m_Syntax_18() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_Syntax_18)); }
	inline UriParser_t3890150400 * get_m_Syntax_18() const { return ___m_Syntax_18; }
	inline UriParser_t3890150400 ** get_address_of_m_Syntax_18() { return &___m_Syntax_18; }
	inline void set_m_Syntax_18(UriParser_t3890150400 * value)
	{
		___m_Syntax_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_Syntax_18), value);
	}

	inline static int32_t get_offset_of_m_DnsSafeHost_19() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_DnsSafeHost_19)); }
	inline String_t* get_m_DnsSafeHost_19() const { return ___m_DnsSafeHost_19; }
	inline String_t** get_address_of_m_DnsSafeHost_19() { return &___m_DnsSafeHost_19; }
	inline void set_m_DnsSafeHost_19(String_t* value)
	{
		___m_DnsSafeHost_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_DnsSafeHost_19), value);
	}

	inline static int32_t get_offset_of_m_Flags_20() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_Flags_20)); }
	inline uint64_t get_m_Flags_20() const { return ___m_Flags_20; }
	inline uint64_t* get_address_of_m_Flags_20() { return &___m_Flags_20; }
	inline void set_m_Flags_20(uint64_t value)
	{
		___m_Flags_20 = value;
	}

	inline static int32_t get_offset_of_m_Info_21() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_Info_21)); }
	inline UriInfo_t1092684687 * get_m_Info_21() const { return ___m_Info_21; }
	inline UriInfo_t1092684687 ** get_address_of_m_Info_21() { return &___m_Info_21; }
	inline void set_m_Info_21(UriInfo_t1092684687 * value)
	{
		___m_Info_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_Info_21), value);
	}

	inline static int32_t get_offset_of_m_iriParsing_22() { return static_cast<int32_t>(offsetof(Uri_t100236324, ___m_iriParsing_22)); }
	inline bool get_m_iriParsing_22() const { return ___m_iriParsing_22; }
	inline bool* get_address_of_m_iriParsing_22() { return &___m_iriParsing_22; }
	inline void set_m_iriParsing_22(bool value)
	{
		___m_iriParsing_22 = value;
	}
};

struct Uri_t100236324_StaticFields
{
public:
	// System.String System.Uri::UriSchemeFile
	String_t* ___UriSchemeFile_0;
	// System.String System.Uri::UriSchemeFtp
	String_t* ___UriSchemeFtp_1;
	// System.String System.Uri::UriSchemeGopher
	String_t* ___UriSchemeGopher_2;
	// System.String System.Uri::UriSchemeHttp
	String_t* ___UriSchemeHttp_3;
	// System.String System.Uri::UriSchemeHttps
	String_t* ___UriSchemeHttps_4;
	// System.String System.Uri::UriSchemeWs
	String_t* ___UriSchemeWs_5;
	// System.String System.Uri::UriSchemeWss
	String_t* ___UriSchemeWss_6;
	// System.String System.Uri::UriSchemeMailto
	String_t* ___UriSchemeMailto_7;
	// System.String System.Uri::UriSchemeNews
	String_t* ___UriSchemeNews_8;
	// System.String System.Uri::UriSchemeNntp
	String_t* ___UriSchemeNntp_9;
	// System.String System.Uri::UriSchemeNetTcp
	String_t* ___UriSchemeNetTcp_10;
	// System.String System.Uri::UriSchemeNetPipe
	String_t* ___UriSchemeNetPipe_11;
	// System.String System.Uri::SchemeDelimiter
	String_t* ___SchemeDelimiter_12;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_ConfigInitialized
	bool ___s_ConfigInitialized_23;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_ConfigInitializing
	bool ___s_ConfigInitializing_24;
	// System.UriIdnScope modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_IdnScope
	int32_t ___s_IdnScope_25;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.Uri::s_IriParsing
	bool ___s_IriParsing_26;
	// System.Boolean System.Uri::useDotNetRelativeOrAbsolute
	bool ___useDotNetRelativeOrAbsolute_27;
	// System.Boolean System.Uri::IsWindowsFileSystem
	bool ___IsWindowsFileSystem_29;
	// System.Object System.Uri::s_initLock
	RuntimeObject * ___s_initLock_30;
	// System.Char[] System.Uri::HexLowerChars
	CharU5BU5D_t3528271667* ___HexLowerChars_34;
	// System.Char[] System.Uri::_WSchars
	CharU5BU5D_t3528271667* ____WSchars_35;

public:
	inline static int32_t get_offset_of_UriSchemeFile_0() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeFile_0)); }
	inline String_t* get_UriSchemeFile_0() const { return ___UriSchemeFile_0; }
	inline String_t** get_address_of_UriSchemeFile_0() { return &___UriSchemeFile_0; }
	inline void set_UriSchemeFile_0(String_t* value)
	{
		___UriSchemeFile_0 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFile_0), value);
	}

	inline static int32_t get_offset_of_UriSchemeFtp_1() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeFtp_1)); }
	inline String_t* get_UriSchemeFtp_1() const { return ___UriSchemeFtp_1; }
	inline String_t** get_address_of_UriSchemeFtp_1() { return &___UriSchemeFtp_1; }
	inline void set_UriSchemeFtp_1(String_t* value)
	{
		___UriSchemeFtp_1 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeFtp_1), value);
	}

	inline static int32_t get_offset_of_UriSchemeGopher_2() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeGopher_2)); }
	inline String_t* get_UriSchemeGopher_2() const { return ___UriSchemeGopher_2; }
	inline String_t** get_address_of_UriSchemeGopher_2() { return &___UriSchemeGopher_2; }
	inline void set_UriSchemeGopher_2(String_t* value)
	{
		___UriSchemeGopher_2 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeGopher_2), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttp_3() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeHttp_3)); }
	inline String_t* get_UriSchemeHttp_3() const { return ___UriSchemeHttp_3; }
	inline String_t** get_address_of_UriSchemeHttp_3() { return &___UriSchemeHttp_3; }
	inline void set_UriSchemeHttp_3(String_t* value)
	{
		___UriSchemeHttp_3 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttp_3), value);
	}

	inline static int32_t get_offset_of_UriSchemeHttps_4() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeHttps_4)); }
	inline String_t* get_UriSchemeHttps_4() const { return ___UriSchemeHttps_4; }
	inline String_t** get_address_of_UriSchemeHttps_4() { return &___UriSchemeHttps_4; }
	inline void set_UriSchemeHttps_4(String_t* value)
	{
		___UriSchemeHttps_4 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeHttps_4), value);
	}

	inline static int32_t get_offset_of_UriSchemeWs_5() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeWs_5)); }
	inline String_t* get_UriSchemeWs_5() const { return ___UriSchemeWs_5; }
	inline String_t** get_address_of_UriSchemeWs_5() { return &___UriSchemeWs_5; }
	inline void set_UriSchemeWs_5(String_t* value)
	{
		___UriSchemeWs_5 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeWs_5), value);
	}

	inline static int32_t get_offset_of_UriSchemeWss_6() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeWss_6)); }
	inline String_t* get_UriSchemeWss_6() const { return ___UriSchemeWss_6; }
	inline String_t** get_address_of_UriSchemeWss_6() { return &___UriSchemeWss_6; }
	inline void set_UriSchemeWss_6(String_t* value)
	{
		___UriSchemeWss_6 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeWss_6), value);
	}

	inline static int32_t get_offset_of_UriSchemeMailto_7() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeMailto_7)); }
	inline String_t* get_UriSchemeMailto_7() const { return ___UriSchemeMailto_7; }
	inline String_t** get_address_of_UriSchemeMailto_7() { return &___UriSchemeMailto_7; }
	inline void set_UriSchemeMailto_7(String_t* value)
	{
		___UriSchemeMailto_7 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeMailto_7), value);
	}

	inline static int32_t get_offset_of_UriSchemeNews_8() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNews_8)); }
	inline String_t* get_UriSchemeNews_8() const { return ___UriSchemeNews_8; }
	inline String_t** get_address_of_UriSchemeNews_8() { return &___UriSchemeNews_8; }
	inline void set_UriSchemeNews_8(String_t* value)
	{
		___UriSchemeNews_8 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNews_8), value);
	}

	inline static int32_t get_offset_of_UriSchemeNntp_9() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNntp_9)); }
	inline String_t* get_UriSchemeNntp_9() const { return ___UriSchemeNntp_9; }
	inline String_t** get_address_of_UriSchemeNntp_9() { return &___UriSchemeNntp_9; }
	inline void set_UriSchemeNntp_9(String_t* value)
	{
		___UriSchemeNntp_9 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNntp_9), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetTcp_10() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNetTcp_10)); }
	inline String_t* get_UriSchemeNetTcp_10() const { return ___UriSchemeNetTcp_10; }
	inline String_t** get_address_of_UriSchemeNetTcp_10() { return &___UriSchemeNetTcp_10; }
	inline void set_UriSchemeNetTcp_10(String_t* value)
	{
		___UriSchemeNetTcp_10 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetTcp_10), value);
	}

	inline static int32_t get_offset_of_UriSchemeNetPipe_11() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___UriSchemeNetPipe_11)); }
	inline String_t* get_UriSchemeNetPipe_11() const { return ___UriSchemeNetPipe_11; }
	inline String_t** get_address_of_UriSchemeNetPipe_11() { return &___UriSchemeNetPipe_11; }
	inline void set_UriSchemeNetPipe_11(String_t* value)
	{
		___UriSchemeNetPipe_11 = value;
		Il2CppCodeGenWriteBarrier((&___UriSchemeNetPipe_11), value);
	}

	inline static int32_t get_offset_of_SchemeDelimiter_12() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___SchemeDelimiter_12)); }
	inline String_t* get_SchemeDelimiter_12() const { return ___SchemeDelimiter_12; }
	inline String_t** get_address_of_SchemeDelimiter_12() { return &___SchemeDelimiter_12; }
	inline void set_SchemeDelimiter_12(String_t* value)
	{
		___SchemeDelimiter_12 = value;
		Il2CppCodeGenWriteBarrier((&___SchemeDelimiter_12), value);
	}

	inline static int32_t get_offset_of_s_ConfigInitialized_23() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___s_ConfigInitialized_23)); }
	inline bool get_s_ConfigInitialized_23() const { return ___s_ConfigInitialized_23; }
	inline bool* get_address_of_s_ConfigInitialized_23() { return &___s_ConfigInitialized_23; }
	inline void set_s_ConfigInitialized_23(bool value)
	{
		___s_ConfigInitialized_23 = value;
	}

	inline static int32_t get_offset_of_s_ConfigInitializing_24() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___s_ConfigInitializing_24)); }
	inline bool get_s_ConfigInitializing_24() const { return ___s_ConfigInitializing_24; }
	inline bool* get_address_of_s_ConfigInitializing_24() { return &___s_ConfigInitializing_24; }
	inline void set_s_ConfigInitializing_24(bool value)
	{
		___s_ConfigInitializing_24 = value;
	}

	inline static int32_t get_offset_of_s_IdnScope_25() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___s_IdnScope_25)); }
	inline int32_t get_s_IdnScope_25() const { return ___s_IdnScope_25; }
	inline int32_t* get_address_of_s_IdnScope_25() { return &___s_IdnScope_25; }
	inline void set_s_IdnScope_25(int32_t value)
	{
		___s_IdnScope_25 = value;
	}

	inline static int32_t get_offset_of_s_IriParsing_26() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___s_IriParsing_26)); }
	inline bool get_s_IriParsing_26() const { return ___s_IriParsing_26; }
	inline bool* get_address_of_s_IriParsing_26() { return &___s_IriParsing_26; }
	inline void set_s_IriParsing_26(bool value)
	{
		___s_IriParsing_26 = value;
	}

	inline static int32_t get_offset_of_useDotNetRelativeOrAbsolute_27() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___useDotNetRelativeOrAbsolute_27)); }
	inline bool get_useDotNetRelativeOrAbsolute_27() const { return ___useDotNetRelativeOrAbsolute_27; }
	inline bool* get_address_of_useDotNetRelativeOrAbsolute_27() { return &___useDotNetRelativeOrAbsolute_27; }
	inline void set_useDotNetRelativeOrAbsolute_27(bool value)
	{
		___useDotNetRelativeOrAbsolute_27 = value;
	}

	inline static int32_t get_offset_of_IsWindowsFileSystem_29() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___IsWindowsFileSystem_29)); }
	inline bool get_IsWindowsFileSystem_29() const { return ___IsWindowsFileSystem_29; }
	inline bool* get_address_of_IsWindowsFileSystem_29() { return &___IsWindowsFileSystem_29; }
	inline void set_IsWindowsFileSystem_29(bool value)
	{
		___IsWindowsFileSystem_29 = value;
	}

	inline static int32_t get_offset_of_s_initLock_30() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___s_initLock_30)); }
	inline RuntimeObject * get_s_initLock_30() const { return ___s_initLock_30; }
	inline RuntimeObject ** get_address_of_s_initLock_30() { return &___s_initLock_30; }
	inline void set_s_initLock_30(RuntimeObject * value)
	{
		___s_initLock_30 = value;
		Il2CppCodeGenWriteBarrier((&___s_initLock_30), value);
	}

	inline static int32_t get_offset_of_HexLowerChars_34() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ___HexLowerChars_34)); }
	inline CharU5BU5D_t3528271667* get_HexLowerChars_34() const { return ___HexLowerChars_34; }
	inline CharU5BU5D_t3528271667** get_address_of_HexLowerChars_34() { return &___HexLowerChars_34; }
	inline void set_HexLowerChars_34(CharU5BU5D_t3528271667* value)
	{
		___HexLowerChars_34 = value;
		Il2CppCodeGenWriteBarrier((&___HexLowerChars_34), value);
	}

	inline static int32_t get_offset_of__WSchars_35() { return static_cast<int32_t>(offsetof(Uri_t100236324_StaticFields, ____WSchars_35)); }
	inline CharU5BU5D_t3528271667* get__WSchars_35() const { return ____WSchars_35; }
	inline CharU5BU5D_t3528271667** get_address_of__WSchars_35() { return &____WSchars_35; }
	inline void set__WSchars_35(CharU5BU5D_t3528271667* value)
	{
		____WSchars_35 = value;
		Il2CppCodeGenWriteBarrier((&____WSchars_35), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URI_T100236324_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef GUILAYOUTOPTION_T811797299_H
#define GUILAYOUTOPTION_T811797299_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUILayoutOption
struct  GUILayoutOption_t811797299  : public RuntimeObject
{
public:
	// UnityEngine.GUILayoutOption/Type UnityEngine.GUILayoutOption::type
	int32_t ___type_0;
	// System.Object UnityEngine.GUILayoutOption::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(GUILayoutOption_t811797299, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(GUILayoutOption_t811797299, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUILAYOUTOPTION_T811797299_H
#ifndef GUISTYLE_T3956901511_H
#define GUISTYLE_T3956901511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUIStyle
struct  GUIStyle_t3956901511  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.GUIStyle::m_Ptr
	intptr_t ___m_Ptr_0;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Normal
	GUIStyleState_t1397964415 * ___m_Normal_1;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Hover
	GUIStyleState_t1397964415 * ___m_Hover_2;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Active
	GUIStyleState_t1397964415 * ___m_Active_3;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_Focused
	GUIStyleState_t1397964415 * ___m_Focused_4;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnNormal
	GUIStyleState_t1397964415 * ___m_OnNormal_5;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnHover
	GUIStyleState_t1397964415 * ___m_OnHover_6;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnActive
	GUIStyleState_t1397964415 * ___m_OnActive_7;
	// UnityEngine.GUIStyleState UnityEngine.GUIStyle::m_OnFocused
	GUIStyleState_t1397964415 * ___m_OnFocused_8;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Border
	RectOffset_t1369453676 * ___m_Border_9;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Padding
	RectOffset_t1369453676 * ___m_Padding_10;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Margin
	RectOffset_t1369453676 * ___m_Margin_11;
	// UnityEngine.RectOffset UnityEngine.GUIStyle::m_Overflow
	RectOffset_t1369453676 * ___m_Overflow_12;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Normal_1)); }
	inline GUIStyleState_t1397964415 * get_m_Normal_1() const { return ___m_Normal_1; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(GUIStyleState_t1397964415 * value)
	{
		___m_Normal_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_Normal_1), value);
	}

	inline static int32_t get_offset_of_m_Hover_2() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Hover_2)); }
	inline GUIStyleState_t1397964415 * get_m_Hover_2() const { return ___m_Hover_2; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Hover_2() { return &___m_Hover_2; }
	inline void set_m_Hover_2(GUIStyleState_t1397964415 * value)
	{
		___m_Hover_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Hover_2), value);
	}

	inline static int32_t get_offset_of_m_Active_3() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Active_3)); }
	inline GUIStyleState_t1397964415 * get_m_Active_3() const { return ___m_Active_3; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Active_3() { return &___m_Active_3; }
	inline void set_m_Active_3(GUIStyleState_t1397964415 * value)
	{
		___m_Active_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Active_3), value);
	}

	inline static int32_t get_offset_of_m_Focused_4() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Focused_4)); }
	inline GUIStyleState_t1397964415 * get_m_Focused_4() const { return ___m_Focused_4; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_Focused_4() { return &___m_Focused_4; }
	inline void set_m_Focused_4(GUIStyleState_t1397964415 * value)
	{
		___m_Focused_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Focused_4), value);
	}

	inline static int32_t get_offset_of_m_OnNormal_5() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnNormal_5)); }
	inline GUIStyleState_t1397964415 * get_m_OnNormal_5() const { return ___m_OnNormal_5; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnNormal_5() { return &___m_OnNormal_5; }
	inline void set_m_OnNormal_5(GUIStyleState_t1397964415 * value)
	{
		___m_OnNormal_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnNormal_5), value);
	}

	inline static int32_t get_offset_of_m_OnHover_6() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnHover_6)); }
	inline GUIStyleState_t1397964415 * get_m_OnHover_6() const { return ___m_OnHover_6; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnHover_6() { return &___m_OnHover_6; }
	inline void set_m_OnHover_6(GUIStyleState_t1397964415 * value)
	{
		___m_OnHover_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnHover_6), value);
	}

	inline static int32_t get_offset_of_m_OnActive_7() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnActive_7)); }
	inline GUIStyleState_t1397964415 * get_m_OnActive_7() const { return ___m_OnActive_7; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnActive_7() { return &___m_OnActive_7; }
	inline void set_m_OnActive_7(GUIStyleState_t1397964415 * value)
	{
		___m_OnActive_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnActive_7), value);
	}

	inline static int32_t get_offset_of_m_OnFocused_8() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_OnFocused_8)); }
	inline GUIStyleState_t1397964415 * get_m_OnFocused_8() const { return ___m_OnFocused_8; }
	inline GUIStyleState_t1397964415 ** get_address_of_m_OnFocused_8() { return &___m_OnFocused_8; }
	inline void set_m_OnFocused_8(GUIStyleState_t1397964415 * value)
	{
		___m_OnFocused_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_OnFocused_8), value);
	}

	inline static int32_t get_offset_of_m_Border_9() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Border_9)); }
	inline RectOffset_t1369453676 * get_m_Border_9() const { return ___m_Border_9; }
	inline RectOffset_t1369453676 ** get_address_of_m_Border_9() { return &___m_Border_9; }
	inline void set_m_Border_9(RectOffset_t1369453676 * value)
	{
		___m_Border_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Border_9), value);
	}

	inline static int32_t get_offset_of_m_Padding_10() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Padding_10)); }
	inline RectOffset_t1369453676 * get_m_Padding_10() const { return ___m_Padding_10; }
	inline RectOffset_t1369453676 ** get_address_of_m_Padding_10() { return &___m_Padding_10; }
	inline void set_m_Padding_10(RectOffset_t1369453676 * value)
	{
		___m_Padding_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_Padding_10), value);
	}

	inline static int32_t get_offset_of_m_Margin_11() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Margin_11)); }
	inline RectOffset_t1369453676 * get_m_Margin_11() const { return ___m_Margin_11; }
	inline RectOffset_t1369453676 ** get_address_of_m_Margin_11() { return &___m_Margin_11; }
	inline void set_m_Margin_11(RectOffset_t1369453676 * value)
	{
		___m_Margin_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Margin_11), value);
	}

	inline static int32_t get_offset_of_m_Overflow_12() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511, ___m_Overflow_12)); }
	inline RectOffset_t1369453676 * get_m_Overflow_12() const { return ___m_Overflow_12; }
	inline RectOffset_t1369453676 ** get_address_of_m_Overflow_12() { return &___m_Overflow_12; }
	inline void set_m_Overflow_12(RectOffset_t1369453676 * value)
	{
		___m_Overflow_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Overflow_12), value);
	}
};

struct GUIStyle_t3956901511_StaticFields
{
public:
	// System.Boolean UnityEngine.GUIStyle::showKeyboardFocus
	bool ___showKeyboardFocus_13;
	// UnityEngine.GUIStyle UnityEngine.GUIStyle::s_None
	GUIStyle_t3956901511 * ___s_None_14;

public:
	inline static int32_t get_offset_of_showKeyboardFocus_13() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511_StaticFields, ___showKeyboardFocus_13)); }
	inline bool get_showKeyboardFocus_13() const { return ___showKeyboardFocus_13; }
	inline bool* get_address_of_showKeyboardFocus_13() { return &___showKeyboardFocus_13; }
	inline void set_showKeyboardFocus_13(bool value)
	{
		___showKeyboardFocus_13 = value;
	}

	inline static int32_t get_offset_of_s_None_14() { return static_cast<int32_t>(offsetof(GUIStyle_t3956901511_StaticFields, ___s_None_14)); }
	inline GUIStyle_t3956901511 * get_s_None_14() const { return ___s_None_14; }
	inline GUIStyle_t3956901511 ** get_address_of_s_None_14() { return &___s_None_14; }
	inline void set_s_None_14(GUIStyle_t3956901511 * value)
	{
		___s_None_14 = value;
		Il2CppCodeGenWriteBarrier((&___s_None_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.GUIStyle
struct GUIStyle_t3956901511_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Normal_1;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Hover_2;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Active_3;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_Focused_4;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnNormal_5;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnHover_6;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnActive_7;
	GUIStyleState_t1397964415_marshaled_pinvoke* ___m_OnFocused_8;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Border_9;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Padding_10;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Margin_11;
	RectOffset_t1369453676_marshaled_pinvoke ___m_Overflow_12;
};
// Native definition for COM marshalling of UnityEngine.GUIStyle
struct GUIStyle_t3956901511_marshaled_com
{
	intptr_t ___m_Ptr_0;
	GUIStyleState_t1397964415_marshaled_com* ___m_Normal_1;
	GUIStyleState_t1397964415_marshaled_com* ___m_Hover_2;
	GUIStyleState_t1397964415_marshaled_com* ___m_Active_3;
	GUIStyleState_t1397964415_marshaled_com* ___m_Focused_4;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnNormal_5;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnHover_6;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnActive_7;
	GUIStyleState_t1397964415_marshaled_com* ___m_OnFocused_8;
	RectOffset_t1369453676_marshaled_com* ___m_Border_9;
	RectOffset_t1369453676_marshaled_com* ___m_Padding_10;
	RectOffset_t1369453676_marshaled_com* ___m_Margin_11;
	RectOffset_t1369453676_marshaled_com* ___m_Overflow_12;
};
#endif // GUISTYLE_T3956901511_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef TEXTURE_T3661962703_H
#define TEXTURE_T3661962703_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture
struct  Texture_t3661962703  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE_T3661962703_H
#ifndef TOUCH_T1921856868_H
#define TOUCH_T1921856868_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Touch
struct  Touch_t1921856868 
{
public:
	// System.Int32 UnityEngine.Touch::m_FingerId
	int32_t ___m_FingerId_0;
	// UnityEngine.Vector2 UnityEngine.Touch::m_Position
	Vector2_t2156229523  ___m_Position_1;
	// UnityEngine.Vector2 UnityEngine.Touch::m_RawPosition
	Vector2_t2156229523  ___m_RawPosition_2;
	// UnityEngine.Vector2 UnityEngine.Touch::m_PositionDelta
	Vector2_t2156229523  ___m_PositionDelta_3;
	// System.Single UnityEngine.Touch::m_TimeDelta
	float ___m_TimeDelta_4;
	// System.Int32 UnityEngine.Touch::m_TapCount
	int32_t ___m_TapCount_5;
	// UnityEngine.TouchPhase UnityEngine.Touch::m_Phase
	int32_t ___m_Phase_6;
	// UnityEngine.TouchType UnityEngine.Touch::m_Type
	int32_t ___m_Type_7;
	// System.Single UnityEngine.Touch::m_Pressure
	float ___m_Pressure_8;
	// System.Single UnityEngine.Touch::m_maximumPossiblePressure
	float ___m_maximumPossiblePressure_9;
	// System.Single UnityEngine.Touch::m_Radius
	float ___m_Radius_10;
	// System.Single UnityEngine.Touch::m_RadiusVariance
	float ___m_RadiusVariance_11;
	// System.Single UnityEngine.Touch::m_AltitudeAngle
	float ___m_AltitudeAngle_12;
	// System.Single UnityEngine.Touch::m_AzimuthAngle
	float ___m_AzimuthAngle_13;

public:
	inline static int32_t get_offset_of_m_FingerId_0() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_FingerId_0)); }
	inline int32_t get_m_FingerId_0() const { return ___m_FingerId_0; }
	inline int32_t* get_address_of_m_FingerId_0() { return &___m_FingerId_0; }
	inline void set_m_FingerId_0(int32_t value)
	{
		___m_FingerId_0 = value;
	}

	inline static int32_t get_offset_of_m_Position_1() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Position_1)); }
	inline Vector2_t2156229523  get_m_Position_1() const { return ___m_Position_1; }
	inline Vector2_t2156229523 * get_address_of_m_Position_1() { return &___m_Position_1; }
	inline void set_m_Position_1(Vector2_t2156229523  value)
	{
		___m_Position_1 = value;
	}

	inline static int32_t get_offset_of_m_RawPosition_2() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RawPosition_2)); }
	inline Vector2_t2156229523  get_m_RawPosition_2() const { return ___m_RawPosition_2; }
	inline Vector2_t2156229523 * get_address_of_m_RawPosition_2() { return &___m_RawPosition_2; }
	inline void set_m_RawPosition_2(Vector2_t2156229523  value)
	{
		___m_RawPosition_2 = value;
	}

	inline static int32_t get_offset_of_m_PositionDelta_3() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_PositionDelta_3)); }
	inline Vector2_t2156229523  get_m_PositionDelta_3() const { return ___m_PositionDelta_3; }
	inline Vector2_t2156229523 * get_address_of_m_PositionDelta_3() { return &___m_PositionDelta_3; }
	inline void set_m_PositionDelta_3(Vector2_t2156229523  value)
	{
		___m_PositionDelta_3 = value;
	}

	inline static int32_t get_offset_of_m_TimeDelta_4() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TimeDelta_4)); }
	inline float get_m_TimeDelta_4() const { return ___m_TimeDelta_4; }
	inline float* get_address_of_m_TimeDelta_4() { return &___m_TimeDelta_4; }
	inline void set_m_TimeDelta_4(float value)
	{
		___m_TimeDelta_4 = value;
	}

	inline static int32_t get_offset_of_m_TapCount_5() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_TapCount_5)); }
	inline int32_t get_m_TapCount_5() const { return ___m_TapCount_5; }
	inline int32_t* get_address_of_m_TapCount_5() { return &___m_TapCount_5; }
	inline void set_m_TapCount_5(int32_t value)
	{
		___m_TapCount_5 = value;
	}

	inline static int32_t get_offset_of_m_Phase_6() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Phase_6)); }
	inline int32_t get_m_Phase_6() const { return ___m_Phase_6; }
	inline int32_t* get_address_of_m_Phase_6() { return &___m_Phase_6; }
	inline void set_m_Phase_6(int32_t value)
	{
		___m_Phase_6 = value;
	}

	inline static int32_t get_offset_of_m_Type_7() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Type_7)); }
	inline int32_t get_m_Type_7() const { return ___m_Type_7; }
	inline int32_t* get_address_of_m_Type_7() { return &___m_Type_7; }
	inline void set_m_Type_7(int32_t value)
	{
		___m_Type_7 = value;
	}

	inline static int32_t get_offset_of_m_Pressure_8() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Pressure_8)); }
	inline float get_m_Pressure_8() const { return ___m_Pressure_8; }
	inline float* get_address_of_m_Pressure_8() { return &___m_Pressure_8; }
	inline void set_m_Pressure_8(float value)
	{
		___m_Pressure_8 = value;
	}

	inline static int32_t get_offset_of_m_maximumPossiblePressure_9() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_maximumPossiblePressure_9)); }
	inline float get_m_maximumPossiblePressure_9() const { return ___m_maximumPossiblePressure_9; }
	inline float* get_address_of_m_maximumPossiblePressure_9() { return &___m_maximumPossiblePressure_9; }
	inline void set_m_maximumPossiblePressure_9(float value)
	{
		___m_maximumPossiblePressure_9 = value;
	}

	inline static int32_t get_offset_of_m_Radius_10() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_Radius_10)); }
	inline float get_m_Radius_10() const { return ___m_Radius_10; }
	inline float* get_address_of_m_Radius_10() { return &___m_Radius_10; }
	inline void set_m_Radius_10(float value)
	{
		___m_Radius_10 = value;
	}

	inline static int32_t get_offset_of_m_RadiusVariance_11() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_RadiusVariance_11)); }
	inline float get_m_RadiusVariance_11() const { return ___m_RadiusVariance_11; }
	inline float* get_address_of_m_RadiusVariance_11() { return &___m_RadiusVariance_11; }
	inline void set_m_RadiusVariance_11(float value)
	{
		___m_RadiusVariance_11 = value;
	}

	inline static int32_t get_offset_of_m_AltitudeAngle_12() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AltitudeAngle_12)); }
	inline float get_m_AltitudeAngle_12() const { return ___m_AltitudeAngle_12; }
	inline float* get_address_of_m_AltitudeAngle_12() { return &___m_AltitudeAngle_12; }
	inline void set_m_AltitudeAngle_12(float value)
	{
		___m_AltitudeAngle_12 = value;
	}

	inline static int32_t get_offset_of_m_AzimuthAngle_13() { return static_cast<int32_t>(offsetof(Touch_t1921856868, ___m_AzimuthAngle_13)); }
	inline float get_m_AzimuthAngle_13() const { return ___m_AzimuthAngle_13; }
	inline float* get_address_of_m_AzimuthAngle_13() { return &___m_AzimuthAngle_13; }
	inline void set_m_AzimuthAngle_13(float value)
	{
		___m_AzimuthAngle_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOUCH_T1921856868_H
#ifndef FACEBOOKDELEGATE_1_T123177164_H
#define FACEBOOKDELEGATE_1_T123177164_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAccessTokenRefreshResult>
struct  FacebookDelegate_1_t123177164  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKDELEGATE_1_T123177164_H
#ifndef FACEBOOKDELEGATE_1_T3420330156_H
#define FACEBOOKDELEGATE_1_T3420330156_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>
struct  FacebookDelegate_1_t3420330156  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKDELEGATE_1_T3420330156_H
#ifndef FACEBOOKDELEGATE_1_T1976622014_H
#define FACEBOOKDELEGATE_1_T1976622014_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>
struct  FacebookDelegate_1_t1976622014  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKDELEGATE_1_T1976622014_H
#ifndef FACEBOOKDELEGATE_1_T138390958_H
#define FACEBOOKDELEGATE_1_T138390958_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>
struct  FacebookDelegate_1_t138390958  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKDELEGATE_1_T138390958_H
#ifndef FACEBOOKDELEGATE_1_T1669328765_H
#define FACEBOOKDELEGATE_1_T1669328765_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>
struct  FacebookDelegate_1_t1669328765  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKDELEGATE_1_T1669328765_H
#ifndef FACEBOOKDELEGATE_1_T2649775846_H
#define FACEBOOKDELEGATE_1_T2649775846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>
struct  FacebookDelegate_1_t2649775846  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKDELEGATE_1_T2649775846_H
#ifndef FACEBOOKDELEGATE_1_T602110559_H
#define FACEBOOKDELEGATE_1_T602110559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>
struct  FacebookDelegate_1_t602110559  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKDELEGATE_1_T602110559_H
#ifndef HIDEUNITYDELEGATE_T1353799728_H
#define HIDEUNITYDELEGATE_T1353799728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.HideUnityDelegate
struct  HideUnityDelegate_t1353799728  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEUNITYDELEGATE_T1353799728_H
#ifndef INITDELEGATE_T3081360126_H
#define INITDELEGATE_T3081360126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.InitDelegate
struct  InitDelegate_t3081360126  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITDELEGATE_T3081360126_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef GUISKIN_T1244372282_H
#define GUISKIN_T1244372282_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.GUISkin
struct  GUISkin_t1244372282  : public ScriptableObject_t2528358522
{
public:
	// UnityEngine.Font UnityEngine.GUISkin::m_Font
	Font_t1956802104 * ___m_Font_4;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_box
	GUIStyle_t3956901511 * ___m_box_5;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_button
	GUIStyle_t3956901511 * ___m_button_6;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_toggle
	GUIStyle_t3956901511 * ___m_toggle_7;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_label
	GUIStyle_t3956901511 * ___m_label_8;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textField
	GUIStyle_t3956901511 * ___m_textField_9;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_textArea
	GUIStyle_t3956901511 * ___m_textArea_10;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_window
	GUIStyle_t3956901511 * ___m_window_11;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSlider
	GUIStyle_t3956901511 * ___m_horizontalSlider_12;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalSliderThumb
	GUIStyle_t3956901511 * ___m_horizontalSliderThumb_13;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSlider
	GUIStyle_t3956901511 * ___m_verticalSlider_14;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalSliderThumb
	GUIStyle_t3956901511 * ___m_verticalSliderThumb_15;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbar
	GUIStyle_t3956901511 * ___m_horizontalScrollbar_16;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarThumb
	GUIStyle_t3956901511 * ___m_horizontalScrollbarThumb_17;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarLeftButton
	GUIStyle_t3956901511 * ___m_horizontalScrollbarLeftButton_18;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_horizontalScrollbarRightButton
	GUIStyle_t3956901511 * ___m_horizontalScrollbarRightButton_19;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbar
	GUIStyle_t3956901511 * ___m_verticalScrollbar_20;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarThumb
	GUIStyle_t3956901511 * ___m_verticalScrollbarThumb_21;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarUpButton
	GUIStyle_t3956901511 * ___m_verticalScrollbarUpButton_22;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_verticalScrollbarDownButton
	GUIStyle_t3956901511 * ___m_verticalScrollbarDownButton_23;
	// UnityEngine.GUIStyle UnityEngine.GUISkin::m_ScrollView
	GUIStyle_t3956901511 * ___m_ScrollView_24;
	// UnityEngine.GUIStyle[] UnityEngine.GUISkin::m_CustomStyles
	GUIStyleU5BU5D_t2383250302* ___m_CustomStyles_25;
	// UnityEngine.GUISettings UnityEngine.GUISkin::m_Settings
	GUISettings_t1774757634 * ___m_Settings_26;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GUIStyle> UnityEngine.GUISkin::m_Styles
	Dictionary_2_t3742157810 * ___m_Styles_28;

public:
	inline static int32_t get_offset_of_m_Font_4() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_Font_4)); }
	inline Font_t1956802104 * get_m_Font_4() const { return ___m_Font_4; }
	inline Font_t1956802104 ** get_address_of_m_Font_4() { return &___m_Font_4; }
	inline void set_m_Font_4(Font_t1956802104 * value)
	{
		___m_Font_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Font_4), value);
	}

	inline static int32_t get_offset_of_m_box_5() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_box_5)); }
	inline GUIStyle_t3956901511 * get_m_box_5() const { return ___m_box_5; }
	inline GUIStyle_t3956901511 ** get_address_of_m_box_5() { return &___m_box_5; }
	inline void set_m_box_5(GUIStyle_t3956901511 * value)
	{
		___m_box_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_box_5), value);
	}

	inline static int32_t get_offset_of_m_button_6() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_button_6)); }
	inline GUIStyle_t3956901511 * get_m_button_6() const { return ___m_button_6; }
	inline GUIStyle_t3956901511 ** get_address_of_m_button_6() { return &___m_button_6; }
	inline void set_m_button_6(GUIStyle_t3956901511 * value)
	{
		___m_button_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_button_6), value);
	}

	inline static int32_t get_offset_of_m_toggle_7() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_toggle_7)); }
	inline GUIStyle_t3956901511 * get_m_toggle_7() const { return ___m_toggle_7; }
	inline GUIStyle_t3956901511 ** get_address_of_m_toggle_7() { return &___m_toggle_7; }
	inline void set_m_toggle_7(GUIStyle_t3956901511 * value)
	{
		___m_toggle_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_toggle_7), value);
	}

	inline static int32_t get_offset_of_m_label_8() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_label_8)); }
	inline GUIStyle_t3956901511 * get_m_label_8() const { return ___m_label_8; }
	inline GUIStyle_t3956901511 ** get_address_of_m_label_8() { return &___m_label_8; }
	inline void set_m_label_8(GUIStyle_t3956901511 * value)
	{
		___m_label_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_label_8), value);
	}

	inline static int32_t get_offset_of_m_textField_9() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_textField_9)); }
	inline GUIStyle_t3956901511 * get_m_textField_9() const { return ___m_textField_9; }
	inline GUIStyle_t3956901511 ** get_address_of_m_textField_9() { return &___m_textField_9; }
	inline void set_m_textField_9(GUIStyle_t3956901511 * value)
	{
		___m_textField_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_textField_9), value);
	}

	inline static int32_t get_offset_of_m_textArea_10() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_textArea_10)); }
	inline GUIStyle_t3956901511 * get_m_textArea_10() const { return ___m_textArea_10; }
	inline GUIStyle_t3956901511 ** get_address_of_m_textArea_10() { return &___m_textArea_10; }
	inline void set_m_textArea_10(GUIStyle_t3956901511 * value)
	{
		___m_textArea_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_textArea_10), value);
	}

	inline static int32_t get_offset_of_m_window_11() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_window_11)); }
	inline GUIStyle_t3956901511 * get_m_window_11() const { return ___m_window_11; }
	inline GUIStyle_t3956901511 ** get_address_of_m_window_11() { return &___m_window_11; }
	inline void set_m_window_11(GUIStyle_t3956901511 * value)
	{
		___m_window_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_window_11), value);
	}

	inline static int32_t get_offset_of_m_horizontalSlider_12() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalSlider_12)); }
	inline GUIStyle_t3956901511 * get_m_horizontalSlider_12() const { return ___m_horizontalSlider_12; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalSlider_12() { return &___m_horizontalSlider_12; }
	inline void set_m_horizontalSlider_12(GUIStyle_t3956901511 * value)
	{
		___m_horizontalSlider_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalSlider_12), value);
	}

	inline static int32_t get_offset_of_m_horizontalSliderThumb_13() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalSliderThumb_13)); }
	inline GUIStyle_t3956901511 * get_m_horizontalSliderThumb_13() const { return ___m_horizontalSliderThumb_13; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalSliderThumb_13() { return &___m_horizontalSliderThumb_13; }
	inline void set_m_horizontalSliderThumb_13(GUIStyle_t3956901511 * value)
	{
		___m_horizontalSliderThumb_13 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalSliderThumb_13), value);
	}

	inline static int32_t get_offset_of_m_verticalSlider_14() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalSlider_14)); }
	inline GUIStyle_t3956901511 * get_m_verticalSlider_14() const { return ___m_verticalSlider_14; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalSlider_14() { return &___m_verticalSlider_14; }
	inline void set_m_verticalSlider_14(GUIStyle_t3956901511 * value)
	{
		___m_verticalSlider_14 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalSlider_14), value);
	}

	inline static int32_t get_offset_of_m_verticalSliderThumb_15() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalSliderThumb_15)); }
	inline GUIStyle_t3956901511 * get_m_verticalSliderThumb_15() const { return ___m_verticalSliderThumb_15; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalSliderThumb_15() { return &___m_verticalSliderThumb_15; }
	inline void set_m_verticalSliderThumb_15(GUIStyle_t3956901511 * value)
	{
		___m_verticalSliderThumb_15 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalSliderThumb_15), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbar_16() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbar_16)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbar_16() const { return ___m_horizontalScrollbar_16; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbar_16() { return &___m_horizontalScrollbar_16; }
	inline void set_m_horizontalScrollbar_16(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbar_16 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbar_16), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarThumb_17() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbarThumb_17)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbarThumb_17() const { return ___m_horizontalScrollbarThumb_17; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbarThumb_17() { return &___m_horizontalScrollbarThumb_17; }
	inline void set_m_horizontalScrollbarThumb_17(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbarThumb_17 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbarThumb_17), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarLeftButton_18() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbarLeftButton_18)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbarLeftButton_18() const { return ___m_horizontalScrollbarLeftButton_18; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbarLeftButton_18() { return &___m_horizontalScrollbarLeftButton_18; }
	inline void set_m_horizontalScrollbarLeftButton_18(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbarLeftButton_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbarLeftButton_18), value);
	}

	inline static int32_t get_offset_of_m_horizontalScrollbarRightButton_19() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_horizontalScrollbarRightButton_19)); }
	inline GUIStyle_t3956901511 * get_m_horizontalScrollbarRightButton_19() const { return ___m_horizontalScrollbarRightButton_19; }
	inline GUIStyle_t3956901511 ** get_address_of_m_horizontalScrollbarRightButton_19() { return &___m_horizontalScrollbarRightButton_19; }
	inline void set_m_horizontalScrollbarRightButton_19(GUIStyle_t3956901511 * value)
	{
		___m_horizontalScrollbarRightButton_19 = value;
		Il2CppCodeGenWriteBarrier((&___m_horizontalScrollbarRightButton_19), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbar_20() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbar_20)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbar_20() const { return ___m_verticalScrollbar_20; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbar_20() { return &___m_verticalScrollbar_20; }
	inline void set_m_verticalScrollbar_20(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbar_20 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbar_20), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarThumb_21() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbarThumb_21)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbarThumb_21() const { return ___m_verticalScrollbarThumb_21; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbarThumb_21() { return &___m_verticalScrollbarThumb_21; }
	inline void set_m_verticalScrollbarThumb_21(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbarThumb_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbarThumb_21), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarUpButton_22() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbarUpButton_22)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbarUpButton_22() const { return ___m_verticalScrollbarUpButton_22; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbarUpButton_22() { return &___m_verticalScrollbarUpButton_22; }
	inline void set_m_verticalScrollbarUpButton_22(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbarUpButton_22 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbarUpButton_22), value);
	}

	inline static int32_t get_offset_of_m_verticalScrollbarDownButton_23() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_verticalScrollbarDownButton_23)); }
	inline GUIStyle_t3956901511 * get_m_verticalScrollbarDownButton_23() const { return ___m_verticalScrollbarDownButton_23; }
	inline GUIStyle_t3956901511 ** get_address_of_m_verticalScrollbarDownButton_23() { return &___m_verticalScrollbarDownButton_23; }
	inline void set_m_verticalScrollbarDownButton_23(GUIStyle_t3956901511 * value)
	{
		___m_verticalScrollbarDownButton_23 = value;
		Il2CppCodeGenWriteBarrier((&___m_verticalScrollbarDownButton_23), value);
	}

	inline static int32_t get_offset_of_m_ScrollView_24() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_ScrollView_24)); }
	inline GUIStyle_t3956901511 * get_m_ScrollView_24() const { return ___m_ScrollView_24; }
	inline GUIStyle_t3956901511 ** get_address_of_m_ScrollView_24() { return &___m_ScrollView_24; }
	inline void set_m_ScrollView_24(GUIStyle_t3956901511 * value)
	{
		___m_ScrollView_24 = value;
		Il2CppCodeGenWriteBarrier((&___m_ScrollView_24), value);
	}

	inline static int32_t get_offset_of_m_CustomStyles_25() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_CustomStyles_25)); }
	inline GUIStyleU5BU5D_t2383250302* get_m_CustomStyles_25() const { return ___m_CustomStyles_25; }
	inline GUIStyleU5BU5D_t2383250302** get_address_of_m_CustomStyles_25() { return &___m_CustomStyles_25; }
	inline void set_m_CustomStyles_25(GUIStyleU5BU5D_t2383250302* value)
	{
		___m_CustomStyles_25 = value;
		Il2CppCodeGenWriteBarrier((&___m_CustomStyles_25), value);
	}

	inline static int32_t get_offset_of_m_Settings_26() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_Settings_26)); }
	inline GUISettings_t1774757634 * get_m_Settings_26() const { return ___m_Settings_26; }
	inline GUISettings_t1774757634 ** get_address_of_m_Settings_26() { return &___m_Settings_26; }
	inline void set_m_Settings_26(GUISettings_t1774757634 * value)
	{
		___m_Settings_26 = value;
		Il2CppCodeGenWriteBarrier((&___m_Settings_26), value);
	}

	inline static int32_t get_offset_of_m_Styles_28() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282, ___m_Styles_28)); }
	inline Dictionary_2_t3742157810 * get_m_Styles_28() const { return ___m_Styles_28; }
	inline Dictionary_2_t3742157810 ** get_address_of_m_Styles_28() { return &___m_Styles_28; }
	inline void set_m_Styles_28(Dictionary_2_t3742157810 * value)
	{
		___m_Styles_28 = value;
		Il2CppCodeGenWriteBarrier((&___m_Styles_28), value);
	}
};

struct GUISkin_t1244372282_StaticFields
{
public:
	// UnityEngine.GUIStyle UnityEngine.GUISkin::ms_Error
	GUIStyle_t3956901511 * ___ms_Error_27;
	// UnityEngine.GUISkin/SkinChangedDelegate UnityEngine.GUISkin::m_SkinChanged
	SkinChangedDelegate_t1143955295 * ___m_SkinChanged_29;
	// UnityEngine.GUISkin UnityEngine.GUISkin::current
	GUISkin_t1244372282 * ___current_30;

public:
	inline static int32_t get_offset_of_ms_Error_27() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282_StaticFields, ___ms_Error_27)); }
	inline GUIStyle_t3956901511 * get_ms_Error_27() const { return ___ms_Error_27; }
	inline GUIStyle_t3956901511 ** get_address_of_ms_Error_27() { return &___ms_Error_27; }
	inline void set_ms_Error_27(GUIStyle_t3956901511 * value)
	{
		___ms_Error_27 = value;
		Il2CppCodeGenWriteBarrier((&___ms_Error_27), value);
	}

	inline static int32_t get_offset_of_m_SkinChanged_29() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282_StaticFields, ___m_SkinChanged_29)); }
	inline SkinChangedDelegate_t1143955295 * get_m_SkinChanged_29() const { return ___m_SkinChanged_29; }
	inline SkinChangedDelegate_t1143955295 ** get_address_of_m_SkinChanged_29() { return &___m_SkinChanged_29; }
	inline void set_m_SkinChanged_29(SkinChangedDelegate_t1143955295 * value)
	{
		___m_SkinChanged_29 = value;
		Il2CppCodeGenWriteBarrier((&___m_SkinChanged_29), value);
	}

	inline static int32_t get_offset_of_current_30() { return static_cast<int32_t>(offsetof(GUISkin_t1244372282_StaticFields, ___current_30)); }
	inline GUISkin_t1244372282 * get_current_30() const { return ___current_30; }
	inline GUISkin_t1244372282 ** get_address_of_current_30() { return &___current_30; }
	inline void set_current_30(GUISkin_t1244372282 * value)
	{
		___current_30 = value;
		Il2CppCodeGenWriteBarrier((&___current_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUISKIN_T1244372282_H
#ifndef TEXTURE2D_T3840446185_H
#define TEXTURE2D_T3840446185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Texture2D
struct  Texture2D_t3840446185  : public Texture_t3661962703
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTURE2D_T3840446185_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef CONSOLEBASE_T1023975560_H
#define CONSOLEBASE_T1023975560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.ConsoleBase
struct  ConsoleBase_t1023975560  : public MonoBehaviour_t3962482529
{
public:
	// System.String Facebook.Unity.Example.ConsoleBase::status
	String_t* ___status_6;
	// System.String Facebook.Unity.Example.ConsoleBase::lastResponse
	String_t* ___lastResponse_7;
	// UnityEngine.Vector2 Facebook.Unity.Example.ConsoleBase::scrollPosition
	Vector2_t2156229523  ___scrollPosition_8;
	// System.Nullable`1<System.Single> Facebook.Unity.Example.ConsoleBase::scaleFactor
	Nullable_1_t3119828856  ___scaleFactor_9;
	// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::textStyle
	GUIStyle_t3956901511 * ___textStyle_10;
	// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::buttonStyle
	GUIStyle_t3956901511 * ___buttonStyle_11;
	// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::textInputStyle
	GUIStyle_t3956901511 * ___textInputStyle_12;
	// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::labelStyle
	GUIStyle_t3956901511 * ___labelStyle_13;
	// UnityEngine.Texture2D Facebook.Unity.Example.ConsoleBase::<LastResponseTexture>k__BackingField
	Texture2D_t3840446185 * ___U3CLastResponseTextureU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_status_6() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___status_6)); }
	inline String_t* get_status_6() const { return ___status_6; }
	inline String_t** get_address_of_status_6() { return &___status_6; }
	inline void set_status_6(String_t* value)
	{
		___status_6 = value;
		Il2CppCodeGenWriteBarrier((&___status_6), value);
	}

	inline static int32_t get_offset_of_lastResponse_7() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___lastResponse_7)); }
	inline String_t* get_lastResponse_7() const { return ___lastResponse_7; }
	inline String_t** get_address_of_lastResponse_7() { return &___lastResponse_7; }
	inline void set_lastResponse_7(String_t* value)
	{
		___lastResponse_7 = value;
		Il2CppCodeGenWriteBarrier((&___lastResponse_7), value);
	}

	inline static int32_t get_offset_of_scrollPosition_8() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___scrollPosition_8)); }
	inline Vector2_t2156229523  get_scrollPosition_8() const { return ___scrollPosition_8; }
	inline Vector2_t2156229523 * get_address_of_scrollPosition_8() { return &___scrollPosition_8; }
	inline void set_scrollPosition_8(Vector2_t2156229523  value)
	{
		___scrollPosition_8 = value;
	}

	inline static int32_t get_offset_of_scaleFactor_9() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___scaleFactor_9)); }
	inline Nullable_1_t3119828856  get_scaleFactor_9() const { return ___scaleFactor_9; }
	inline Nullable_1_t3119828856 * get_address_of_scaleFactor_9() { return &___scaleFactor_9; }
	inline void set_scaleFactor_9(Nullable_1_t3119828856  value)
	{
		___scaleFactor_9 = value;
	}

	inline static int32_t get_offset_of_textStyle_10() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___textStyle_10)); }
	inline GUIStyle_t3956901511 * get_textStyle_10() const { return ___textStyle_10; }
	inline GUIStyle_t3956901511 ** get_address_of_textStyle_10() { return &___textStyle_10; }
	inline void set_textStyle_10(GUIStyle_t3956901511 * value)
	{
		___textStyle_10 = value;
		Il2CppCodeGenWriteBarrier((&___textStyle_10), value);
	}

	inline static int32_t get_offset_of_buttonStyle_11() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___buttonStyle_11)); }
	inline GUIStyle_t3956901511 * get_buttonStyle_11() const { return ___buttonStyle_11; }
	inline GUIStyle_t3956901511 ** get_address_of_buttonStyle_11() { return &___buttonStyle_11; }
	inline void set_buttonStyle_11(GUIStyle_t3956901511 * value)
	{
		___buttonStyle_11 = value;
		Il2CppCodeGenWriteBarrier((&___buttonStyle_11), value);
	}

	inline static int32_t get_offset_of_textInputStyle_12() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___textInputStyle_12)); }
	inline GUIStyle_t3956901511 * get_textInputStyle_12() const { return ___textInputStyle_12; }
	inline GUIStyle_t3956901511 ** get_address_of_textInputStyle_12() { return &___textInputStyle_12; }
	inline void set_textInputStyle_12(GUIStyle_t3956901511 * value)
	{
		___textInputStyle_12 = value;
		Il2CppCodeGenWriteBarrier((&___textInputStyle_12), value);
	}

	inline static int32_t get_offset_of_labelStyle_13() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___labelStyle_13)); }
	inline GUIStyle_t3956901511 * get_labelStyle_13() const { return ___labelStyle_13; }
	inline GUIStyle_t3956901511 ** get_address_of_labelStyle_13() { return &___labelStyle_13; }
	inline void set_labelStyle_13(GUIStyle_t3956901511 * value)
	{
		___labelStyle_13 = value;
		Il2CppCodeGenWriteBarrier((&___labelStyle_13), value);
	}

	inline static int32_t get_offset_of_U3CLastResponseTextureU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___U3CLastResponseTextureU3Ek__BackingField_14)); }
	inline Texture2D_t3840446185 * get_U3CLastResponseTextureU3Ek__BackingField_14() const { return ___U3CLastResponseTextureU3Ek__BackingField_14; }
	inline Texture2D_t3840446185 ** get_address_of_U3CLastResponseTextureU3Ek__BackingField_14() { return &___U3CLastResponseTextureU3Ek__BackingField_14; }
	inline void set_U3CLastResponseTextureU3Ek__BackingField_14(Texture2D_t3840446185 * value)
	{
		___U3CLastResponseTextureU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastResponseTextureU3Ek__BackingField_14), value);
	}
};

struct ConsoleBase_t1023975560_StaticFields
{
public:
	// System.Collections.Generic.Stack`1<System.String> Facebook.Unity.Example.ConsoleBase::menuStack
	Stack_1_t2690840144 * ___menuStack_5;

public:
	inline static int32_t get_offset_of_menuStack_5() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560_StaticFields, ___menuStack_5)); }
	inline Stack_1_t2690840144 * get_menuStack_5() const { return ___menuStack_5; }
	inline Stack_1_t2690840144 ** get_address_of_menuStack_5() { return &___menuStack_5; }
	inline void set_menuStack_5(Stack_1_t2690840144 * value)
	{
		___menuStack_5 = value;
		Il2CppCodeGenWriteBarrier((&___menuStack_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSOLEBASE_T1023975560_H
#ifndef LOGVIEW_T1067263371_H
#define LOGVIEW_T1067263371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.LogView
struct  LogView_t1067263371  : public ConsoleBase_t1023975560
{
public:

public:
};

struct LogView_t1067263371_StaticFields
{
public:
	// System.String Facebook.Unity.Example.LogView::datePatt
	String_t* ___datePatt_15;
	// System.Collections.Generic.IList`1<System.String> Facebook.Unity.Example.LogView::events
	RuntimeObject* ___events_16;

public:
	inline static int32_t get_offset_of_datePatt_15() { return static_cast<int32_t>(offsetof(LogView_t1067263371_StaticFields, ___datePatt_15)); }
	inline String_t* get_datePatt_15() const { return ___datePatt_15; }
	inline String_t** get_address_of_datePatt_15() { return &___datePatt_15; }
	inline void set_datePatt_15(String_t* value)
	{
		___datePatt_15 = value;
		Il2CppCodeGenWriteBarrier((&___datePatt_15), value);
	}

	inline static int32_t get_offset_of_events_16() { return static_cast<int32_t>(offsetof(LogView_t1067263371_StaticFields, ___events_16)); }
	inline RuntimeObject* get_events_16() const { return ___events_16; }
	inline RuntimeObject** get_address_of_events_16() { return &___events_16; }
	inline void set_events_16(RuntimeObject* value)
	{
		___events_16 = value;
		Il2CppCodeGenWriteBarrier((&___events_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGVIEW_T1067263371_H
#ifndef MENUBASE_T1079888489_H
#define MENUBASE_T1079888489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.MenuBase
struct  MenuBase_t1079888489  : public ConsoleBase_t1023975560
{
public:

public:
};

struct MenuBase_t1079888489_StaticFields
{
public:
	// Facebook.Unity.ShareDialogMode Facebook.Unity.Example.MenuBase::shareDialogMode
	int32_t ___shareDialogMode_15;

public:
	inline static int32_t get_offset_of_shareDialogMode_15() { return static_cast<int32_t>(offsetof(MenuBase_t1079888489_StaticFields, ___shareDialogMode_15)); }
	inline int32_t get_shareDialogMode_15() const { return ___shareDialogMode_15; }
	inline int32_t* get_address_of_shareDialogMode_15() { return &___shareDialogMode_15; }
	inline void set_shareDialogMode_15(int32_t value)
	{
		___shareDialogMode_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUBASE_T1079888489_H
#ifndef ACCESSTOKENMENU_T4028641200_H
#define ACCESSTOKENMENU_T4028641200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AccessTokenMenu
struct  AccessTokenMenu_t4028641200  : public MenuBase_t1079888489
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESSTOKENMENU_T4028641200_H
#ifndef APPEVENTS_T3645639549_H
#define APPEVENTS_T3645639549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AppEvents
struct  AppEvents_t3645639549  : public MenuBase_t1079888489
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPEVENTS_T3645639549_H
#ifndef APPLINKS_T2028121612_H
#define APPLINKS_T2028121612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AppLinks
struct  AppLinks_t2028121612  : public MenuBase_t1079888489
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLINKS_T2028121612_H
#ifndef APPREQUESTS_T2419817778_H
#define APPREQUESTS_T2419817778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AppRequests
struct  AppRequests_t2419817778  : public MenuBase_t1079888489
{
public:
	// System.String Facebook.Unity.Example.AppRequests::requestMessage
	String_t* ___requestMessage_16;
	// System.String Facebook.Unity.Example.AppRequests::requestTo
	String_t* ___requestTo_17;
	// System.String Facebook.Unity.Example.AppRequests::requestFilter
	String_t* ___requestFilter_18;
	// System.String Facebook.Unity.Example.AppRequests::requestExcludes
	String_t* ___requestExcludes_19;
	// System.String Facebook.Unity.Example.AppRequests::requestMax
	String_t* ___requestMax_20;
	// System.String Facebook.Unity.Example.AppRequests::requestData
	String_t* ___requestData_21;
	// System.String Facebook.Unity.Example.AppRequests::requestTitle
	String_t* ___requestTitle_22;
	// System.String Facebook.Unity.Example.AppRequests::requestObjectID
	String_t* ___requestObjectID_23;
	// System.Int32 Facebook.Unity.Example.AppRequests::selectedAction
	int32_t ___selectedAction_24;
	// System.String[] Facebook.Unity.Example.AppRequests::actionTypeStrings
	StringU5BU5D_t1281789340* ___actionTypeStrings_25;

public:
	inline static int32_t get_offset_of_requestMessage_16() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestMessage_16)); }
	inline String_t* get_requestMessage_16() const { return ___requestMessage_16; }
	inline String_t** get_address_of_requestMessage_16() { return &___requestMessage_16; }
	inline void set_requestMessage_16(String_t* value)
	{
		___requestMessage_16 = value;
		Il2CppCodeGenWriteBarrier((&___requestMessage_16), value);
	}

	inline static int32_t get_offset_of_requestTo_17() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestTo_17)); }
	inline String_t* get_requestTo_17() const { return ___requestTo_17; }
	inline String_t** get_address_of_requestTo_17() { return &___requestTo_17; }
	inline void set_requestTo_17(String_t* value)
	{
		___requestTo_17 = value;
		Il2CppCodeGenWriteBarrier((&___requestTo_17), value);
	}

	inline static int32_t get_offset_of_requestFilter_18() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestFilter_18)); }
	inline String_t* get_requestFilter_18() const { return ___requestFilter_18; }
	inline String_t** get_address_of_requestFilter_18() { return &___requestFilter_18; }
	inline void set_requestFilter_18(String_t* value)
	{
		___requestFilter_18 = value;
		Il2CppCodeGenWriteBarrier((&___requestFilter_18), value);
	}

	inline static int32_t get_offset_of_requestExcludes_19() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestExcludes_19)); }
	inline String_t* get_requestExcludes_19() const { return ___requestExcludes_19; }
	inline String_t** get_address_of_requestExcludes_19() { return &___requestExcludes_19; }
	inline void set_requestExcludes_19(String_t* value)
	{
		___requestExcludes_19 = value;
		Il2CppCodeGenWriteBarrier((&___requestExcludes_19), value);
	}

	inline static int32_t get_offset_of_requestMax_20() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestMax_20)); }
	inline String_t* get_requestMax_20() const { return ___requestMax_20; }
	inline String_t** get_address_of_requestMax_20() { return &___requestMax_20; }
	inline void set_requestMax_20(String_t* value)
	{
		___requestMax_20 = value;
		Il2CppCodeGenWriteBarrier((&___requestMax_20), value);
	}

	inline static int32_t get_offset_of_requestData_21() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestData_21)); }
	inline String_t* get_requestData_21() const { return ___requestData_21; }
	inline String_t** get_address_of_requestData_21() { return &___requestData_21; }
	inline void set_requestData_21(String_t* value)
	{
		___requestData_21 = value;
		Il2CppCodeGenWriteBarrier((&___requestData_21), value);
	}

	inline static int32_t get_offset_of_requestTitle_22() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestTitle_22)); }
	inline String_t* get_requestTitle_22() const { return ___requestTitle_22; }
	inline String_t** get_address_of_requestTitle_22() { return &___requestTitle_22; }
	inline void set_requestTitle_22(String_t* value)
	{
		___requestTitle_22 = value;
		Il2CppCodeGenWriteBarrier((&___requestTitle_22), value);
	}

	inline static int32_t get_offset_of_requestObjectID_23() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestObjectID_23)); }
	inline String_t* get_requestObjectID_23() const { return ___requestObjectID_23; }
	inline String_t** get_address_of_requestObjectID_23() { return &___requestObjectID_23; }
	inline void set_requestObjectID_23(String_t* value)
	{
		___requestObjectID_23 = value;
		Il2CppCodeGenWriteBarrier((&___requestObjectID_23), value);
	}

	inline static int32_t get_offset_of_selectedAction_24() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___selectedAction_24)); }
	inline int32_t get_selectedAction_24() const { return ___selectedAction_24; }
	inline int32_t* get_address_of_selectedAction_24() { return &___selectedAction_24; }
	inline void set_selectedAction_24(int32_t value)
	{
		___selectedAction_24 = value;
	}

	inline static int32_t get_offset_of_actionTypeStrings_25() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___actionTypeStrings_25)); }
	inline StringU5BU5D_t1281789340* get_actionTypeStrings_25() const { return ___actionTypeStrings_25; }
	inline StringU5BU5D_t1281789340** get_address_of_actionTypeStrings_25() { return &___actionTypeStrings_25; }
	inline void set_actionTypeStrings_25(StringU5BU5D_t1281789340* value)
	{
		___actionTypeStrings_25 = value;
		Il2CppCodeGenWriteBarrier((&___actionTypeStrings_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPREQUESTS_T2419817778_H
#ifndef DIALOGSHARE_T2334043085_H
#define DIALOGSHARE_T2334043085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.DialogShare
struct  DialogShare_t2334043085  : public MenuBase_t1079888489
{
public:
	// System.String Facebook.Unity.Example.DialogShare::shareLink
	String_t* ___shareLink_16;
	// System.String Facebook.Unity.Example.DialogShare::shareTitle
	String_t* ___shareTitle_17;
	// System.String Facebook.Unity.Example.DialogShare::shareDescription
	String_t* ___shareDescription_18;
	// System.String Facebook.Unity.Example.DialogShare::shareImage
	String_t* ___shareImage_19;
	// System.String Facebook.Unity.Example.DialogShare::feedTo
	String_t* ___feedTo_20;
	// System.String Facebook.Unity.Example.DialogShare::feedLink
	String_t* ___feedLink_21;
	// System.String Facebook.Unity.Example.DialogShare::feedTitle
	String_t* ___feedTitle_22;
	// System.String Facebook.Unity.Example.DialogShare::feedCaption
	String_t* ___feedCaption_23;
	// System.String Facebook.Unity.Example.DialogShare::feedDescription
	String_t* ___feedDescription_24;
	// System.String Facebook.Unity.Example.DialogShare::feedImage
	String_t* ___feedImage_25;
	// System.String Facebook.Unity.Example.DialogShare::feedMediaSource
	String_t* ___feedMediaSource_26;

public:
	inline static int32_t get_offset_of_shareLink_16() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___shareLink_16)); }
	inline String_t* get_shareLink_16() const { return ___shareLink_16; }
	inline String_t** get_address_of_shareLink_16() { return &___shareLink_16; }
	inline void set_shareLink_16(String_t* value)
	{
		___shareLink_16 = value;
		Il2CppCodeGenWriteBarrier((&___shareLink_16), value);
	}

	inline static int32_t get_offset_of_shareTitle_17() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___shareTitle_17)); }
	inline String_t* get_shareTitle_17() const { return ___shareTitle_17; }
	inline String_t** get_address_of_shareTitle_17() { return &___shareTitle_17; }
	inline void set_shareTitle_17(String_t* value)
	{
		___shareTitle_17 = value;
		Il2CppCodeGenWriteBarrier((&___shareTitle_17), value);
	}

	inline static int32_t get_offset_of_shareDescription_18() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___shareDescription_18)); }
	inline String_t* get_shareDescription_18() const { return ___shareDescription_18; }
	inline String_t** get_address_of_shareDescription_18() { return &___shareDescription_18; }
	inline void set_shareDescription_18(String_t* value)
	{
		___shareDescription_18 = value;
		Il2CppCodeGenWriteBarrier((&___shareDescription_18), value);
	}

	inline static int32_t get_offset_of_shareImage_19() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___shareImage_19)); }
	inline String_t* get_shareImage_19() const { return ___shareImage_19; }
	inline String_t** get_address_of_shareImage_19() { return &___shareImage_19; }
	inline void set_shareImage_19(String_t* value)
	{
		___shareImage_19 = value;
		Il2CppCodeGenWriteBarrier((&___shareImage_19), value);
	}

	inline static int32_t get_offset_of_feedTo_20() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___feedTo_20)); }
	inline String_t* get_feedTo_20() const { return ___feedTo_20; }
	inline String_t** get_address_of_feedTo_20() { return &___feedTo_20; }
	inline void set_feedTo_20(String_t* value)
	{
		___feedTo_20 = value;
		Il2CppCodeGenWriteBarrier((&___feedTo_20), value);
	}

	inline static int32_t get_offset_of_feedLink_21() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___feedLink_21)); }
	inline String_t* get_feedLink_21() const { return ___feedLink_21; }
	inline String_t** get_address_of_feedLink_21() { return &___feedLink_21; }
	inline void set_feedLink_21(String_t* value)
	{
		___feedLink_21 = value;
		Il2CppCodeGenWriteBarrier((&___feedLink_21), value);
	}

	inline static int32_t get_offset_of_feedTitle_22() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___feedTitle_22)); }
	inline String_t* get_feedTitle_22() const { return ___feedTitle_22; }
	inline String_t** get_address_of_feedTitle_22() { return &___feedTitle_22; }
	inline void set_feedTitle_22(String_t* value)
	{
		___feedTitle_22 = value;
		Il2CppCodeGenWriteBarrier((&___feedTitle_22), value);
	}

	inline static int32_t get_offset_of_feedCaption_23() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___feedCaption_23)); }
	inline String_t* get_feedCaption_23() const { return ___feedCaption_23; }
	inline String_t** get_address_of_feedCaption_23() { return &___feedCaption_23; }
	inline void set_feedCaption_23(String_t* value)
	{
		___feedCaption_23 = value;
		Il2CppCodeGenWriteBarrier((&___feedCaption_23), value);
	}

	inline static int32_t get_offset_of_feedDescription_24() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___feedDescription_24)); }
	inline String_t* get_feedDescription_24() const { return ___feedDescription_24; }
	inline String_t** get_address_of_feedDescription_24() { return &___feedDescription_24; }
	inline void set_feedDescription_24(String_t* value)
	{
		___feedDescription_24 = value;
		Il2CppCodeGenWriteBarrier((&___feedDescription_24), value);
	}

	inline static int32_t get_offset_of_feedImage_25() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___feedImage_25)); }
	inline String_t* get_feedImage_25() const { return ___feedImage_25; }
	inline String_t** get_address_of_feedImage_25() { return &___feedImage_25; }
	inline void set_feedImage_25(String_t* value)
	{
		___feedImage_25 = value;
		Il2CppCodeGenWriteBarrier((&___feedImage_25), value);
	}

	inline static int32_t get_offset_of_feedMediaSource_26() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___feedMediaSource_26)); }
	inline String_t* get_feedMediaSource_26() const { return ___feedMediaSource_26; }
	inline String_t** get_address_of_feedMediaSource_26() { return &___feedMediaSource_26; }
	inline void set_feedMediaSource_26(String_t* value)
	{
		___feedMediaSource_26 = value;
		Il2CppCodeGenWriteBarrier((&___feedMediaSource_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGSHARE_T2334043085_H
#ifndef GRAPHREQUEST_T4047451309_H
#define GRAPHREQUEST_T4047451309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.GraphRequest
struct  GraphRequest_t4047451309  : public MenuBase_t1079888489
{
public:
	// System.String Facebook.Unity.Example.GraphRequest::apiQuery
	String_t* ___apiQuery_16;
	// UnityEngine.Texture2D Facebook.Unity.Example.GraphRequest::profilePic
	Texture2D_t3840446185 * ___profilePic_17;

public:
	inline static int32_t get_offset_of_apiQuery_16() { return static_cast<int32_t>(offsetof(GraphRequest_t4047451309, ___apiQuery_16)); }
	inline String_t* get_apiQuery_16() const { return ___apiQuery_16; }
	inline String_t** get_address_of_apiQuery_16() { return &___apiQuery_16; }
	inline void set_apiQuery_16(String_t* value)
	{
		___apiQuery_16 = value;
		Il2CppCodeGenWriteBarrier((&___apiQuery_16), value);
	}

	inline static int32_t get_offset_of_profilePic_17() { return static_cast<int32_t>(offsetof(GraphRequest_t4047451309, ___profilePic_17)); }
	inline Texture2D_t3840446185 * get_profilePic_17() const { return ___profilePic_17; }
	inline Texture2D_t3840446185 ** get_address_of_profilePic_17() { return &___profilePic_17; }
	inline void set_profilePic_17(Texture2D_t3840446185 * value)
	{
		___profilePic_17 = value;
		Il2CppCodeGenWriteBarrier((&___profilePic_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHREQUEST_T4047451309_H
#ifndef MAINMENU_T1823058806_H
#define MAINMENU_T1823058806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.MainMenu
struct  MainMenu_t1823058806  : public MenuBase_t1079888489
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINMENU_T1823058806_H
#ifndef PAY_T1264260185_H
#define PAY_T1264260185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.Pay
struct  Pay_t1264260185  : public MenuBase_t1079888489
{
public:
	// System.String Facebook.Unity.Example.Pay::payProduct
	String_t* ___payProduct_16;

public:
	inline static int32_t get_offset_of_payProduct_16() { return static_cast<int32_t>(offsetof(Pay_t1264260185, ___payProduct_16)); }
	inline String_t* get_payProduct_16() const { return ___payProduct_16; }
	inline String_t** get_address_of_payProduct_16() { return &___payProduct_16; }
	inline void set_payProduct_16(String_t* value)
	{
		___payProduct_16 = value;
		Il2CppCodeGenWriteBarrier((&___payProduct_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAY_T1264260185_H
// System.String[]
struct StringU5BU5D_t1281789340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t2510215842  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GUILayoutOption_t811797299 * m_Items[1];

public:
	inline GUILayoutOption_t811797299 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GUILayoutOption_t811797299 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GUILayoutOption_t811797299 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline GUILayoutOption_t811797299 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GUILayoutOption_t811797299 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GUILayoutOption_t811797299 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};
// System.Char[]
struct CharU5BU5D_t3528271667  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Il2CppChar m_Items[1];

public:
	inline Il2CppChar GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Il2CppChar value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline Il2CppChar GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Il2CppChar* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Il2CppChar value)
	{
		m_Items[index] = value;
	}
};
// System.Byte[]
struct ByteU5BU5D_t4116647657  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


// System.Void Facebook.Unity.FacebookDelegate`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void FacebookDelegate_1__ctor_m3403141292_gshared (FacebookDelegate_1_t903090771 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m518943619_gshared (Dictionary_2_t132545152 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::Add(!0,!1)
extern "C" IL2CPP_METHOD_ATTR void Dictionary_2_Add_m3105409630_gshared (Dictionary_2_t132545152 * __this, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void List_1__ctor_m2321703786_gshared (List_1_t257213610 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
extern "C" IL2CPP_METHOD_ATTR void List_1_Add_m3338814081_gshared (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Void System.Nullable`1<System.Int32>::.ctor(!0)
extern "C" IL2CPP_METHOD_ATTR void Nullable_1__ctor_m2962682148_gshared (Nullable_1_t378540539 * __this, int32_t p0, const RuntimeMethod* method);
// !!0[] System.Array::Empty<System.Object>()
extern "C" IL2CPP_METHOD_ATTR ObjectU5BU5D_t2843939325* Array_Empty_TisRuntimeObject_m4268358689_gshared (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<Facebook.Unity.OGActionType>::get_HasValue()
extern "C" IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_m604914582_gshared (Nullable_1_t2119916463 * __this, const RuntimeMethod* method);
// !0 System.Nullable`1<Facebook.Unity.OGActionType>::get_Value()
extern "C" IL2CPP_METHOD_ATTR int32_t Nullable_1_get_Value_m1111532194_gshared (Nullable_1_t2119916463 * __this, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::OfType<System.Object>(System.Collections.IEnumerable)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Enumerable_OfType_TisRuntimeObject_m444860550_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" IL2CPP_METHOD_ATTR List_1_t257213610 * Enumerable_ToList_TisRuntimeObject_m2366248129_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// System.Void System.Nullable`1<Facebook.Unity.OGActionType>::.ctor(!0)
extern "C" IL2CPP_METHOD_ATTR void Nullable_1__ctor_m2532889892_gshared (Nullable_1_t2119916463 * __this, int32_t p0, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<System.Single>::get_HasValue()
extern "C" IL2CPP_METHOD_ATTR bool Nullable_1_get_HasValue_m2149791491_gshared (Nullable_1_t3119828856 * __this, const RuntimeMethod* method);
// System.Void System.Nullable`1<System.Single>::.ctor(!0)
extern "C" IL2CPP_METHOD_ATTR void Nullable_1__ctor_m3964206222_gshared (Nullable_1_t3119828856 * __this, float p0, const RuntimeMethod* method);
// !0 System.Nullable`1<System.Single>::get_Value()
extern "C" IL2CPP_METHOD_ATTR float Nullable_1_get_Value_m4168550405_gshared (Nullable_1_t3119828856 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(!0)
extern "C" IL2CPP_METHOD_ATTR void Stack_1_Push_m1669856732_gshared (Stack_1_t3923495619 * __this, RuntimeObject * p0, const RuntimeMethod* method);
// System.Boolean System.Linq.Enumerable::Any<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" IL2CPP_METHOD_ATTR bool Enumerable_Any_TisRuntimeObject_m3173759778_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);
// !0 System.Collections.Generic.Stack`1<System.Object>::Pop()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * Stack_1_Pop_m756553478_gshared (Stack_1_t3923495619 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Stack_1__ctor_m3164958980_gshared (Stack_1_t3923495619 * __this, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
extern "C" IL2CPP_METHOD_ATTR ObjectU5BU5D_t2843939325* Enumerable_ToArray_TisRuntimeObject_m387236094_gshared (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method);

// System.Void Facebook.Unity.Example.MenuBase::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MenuBase__ctor_m2160361082 (MenuBase_t1079888489 * __this, const RuntimeMethod* method);
// System.Boolean Facebook.Unity.Example.ConsoleBase::Button(System.String)
extern "C" IL2CPP_METHOD_ATTR bool ConsoleBase_Button_m1005519490 (ConsoleBase_t1023975560 * __this, String_t* ___label0, const RuntimeMethod* method);
// System.Void Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAccessTokenRefreshResult>::.ctor(System.Object,System.IntPtr)
inline void FacebookDelegate_1__ctor_m1454705827 (FacebookDelegate_1_t123177164 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (FacebookDelegate_1_t123177164 *, RuntimeObject *, intptr_t, const RuntimeMethod*))FacebookDelegate_1__ctor_m3403141292_gshared)(__this, p0, p1, method);
}
// System.Void Facebook.Unity.FB/Mobile::RefreshCurrentAccessToken(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAccessTokenRefreshResult>)
extern "C" IL2CPP_METHOD_ATTR void Mobile_RefreshCurrentAccessToken_m1786803032 (RuntimeObject * __this /* static, unused */, FacebookDelegate_1_t123177164 * p0, const RuntimeMethod* method);
// System.Void Facebook.Unity.Example.ConsoleBase::set_Status(System.String)
extern "C" IL2CPP_METHOD_ATTR void ConsoleBase_set_Status_m4147399384 (ConsoleBase_t1023975560 * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::.ctor()
inline void Dictionary_2__ctor_m15304876 (Dictionary_2_t2865362463 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t2865362463 *, const RuntimeMethod*))Dictionary_2__ctor_m518943619_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::Add(!0,!1)
inline void Dictionary_2_Add_m3724819337 (Dictionary_2_t2865362463 * __this, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t2865362463 *, String_t*, RuntimeObject *, const RuntimeMethod*))Dictionary_2_Add_m3105409630_gshared)(__this, p0, p1, method);
}
// System.Void Facebook.Unity.FB::LogAppEvent(System.String,System.Nullable`1<System.Single>,System.Collections.Generic.Dictionary`2<System.String,System.Object>)
extern "C" IL2CPP_METHOD_ATTR void FB_LogAppEvent_m1603351799 (RuntimeObject * __this /* static, unused */, String_t* p0, Nullable_1_t3119828856  p1, Dictionary_2_t2865362463 * p2, const RuntimeMethod* method);
// System.String Facebook.Unity.FB::get_AppId()
extern "C" IL2CPP_METHOD_ATTR String_t* FB_get_AppId_m2803391701 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m3937257545 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Void Facebook.Unity.Example.LogView::AddLog(System.String)
extern "C" IL2CPP_METHOD_ATTR void LogView_AddLog_m1637420510 (RuntimeObject * __this /* static, unused */, String_t* ___log0, const RuntimeMethod* method);
// System.Void Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>::.ctor(System.Object,System.IntPtr)
inline void FacebookDelegate_1__ctor_m2125150161 (FacebookDelegate_1_t3420330156 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (FacebookDelegate_1_t3420330156 *, RuntimeObject *, intptr_t, const RuntimeMethod*))FacebookDelegate_1__ctor_m3403141292_gshared)(__this, p0, p1, method);
}
// System.Void Facebook.Unity.FB::GetAppLink(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern "C" IL2CPP_METHOD_ATTR void FB_GetAppLink_m2933190873 (RuntimeObject * __this /* static, unused */, FacebookDelegate_1_t3420330156 * p0, const RuntimeMethod* method);
// System.Boolean Facebook.Unity.Constants::get_IsMobile()
extern "C" IL2CPP_METHOD_ATTR bool Constants_get_IsMobile_m316667921 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void Facebook.Unity.FB/Mobile::FetchDeferredAppLinkData(Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppLinkResult>)
extern "C" IL2CPP_METHOD_ATTR void Mobile_FetchDeferredAppLinkData_m3022173889 (RuntimeObject * __this /* static, unused */, FacebookDelegate_1_t3420330156 * p0, const RuntimeMethod* method);
// System.Void Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>::.ctor(System.Object,System.IntPtr)
inline void FacebookDelegate_1__ctor_m1320861630 (FacebookDelegate_1_t1976622014 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (FacebookDelegate_1_t1976622014 *, RuntimeObject *, intptr_t, const RuntimeMethod*))FacebookDelegate_1__ctor_m3403141292_gshared)(__this, p0, p1, method);
}
// System.Void Facebook.Unity.FB::AppRequest(System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.Collections.Generic.IEnumerable`1<System.Object>,System.Collections.Generic.IEnumerable`1<System.String>,System.Nullable`1<System.Int32>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern "C" IL2CPP_METHOD_ATTR void FB_AppRequest_m781265255 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject* p1, RuntimeObject* p2, RuntimeObject* p3, Nullable_1_t378540539  p4, String_t* p5, String_t* p6, FacebookDelegate_1_t1976622014 * p7, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
inline void List_1__ctor_m2321703786 (List_1_t257213610 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t257213610 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
inline void List_1_Add_m3338814081 (List_1_t257213610 * __this, RuntimeObject * p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t257213610 *, RuntimeObject *, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.Void System.Nullable`1<System.Int32>::.ctor(!0)
inline void Nullable_1__ctor_m2962682148 (Nullable_1_t378540539 * __this, int32_t p0, const RuntimeMethod* method)
{
	((  void (*) (Nullable_1_t378540539 *, int32_t, const RuntimeMethod*))Nullable_1__ctor_m2962682148_gshared)(__this, p0, method);
}
// System.Void Facebook.Unity.Example.ConsoleBase::LabelAndTextField(System.String,System.String&)
extern "C" IL2CPP_METHOD_ATTR void ConsoleBase_LabelAndTextField_m2312391562 (ConsoleBase_t1023975560 * __this, String_t* ___label0, String_t** ___text1, const RuntimeMethod* method);
// !!0[] System.Array::Empty<UnityEngine.GUILayoutOption>()
inline GUILayoutOptionU5BU5D_t2510215842* Array_Empty_TisGUILayoutOption_t811797299_m4167206926 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	return ((  GUILayoutOptionU5BU5D_t2510215842* (*) (RuntimeObject * /* static, unused */, const RuntimeMethod*))Array_Empty_TisRuntimeObject_m4268358689_gshared)(__this /* static, unused */, method);
}
// System.Void UnityEngine.GUILayout::BeginHorizontal(UnityEngine.GUILayoutOption[])
extern "C" IL2CPP_METHOD_ATTR void GUILayout_BeginHorizontal_m1655989246 (RuntimeObject * __this /* static, unused */, GUILayoutOptionU5BU5D_t2510215842* p0, const RuntimeMethod* method);
// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_LabelStyle()
extern "C" IL2CPP_METHOD_ATTR GUIStyle_t3956901511 * ConsoleBase_get_LabelStyle_m3856255500 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method);
// System.Single Facebook.Unity.Example.ConsoleBase::get_ScaleFactor()
extern "C" IL2CPP_METHOD_ATTR float ConsoleBase_get_ScaleFactor_m540546232 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method);
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::MaxWidth(System.Single)
extern "C" IL2CPP_METHOD_ATTR GUILayoutOption_t811797299 * GUILayout_MaxWidth_m3297347705 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method);
// System.Void UnityEngine.GUILayout::Label(System.String,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C" IL2CPP_METHOD_ATTR void GUILayout_Label_m1096010274 (RuntimeObject * __this /* static, unused */, String_t* p0, GUIStyle_t3956901511 * p1, GUILayoutOptionU5BU5D_t2510215842* p2, const RuntimeMethod* method);
// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_ButtonStyle()
extern "C" IL2CPP_METHOD_ATTR GUIStyle_t3956901511 * ConsoleBase_get_ButtonStyle_m1393828074 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method);
// System.Int32 Facebook.Unity.Example.ConsoleBase::get_ButtonHeight()
extern "C" IL2CPP_METHOD_ATTR int32_t ConsoleBase_get_ButtonHeight_m1783046984 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::MinHeight(System.Single)
extern "C" IL2CPP_METHOD_ATTR GUILayoutOption_t811797299 * GUILayout_MinHeight_m98186407 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method);
// System.Int32 Facebook.Unity.Example.ConsoleBase::get_MainWindowWidth()
extern "C" IL2CPP_METHOD_ATTR int32_t ConsoleBase_get_MainWindowWidth_m4254761416 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Int32 UnityEngine.GUILayout::Toolbar(System.Int32,System.String[],UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C" IL2CPP_METHOD_ATTR int32_t GUILayout_Toolbar_m4192068045 (RuntimeObject * __this /* static, unused */, int32_t p0, StringU5BU5D_t1281789340* p1, GUIStyle_t3956901511 * p2, GUILayoutOptionU5BU5D_t2510215842* p3, const RuntimeMethod* method);
// System.Void UnityEngine.GUILayout::EndHorizontal()
extern "C" IL2CPP_METHOD_ATTR void GUILayout_EndHorizontal_m125407884 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Nullable`1<Facebook.Unity.OGActionType> Facebook.Unity.Example.AppRequests::GetSelectedOGActionType()
extern "C" IL2CPP_METHOD_ATTR Nullable_1_t2119916463  AppRequests_GetSelectedOGActionType_m1566487639 (AppRequests_t2419817778 * __this, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<Facebook.Unity.OGActionType>::get_HasValue()
inline bool Nullable_1_get_HasValue_m604914582 (Nullable_1_t2119916463 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Nullable_1_t2119916463 *, const RuntimeMethod*))Nullable_1_get_HasValue_m604914582_gshared)(__this, method);
}
// !0 System.Nullable`1<Facebook.Unity.OGActionType>::get_Value()
inline int32_t Nullable_1_get_Value_m1111532194 (Nullable_1_t2119916463 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Nullable_1_t2119916463 *, const RuntimeMethod*))Nullable_1_get_Value_m1111532194_gshared)(__this, method);
}
// System.Boolean System.String::IsNullOrEmpty(System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_IsNullOrEmpty_m2969720369 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.String[] System.String::Split(System.Char[])
extern "C" IL2CPP_METHOD_ATTR StringU5BU5D_t1281789340* String_Split_m3646115398 (String_t* __this, CharU5BU5D_t3528271667* p0, const RuntimeMethod* method);
// System.Void Facebook.Unity.FB::AppRequest(System.String,Facebook.Unity.OGActionType,System.String,System.Collections.Generic.IEnumerable`1<System.String>,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>)
extern "C" IL2CPP_METHOD_ATTR void FB_AppRequest_m3170977800 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, String_t* p2, RuntimeObject* p3, String_t* p4, String_t* p5, FacebookDelegate_1_t1976622014 * p6, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<!!0> System.Linq.Enumerable::OfType<System.Object>(System.Collections.IEnumerable)
inline RuntimeObject* Enumerable_OfType_TisRuntimeObject_m444860550 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method)
{
	return ((  RuntimeObject* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_OfType_TisRuntimeObject_m444860550_gshared)(__this /* static, unused */, p0, method);
}
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
inline List_1_t257213610 * Enumerable_ToList_TisRuntimeObject_m2366248129 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method)
{
	return ((  List_1_t257213610 * (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_ToList_TisRuntimeObject_m2366248129_gshared)(__this /* static, unused */, p0, method);
}
// System.Int32 System.Int32::Parse(System.String)
extern "C" IL2CPP_METHOD_ATTR int32_t Int32_Parse_m1033611559 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR bool String_op_Equality_m920492651 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Void System.Nullable`1<Facebook.Unity.OGActionType>::.ctor(!0)
inline void Nullable_1__ctor_m2532889892 (Nullable_1_t2119916463 * __this, int32_t p0, const RuntimeMethod* method)
{
	((  void (*) (Nullable_1_t2119916463 *, int32_t, const RuntimeMethod*))Nullable_1__ctor_m2532889892_gshared)(__this, p0, method);
}
// UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Vector2_get_zero_m540426400 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_m1579109191 (MonoBehaviour_t3962482529 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_width()
extern "C" IL2CPP_METHOD_ATTR int32_t Screen_get_width_m345039817 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean System.Nullable`1<System.Single>::get_HasValue()
inline bool Nullable_1_get_HasValue_m2149791491 (Nullable_1_t3119828856 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Nullable_1_t3119828856 *, const RuntimeMethod*))Nullable_1_get_HasValue_m2149791491_gshared)(__this, method);
}
// System.Single UnityEngine.Screen::get_dpi()
extern "C" IL2CPP_METHOD_ATTR float Screen_get_dpi_m495672463 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void System.Nullable`1<System.Single>::.ctor(!0)
inline void Nullable_1__ctor_m3964206222 (Nullable_1_t3119828856 * __this, float p0, const RuntimeMethod* method)
{
	((  void (*) (Nullable_1_t3119828856 *, float, const RuntimeMethod*))Nullable_1__ctor_m3964206222_gshared)(__this, p0, method);
}
// !0 System.Nullable`1<System.Single>::get_Value()
inline float Nullable_1_get_Value_m4168550405 (Nullable_1_t3119828856 * __this, const RuntimeMethod* method)
{
	return ((  float (*) (Nullable_1_t3119828856 *, const RuntimeMethod*))Nullable_1_get_Value_m4168550405_gshared)(__this, method);
}
// UnityEngine.GUISkin UnityEngine.GUI::get_skin()
extern "C" IL2CPP_METHOD_ATTR GUISkin_t1244372282 * GUI_get_skin_m1874615010 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_textArea()
extern "C" IL2CPP_METHOD_ATTR GUIStyle_t3956901511 * GUISkin_get_textArea_m1928475264 (GUISkin_t1244372282 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GUIStyle::.ctor(UnityEngine.GUIStyle)
extern "C" IL2CPP_METHOD_ATTR void GUIStyle__ctor_m2912682974 (GUIStyle_t3956901511 * __this, GUIStyle_t3956901511 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.GUIStyle::set_alignment(UnityEngine.TextAnchor)
extern "C" IL2CPP_METHOD_ATTR void GUIStyle_set_alignment_m3944619660 (GUIStyle_t3956901511 * __this, int32_t p0, const RuntimeMethod* method);
// System.Void UnityEngine.GUIStyle::set_wordWrap(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void GUIStyle_set_wordWrap_m1419501823 (GUIStyle_t3956901511 * __this, bool p0, const RuntimeMethod* method);
// System.Void UnityEngine.RectOffset::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void RectOffset__ctor_m732140021 (RectOffset_t1369453676 * __this, int32_t p0, int32_t p1, int32_t p2, int32_t p3, const RuntimeMethod* method);
// System.Void UnityEngine.GUIStyle::set_padding(UnityEngine.RectOffset)
extern "C" IL2CPP_METHOD_ATTR void GUIStyle_set_padding_m3302456044 (GUIStyle_t3956901511 * __this, RectOffset_t1369453676 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.GUIStyle::set_stretchHeight(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void GUIStyle_set_stretchHeight_m3442828884 (GUIStyle_t3956901511 * __this, bool p0, const RuntimeMethod* method);
// System.Void UnityEngine.GUIStyle::set_stretchWidth(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void GUIStyle_set_stretchWidth_m2564034386 (GUIStyle_t3956901511 * __this, bool p0, const RuntimeMethod* method);
// System.Int32 Facebook.Unity.Example.ConsoleBase::get_FontSize()
extern "C" IL2CPP_METHOD_ATTR int32_t ConsoleBase_get_FontSize_m754853133 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GUIStyle::set_fontSize(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void GUIStyle_set_fontSize_m1566850023 (GUIStyle_t3956901511 * __this, int32_t p0, const RuntimeMethod* method);
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_button()
extern "C" IL2CPP_METHOD_ATTR GUIStyle_t3956901511 * GUISkin_get_button_m2792560996 (GUISkin_t1244372282 * __this, const RuntimeMethod* method);
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_textField()
extern "C" IL2CPP_METHOD_ATTR GUIStyle_t3956901511 * GUISkin_get_textField_m1918733755 (GUISkin_t1244372282 * __this, const RuntimeMethod* method);
// UnityEngine.GUIStyle UnityEngine.GUISkin::get_label()
extern "C" IL2CPP_METHOD_ATTR GUIStyle_t3956901511 * GUISkin_get_label_m1693050720 (GUISkin_t1244372282 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Application::set_targetFrameRate(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Application_set_targetFrameRate_m3682352535 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.GUILayout::Button(System.String,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C" IL2CPP_METHOD_ATTR bool GUILayout_Button_m3728892478 (RuntimeObject * __this /* static, unused */, String_t* p0, GUIStyle_t3956901511 * p1, GUILayoutOptionU5BU5D_t2510215842* p2, const RuntimeMethod* method);
// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_TextInputStyle()
extern "C" IL2CPP_METHOD_ATTR GUIStyle_t3956901511 * ConsoleBase_get_TextInputStyle_m927335950 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method);
// System.String UnityEngine.GUILayout::TextField(System.String,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C" IL2CPP_METHOD_ATTR String_t* GUILayout_TextField_m4150770174 (RuntimeObject * __this /* static, unused */, String_t* p0, GUIStyle_t3956901511 * p1, GUILayoutOptionU5BU5D_t2510215842* p2, const RuntimeMethod* method);
// UnityEngine.ScreenOrientation UnityEngine.Screen::get_orientation()
extern "C" IL2CPP_METHOD_ATTR int32_t Screen_get_orientation_m3354122719 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Type System.Object::GetType()
extern "C" IL2CPP_METHOD_ATTR Type_t * Object_GetType_m88164663 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.String>::Push(!0)
inline void Stack_1_Push_m559717952 (Stack_1_t2690840144 * __this, String_t* p0, const RuntimeMethod* method)
{
	((  void (*) (Stack_1_t2690840144 *, String_t*, const RuntimeMethod*))Stack_1_Push_m1669856732_gshared)(__this, p0, method);
}
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern "C" IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m1758133949 (RuntimeObject * __this /* static, unused */, String_t* p0, const RuntimeMethod* method);
// System.Boolean System.Linq.Enumerable::Any<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
inline bool Enumerable_Any_TisString_t_m3986012742 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method)
{
	return ((  bool (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_Any_TisRuntimeObject_m3173759778_gshared)(__this /* static, unused */, p0, method);
}
// !0 System.Collections.Generic.Stack`1<System.String>::Pop()
inline String_t* Stack_1_Pop_m812987992 (Stack_1_t2690840144 * __this, const RuntimeMethod* method)
{
	return ((  String_t* (*) (Stack_1_t2690840144 *, const RuntimeMethod*))Stack_1_Pop_m756553478_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Stack`1<System.String>::.ctor()
inline void Stack_1__ctor_m2088216795 (Stack_1_t2690840144 * __this, const RuntimeMethod* method)
{
	((  void (*) (Stack_1_t2690840144 *, const RuntimeMethod*))Stack_1__ctor_m3164958980_gshared)(__this, method);
}
// System.Boolean UnityEngine.GUI::get_enabled()
extern "C" IL2CPP_METHOD_ATTR bool GUI_get_enabled_m1448901857 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void System.Uri::.ctor(System.String)
extern "C" IL2CPP_METHOD_ATTR void Uri__ctor_m800430703 (Uri_t100236324 * __this, String_t* p0, const RuntimeMethod* method);
// System.Void Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>::.ctor(System.Object,System.IntPtr)
inline void FacebookDelegate_1__ctor_m3417605933 (FacebookDelegate_1_t602110559 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (FacebookDelegate_1_t602110559 *, RuntimeObject *, intptr_t, const RuntimeMethod*))FacebookDelegate_1__ctor_m3403141292_gshared)(__this, p0, p1, method);
}
// System.Void Facebook.Unity.FB::ShareLink(System.Uri,System.String,System.String,System.Uri,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern "C" IL2CPP_METHOD_ATTR void FB_ShareLink_m3039827802 (RuntimeObject * __this /* static, unused */, Uri_t100236324 * p0, String_t* p1, String_t* p2, Uri_t100236324 * p3, FacebookDelegate_1_t602110559 * p4, const RuntimeMethod* method);
// System.Boolean Facebook.Unity.Constants::get_IsEditor()
extern "C" IL2CPP_METHOD_ATTR bool Constants_get_IsEditor_m1554664543 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Boolean Facebook.Unity.FB::get_IsLoggedIn()
extern "C" IL2CPP_METHOD_ATTR bool FB_get_IsLoggedIn_m2875052829 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.GUI::set_enabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void GUI_set_enabled_m3922032131 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method);
// System.Void Facebook.Unity.FB::FeedShare(System.String,System.Uri,System.String,System.String,System.String,System.Uri,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>)
extern "C" IL2CPP_METHOD_ATTR void FB_FeedShare_m1574327390 (RuntimeObject * __this /* static, unused */, String_t* p0, Uri_t100236324 * p1, String_t* p2, String_t* p3, String_t* p4, Uri_t100236324 * p5, String_t* p6, FacebookDelegate_1_t602110559 * p7, const RuntimeMethod* method);
// System.Void Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>::.ctor(System.Object,System.IntPtr)
inline void FacebookDelegate_1__ctor_m411964393 (FacebookDelegate_1_t138390958 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (FacebookDelegate_1_t138390958 *, RuntimeObject *, intptr_t, const RuntimeMethod*))FacebookDelegate_1__ctor_m3403141292_gshared)(__this, p0, p1, method);
}
// System.Void Facebook.Unity.FB::API(System.String,Facebook.Unity.HttpMethod,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>,System.Collections.Generic.IDictionary`2<System.String,System.String>)
extern "C" IL2CPP_METHOD_ATTR void FB_API_m2949212365 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, FacebookDelegate_1_t138390958 * p2, RuntimeObject* p3, const RuntimeMethod* method);
// System.Collections.IEnumerator Facebook.Unity.Example.GraphRequest::TakeScreenshot()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* GraphRequest_TakeScreenshot_m1191488447 (GraphRequest_t4047451309 * __this, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C" IL2CPP_METHOD_ATTR Coroutine_t3829159415 * MonoBehaviour_StartCoroutine_m3411253000 (MonoBehaviour_t3962482529 * __this, RuntimeObject* p0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" IL2CPP_METHOD_ATTR bool Object_op_Inequality_m4071470834 (RuntimeObject * __this /* static, unused */, Object_t631007953 * p0, Object_t631007953 * p1, const RuntimeMethod* method);
// System.Void UnityEngine.GUILayout::Box(UnityEngine.Texture,UnityEngine.GUILayoutOption[])
extern "C" IL2CPP_METHOD_ATTR void GUILayout_Box_m1016637907 (RuntimeObject * __this /* static, unused */, Texture_t3661962703 * p0, GUILayoutOptionU5BU5D_t2510215842* p1, const RuntimeMethod* method);
// System.Void Facebook.Unity.Example.MenuBase::HandleResult(Facebook.Unity.IResult)
extern "C" IL2CPP_METHOD_ATTR void MenuBase_HandleResult_m3006125243 (MenuBase_t1079888489 * __this, RuntimeObject* ___result0, const RuntimeMethod* method);
// System.Void Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CTakeScreenshotU3Ec__Iterator0__ctor_m88611805 (U3CTakeScreenshotU3Ec__Iterator0_t3544038915 * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForEndOfFrame::.ctor()
extern "C" IL2CPP_METHOD_ATTR void WaitForEndOfFrame__ctor_m1381314187 (WaitForEndOfFrame_t1314943911 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Screen::get_height()
extern "C" IL2CPP_METHOD_ATTR int32_t Screen_get_height_m1623532518 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Texture2D__ctor_m2862217990 (Texture2D_t3840446185 * __this, int32_t p0, int32_t p1, int32_t p2, bool p3, const RuntimeMethod* method);
// System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern "C" IL2CPP_METHOD_ATTR void Rect__ctor_m2614021312 (Rect_t2360479859 * __this, float p0, float p1, float p2, float p3, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void Texture2D_ReadPixels_m3395504488 (Texture2D_t3840446185 * __this, Rect_t2360479859  p0, int32_t p1, int32_t p2, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::Apply()
extern "C" IL2CPP_METHOD_ATTR void Texture2D_Apply_m2271746283 (Texture2D_t3840446185 * __this, const RuntimeMethod* method);
// System.Byte[] UnityEngine.ImageConversion::EncodeToPNG(UnityEngine.Texture2D)
extern "C" IL2CPP_METHOD_ATTR ByteU5BU5D_t4116647657* ImageConversion_EncodeToPNG_m2292631531 (RuntimeObject * __this /* static, unused */, Texture2D_t3840446185 * p0, const RuntimeMethod* method);
// System.Void UnityEngine.WWWForm::.ctor()
extern "C" IL2CPP_METHOD_ATTR void WWWForm__ctor_m2465700452 (WWWForm_t4064702195 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.WWWForm::AddBinaryData(System.String,System.Byte[],System.String)
extern "C" IL2CPP_METHOD_ATTR void WWWForm_AddBinaryData_m344280980 (WWWForm_t4064702195 * __this, String_t* p0, ByteU5BU5D_t4116647657* p1, String_t* p2, const RuntimeMethod* method);
// System.Void UnityEngine.WWWForm::AddField(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void WWWForm_AddField_m2357902982 (WWWForm_t4064702195 * __this, String_t* p0, String_t* p1, const RuntimeMethod* method);
// System.Void Facebook.Unity.FB::API(System.String,Facebook.Unity.HttpMethod,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>,UnityEngine.WWWForm)
extern "C" IL2CPP_METHOD_ATTR void FB_API_m1010533072 (RuntimeObject * __this /* static, unused */, String_t* p0, int32_t p1, FacebookDelegate_1_t138390958 * p2, WWWForm_t4064702195 * p3, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
extern "C" IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m2730133172 (NotSupportedException_t1314879016 * __this, const RuntimeMethod* method);
// System.Void Facebook.Unity.Example.ConsoleBase::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ConsoleBase__ctor_m3547073371 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method);
// System.DateTime System.DateTime::get_Now()
extern "C" IL2CPP_METHOD_ATTR DateTime_t3738529785  DateTime_get_Now_m1277138875 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String System.DateTime::ToString(System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* DateTime_ToString_m3718521780 (DateTime_t3738529785 * __this, String_t* p0, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_m2556382932 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, RuntimeObject * p2, const RuntimeMethod* method);
// System.Void UnityEngine.GUILayout::BeginVertical(UnityEngine.GUILayoutOption[])
extern "C" IL2CPP_METHOD_ATTR void GUILayout_BeginVertical_m1213265852 (RuntimeObject * __this /* static, unused */, GUILayoutOptionU5BU5D_t2510215842* p0, const RuntimeMethod* method);
// System.Void Facebook.Unity.Example.ConsoleBase::GoBack()
extern "C" IL2CPP_METHOD_ATTR void ConsoleBase_GoBack_m3535004784 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C" IL2CPP_METHOD_ATTR int32_t Input_get_touchCount_m3403849067 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C" IL2CPP_METHOD_ATTR Touch_t1921856868  Input_GetTouch_m2192712756 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C" IL2CPP_METHOD_ATTR int32_t Touch_get_phase_m214549210 (Touch_t1921856868 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 Facebook.Unity.Example.ConsoleBase::get_ScrollPosition()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  ConsoleBase_get_ScrollPosition_m2128236017 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.Touch::get_deltaPosition()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  Touch_get_deltaPosition_m2389653382 (Touch_t1921856868 * __this, const RuntimeMethod* method);
// System.Void Facebook.Unity.Example.ConsoleBase::set_ScrollPosition(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void ConsoleBase_set_ScrollPosition_m589751691 (ConsoleBase_t1023975560 * __this, Vector2_t2156229523  ___value0, const RuntimeMethod* method);
// System.Int32 Facebook.Unity.Example.ConsoleBase::get_MainWindowFullWidth()
extern "C" IL2CPP_METHOD_ATTR int32_t ConsoleBase_get_MainWindowFullWidth_m476492915 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::MinWidth(System.Single)
extern "C" IL2CPP_METHOD_ATTR GUILayoutOption_t811797299 * GUILayout_MinWidth_m3524212012 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method);
// UnityEngine.Vector2 UnityEngine.GUILayout::BeginScrollView(UnityEngine.Vector2,UnityEngine.GUILayoutOption[])
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  GUILayout_BeginScrollView_m3542895037 (RuntimeObject * __this /* static, unused */, Vector2_t2156229523  p0, GUILayoutOptionU5BU5D_t2510215842* p1, const RuntimeMethod* method);
// !!0[] System.Linq.Enumerable::ToArray<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
inline StringU5BU5D_t1281789340* Enumerable_ToArray_TisString_t_m2924696684 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, const RuntimeMethod* method)
{
	return ((  StringU5BU5D_t1281789340* (*) (RuntimeObject * /* static, unused */, RuntimeObject*, const RuntimeMethod*))Enumerable_ToArray_TisRuntimeObject_m387236094_gshared)(__this /* static, unused */, p0, method);
}
// System.String System.String::Join(System.String,System.String[])
extern "C" IL2CPP_METHOD_ATTR String_t* String_Join_m2050845953 (RuntimeObject * __this /* static, unused */, String_t* p0, StringU5BU5D_t1281789340* p1, const RuntimeMethod* method);
// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_TextStyle()
extern "C" IL2CPP_METHOD_ATTR GUIStyle_t3956901511 * ConsoleBase_get_TextStyle_m2535314378 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method);
// UnityEngine.GUILayoutOption UnityEngine.GUILayout::ExpandHeight(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR GUILayoutOption_t811797299 * GUILayout_ExpandHeight_m4126348219 (RuntimeObject * __this /* static, unused */, bool p0, const RuntimeMethod* method);
// System.String UnityEngine.GUILayout::TextArea(System.String,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C" IL2CPP_METHOD_ATTR String_t* GUILayout_TextArea_m894334590 (RuntimeObject * __this /* static, unused */, String_t* p0, GUIStyle_t3956901511 * p1, GUILayoutOptionU5BU5D_t2510215842* p2, const RuntimeMethod* method);
// System.Void UnityEngine.GUILayout::EndScrollView()
extern "C" IL2CPP_METHOD_ATTR void GUILayout_EndScrollView_m1561653937 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.GUILayout::EndVertical()
extern "C" IL2CPP_METHOD_ATTR void GUILayout_EndVertical_m3051805938 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.String>::.ctor()
inline void List_1__ctor_m706204246 (List_1_t3319525431 * __this, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3319525431 *, const RuntimeMethod*))List_1__ctor_m2321703786_gshared)(__this, method);
}
// System.Void Facebook.Unity.InitDelegate::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void InitDelegate__ctor_m556099265 (InitDelegate_t3081360126 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void Facebook.Unity.HideUnityDelegate::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void HideUnityDelegate__ctor_m1747604723 (HideUnityDelegate_t1353799728 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);
// System.Void Facebook.Unity.FB::Init(Facebook.Unity.InitDelegate,Facebook.Unity.HideUnityDelegate,System.String)
extern "C" IL2CPP_METHOD_ATTR void FB_Init_m1856465676 (RuntimeObject * __this /* static, unused */, InitDelegate_t3081360126 * p0, HideUnityDelegate_t1353799728 * p1, String_t* p2, const RuntimeMethod* method);
// System.Boolean Facebook.Unity.FB::get_IsInitialized()
extern "C" IL2CPP_METHOD_ATTR bool FB_get_IsInitialized_m3789500881 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void Facebook.Unity.Example.MainMenu::CallFBLogin()
extern "C" IL2CPP_METHOD_ATTR void MainMenu_CallFBLogin_m2730593439 (MainMenu_t1823058806 * __this, const RuntimeMethod* method);
// System.Void Facebook.Unity.Example.MainMenu::CallFBLoginForPublish()
extern "C" IL2CPP_METHOD_ATTR void MainMenu_CallFBLoginForPublish_m2374054384 (MainMenu_t1823058806 * __this, const RuntimeMethod* method);
// System.Int32 Facebook.Unity.Example.ConsoleBase::get_MarginFix()
extern "C" IL2CPP_METHOD_ATTR int32_t ConsoleBase_get_MarginFix_m3965407477 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void UnityEngine.GUILayout::Label(UnityEngine.GUIContent,UnityEngine.GUILayoutOption[])
extern "C" IL2CPP_METHOD_ATTR void GUILayout_Label_m1547104774 (RuntimeObject * __this /* static, unused */, GUIContent_t3050628031 * p0, GUILayoutOptionU5BU5D_t2510215842* p1, const RuntimeMethod* method);
// System.Void Facebook.Unity.Example.MainMenu::CallFBLogout()
extern "C" IL2CPP_METHOD_ATTR void MainMenu_CallFBLogout_m4130237067 (MainMenu_t1823058806 * __this, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
extern "C" IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m1620074514 (RuntimeObject * __this /* static, unused */, RuntimeTypeHandle_t3027515415  p0, const RuntimeMethod* method);
// System.Void Facebook.Unity.Example.ConsoleBase::SwitchMenu(System.Type)
extern "C" IL2CPP_METHOD_ATTR void ConsoleBase_SwitchMenu_m676980302 (ConsoleBase_t1023975560 * __this, Type_t * ___menuClass0, const RuntimeMethod* method);
// System.Boolean Facebook.Unity.Constants::get_IsWeb()
extern "C" IL2CPP_METHOD_ATTR bool Constants_get_IsWeb_m1986090831 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.String>::Add(!0)
inline void List_1_Add_m1685793073 (List_1_t3319525431 * __this, String_t* p0, const RuntimeMethod* method)
{
	((  void (*) (List_1_t3319525431 *, String_t*, const RuntimeMethod*))List_1_Add_m3338814081_gshared)(__this, p0, method);
}
// System.Void Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>::.ctor(System.Object,System.IntPtr)
inline void FacebookDelegate_1__ctor_m2272005295 (FacebookDelegate_1_t1669328765 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (FacebookDelegate_1_t1669328765 *, RuntimeObject *, intptr_t, const RuntimeMethod*))FacebookDelegate_1__ctor_m3403141292_gshared)(__this, p0, p1, method);
}
// System.Void Facebook.Unity.FB::LogInWithReadPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern "C" IL2CPP_METHOD_ATTR void FB_LogInWithReadPermissions_m2288754438 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, FacebookDelegate_1_t1669328765 * p1, const RuntimeMethod* method);
// System.Void Facebook.Unity.FB::LogInWithPublishPermissions(System.Collections.Generic.IEnumerable`1<System.String>,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.ILoginResult>)
extern "C" IL2CPP_METHOD_ATTR void FB_LogInWithPublishPermissions_m2760240870 (RuntimeObject * __this /* static, unused */, RuntimeObject* p0, FacebookDelegate_1_t1669328765 * p1, const RuntimeMethod* method);
// System.Void Facebook.Unity.FB::LogOut()
extern "C" IL2CPP_METHOD_ATTR void FB_LogOut_m3633383663 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void Facebook.Unity.Example.ConsoleBase::set_LastResponse(System.String)
extern "C" IL2CPP_METHOD_ATTR void ConsoleBase_set_LastResponse_m3622711220 (ConsoleBase_t1023975560 * __this, String_t* ___value0, const RuntimeMethod* method);
// Facebook.Unity.AccessToken Facebook.Unity.AccessToken::get_CurrentAccessToken()
extern "C" IL2CPP_METHOD_ATTR AccessToken_t2431487013 * AccessToken_get_CurrentAccessToken_m3037387743 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Format_m2844511972 (RuntimeObject * __this /* static, unused */, String_t* p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_m904156431 (RuntimeObject * __this /* static, unused */, RuntimeObject * p0, RuntimeObject * p1, const RuntimeMethod* method);
// System.String Facebook.Unity.Example.ConsoleBase::get_LastResponse()
extern "C" IL2CPP_METHOD_ATTR String_t* ConsoleBase_get_LastResponse_m2081918938 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method);
// System.Void Facebook.Unity.Example.ConsoleBase::set_LastResponseTexture(UnityEngine.Texture2D)
extern "C" IL2CPP_METHOD_ATTR void ConsoleBase_set_LastResponseTexture_m2611436616 (ConsoleBase_t1023975560 * __this, Texture2D_t3840446185 * ___value0, const RuntimeMethod* method);
// System.Boolean Facebook.Unity.Example.ConsoleBase::IsHorizontalLayout()
extern "C" IL2CPP_METHOD_ATTR bool ConsoleBase_IsHorizontalLayout_m525810539 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method);
// System.Void Facebook.Unity.Example.MenuBase::AddStatus()
extern "C" IL2CPP_METHOD_ATTR void MenuBase_AddStatus_m319405204 (MenuBase_t1079888489 * __this, const RuntimeMethod* method);
// System.Void Facebook.Unity.Example.MenuBase::AddBackButton()
extern "C" IL2CPP_METHOD_ATTR void MenuBase_AddBackButton_m214045700 (MenuBase_t1079888489 * __this, const RuntimeMethod* method);
// System.Void Facebook.Unity.Example.MenuBase::AddLogButton()
extern "C" IL2CPP_METHOD_ATTR void MenuBase_AddLogButton_m1613724113 (MenuBase_t1079888489 * __this, const RuntimeMethod* method);
// System.Void Facebook.Unity.Example.MenuBase::AddDialogModeButtons()
extern "C" IL2CPP_METHOD_ATTR void MenuBase_AddDialogModeButtons_m2182366232 (MenuBase_t1079888489 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GUILayout::Space(System.Single)
extern "C" IL2CPP_METHOD_ATTR void GUILayout_Space_m3613068479 (RuntimeObject * __this /* static, unused */, float p0, const RuntimeMethod* method);
// System.String Facebook.Unity.Example.ConsoleBase::get_Status()
extern "C" IL2CPP_METHOD_ATTR String_t* ConsoleBase_get_Status_m2983343794 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.GUILayout::Box(System.String,UnityEngine.GUIStyle,UnityEngine.GUILayoutOption[])
extern "C" IL2CPP_METHOD_ATTR void GUILayout_Box_m3847685353 (RuntimeObject * __this /* static, unused */, String_t* p0, GUIStyle_t3956901511 * p1, GUILayoutOptionU5BU5D_t2510215842* p2, const RuntimeMethod* method);
// System.Collections.Generic.Stack`1<System.String> Facebook.Unity.Example.ConsoleBase::get_MenuStack()
extern "C" IL2CPP_METHOD_ATTR Stack_1_t2690840144 * ConsoleBase_get_MenuStack_m1135196678 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Array System.Enum::GetValues(System.Type)
extern "C" IL2CPP_METHOD_ATTR RuntimeArray * Enum_GetValues_m4192343468 (RuntimeObject * __this /* static, unused */, Type_t * p0, const RuntimeMethod* method);
// System.Collections.IEnumerator System.Array::GetEnumerator()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* Array_GetEnumerator_m4277730612 (RuntimeArray * __this, const RuntimeMethod* method);
// System.Void Facebook.Unity.Example.MenuBase::AddDialogModeButton(Facebook.Unity.ShareDialogMode)
extern "C" IL2CPP_METHOD_ATTR void MenuBase_AddDialogModeButton_m2926670472 (MenuBase_t1079888489 * __this, int32_t ___mode0, const RuntimeMethod* method);
// System.Void Facebook.Unity.FB/Mobile::set_ShareDialogMode(Facebook.Unity.ShareDialogMode)
extern "C" IL2CPP_METHOD_ATTR void Mobile_set_ShareDialogMode_m2548225244 (RuntimeObject * __this /* static, unused */, int32_t p0, const RuntimeMethod* method);
// System.Void Facebook.Unity.Example.Pay::CallFBPay()
extern "C" IL2CPP_METHOD_ATTR void Pay_CallFBPay_m2256746739 (Pay_t1264260185 * __this, const RuntimeMethod* method);
// System.Void Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>::.ctor(System.Object,System.IntPtr)
inline void FacebookDelegate_1__ctor_m3067526517 (FacebookDelegate_1_t2649775846 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (FacebookDelegate_1_t2649775846 *, RuntimeObject *, intptr_t, const RuntimeMethod*))FacebookDelegate_1__ctor_m3403141292_gshared)(__this, p0, p1, method);
}
// System.Void Facebook.Unity.FB/Canvas::Pay(System.String,System.String,System.Int32,System.Nullable`1<System.Int32>,System.Nullable`1<System.Int32>,System.String,System.String,System.String,Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IPayResult>)
extern "C" IL2CPP_METHOD_ATTR void Canvas_Pay_m384955350 (RuntimeObject * __this /* static, unused */, String_t* p0, String_t* p1, int32_t p2, Nullable_1_t378540539  p3, Nullable_1_t378540539  p4, String_t* p5, String_t* p6, String_t* p7, FacebookDelegate_1_t2649775846 * p8, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.Unity.Example.AccessTokenMenu::.ctor()
extern "C" IL2CPP_METHOD_ATTR void AccessTokenMenu__ctor_m266687190 (AccessTokenMenu_t4028641200 * __this, const RuntimeMethod* method)
{
	{
		MenuBase__ctor_m2160361082(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.AccessTokenMenu::GetGui()
extern "C" IL2CPP_METHOD_ATTR void AccessTokenMenu_GetGui_m2805584269 (AccessTokenMenu_t4028641200 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AccessTokenMenu_GetGui_m2805584269_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ConsoleBase_Button_m1005519490(__this, _stringLiteral3367156840, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		intptr_t L_1 = (intptr_t)MenuBase_HandleResult_m3006125243_RuntimeMethod_var;
		FacebookDelegate_1_t123177164 * L_2 = (FacebookDelegate_1_t123177164 *)il2cpp_codegen_object_new(FacebookDelegate_1_t123177164_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m1454705827(L_2, __this, (intptr_t)L_1, /*hidden argument*/FacebookDelegate_1__ctor_m1454705827_RuntimeMethod_var);
		Mobile_RefreshCurrentAccessToken_m1786803032(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.Unity.Example.AppEvents::.ctor()
extern "C" IL2CPP_METHOD_ATTR void AppEvents__ctor_m161944282 (AppEvents_t3645639549 * __this, const RuntimeMethod* method)
{
	{
		MenuBase__ctor_m2160361082(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.AppEvents::GetGui()
extern "C" IL2CPP_METHOD_ATTR void AppEvents_GetGui_m1498937247 (AppEvents_t3645639549 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppEvents_GetGui_m1498937247_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Nullable_1_t3119828856  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Dictionary_2_t2865362463 * V_1 = NULL;
	{
		bool L_0 = ConsoleBase_Button_m1005519490(__this, _stringLiteral1839852391, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0059;
		}
	}
	{
		ConsoleBase_set_Status_m4147399384(__this, _stringLiteral3033606244, /*hidden argument*/NULL);
		il2cpp_codegen_initobj((&V_0), sizeof(Nullable_1_t3119828856 ));
		Nullable_1_t3119828856  L_1 = V_0;
		Dictionary_2_t2865362463 * L_2 = (Dictionary_2_t2865362463 *)il2cpp_codegen_object_new(Dictionary_2_t2865362463_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m15304876(L_2, /*hidden argument*/Dictionary_2__ctor_m15304876_RuntimeMethod_var);
		V_1 = L_2;
		Dictionary_2_t2865362463 * L_3 = V_1;
		NullCheck(L_3);
		Dictionary_2_Add_m3724819337(L_3, _stringLiteral454323450, _stringLiteral2072270356, /*hidden argument*/Dictionary_2_Add_m3724819337_RuntimeMethod_var);
		Dictionary_2_t2865362463 * L_4 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		FB_LogAppEvent_m1603351799(NULL /*static, unused*/, _stringLiteral1237965765, L_1, L_4, /*hidden argument*/NULL);
		String_t* L_5 = FB_get_AppId_m2803391701(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_6 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral403140625, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LogView_t1067263371_il2cpp_TypeInfo_var);
		LogView_AddLog_m1637420510(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_0059:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.Unity.Example.AppLinks::.ctor()
extern "C" IL2CPP_METHOD_ATTR void AppLinks__ctor_m4014524470 (AppLinks_t2028121612 * __this, const RuntimeMethod* method)
{
	{
		MenuBase__ctor_m2160361082(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.AppLinks::GetGui()
extern "C" IL2CPP_METHOD_ATTR void AppLinks_GetGui_m3812488917 (AppLinks_t2028121612 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppLinks_GetGui_m3812488917_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ConsoleBase_Button_m1005519490(__this, _stringLiteral3661832575, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0021;
		}
	}
	{
		intptr_t L_1 = (intptr_t)MenuBase_HandleResult_m3006125243_RuntimeMethod_var;
		FacebookDelegate_1_t3420330156 * L_2 = (FacebookDelegate_1_t3420330156 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3420330156_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2125150161(L_2, __this, (intptr_t)L_1, /*hidden argument*/FacebookDelegate_1__ctor_m2125150161_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		FB_GetAppLink_m2933190873(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_0021:
	{
		bool L_3 = Constants_get_IsMobile_m316667921(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_004c;
		}
	}
	{
		bool L_4 = ConsoleBase_Button_m1005519490(__this, _stringLiteral1357171336, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_004c;
		}
	}
	{
		intptr_t L_5 = (intptr_t)MenuBase_HandleResult_m3006125243_RuntimeMethod_var;
		FacebookDelegate_1_t3420330156 * L_6 = (FacebookDelegate_1_t3420330156 *)il2cpp_codegen_object_new(FacebookDelegate_1_t3420330156_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2125150161(L_6, __this, (intptr_t)L_5, /*hidden argument*/FacebookDelegate_1__ctor_m2125150161_RuntimeMethod_var);
		Mobile_FetchDeferredAppLinkData_m3022173889(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
	}

IL_004c:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.Unity.Example.AppRequests::.ctor()
extern "C" IL2CPP_METHOD_ATTR void AppRequests__ctor_m3895077944 (AppRequests_t2419817778 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppRequests__ctor_m3895077944_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_requestMessage_16(L_0);
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_requestTo_17(L_1);
		String_t* L_2 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_requestFilter_18(L_2);
		String_t* L_3 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_requestExcludes_19(L_3);
		String_t* L_4 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_requestMax_20(L_4);
		String_t* L_5 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_requestData_21(L_5);
		String_t* L_6 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_requestTitle_22(L_6);
		String_t* L_7 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_requestObjectID_23(L_7);
		StringU5BU5D_t1281789340* L_8 = (StringU5BU5D_t1281789340*)SZArrayNew(StringU5BU5D_t1281789340_il2cpp_TypeInfo_var, (uint32_t)4);
		StringU5BU5D_t1281789340* L_9 = L_8;
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, _stringLiteral3191340502);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral3191340502);
		StringU5BU5D_t1281789340* L_10 = L_9;
		V_0 = 0;
		RuntimeObject * L_11 = Box(OGActionType_t397354381_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_11);
		String_t* L_12 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_11);
		V_0 = *(int32_t*)UnBox(L_11);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_12);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(1), (String_t*)L_12);
		StringU5BU5D_t1281789340* L_13 = L_10;
		V_1 = 1;
		RuntimeObject * L_14 = Box(OGActionType_t397354381_il2cpp_TypeInfo_var, (&V_1));
		NullCheck(L_14);
		String_t* L_15 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_14);
		V_1 = *(int32_t*)UnBox(L_14);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_15);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)L_15);
		StringU5BU5D_t1281789340* L_16 = L_13;
		V_2 = 2;
		RuntimeObject * L_17 = Box(OGActionType_t397354381_il2cpp_TypeInfo_var, (&V_2));
		NullCheck(L_17);
		String_t* L_18 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_17);
		V_2 = *(int32_t*)UnBox(L_17);
		NullCheck(L_16);
		ArrayElementTypeCheck (L_16, L_18);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(3), (String_t*)L_18);
		__this->set_actionTypeStrings_25(L_16);
		MenuBase__ctor_m2160361082(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.AppRequests::GetGui()
extern "C" IL2CPP_METHOD_ATTR void AppRequests_GetGui_m3488322347 (AppRequests_t2419817778 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppRequests_GetGui_m3488322347_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Nullable_1_t378540539  V_0;
	memset(&V_0, 0, sizeof(V_0));
	List_1_t257213610 * V_1 = NULL;
	List_1_t257213610 * V_2 = NULL;
	List_1_t257213610 * V_3 = NULL;
	Nullable_1_t2119916463  V_4;
	memset(&V_4, 0, sizeof(V_4));
	String_t* G_B10_0 = NULL;
	int32_t G_B10_1 = 0;
	String_t* G_B10_2 = NULL;
	String_t* G_B9_0 = NULL;
	int32_t G_B9_1 = 0;
	String_t* G_B9_2 = NULL;
	StringU5BU5D_t1281789340* G_B11_0 = NULL;
	String_t* G_B11_1 = NULL;
	int32_t G_B11_2 = 0;
	String_t* G_B11_3 = NULL;
	String_t* G_B14_0 = NULL;
	String_t* G_B13_0 = NULL;
	StringU5BU5D_t1281789340* G_B15_0 = NULL;
	String_t* G_B15_1 = NULL;
	StringU5BU5D_t1281789340* G_B17_0 = NULL;
	String_t* G_B17_1 = NULL;
	StringU5BU5D_t1281789340* G_B16_0 = NULL;
	String_t* G_B16_1 = NULL;
	List_1_t257213610 * G_B18_0 = NULL;
	StringU5BU5D_t1281789340* G_B18_1 = NULL;
	String_t* G_B18_2 = NULL;
	List_1_t257213610 * G_B20_0 = NULL;
	StringU5BU5D_t1281789340* G_B20_1 = NULL;
	String_t* G_B20_2 = NULL;
	List_1_t257213610 * G_B19_0 = NULL;
	StringU5BU5D_t1281789340* G_B19_1 = NULL;
	String_t* G_B19_2 = NULL;
	StringU5BU5D_t1281789340* G_B21_0 = NULL;
	List_1_t257213610 * G_B21_1 = NULL;
	StringU5BU5D_t1281789340* G_B21_2 = NULL;
	String_t* G_B21_3 = NULL;
	StringU5BU5D_t1281789340* G_B23_0 = NULL;
	List_1_t257213610 * G_B23_1 = NULL;
	StringU5BU5D_t1281789340* G_B23_2 = NULL;
	String_t* G_B23_3 = NULL;
	StringU5BU5D_t1281789340* G_B22_0 = NULL;
	List_1_t257213610 * G_B22_1 = NULL;
	StringU5BU5D_t1281789340* G_B22_2 = NULL;
	String_t* G_B22_3 = NULL;
	int32_t G_B24_0 = 0;
	StringU5BU5D_t1281789340* G_B24_1 = NULL;
	List_1_t257213610 * G_B24_2 = NULL;
	StringU5BU5D_t1281789340* G_B24_3 = NULL;
	String_t* G_B24_4 = NULL;
	{
		bool L_0 = ConsoleBase_Button_m1005519490(__this, _stringLiteral2523608186, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_003c;
		}
	}
	{
		il2cpp_codegen_initobj((&V_0), sizeof(Nullable_1_t378540539 ));
		Nullable_1_t378540539  L_1 = V_0;
		String_t* L_2 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		String_t* L_3 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		intptr_t L_4 = (intptr_t)MenuBase_HandleResult_m3006125243_RuntimeMethod_var;
		FacebookDelegate_1_t1976622014 * L_5 = (FacebookDelegate_1_t1976622014 *)il2cpp_codegen_object_new(FacebookDelegate_1_t1976622014_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m1320861630(L_5, __this, (intptr_t)L_4, /*hidden argument*/FacebookDelegate_1__ctor_m1320861630_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		FB_AppRequest_m781265255(NULL /*static, unused*/, _stringLiteral2394881543, (RuntimeObject*)NULL, (RuntimeObject*)NULL, (RuntimeObject*)NULL, L_1, L_2, L_3, L_5, /*hidden argument*/NULL);
	}

IL_003c:
	{
		bool L_6 = ConsoleBase_Button_m1005519490(__this, _stringLiteral415541884, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0088;
		}
	}
	{
		List_1_t257213610 * L_7 = (List_1_t257213610 *)il2cpp_codegen_object_new(List_1_t257213610_il2cpp_TypeInfo_var);
		List_1__ctor_m2321703786(L_7, /*hidden argument*/List_1__ctor_m2321703786_RuntimeMethod_var);
		V_2 = L_7;
		List_1_t257213610 * L_8 = V_2;
		NullCheck(L_8);
		List_1_Add_m3338814081(L_8, _stringLiteral1810187820, /*hidden argument*/List_1_Add_m3338814081_RuntimeMethod_var);
		List_1_t257213610 * L_9 = V_2;
		V_1 = L_9;
		List_1_t257213610 * L_10 = V_1;
		Nullable_1_t378540539  L_11;
		memset(&L_11, 0, sizeof(L_11));
		Nullable_1__ctor_m2962682148((&L_11), 0, /*hidden argument*/Nullable_1__ctor_m2962682148_RuntimeMethod_var);
		String_t* L_12 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		String_t* L_13 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		intptr_t L_14 = (intptr_t)MenuBase_HandleResult_m3006125243_RuntimeMethod_var;
		FacebookDelegate_1_t1976622014 * L_15 = (FacebookDelegate_1_t1976622014 *)il2cpp_codegen_object_new(FacebookDelegate_1_t1976622014_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m1320861630(L_15, __this, (intptr_t)L_14, /*hidden argument*/FacebookDelegate_1__ctor_m1320861630_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		FB_AppRequest_m781265255(NULL /*static, unused*/, _stringLiteral2394881543, (RuntimeObject*)NULL, L_10, (RuntimeObject*)NULL, L_11, L_12, L_13, L_15, /*hidden argument*/NULL);
	}

IL_0088:
	{
		bool L_16 = ConsoleBase_Button_m1005519490(__this, _stringLiteral2644898952, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_00d4;
		}
	}
	{
		List_1_t257213610 * L_17 = (List_1_t257213610 *)il2cpp_codegen_object_new(List_1_t257213610_il2cpp_TypeInfo_var);
		List_1__ctor_m2321703786(L_17, /*hidden argument*/List_1__ctor_m2321703786_RuntimeMethod_var);
		V_2 = L_17;
		List_1_t257213610 * L_18 = V_2;
		NullCheck(L_18);
		List_1_Add_m3338814081(L_18, _stringLiteral1346698490, /*hidden argument*/List_1_Add_m3338814081_RuntimeMethod_var);
		List_1_t257213610 * L_19 = V_2;
		V_3 = L_19;
		List_1_t257213610 * L_20 = V_3;
		Nullable_1_t378540539  L_21;
		memset(&L_21, 0, sizeof(L_21));
		Nullable_1__ctor_m2962682148((&L_21), 0, /*hidden argument*/Nullable_1__ctor_m2962682148_RuntimeMethod_var);
		String_t* L_22 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		String_t* L_23 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		intptr_t L_24 = (intptr_t)MenuBase_HandleResult_m3006125243_RuntimeMethod_var;
		FacebookDelegate_1_t1976622014 * L_25 = (FacebookDelegate_1_t1976622014 *)il2cpp_codegen_object_new(FacebookDelegate_1_t1976622014_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m1320861630(L_25, __this, (intptr_t)L_24, /*hidden argument*/FacebookDelegate_1__ctor_m1320861630_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		FB_AppRequest_m781265255(NULL /*static, unused*/, _stringLiteral2394881543, (RuntimeObject*)NULL, L_20, (RuntimeObject*)NULL, L_21, L_22, L_23, L_25, /*hidden argument*/NULL);
	}

IL_00d4:
	{
		String_t** L_26 = __this->get_address_of_requestMessage_16();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral1769972441, (String_t**)L_26, /*hidden argument*/NULL);
		String_t** L_27 = __this->get_address_of_requestTo_17();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral3978312045, (String_t**)L_27, /*hidden argument*/NULL);
		String_t** L_28 = __this->get_address_of_requestFilter_18();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral1207775305, (String_t**)L_28, /*hidden argument*/NULL);
		String_t** L_29 = __this->get_address_of_requestExcludes_19();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral1383326236, (String_t**)L_29, /*hidden argument*/NULL);
		String_t** L_30 = __this->get_address_of_requestExcludes_19();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral3787658585, (String_t**)L_30, /*hidden argument*/NULL);
		String_t** L_31 = __this->get_address_of_requestMax_20();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral4089792011, (String_t**)L_31, /*hidden argument*/NULL);
		String_t** L_32 = __this->get_address_of_requestData_21();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral3863765171, (String_t**)L_32, /*hidden argument*/NULL);
		String_t** L_33 = __this->get_address_of_requestTitle_22();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral1259398883, (String_t**)L_33, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2510215842* L_34 = Array_Empty_TisGUILayoutOption_t811797299_m4167206926(NULL /*static, unused*/, /*hidden argument*/Array_Empty_TisGUILayoutOption_t811797299_m4167206926_RuntimeMethod_var);
		GUILayout_BeginHorizontal_m1655989246(NULL /*static, unused*/, L_34, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_35 = ConsoleBase_get_LabelStyle_m3856255500(__this, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2510215842* L_36 = (GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)1);
		GUILayoutOptionU5BU5D_t2510215842* L_37 = L_36;
		float L_38 = ConsoleBase_get_ScaleFactor_m540546232(__this, /*hidden argument*/NULL);
		GUILayoutOption_t811797299 * L_39 = GUILayout_MaxWidth_m3297347705(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)(200.0f), (float)L_38)), /*hidden argument*/NULL);
		NullCheck(L_37);
		ArrayElementTypeCheck (L_37, L_39);
		(L_37)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t811797299 *)L_39);
		GUILayout_Label_m1096010274(NULL /*static, unused*/, _stringLiteral2599999525, L_35, L_37, /*hidden argument*/NULL);
		int32_t L_40 = __this->get_selectedAction_24();
		StringU5BU5D_t1281789340* L_41 = __this->get_actionTypeStrings_25();
		GUIStyle_t3956901511 * L_42 = ConsoleBase_get_ButtonStyle_m1393828074(__this, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2510215842* L_43 = (GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)2);
		GUILayoutOptionU5BU5D_t2510215842* L_44 = L_43;
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t1023975560_il2cpp_TypeInfo_var);
		int32_t L_45 = ConsoleBase_get_ButtonHeight_m1783046984(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_46 = ConsoleBase_get_ScaleFactor_m540546232(__this, /*hidden argument*/NULL);
		GUILayoutOption_t811797299 * L_47 = GUILayout_MinHeight_m98186407(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)(((float)((float)L_45))), (float)L_46)), /*hidden argument*/NULL);
		NullCheck(L_44);
		ArrayElementTypeCheck (L_44, L_47);
		(L_44)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t811797299 *)L_47);
		GUILayoutOptionU5BU5D_t2510215842* L_48 = L_44;
		int32_t L_49 = ConsoleBase_get_MainWindowWidth_m4254761416(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t811797299 * L_50 = GUILayout_MaxWidth_m3297347705(NULL /*static, unused*/, (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_49, (int32_t)((int32_t)150)))))), /*hidden argument*/NULL);
		NullCheck(L_48);
		ArrayElementTypeCheck (L_48, L_50);
		(L_48)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t811797299 *)L_50);
		int32_t L_51 = GUILayout_Toolbar_m4192068045(NULL /*static, unused*/, L_40, L_41, L_42, L_48, /*hidden argument*/NULL);
		__this->set_selectedAction_24(L_51);
		GUILayout_EndHorizontal_m125407884(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t** L_52 = __this->get_address_of_requestObjectID_23();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral1410652248, (String_t**)L_52, /*hidden argument*/NULL);
		bool L_53 = ConsoleBase_Button_m1005519490(__this, _stringLiteral606735498, /*hidden argument*/NULL);
		if (!L_53)
		{
			goto IL_034e;
		}
	}
	{
		Nullable_1_t2119916463  L_54 = AppRequests_GetSelectedOGActionType_m1566487639(__this, /*hidden argument*/NULL);
		V_4 = L_54;
		bool L_55 = Nullable_1_get_HasValue_m604914582((Nullable_1_t2119916463 *)(&V_4), /*hidden argument*/Nullable_1_get_HasValue_m604914582_RuntimeMethod_var);
		if (!L_55)
		{
			goto IL_0277;
		}
	}
	{
		String_t* L_56 = __this->get_requestMessage_16();
		int32_t L_57 = Nullable_1_get_Value_m1111532194((Nullable_1_t2119916463 *)(&V_4), /*hidden argument*/Nullable_1_get_Value_m1111532194_RuntimeMethod_var);
		String_t* L_58 = __this->get_requestObjectID_23();
		String_t* L_59 = __this->get_requestTo_17();
		bool L_60 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		G_B9_0 = L_58;
		G_B9_1 = L_57;
		G_B9_2 = L_56;
		if (!L_60)
		{
			G_B10_0 = L_58;
			G_B10_1 = L_57;
			G_B10_2 = L_56;
			goto IL_023f;
		}
	}
	{
		G_B11_0 = ((StringU5BU5D_t1281789340*)(NULL));
		G_B11_1 = G_B9_0;
		G_B11_2 = G_B9_1;
		G_B11_3 = G_B9_2;
		goto IL_0255;
	}

IL_023f:
	{
		String_t* L_61 = __this->get_requestTo_17();
		CharU5BU5D_t3528271667* L_62 = (CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t3528271667* L_63 = L_62;
		NullCheck(L_63);
		(L_63)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		NullCheck(L_61);
		StringU5BU5D_t1281789340* L_64 = String_Split_m3646115398(L_61, L_63, /*hidden argument*/NULL);
		G_B11_0 = L_64;
		G_B11_1 = G_B10_0;
		G_B11_2 = G_B10_1;
		G_B11_3 = G_B10_2;
	}

IL_0255:
	{
		String_t* L_65 = __this->get_requestData_21();
		String_t* L_66 = __this->get_requestTitle_22();
		intptr_t L_67 = (intptr_t)MenuBase_HandleResult_m3006125243_RuntimeMethod_var;
		FacebookDelegate_1_t1976622014 * L_68 = (FacebookDelegate_1_t1976622014 *)il2cpp_codegen_object_new(FacebookDelegate_1_t1976622014_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m1320861630(L_68, __this, (intptr_t)L_67, /*hidden argument*/FacebookDelegate_1__ctor_m1320861630_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		FB_AppRequest_m3170977800(NULL /*static, unused*/, G_B11_3, G_B11_2, G_B11_1, (RuntimeObject*)(RuntimeObject*)G_B11_0, L_65, L_66, L_68, /*hidden argument*/NULL);
		goto IL_034e;
	}

IL_0277:
	{
		String_t* L_69 = __this->get_requestMessage_16();
		String_t* L_70 = __this->get_requestTo_17();
		bool L_71 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_70, /*hidden argument*/NULL);
		G_B13_0 = L_69;
		if (!L_71)
		{
			G_B14_0 = L_69;
			goto IL_0293;
		}
	}
	{
		G_B15_0 = ((StringU5BU5D_t1281789340*)(NULL));
		G_B15_1 = G_B13_0;
		goto IL_02a9;
	}

IL_0293:
	{
		String_t* L_72 = __this->get_requestTo_17();
		CharU5BU5D_t3528271667* L_73 = (CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t3528271667* L_74 = L_73;
		NullCheck(L_74);
		(L_74)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		NullCheck(L_72);
		StringU5BU5D_t1281789340* L_75 = String_Split_m3646115398(L_72, L_74, /*hidden argument*/NULL);
		G_B15_0 = L_75;
		G_B15_1 = G_B14_0;
	}

IL_02a9:
	{
		String_t* L_76 = __this->get_requestFilter_18();
		bool L_77 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_76, /*hidden argument*/NULL);
		G_B16_0 = G_B15_0;
		G_B16_1 = G_B15_1;
		if (!L_77)
		{
			G_B17_0 = G_B15_0;
			G_B17_1 = G_B15_1;
			goto IL_02bf;
		}
	}
	{
		G_B18_0 = ((List_1_t257213610 *)(NULL));
		G_B18_1 = G_B16_0;
		G_B18_2 = G_B16_1;
		goto IL_02df;
	}

IL_02bf:
	{
		String_t* L_78 = __this->get_requestFilter_18();
		CharU5BU5D_t3528271667* L_79 = (CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t3528271667* L_80 = L_79;
		NullCheck(L_80);
		(L_80)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		NullCheck(L_78);
		StringU5BU5D_t1281789340* L_81 = String_Split_m3646115398(L_78, L_80, /*hidden argument*/NULL);
		RuntimeObject* L_82 = Enumerable_OfType_TisRuntimeObject_m444860550(NULL /*static, unused*/, (RuntimeObject*)(RuntimeObject*)L_81, /*hidden argument*/Enumerable_OfType_TisRuntimeObject_m444860550_RuntimeMethod_var);
		List_1_t257213610 * L_83 = Enumerable_ToList_TisRuntimeObject_m2366248129(NULL /*static, unused*/, L_82, /*hidden argument*/Enumerable_ToList_TisRuntimeObject_m2366248129_RuntimeMethod_var);
		G_B18_0 = L_83;
		G_B18_1 = G_B17_0;
		G_B18_2 = G_B17_1;
	}

IL_02df:
	{
		String_t* L_84 = __this->get_requestExcludes_19();
		bool L_85 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_84, /*hidden argument*/NULL);
		G_B19_0 = G_B18_0;
		G_B19_1 = G_B18_1;
		G_B19_2 = G_B18_2;
		if (!L_85)
		{
			G_B20_0 = G_B18_0;
			G_B20_1 = G_B18_1;
			G_B20_2 = G_B18_2;
			goto IL_02f5;
		}
	}
	{
		G_B21_0 = ((StringU5BU5D_t1281789340*)(NULL));
		G_B21_1 = G_B19_0;
		G_B21_2 = G_B19_1;
		G_B21_3 = G_B19_2;
		goto IL_030b;
	}

IL_02f5:
	{
		String_t* L_86 = __this->get_requestExcludes_19();
		CharU5BU5D_t3528271667* L_87 = (CharU5BU5D_t3528271667*)SZArrayNew(CharU5BU5D_t3528271667_il2cpp_TypeInfo_var, (uint32_t)1);
		CharU5BU5D_t3528271667* L_88 = L_87;
		NullCheck(L_88);
		(L_88)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppChar)((int32_t)44));
		NullCheck(L_86);
		StringU5BU5D_t1281789340* L_89 = String_Split_m3646115398(L_86, L_88, /*hidden argument*/NULL);
		G_B21_0 = L_89;
		G_B21_1 = G_B20_0;
		G_B21_2 = G_B20_1;
		G_B21_3 = G_B20_2;
	}

IL_030b:
	{
		String_t* L_90 = __this->get_requestMax_20();
		bool L_91 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_90, /*hidden argument*/NULL);
		G_B22_0 = G_B21_0;
		G_B22_1 = G_B21_1;
		G_B22_2 = G_B21_2;
		G_B22_3 = G_B21_3;
		if (!L_91)
		{
			G_B23_0 = G_B21_0;
			G_B23_1 = G_B21_1;
			G_B23_2 = G_B21_2;
			G_B23_3 = G_B21_3;
			goto IL_0321;
		}
	}
	{
		G_B24_0 = 0;
		G_B24_1 = G_B22_0;
		G_B24_2 = G_B22_1;
		G_B24_3 = G_B22_2;
		G_B24_4 = G_B22_3;
		goto IL_032c;
	}

IL_0321:
	{
		String_t* L_92 = __this->get_requestMax_20();
		int32_t L_93 = Int32_Parse_m1033611559(NULL /*static, unused*/, L_92, /*hidden argument*/NULL);
		G_B24_0 = L_93;
		G_B24_1 = G_B23_0;
		G_B24_2 = G_B23_1;
		G_B24_3 = G_B23_2;
		G_B24_4 = G_B23_3;
	}

IL_032c:
	{
		Nullable_1_t378540539  L_94;
		memset(&L_94, 0, sizeof(L_94));
		Nullable_1__ctor_m2962682148((&L_94), G_B24_0, /*hidden argument*/Nullable_1__ctor_m2962682148_RuntimeMethod_var);
		String_t* L_95 = __this->get_requestData_21();
		String_t* L_96 = __this->get_requestTitle_22();
		intptr_t L_97 = (intptr_t)MenuBase_HandleResult_m3006125243_RuntimeMethod_var;
		FacebookDelegate_1_t1976622014 * L_98 = (FacebookDelegate_1_t1976622014 *)il2cpp_codegen_object_new(FacebookDelegate_1_t1976622014_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m1320861630(L_98, __this, (intptr_t)L_97, /*hidden argument*/FacebookDelegate_1__ctor_m1320861630_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		FB_AppRequest_m781265255(NULL /*static, unused*/, G_B24_4, (RuntimeObject*)(RuntimeObject*)G_B24_3, G_B24_2, (RuntimeObject*)(RuntimeObject*)G_B24_1, L_94, L_95, L_96, L_98, /*hidden argument*/NULL);
	}

IL_034e:
	{
		return;
	}
}
// System.Nullable`1<Facebook.Unity.OGActionType> Facebook.Unity.Example.AppRequests::GetSelectedOGActionType()
extern "C" IL2CPP_METHOD_ATTR Nullable_1_t2119916463  AppRequests_GetSelectedOGActionType_m1566487639 (AppRequests_t2419817778 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (AppRequests_GetSelectedOGActionType_m1566487639_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	Nullable_1_t2119916463  V_4;
	memset(&V_4, 0, sizeof(V_4));
	{
		StringU5BU5D_t1281789340* L_0 = __this->get_actionTypeStrings_25();
		int32_t L_1 = __this->get_selectedAction_24();
		NullCheck(L_0);
		int32_t L_2 = L_1;
		String_t* L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_0 = L_3;
		String_t* L_4 = V_0;
		V_1 = 0;
		RuntimeObject * L_5 = Box(OGActionType_t397354381_il2cpp_TypeInfo_var, (&V_1));
		NullCheck(L_5);
		String_t* L_6 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_5);
		V_1 = *(int32_t*)UnBox(L_5);
		bool L_7 = String_op_Equality_m920492651(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_002f;
		}
	}
	{
		Nullable_1_t2119916463  L_8;
		memset(&L_8, 0, sizeof(L_8));
		Nullable_1__ctor_m2532889892((&L_8), 0, /*hidden argument*/Nullable_1__ctor_m2532889892_RuntimeMethod_var);
		return L_8;
	}

IL_002f:
	{
		String_t* L_9 = V_0;
		V_2 = 1;
		RuntimeObject * L_10 = Box(OGActionType_t397354381_il2cpp_TypeInfo_var, (&V_2));
		NullCheck(L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_10);
		V_2 = *(int32_t*)UnBox(L_10);
		bool L_12 = String_op_Equality_m920492651(NULL /*static, unused*/, L_9, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0050;
		}
	}
	{
		Nullable_1_t2119916463  L_13;
		memset(&L_13, 0, sizeof(L_13));
		Nullable_1__ctor_m2532889892((&L_13), 1, /*hidden argument*/Nullable_1__ctor_m2532889892_RuntimeMethod_var);
		return L_13;
	}

IL_0050:
	{
		String_t* L_14 = V_0;
		V_3 = 2;
		RuntimeObject * L_15 = Box(OGActionType_t397354381_il2cpp_TypeInfo_var, (&V_3));
		NullCheck(L_15);
		String_t* L_16 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_15);
		V_3 = *(int32_t*)UnBox(L_15);
		bool L_17 = String_op_Equality_m920492651(NULL /*static, unused*/, L_14, L_16, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_0071;
		}
	}
	{
		Nullable_1_t2119916463  L_18;
		memset(&L_18, 0, sizeof(L_18));
		Nullable_1__ctor_m2532889892((&L_18), 2, /*hidden argument*/Nullable_1__ctor_m2532889892_RuntimeMethod_var);
		return L_18;
	}

IL_0071:
	{
		il2cpp_codegen_initobj((&V_4), sizeof(Nullable_1_t2119916463 ));
		Nullable_1_t2119916463  L_19 = V_4;
		return L_19;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.Unity.Example.ConsoleBase::.ctor()
extern "C" IL2CPP_METHOD_ATTR void ConsoleBase__ctor_m3547073371 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase__ctor_m3547073371_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_status_6(_stringLiteral3524439568);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_lastResponse_7(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Vector2_t2156229523_il2cpp_TypeInfo_var);
		Vector2_t2156229523  L_1 = Vector2_get_zero_m540426400(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_scrollPosition_8(L_1);
		MonoBehaviour__ctor_m1579109191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 Facebook.Unity.Example.ConsoleBase::get_ButtonHeight()
extern "C" IL2CPP_METHOD_ATTR int32_t ConsoleBase_get_ButtonHeight_m1783046984 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = Constants_get_IsMobile_m316667921(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		G_B3_0 = ((int32_t)60);
		goto IL_0013;
	}

IL_0011:
	{
		G_B3_0 = ((int32_t)24);
	}

IL_0013:
	{
		return G_B3_0;
	}
}
// System.Int32 Facebook.Unity.Example.ConsoleBase::get_MainWindowWidth()
extern "C" IL2CPP_METHOD_ATTR int32_t ConsoleBase_get_MainWindowWidth_m4254761416 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = Constants_get_IsMobile_m316667921(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)((int32_t)30)));
		goto IL_001c;
	}

IL_0017:
	{
		G_B3_0 = ((int32_t)700);
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// System.Int32 Facebook.Unity.Example.ConsoleBase::get_MainWindowFullWidth()
extern "C" IL2CPP_METHOD_ATTR int32_t ConsoleBase_get_MainWindowFullWidth_m476492915 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = Constants_get_IsMobile_m316667921(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_0019;
	}

IL_0014:
	{
		G_B3_0 = ((int32_t)760);
	}

IL_0019:
	{
		return G_B3_0;
	}
}
// System.Int32 Facebook.Unity.Example.ConsoleBase::get_MarginFix()
extern "C" IL2CPP_METHOD_ATTR int32_t ConsoleBase_get_MarginFix_m3965407477 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	int32_t G_B3_0 = 0;
	{
		bool L_0 = Constants_get_IsMobile_m316667921(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0010;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0012;
	}

IL_0010:
	{
		G_B3_0 = ((int32_t)48);
	}

IL_0012:
	{
		return G_B3_0;
	}
}
// System.Collections.Generic.Stack`1<System.String> Facebook.Unity.Example.ConsoleBase::get_MenuStack()
extern "C" IL2CPP_METHOD_ATTR Stack_1_t2690840144 * ConsoleBase_get_MenuStack_m1135196678 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_get_MenuStack_m1135196678_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t1023975560_il2cpp_TypeInfo_var);
		Stack_1_t2690840144 * L_0 = ((ConsoleBase_t1023975560_StaticFields*)il2cpp_codegen_static_fields_for(ConsoleBase_t1023975560_il2cpp_TypeInfo_var))->get_menuStack_5();
		return L_0;
	}
}
// System.Void Facebook.Unity.Example.ConsoleBase::set_MenuStack(System.Collections.Generic.Stack`1<System.String>)
extern "C" IL2CPP_METHOD_ATTR void ConsoleBase_set_MenuStack_m582115117 (RuntimeObject * __this /* static, unused */, Stack_1_t2690840144 * ___value0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_set_MenuStack_m582115117_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stack_1_t2690840144 * L_0 = ___value0;
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t1023975560_il2cpp_TypeInfo_var);
		((ConsoleBase_t1023975560_StaticFields*)il2cpp_codegen_static_fields_for(ConsoleBase_t1023975560_il2cpp_TypeInfo_var))->set_menuStack_5(L_0);
		return;
	}
}
// System.String Facebook.Unity.Example.ConsoleBase::get_Status()
extern "C" IL2CPP_METHOD_ATTR String_t* ConsoleBase_get_Status_m2983343794 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_status_6();
		return L_0;
	}
}
// System.Void Facebook.Unity.Example.ConsoleBase::set_Status(System.String)
extern "C" IL2CPP_METHOD_ATTR void ConsoleBase_set_Status_m4147399384 (ConsoleBase_t1023975560 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_status_6(L_0);
		return;
	}
}
// UnityEngine.Texture2D Facebook.Unity.Example.ConsoleBase::get_LastResponseTexture()
extern "C" IL2CPP_METHOD_ATTR Texture2D_t3840446185 * ConsoleBase_get_LastResponseTexture_m4282689491 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method)
{
	{
		Texture2D_t3840446185 * L_0 = __this->get_U3CLastResponseTextureU3Ek__BackingField_14();
		return L_0;
	}
}
// System.Void Facebook.Unity.Example.ConsoleBase::set_LastResponseTexture(UnityEngine.Texture2D)
extern "C" IL2CPP_METHOD_ATTR void ConsoleBase_set_LastResponseTexture_m2611436616 (ConsoleBase_t1023975560 * __this, Texture2D_t3840446185 * ___value0, const RuntimeMethod* method)
{
	{
		Texture2D_t3840446185 * L_0 = ___value0;
		__this->set_U3CLastResponseTextureU3Ek__BackingField_14(L_0);
		return;
	}
}
// System.String Facebook.Unity.Example.ConsoleBase::get_LastResponse()
extern "C" IL2CPP_METHOD_ATTR String_t* ConsoleBase_get_LastResponse_m2081918938 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = __this->get_lastResponse_7();
		return L_0;
	}
}
// System.Void Facebook.Unity.Example.ConsoleBase::set_LastResponse(System.String)
extern "C" IL2CPP_METHOD_ATTR void ConsoleBase_set_LastResponse_m3622711220 (ConsoleBase_t1023975560 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_lastResponse_7(L_0);
		return;
	}
}
// UnityEngine.Vector2 Facebook.Unity.Example.ConsoleBase::get_ScrollPosition()
extern "C" IL2CPP_METHOD_ATTR Vector2_t2156229523  ConsoleBase_get_ScrollPosition_m2128236017 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method)
{
	{
		Vector2_t2156229523  L_0 = __this->get_scrollPosition_8();
		return L_0;
	}
}
// System.Void Facebook.Unity.Example.ConsoleBase::set_ScrollPosition(UnityEngine.Vector2)
extern "C" IL2CPP_METHOD_ATTR void ConsoleBase_set_ScrollPosition_m589751691 (ConsoleBase_t1023975560 * __this, Vector2_t2156229523  ___value0, const RuntimeMethod* method)
{
	{
		Vector2_t2156229523  L_0 = ___value0;
		__this->set_scrollPosition_8(L_0);
		return;
	}
}
// System.Single Facebook.Unity.Example.ConsoleBase::get_ScaleFactor()
extern "C" IL2CPP_METHOD_ATTR float ConsoleBase_get_ScaleFactor_m540546232 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_get_ScaleFactor_m540546232_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Nullable_1_t3119828856 * L_0 = __this->get_address_of_scaleFactor_9();
		bool L_1 = Nullable_1_get_HasValue_m2149791491((Nullable_1_t3119828856 *)L_0, /*hidden argument*/Nullable_1_get_HasValue_m2149791491_RuntimeMethod_var);
		if (L_1)
		{
			goto IL_0026;
		}
	}
	{
		float L_2 = Screen_get_dpi_m495672463(NULL /*static, unused*/, /*hidden argument*/NULL);
		Nullable_1_t3119828856  L_3;
		memset(&L_3, 0, sizeof(L_3));
		Nullable_1__ctor_m3964206222((&L_3), ((float)((float)L_2/(float)(160.0f))), /*hidden argument*/Nullable_1__ctor_m3964206222_RuntimeMethod_var);
		__this->set_scaleFactor_9(L_3);
	}

IL_0026:
	{
		Nullable_1_t3119828856 * L_4 = __this->get_address_of_scaleFactor_9();
		float L_5 = Nullable_1_get_Value_m4168550405((Nullable_1_t3119828856 *)L_4, /*hidden argument*/Nullable_1_get_Value_m4168550405_RuntimeMethod_var);
		return L_5;
	}
}
// System.Int32 Facebook.Unity.Example.ConsoleBase::get_FontSize()
extern "C" IL2CPP_METHOD_ATTR int32_t ConsoleBase_get_FontSize_m754853133 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_get_FontSize_m754853133_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		float L_0 = ConsoleBase_get_ScaleFactor_m540546232(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Math_t1671470975_il2cpp_TypeInfo_var);
		double L_1 = bankers_round((((double)((double)((float)il2cpp_codegen_multiply((float)L_0, (float)(16.0f)))))));
		return (((int32_t)((int32_t)L_1)));
	}
}
// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_TextStyle()
extern "C" IL2CPP_METHOD_ATTR GUIStyle_t3956901511 * ConsoleBase_get_TextStyle_m2535314378 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_get_TextStyle_m2535314378_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIStyle_t3956901511 * L_0 = __this->get_textStyle_10();
		if (L_0)
		{
			goto IL_0079;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUISkin_t1244372282 * L_1 = GUI_get_skin_m1874615010(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyle_t3956901511 * L_2 = GUISkin_get_textArea_m1928475264(L_1, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_3 = (GUIStyle_t3956901511 *)il2cpp_codegen_object_new(GUIStyle_t3956901511_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2912682974(L_3, L_2, /*hidden argument*/NULL);
		__this->set_textStyle_10(L_3);
		GUIStyle_t3956901511 * L_4 = __this->get_textStyle_10();
		NullCheck(L_4);
		GUIStyle_set_alignment_m3944619660(L_4, 0, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_5 = __this->get_textStyle_10();
		NullCheck(L_5);
		GUIStyle_set_wordWrap_m1419501823(L_5, (bool)1, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_6 = __this->get_textStyle_10();
		RectOffset_t1369453676 * L_7 = (RectOffset_t1369453676 *)il2cpp_codegen_object_new(RectOffset_t1369453676_il2cpp_TypeInfo_var);
		RectOffset__ctor_m732140021(L_7, ((int32_t)10), ((int32_t)10), ((int32_t)10), ((int32_t)10), /*hidden argument*/NULL);
		NullCheck(L_6);
		GUIStyle_set_padding_m3302456044(L_6, L_7, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_8 = __this->get_textStyle_10();
		NullCheck(L_8);
		GUIStyle_set_stretchHeight_m3442828884(L_8, (bool)1, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_9 = __this->get_textStyle_10();
		NullCheck(L_9);
		GUIStyle_set_stretchWidth_m2564034386(L_9, (bool)0, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_10 = __this->get_textStyle_10();
		int32_t L_11 = ConsoleBase_get_FontSize_m754853133(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		GUIStyle_set_fontSize_m1566850023(L_10, L_11, /*hidden argument*/NULL);
	}

IL_0079:
	{
		GUIStyle_t3956901511 * L_12 = __this->get_textStyle_10();
		return L_12;
	}
}
// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_ButtonStyle()
extern "C" IL2CPP_METHOD_ATTR GUIStyle_t3956901511 * ConsoleBase_get_ButtonStyle_m1393828074 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_get_ButtonStyle_m1393828074_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIStyle_t3956901511 * L_0 = __this->get_buttonStyle_11();
		if (L_0)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUISkin_t1244372282 * L_1 = GUI_get_skin_m1874615010(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyle_t3956901511 * L_2 = GUISkin_get_button_m2792560996(L_1, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_3 = (GUIStyle_t3956901511 *)il2cpp_codegen_object_new(GUIStyle_t3956901511_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2912682974(L_3, L_2, /*hidden argument*/NULL);
		__this->set_buttonStyle_11(L_3);
		GUIStyle_t3956901511 * L_4 = __this->get_buttonStyle_11();
		int32_t L_5 = ConsoleBase_get_FontSize_m754853133(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_set_fontSize_m1566850023(L_4, L_5, /*hidden argument*/NULL);
	}

IL_0031:
	{
		GUIStyle_t3956901511 * L_6 = __this->get_buttonStyle_11();
		return L_6;
	}
}
// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_TextInputStyle()
extern "C" IL2CPP_METHOD_ATTR GUIStyle_t3956901511 * ConsoleBase_get_TextInputStyle_m927335950 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_get_TextInputStyle_m927335950_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIStyle_t3956901511 * L_0 = __this->get_textInputStyle_12();
		if (L_0)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUISkin_t1244372282 * L_1 = GUI_get_skin_m1874615010(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyle_t3956901511 * L_2 = GUISkin_get_textField_m1918733755(L_1, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_3 = (GUIStyle_t3956901511 *)il2cpp_codegen_object_new(GUIStyle_t3956901511_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2912682974(L_3, L_2, /*hidden argument*/NULL);
		__this->set_textInputStyle_12(L_3);
		GUIStyle_t3956901511 * L_4 = __this->get_textInputStyle_12();
		int32_t L_5 = ConsoleBase_get_FontSize_m754853133(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_set_fontSize_m1566850023(L_4, L_5, /*hidden argument*/NULL);
	}

IL_0031:
	{
		GUIStyle_t3956901511 * L_6 = __this->get_textInputStyle_12();
		return L_6;
	}
}
// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::get_LabelStyle()
extern "C" IL2CPP_METHOD_ATTR GUIStyle_t3956901511 * ConsoleBase_get_LabelStyle_m3856255500 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_get_LabelStyle_m3856255500_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUIStyle_t3956901511 * L_0 = __this->get_labelStyle_13();
		if (L_0)
		{
			goto IL_0031;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUISkin_t1244372282 * L_1 = GUI_get_skin_m1874615010(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_1);
		GUIStyle_t3956901511 * L_2 = GUISkin_get_label_m1693050720(L_1, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_3 = (GUIStyle_t3956901511 *)il2cpp_codegen_object_new(GUIStyle_t3956901511_il2cpp_TypeInfo_var);
		GUIStyle__ctor_m2912682974(L_3, L_2, /*hidden argument*/NULL);
		__this->set_labelStyle_13(L_3);
		GUIStyle_t3956901511 * L_4 = __this->get_labelStyle_13();
		int32_t L_5 = ConsoleBase_get_FontSize_m754853133(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		GUIStyle_set_fontSize_m1566850023(L_4, L_5, /*hidden argument*/NULL);
	}

IL_0031:
	{
		GUIStyle_t3956901511 * L_6 = __this->get_labelStyle_13();
		return L_6;
	}
}
// System.Void Facebook.Unity.Example.ConsoleBase::Awake()
extern "C" IL2CPP_METHOD_ATTR void ConsoleBase_Awake_m3771769456 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method)
{
	{
		Application_set_targetFrameRate_m3682352535(NULL /*static, unused*/, ((int32_t)60), /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Facebook.Unity.Example.ConsoleBase::Button(System.String)
extern "C" IL2CPP_METHOD_ATTR bool ConsoleBase_Button_m1005519490 (ConsoleBase_t1023975560 * __this, String_t* ___label0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_Button_m1005519490_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ___label0;
		GUIStyle_t3956901511 * L_1 = ConsoleBase_get_ButtonStyle_m1393828074(__this, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2510215842* L_2 = (GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)2);
		GUILayoutOptionU5BU5D_t2510215842* L_3 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t1023975560_il2cpp_TypeInfo_var);
		int32_t L_4 = ConsoleBase_get_ButtonHeight_m1783046984(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_5 = ConsoleBase_get_ScaleFactor_m540546232(__this, /*hidden argument*/NULL);
		GUILayoutOption_t811797299 * L_6 = GUILayout_MinHeight_m98186407(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)(((float)((float)L_4))), (float)L_5)), /*hidden argument*/NULL);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t811797299 *)L_6);
		GUILayoutOptionU5BU5D_t2510215842* L_7 = L_3;
		int32_t L_8 = ConsoleBase_get_MainWindowWidth_m4254761416(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t811797299 * L_9 = GUILayout_MaxWidth_m3297347705(NULL /*static, unused*/, (((float)((float)L_8))), /*hidden argument*/NULL);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_9);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t811797299 *)L_9);
		bool L_10 = GUILayout_Button_m3728892478(NULL /*static, unused*/, L_0, L_1, L_7, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void Facebook.Unity.Example.ConsoleBase::LabelAndTextField(System.String,System.String&)
extern "C" IL2CPP_METHOD_ATTR void ConsoleBase_LabelAndTextField_m2312391562 (ConsoleBase_t1023975560 * __this, String_t* ___label0, String_t** ___text1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_LabelAndTextField_m2312391562_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUILayoutOptionU5BU5D_t2510215842* L_0 = Array_Empty_TisGUILayoutOption_t811797299_m4167206926(NULL /*static, unused*/, /*hidden argument*/Array_Empty_TisGUILayoutOption_t811797299_m4167206926_RuntimeMethod_var);
		GUILayout_BeginHorizontal_m1655989246(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		String_t* L_1 = ___label0;
		GUIStyle_t3956901511 * L_2 = ConsoleBase_get_LabelStyle_m3856255500(__this, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2510215842* L_3 = (GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)1);
		GUILayoutOptionU5BU5D_t2510215842* L_4 = L_3;
		float L_5 = ConsoleBase_get_ScaleFactor_m540546232(__this, /*hidden argument*/NULL);
		GUILayoutOption_t811797299 * L_6 = GUILayout_MaxWidth_m3297347705(NULL /*static, unused*/, ((float)il2cpp_codegen_multiply((float)(200.0f), (float)L_5)), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_6);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t811797299 *)L_6);
		GUILayout_Label_m1096010274(NULL /*static, unused*/, L_1, L_2, L_4, /*hidden argument*/NULL);
		String_t** L_7 = ___text1;
		String_t** L_8 = ___text1;
		String_t* L_9 = *((String_t**)L_8);
		GUIStyle_t3956901511 * L_10 = ConsoleBase_get_TextInputStyle_m927335950(__this, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2510215842* L_11 = (GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)1);
		GUILayoutOptionU5BU5D_t2510215842* L_12 = L_11;
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t1023975560_il2cpp_TypeInfo_var);
		int32_t L_13 = ConsoleBase_get_MainWindowWidth_m4254761416(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t811797299 * L_14 = GUILayout_MaxWidth_m3297347705(NULL /*static, unused*/, (((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_13, (int32_t)((int32_t)150)))))), /*hidden argument*/NULL);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t811797299 *)L_14);
		String_t* L_15 = GUILayout_TextField_m4150770174(NULL /*static, unused*/, L_9, L_10, L_12, /*hidden argument*/NULL);
		*((RuntimeObject **)L_7) = (RuntimeObject *)L_15;
		Il2CppCodeGenWriteBarrier((RuntimeObject **)L_7, (RuntimeObject *)L_15);
		GUILayout_EndHorizontal_m125407884(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Facebook.Unity.Example.ConsoleBase::IsHorizontalLayout()
extern "C" IL2CPP_METHOD_ATTR bool ConsoleBase_IsHorizontalLayout_m525810539 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = Screen_get_orientation_m3354122719(NULL /*static, unused*/, /*hidden argument*/NULL);
		return (bool)((((int32_t)L_0) == ((int32_t)3))? 1 : 0);
	}
}
// System.Void Facebook.Unity.Example.ConsoleBase::SwitchMenu(System.Type)
extern "C" IL2CPP_METHOD_ATTR void ConsoleBase_SwitchMenu_m676980302 (ConsoleBase_t1023975560 * __this, Type_t * ___menuClass0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_SwitchMenu_m676980302_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t1023975560_il2cpp_TypeInfo_var);
		Stack_1_t2690840144 * L_0 = ((ConsoleBase_t1023975560_StaticFields*)il2cpp_codegen_static_fields_for(ConsoleBase_t1023975560_il2cpp_TypeInfo_var))->get_menuStack_5();
		Type_t * L_1 = Object_GetType_m88164663(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, L_1);
		NullCheck(L_0);
		Stack_1_Push_m559717952(L_0, L_2, /*hidden argument*/Stack_1_Push_m559717952_RuntimeMethod_var);
		Type_t * L_3 = ___menuClass0;
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, L_3);
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.ConsoleBase::GoBack()
extern "C" IL2CPP_METHOD_ATTR void ConsoleBase_GoBack_m3535004784 (ConsoleBase_t1023975560 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase_GoBack_m3535004784_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t1023975560_il2cpp_TypeInfo_var);
		Stack_1_t2690840144 * L_0 = ((ConsoleBase_t1023975560_StaticFields*)il2cpp_codegen_static_fields_for(ConsoleBase_t1023975560_il2cpp_TypeInfo_var))->get_menuStack_5();
		bool L_1 = Enumerable_Any_TisString_t_m3986012742(NULL /*static, unused*/, L_0, /*hidden argument*/Enumerable_Any_TisString_t_m3986012742_RuntimeMethod_var);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t1023975560_il2cpp_TypeInfo_var);
		Stack_1_t2690840144 * L_2 = ((ConsoleBase_t1023975560_StaticFields*)il2cpp_codegen_static_fields_for(ConsoleBase_t1023975560_il2cpp_TypeInfo_var))->get_menuStack_5();
		NullCheck(L_2);
		String_t* L_3 = Stack_1_Pop_m812987992(L_2, /*hidden argument*/Stack_1_Pop_m812987992_RuntimeMethod_var);
		SceneManager_LoadScene_m1758133949(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
	}

IL_001e:
	{
		return;
	}
}
// System.Void Facebook.Unity.Example.ConsoleBase::.cctor()
extern "C" IL2CPP_METHOD_ATTR void ConsoleBase__cctor_m1709056498 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ConsoleBase__cctor_m1709056498_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Stack_1_t2690840144 * L_0 = (Stack_1_t2690840144 *)il2cpp_codegen_object_new(Stack_1_t2690840144_il2cpp_TypeInfo_var);
		Stack_1__ctor_m2088216795(L_0, /*hidden argument*/Stack_1__ctor_m2088216795_RuntimeMethod_var);
		((ConsoleBase_t1023975560_StaticFields*)il2cpp_codegen_static_fields_for(ConsoleBase_t1023975560_il2cpp_TypeInfo_var))->set_menuStack_5(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.Unity.Example.DialogShare::.ctor()
extern "C" IL2CPP_METHOD_ATTR void DialogShare__ctor_m3148804403 (DialogShare_t2334043085 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DialogShare__ctor_m3148804403_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		__this->set_shareLink_16(_stringLiteral733908546);
		__this->set_shareTitle_17(_stringLiteral677797808);
		__this->set_shareDescription_18(_stringLiteral828973377);
		__this->set_shareImage_19(_stringLiteral4160356986);
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_feedTo_20(L_0);
		__this->set_feedLink_21(_stringLiteral733908546);
		__this->set_feedTitle_22(_stringLiteral2854633673);
		__this->set_feedCaption_23(_stringLiteral2487819760);
		__this->set_feedDescription_24(_stringLiteral1235538921);
		__this->set_feedImage_25(_stringLiteral135812671);
		String_t* L_1 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_feedMediaSource_26(L_1);
		MenuBase__ctor_m2160361082(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Facebook.Unity.Example.DialogShare::ShowDialogModeSelector()
extern "C" IL2CPP_METHOD_ATTR bool DialogShare_ShowDialogModeSelector_m1708058086 (DialogShare_t2334043085 * __this, const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
// System.Void Facebook.Unity.Example.DialogShare::GetGui()
extern "C" IL2CPP_METHOD_ATTR void DialogShare_GetGui_m2888532382 (DialogShare_t2334043085 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (DialogShare_GetGui_m2888532382_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B11_0 = 0;
	int32_t G_B13_0 = 0;
	int32_t G_B15_0 = 0;
	String_t* G_B20_0 = NULL;
	String_t* G_B19_0 = NULL;
	Uri_t100236324 * G_B21_0 = NULL;
	String_t* G_B21_1 = NULL;
	String_t* G_B23_0 = NULL;
	String_t* G_B23_1 = NULL;
	String_t* G_B23_2 = NULL;
	Uri_t100236324 * G_B23_3 = NULL;
	String_t* G_B23_4 = NULL;
	String_t* G_B22_0 = NULL;
	String_t* G_B22_1 = NULL;
	String_t* G_B22_2 = NULL;
	Uri_t100236324 * G_B22_3 = NULL;
	String_t* G_B22_4 = NULL;
	Uri_t100236324 * G_B24_0 = NULL;
	String_t* G_B24_1 = NULL;
	String_t* G_B24_2 = NULL;
	String_t* G_B24_3 = NULL;
	Uri_t100236324 * G_B24_4 = NULL;
	String_t* G_B24_5 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		bool L_0 = GUI_get_enabled_m1448901857(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = ConsoleBase_Button_m1005519490(__this, _stringLiteral4147269477, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_003c;
		}
	}
	{
		Uri_t100236324 * L_2 = (Uri_t100236324 *)il2cpp_codegen_object_new(Uri_t100236324_il2cpp_TypeInfo_var);
		Uri__ctor_m800430703(L_2, _stringLiteral733908546, /*hidden argument*/NULL);
		String_t* L_3 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		String_t* L_4 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		intptr_t L_5 = (intptr_t)MenuBase_HandleResult_m3006125243_RuntimeMethod_var;
		FacebookDelegate_1_t602110559 * L_6 = (FacebookDelegate_1_t602110559 *)il2cpp_codegen_object_new(FacebookDelegate_1_t602110559_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m3417605933(L_6, __this, (intptr_t)L_5, /*hidden argument*/FacebookDelegate_1__ctor_m3417605933_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		FB_ShareLink_m3039827802(NULL /*static, unused*/, L_2, L_3, L_4, (Uri_t100236324 *)NULL, L_6, /*hidden argument*/NULL);
	}

IL_003c:
	{
		bool L_7 = ConsoleBase_Button_m1005519490(__this, _stringLiteral3858022679, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_007b;
		}
	}
	{
		Uri_t100236324 * L_8 = (Uri_t100236324 *)il2cpp_codegen_object_new(Uri_t100236324_il2cpp_TypeInfo_var);
		Uri__ctor_m800430703(L_8, _stringLiteral733908546, /*hidden argument*/NULL);
		Uri_t100236324 * L_9 = (Uri_t100236324 *)il2cpp_codegen_object_new(Uri_t100236324_il2cpp_TypeInfo_var);
		Uri__ctor_m800430703(L_9, _stringLiteral4160356986, /*hidden argument*/NULL);
		intptr_t L_10 = (intptr_t)MenuBase_HandleResult_m3006125243_RuntimeMethod_var;
		FacebookDelegate_1_t602110559 * L_11 = (FacebookDelegate_1_t602110559 *)il2cpp_codegen_object_new(FacebookDelegate_1_t602110559_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m3417605933(L_11, __this, (intptr_t)L_10, /*hidden argument*/FacebookDelegate_1__ctor_m3417605933_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		FB_ShareLink_m3039827802(NULL /*static, unused*/, L_8, _stringLiteral379382575, _stringLiteral917394094, L_9, L_11, /*hidden argument*/NULL);
	}

IL_007b:
	{
		String_t** L_12 = __this->get_address_of_shareLink_16();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral408850744, (String_t**)L_12, /*hidden argument*/NULL);
		String_t** L_13 = __this->get_address_of_shareTitle_17();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral3963994475, (String_t**)L_13, /*hidden argument*/NULL);
		String_t** L_14 = __this->get_address_of_shareDescription_18();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral2833616318, (String_t**)L_14, /*hidden argument*/NULL);
		String_t** L_15 = __this->get_address_of_shareImage_19();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral820751583, (String_t**)L_15, /*hidden argument*/NULL);
		bool L_16 = ConsoleBase_Button_m1005519490(__this, _stringLiteral450383487, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0102;
		}
	}
	{
		String_t* L_17 = __this->get_shareLink_16();
		Uri_t100236324 * L_18 = (Uri_t100236324 *)il2cpp_codegen_object_new(Uri_t100236324_il2cpp_TypeInfo_var);
		Uri__ctor_m800430703(L_18, L_17, /*hidden argument*/NULL);
		String_t* L_19 = __this->get_shareTitle_17();
		String_t* L_20 = __this->get_shareDescription_18();
		String_t* L_21 = __this->get_shareImage_19();
		Uri_t100236324 * L_22 = (Uri_t100236324 *)il2cpp_codegen_object_new(Uri_t100236324_il2cpp_TypeInfo_var);
		Uri__ctor_m800430703(L_22, L_21, /*hidden argument*/NULL);
		intptr_t L_23 = (intptr_t)MenuBase_HandleResult_m3006125243_RuntimeMethod_var;
		FacebookDelegate_1_t602110559 * L_24 = (FacebookDelegate_1_t602110559 *)il2cpp_codegen_object_new(FacebookDelegate_1_t602110559_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m3417605933(L_24, __this, (intptr_t)L_23, /*hidden argument*/FacebookDelegate_1__ctor_m3417605933_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		FB_ShareLink_m3039827802(NULL /*static, unused*/, L_18, L_19, L_20, L_22, L_24, /*hidden argument*/NULL);
	}

IL_0102:
	{
		bool L_25 = V_0;
		if (!L_25)
		{
			goto IL_0129;
		}
	}
	{
		bool L_26 = Constants_get_IsEditor_m1554664543(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_0126;
		}
	}
	{
		bool L_27 = Constants_get_IsEditor_m1554664543(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_0123;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		bool L_28 = FB_get_IsLoggedIn_m2875052829(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B11_0 = ((int32_t)(L_28));
		goto IL_0124;
	}

IL_0123:
	{
		G_B11_0 = 0;
	}

IL_0124:
	{
		G_B13_0 = G_B11_0;
		goto IL_0127;
	}

IL_0126:
	{
		G_B13_0 = 1;
	}

IL_0127:
	{
		G_B15_0 = G_B13_0;
		goto IL_012a;
	}

IL_0129:
	{
		G_B15_0 = 0;
	}

IL_012a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_enabled_m3922032131(NULL /*static, unused*/, (bool)G_B15_0, /*hidden argument*/NULL);
		bool L_29 = ConsoleBase_Button_m1005519490(__this, _stringLiteral4052139789, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_017d;
		}
	}
	{
		String_t* L_30 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		Uri_t100236324 * L_31 = (Uri_t100236324 *)il2cpp_codegen_object_new(Uri_t100236324_il2cpp_TypeInfo_var);
		Uri__ctor_m800430703(L_31, _stringLiteral733908546, /*hidden argument*/NULL);
		Uri_t100236324 * L_32 = (Uri_t100236324 *)il2cpp_codegen_object_new(Uri_t100236324_il2cpp_TypeInfo_var);
		Uri__ctor_m800430703(L_32, _stringLiteral135812671, /*hidden argument*/NULL);
		String_t* L_33 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		intptr_t L_34 = (intptr_t)MenuBase_HandleResult_m3006125243_RuntimeMethod_var;
		FacebookDelegate_1_t602110559 * L_35 = (FacebookDelegate_1_t602110559 *)il2cpp_codegen_object_new(FacebookDelegate_1_t602110559_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m3417605933(L_35, __this, (intptr_t)L_34, /*hidden argument*/FacebookDelegate_1__ctor_m3417605933_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		FB_FeedShare_m1574327390(NULL /*static, unused*/, L_30, L_31, _stringLiteral2854633673, _stringLiteral2552831472, _stringLiteral1235538921, L_32, L_33, L_35, /*hidden argument*/NULL);
	}

IL_017d:
	{
		String_t** L_36 = __this->get_address_of_feedTo_20();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral3454777324, (String_t**)L_36, /*hidden argument*/NULL);
		String_t** L_37 = __this->get_address_of_feedLink_21();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral408850744, (String_t**)L_37, /*hidden argument*/NULL);
		String_t** L_38 = __this->get_address_of_feedTitle_22();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral3963994475, (String_t**)L_38, /*hidden argument*/NULL);
		String_t** L_39 = __this->get_address_of_feedCaption_23();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral3247624226, (String_t**)L_39, /*hidden argument*/NULL);
		String_t** L_40 = __this->get_address_of_feedDescription_24();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral2833616318, (String_t**)L_40, /*hidden argument*/NULL);
		String_t** L_41 = __this->get_address_of_feedImage_25();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral820751583, (String_t**)L_41, /*hidden argument*/NULL);
		String_t** L_42 = __this->get_address_of_feedMediaSource_26();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral2965269087, (String_t**)L_42, /*hidden argument*/NULL);
		bool L_43 = ConsoleBase_Button_m1005519490(__this, _stringLiteral3369767408, /*hidden argument*/NULL);
		if (!L_43)
		{
			goto IL_0275;
		}
	}
	{
		String_t* L_44 = __this->get_feedTo_20();
		String_t* L_45 = __this->get_feedLink_21();
		bool L_46 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_45, /*hidden argument*/NULL);
		G_B19_0 = L_44;
		if (!L_46)
		{
			G_B20_0 = L_44;
			goto IL_0220;
		}
	}
	{
		G_B21_0 = ((Uri_t100236324 *)(NULL));
		G_B21_1 = G_B19_0;
		goto IL_022b;
	}

IL_0220:
	{
		String_t* L_47 = __this->get_feedLink_21();
		Uri_t100236324 * L_48 = (Uri_t100236324 *)il2cpp_codegen_object_new(Uri_t100236324_il2cpp_TypeInfo_var);
		Uri__ctor_m800430703(L_48, L_47, /*hidden argument*/NULL);
		G_B21_0 = L_48;
		G_B21_1 = G_B20_0;
	}

IL_022b:
	{
		String_t* L_49 = __this->get_feedTitle_22();
		String_t* L_50 = __this->get_feedCaption_23();
		String_t* L_51 = __this->get_feedDescription_24();
		String_t* L_52 = __this->get_feedImage_25();
		bool L_53 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		G_B22_0 = L_51;
		G_B22_1 = L_50;
		G_B22_2 = L_49;
		G_B22_3 = G_B21_0;
		G_B22_4 = G_B21_1;
		if (!L_53)
		{
			G_B23_0 = L_51;
			G_B23_1 = L_50;
			G_B23_2 = L_49;
			G_B23_3 = G_B21_0;
			G_B23_4 = G_B21_1;
			goto IL_0253;
		}
	}
	{
		G_B24_0 = ((Uri_t100236324 *)(NULL));
		G_B24_1 = G_B22_0;
		G_B24_2 = G_B22_1;
		G_B24_3 = G_B22_2;
		G_B24_4 = G_B22_3;
		G_B24_5 = G_B22_4;
		goto IL_025e;
	}

IL_0253:
	{
		String_t* L_54 = __this->get_feedImage_25();
		Uri_t100236324 * L_55 = (Uri_t100236324 *)il2cpp_codegen_object_new(Uri_t100236324_il2cpp_TypeInfo_var);
		Uri__ctor_m800430703(L_55, L_54, /*hidden argument*/NULL);
		G_B24_0 = L_55;
		G_B24_1 = G_B23_0;
		G_B24_2 = G_B23_1;
		G_B24_3 = G_B23_2;
		G_B24_4 = G_B23_3;
		G_B24_5 = G_B23_4;
	}

IL_025e:
	{
		String_t* L_56 = __this->get_feedMediaSource_26();
		intptr_t L_57 = (intptr_t)MenuBase_HandleResult_m3006125243_RuntimeMethod_var;
		FacebookDelegate_1_t602110559 * L_58 = (FacebookDelegate_1_t602110559 *)il2cpp_codegen_object_new(FacebookDelegate_1_t602110559_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m3417605933(L_58, __this, (intptr_t)L_57, /*hidden argument*/FacebookDelegate_1__ctor_m3417605933_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		FB_FeedShare_m1574327390(NULL /*static, unused*/, G_B24_5, G_B24_4, G_B24_3, G_B24_2, G_B24_1, G_B24_0, L_56, L_58, /*hidden argument*/NULL);
	}

IL_0275:
	{
		bool L_59 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_enabled_m3922032131(NULL /*static, unused*/, L_59, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.Unity.Example.GraphRequest::.ctor()
extern "C" IL2CPP_METHOD_ATTR void GraphRequest__ctor_m89463010 (GraphRequest_t4047451309 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GraphRequest__ctor_m89463010_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_apiQuery_16(L_0);
		MenuBase__ctor_m2160361082(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.GraphRequest::GetGui()
extern "C" IL2CPP_METHOD_ATTR void GraphRequest_GetGui_m1928235709 (GraphRequest_t4047451309 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GraphRequest_GetGui_m1928235709_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		bool L_0 = GUI_get_enabled_m1448901857(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		bool L_2 = FB_get_IsLoggedIn_m2875052829(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B3_0 = ((int32_t)(L_2));
		goto IL_0014;
	}

IL_0013:
	{
		G_B3_0 = 0;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_enabled_m3922032131(NULL /*static, unused*/, (bool)G_B3_0, /*hidden argument*/NULL);
		bool L_3 = ConsoleBase_Button_m1005519490(__this, _stringLiteral3024588073, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0041;
		}
	}
	{
		intptr_t L_4 = (intptr_t)MenuBase_HandleResult_m3006125243_RuntimeMethod_var;
		FacebookDelegate_1_t138390958 * L_5 = (FacebookDelegate_1_t138390958 *)il2cpp_codegen_object_new(FacebookDelegate_1_t138390958_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m411964393(L_5, __this, (intptr_t)L_4, /*hidden argument*/FacebookDelegate_1__ctor_m411964393_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		FB_API_m2949212365(NULL /*static, unused*/, _stringLiteral1700118802, 0, L_5, (RuntimeObject*)NULL, /*hidden argument*/NULL);
	}

IL_0041:
	{
		bool L_6 = ConsoleBase_Button_m1005519490(__this, _stringLiteral626585464, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0069;
		}
	}
	{
		intptr_t L_7 = (intptr_t)GraphRequest_ProfilePhotoCallback_m2406981014_RuntimeMethod_var;
		FacebookDelegate_1_t138390958 * L_8 = (FacebookDelegate_1_t138390958 *)il2cpp_codegen_object_new(FacebookDelegate_1_t138390958_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m411964393(L_8, __this, (intptr_t)L_7, /*hidden argument*/FacebookDelegate_1__ctor_m411964393_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		FB_API_m2949212365(NULL /*static, unused*/, _stringLiteral3357641048, 0, L_8, (RuntimeObject*)NULL, /*hidden argument*/NULL);
	}

IL_0069:
	{
		bool L_9 = ConsoleBase_Button_m1005519490(__this, _stringLiteral3329508946, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0086;
		}
	}
	{
		RuntimeObject* L_10 = GraphRequest_TakeScreenshot_m1191488447(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m3411253000(__this, L_10, /*hidden argument*/NULL);
	}

IL_0086:
	{
		String_t** L_11 = __this->get_address_of_apiQuery_16();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral475705030, (String_t**)L_11, /*hidden argument*/NULL);
		bool L_12 = ConsoleBase_Button_m1005519490(__this, _stringLiteral1674951175, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00c0;
		}
	}
	{
		String_t* L_13 = __this->get_apiQuery_16();
		intptr_t L_14 = (intptr_t)MenuBase_HandleResult_m3006125243_RuntimeMethod_var;
		FacebookDelegate_1_t138390958 * L_15 = (FacebookDelegate_1_t138390958 *)il2cpp_codegen_object_new(FacebookDelegate_1_t138390958_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m411964393(L_15, __this, (intptr_t)L_14, /*hidden argument*/FacebookDelegate_1__ctor_m411964393_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		FB_API_m2949212365(NULL /*static, unused*/, L_13, 0, L_15, (RuntimeObject*)NULL, /*hidden argument*/NULL);
	}

IL_00c0:
	{
		Texture2D_t3840446185 * L_16 = __this->get_profilePic_17();
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_17 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_16, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_00e1;
		}
	}
	{
		Texture2D_t3840446185 * L_18 = __this->get_profilePic_17();
		GUILayoutOptionU5BU5D_t2510215842* L_19 = Array_Empty_TisGUILayoutOption_t811797299_m4167206926(NULL /*static, unused*/, /*hidden argument*/Array_Empty_TisGUILayoutOption_t811797299_m4167206926_RuntimeMethod_var);
		GUILayout_Box_m1016637907(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
	}

IL_00e1:
	{
		bool L_20 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_enabled_m3922032131(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.GraphRequest::ProfilePhotoCallback(Facebook.Unity.IGraphResult)
extern "C" IL2CPP_METHOD_ATTR void GraphRequest_ProfilePhotoCallback_m2406981014 (GraphRequest_t4047451309 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GraphRequest_ProfilePhotoCallback_m2406981014_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___result0;
		NullCheck(L_0);
		String_t* L_1 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Facebook.Unity.IResult::get_Error() */, IResult_t2818782343_il2cpp_TypeInfo_var, L_0);
		bool L_2 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_002d;
		}
	}
	{
		RuntimeObject* L_3 = ___result0;
		NullCheck(L_3);
		Texture2D_t3840446185 * L_4 = InterfaceFuncInvoker0< Texture2D_t3840446185 * >::Invoke(0 /* UnityEngine.Texture2D Facebook.Unity.IGraphResult::get_Texture() */, IGraphResult_t2315406351_il2cpp_TypeInfo_var, L_3);
		IL2CPP_RUNTIME_CLASS_INIT(Object_t631007953_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m4071470834(NULL /*static, unused*/, L_4, (Object_t631007953 *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002d;
		}
	}
	{
		RuntimeObject* L_6 = ___result0;
		NullCheck(L_6);
		Texture2D_t3840446185 * L_7 = InterfaceFuncInvoker0< Texture2D_t3840446185 * >::Invoke(0 /* UnityEngine.Texture2D Facebook.Unity.IGraphResult::get_Texture() */, IGraphResult_t2315406351_il2cpp_TypeInfo_var, L_6);
		__this->set_profilePic_17(L_7);
	}

IL_002d:
	{
		RuntimeObject* L_8 = ___result0;
		MenuBase_HandleResult_m3006125243(__this, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Facebook.Unity.Example.GraphRequest::TakeScreenshot()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* GraphRequest_TakeScreenshot_m1191488447 (GraphRequest_t4047451309 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (GraphRequest_TakeScreenshot_m1191488447_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CTakeScreenshotU3Ec__Iterator0_t3544038915 * V_0 = NULL;
	{
		U3CTakeScreenshotU3Ec__Iterator0_t3544038915 * L_0 = (U3CTakeScreenshotU3Ec__Iterator0_t3544038915 *)il2cpp_codegen_object_new(U3CTakeScreenshotU3Ec__Iterator0_t3544038915_il2cpp_TypeInfo_var);
		U3CTakeScreenshotU3Ec__Iterator0__ctor_m88611805(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CTakeScreenshotU3Ec__Iterator0_t3544038915 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U24this_5(__this);
		U3CTakeScreenshotU3Ec__Iterator0_t3544038915 * L_2 = V_0;
		return L_2;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CTakeScreenshotU3Ec__Iterator0__ctor_m88611805 (U3CTakeScreenshotU3Ec__Iterator0_t3544038915 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::MoveNext()
extern "C" IL2CPP_METHOD_ATTR bool U3CTakeScreenshotU3Ec__Iterator0_MoveNext_m4252001862 (U3CTakeScreenshotU3Ec__Iterator0_t3544038915 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CTakeScreenshotU3Ec__Iterator0_MoveNext_m4252001862_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_U24PC_8();
		V_0 = L_0;
		__this->set_U24PC_8((-1));
		uint32_t L_1 = V_0;
		switch (L_1)
		{
			case 0:
			{
				goto IL_0021;
			}
			case 1:
			{
				goto IL_0040;
			}
		}
	}
	{
		goto IL_0119;
	}

IL_0021:
	{
		WaitForEndOfFrame_t1314943911 * L_2 = (WaitForEndOfFrame_t1314943911 *)il2cpp_codegen_object_new(WaitForEndOfFrame_t1314943911_il2cpp_TypeInfo_var);
		WaitForEndOfFrame__ctor_m1381314187(L_2, /*hidden argument*/NULL);
		__this->set_U24current_6(L_2);
		bool L_3 = __this->get_U24disposing_7();
		if (L_3)
		{
			goto IL_003b;
		}
	}
	{
		__this->set_U24PC_8(1);
	}

IL_003b:
	{
		goto IL_011b;
	}

IL_0040:
	{
		int32_t L_4 = Screen_get_width_m345039817(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CwidthU3E__0_0(L_4);
		int32_t L_5 = Screen_get_height_m1623532518(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_U3CheightU3E__0_1(L_5);
		int32_t L_6 = __this->get_U3CwidthU3E__0_0();
		int32_t L_7 = __this->get_U3CheightU3E__0_1();
		Texture2D_t3840446185 * L_8 = (Texture2D_t3840446185 *)il2cpp_codegen_object_new(Texture2D_t3840446185_il2cpp_TypeInfo_var);
		Texture2D__ctor_m2862217990(L_8, L_6, L_7, 3, (bool)0, /*hidden argument*/NULL);
		__this->set_U3CtexU3E__0_2(L_8);
		Texture2D_t3840446185 * L_9 = __this->get_U3CtexU3E__0_2();
		int32_t L_10 = __this->get_U3CwidthU3E__0_0();
		int32_t L_11 = __this->get_U3CheightU3E__0_1();
		Rect_t2360479859  L_12;
		memset(&L_12, 0, sizeof(L_12));
		Rect__ctor_m2614021312((&L_12), (0.0f), (0.0f), (((float)((float)L_10))), (((float)((float)L_11))), /*hidden argument*/NULL);
		NullCheck(L_9);
		Texture2D_ReadPixels_m3395504488(L_9, L_12, 0, 0, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_13 = __this->get_U3CtexU3E__0_2();
		NullCheck(L_13);
		Texture2D_Apply_m2271746283(L_13, /*hidden argument*/NULL);
		Texture2D_t3840446185 * L_14 = __this->get_U3CtexU3E__0_2();
		ByteU5BU5D_t4116647657* L_15 = ImageConversion_EncodeToPNG_m2292631531(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		__this->set_U3CscreenshotU3E__0_3(L_15);
		WWWForm_t4064702195 * L_16 = (WWWForm_t4064702195 *)il2cpp_codegen_object_new(WWWForm_t4064702195_il2cpp_TypeInfo_var);
		WWWForm__ctor_m2465700452(L_16, /*hidden argument*/NULL);
		__this->set_U3CwwwFormU3E__0_4(L_16);
		WWWForm_t4064702195 * L_17 = __this->get_U3CwwwFormU3E__0_4();
		ByteU5BU5D_t4116647657* L_18 = __this->get_U3CscreenshotU3E__0_3();
		NullCheck(L_17);
		WWWForm_AddBinaryData_m344280980(L_17, _stringLiteral820750591, L_18, _stringLiteral1458937931, /*hidden argument*/NULL);
		WWWForm_t4064702195 * L_19 = __this->get_U3CwwwFormU3E__0_4();
		NullCheck(L_19);
		WWWForm_AddField_m2357902982(L_19, _stringLiteral3253941996, _stringLiteral568801581, /*hidden argument*/NULL);
		GraphRequest_t4047451309 * L_20 = __this->get_U24this_5();
		intptr_t L_21 = (intptr_t)MenuBase_HandleResult_m3006125243_RuntimeMethod_var;
		FacebookDelegate_1_t138390958 * L_22 = (FacebookDelegate_1_t138390958 *)il2cpp_codegen_object_new(FacebookDelegate_1_t138390958_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m411964393(L_22, L_20, (intptr_t)L_21, /*hidden argument*/FacebookDelegate_1__ctor_m411964393_RuntimeMethod_var);
		WWWForm_t4064702195 * L_23 = __this->get_U3CwwwFormU3E__0_4();
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		FB_API_m1010533072(NULL /*static, unused*/, _stringLiteral80284664, 1, L_22, L_23, /*hidden argument*/NULL);
		__this->set_U24PC_8((-1));
	}

IL_0119:
	{
		return (bool)0;
	}

IL_011b:
	{
		return (bool)1;
	}
}
// System.Object Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CTakeScreenshotU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1741985507 (U3CTakeScreenshotU3Ec__Iterator0_t3544038915 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Object Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CTakeScreenshotU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m3041941269 (U3CTakeScreenshotU3Ec__Iterator0_t3544038915 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U24current_6();
		return L_0;
	}
}
// System.Void Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::Dispose()
extern "C" IL2CPP_METHOD_ATTR void U3CTakeScreenshotU3Ec__Iterator0_Dispose_m4180331554 (U3CTakeScreenshotU3Ec__Iterator0_t3544038915 * __this, const RuntimeMethod* method)
{
	{
		__this->set_U24disposing_7((bool)1);
		__this->set_U24PC_8((-1));
		return;
	}
}
// System.Void Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::Reset()
extern "C" IL2CPP_METHOD_ATTR void U3CTakeScreenshotU3Ec__Iterator0_Reset_m262479894 (U3CTakeScreenshotU3Ec__Iterator0_t3544038915 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CTakeScreenshotU3Ec__Iterator0_Reset_m262479894_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NotSupportedException_t1314879016 * L_0 = (NotSupportedException_t1314879016 *)il2cpp_codegen_object_new(NotSupportedException_t1314879016_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m2730133172(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, NULL, U3CTakeScreenshotU3Ec__Iterator0_Reset_m262479894_RuntimeMethod_var);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.Unity.Example.LogView::.ctor()
extern "C" IL2CPP_METHOD_ATTR void LogView__ctor_m298494391 (LogView_t1067263371 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LogView__ctor_m298494391_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t1023975560_il2cpp_TypeInfo_var);
		ConsoleBase__ctor_m3547073371(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.LogView::AddLog(System.String)
extern "C" IL2CPP_METHOD_ATTR void LogView_AddLog_m1637420510 (RuntimeObject * __this /* static, unused */, String_t* ___log0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LogView_AddLog_m1637420510_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DateTime_t3738529785  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		IL2CPP_RUNTIME_CLASS_INIT(LogView_t1067263371_il2cpp_TypeInfo_var);
		RuntimeObject* L_0 = ((LogView_t1067263371_StaticFields*)il2cpp_codegen_static_fields_for(LogView_t1067263371_il2cpp_TypeInfo_var))->get_events_16();
		IL2CPP_RUNTIME_CLASS_INIT(DateTime_t3738529785_il2cpp_TypeInfo_var);
		DateTime_t3738529785  L_1 = DateTime_get_Now_m1277138875(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = ((LogView_t1067263371_StaticFields*)il2cpp_codegen_static_fields_for(LogView_t1067263371_il2cpp_TypeInfo_var))->get_datePatt_15();
		String_t* L_3 = DateTime_ToString_m3718521780((DateTime_t3738529785 *)(&V_0), L_2, /*hidden argument*/NULL);
		String_t* L_4 = ___log0;
		String_t* L_5 = String_Format_m2556382932(NULL /*static, unused*/, _stringLiteral1499488298, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		InterfaceActionInvoker2< int32_t, String_t* >::Invoke(3 /* System.Void System.Collections.Generic.IList`1<System.String>::Insert(System.Int32,!0) */, IList_1_t3662770472_il2cpp_TypeInfo_var, L_0, 0, L_5);
		return;
	}
}
// System.Void Facebook.Unity.Example.LogView::OnGUI()
extern "C" IL2CPP_METHOD_ATTR void LogView_OnGUI_m1199105350 (LogView_t1067263371 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LogView_OnGUI_m1199105350_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t1921856868  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Touch_t1921856868  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		GUILayoutOptionU5BU5D_t2510215842* L_0 = Array_Empty_TisGUILayoutOption_t811797299_m4167206926(NULL /*static, unused*/, /*hidden argument*/Array_Empty_TisGUILayoutOption_t811797299_m4167206926_RuntimeMethod_var);
		GUILayout_BeginVertical_m1213265852(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		bool L_1 = ConsoleBase_Button_m1005519490(__this, _stringLiteral1167841337, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0020;
		}
	}
	{
		ConsoleBase_GoBack_m3535004784(__this, /*hidden argument*/NULL);
	}

IL_0020:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		int32_t L_2 = Input_get_touchCount_m3403849067(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_2) <= ((int32_t)0)))
		{
			goto IL_0071;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Touch_t1921856868  L_3 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_3;
		int32_t L_4 = Touch_get_phase_m214549210((Touch_t1921856868 *)(&V_0), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_0071;
		}
	}
	{
		Vector2_t2156229523  L_5 = ConsoleBase_get_ScrollPosition_m2128236017(__this, /*hidden argument*/NULL);
		V_1 = L_5;
		Vector2_t2156229523 * L_6 = (&V_1);
		float L_7 = L_6->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Touch_t1921856868  L_8 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_2 = L_8;
		Vector2_t2156229523  L_9 = Touch_get_deltaPosition_m2389653382((Touch_t1921856868 *)(&V_2), /*hidden argument*/NULL);
		V_3 = L_9;
		float L_10 = (&V_3)->get_y_1();
		L_6->set_y_1(((float)il2cpp_codegen_add((float)L_7, (float)L_10)));
		Vector2_t2156229523  L_11 = V_1;
		ConsoleBase_set_ScrollPosition_m589751691(__this, L_11, /*hidden argument*/NULL);
	}

IL_0071:
	{
		Vector2_t2156229523  L_12 = ConsoleBase_get_ScrollPosition_m2128236017(__this, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2510215842* L_13 = (GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)1);
		GUILayoutOptionU5BU5D_t2510215842* L_14 = L_13;
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t1023975560_il2cpp_TypeInfo_var);
		int32_t L_15 = ConsoleBase_get_MainWindowFullWidth_m476492915(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t811797299 * L_16 = GUILayout_MinWidth_m3524212012(NULL /*static, unused*/, (((float)((float)L_15))), /*hidden argument*/NULL);
		NullCheck(L_14);
		ArrayElementTypeCheck (L_14, L_16);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t811797299 *)L_16);
		Vector2_t2156229523  L_17 = GUILayout_BeginScrollView_m3542895037(NULL /*static, unused*/, L_12, L_14, /*hidden argument*/NULL);
		ConsoleBase_set_ScrollPosition_m589751691(__this, L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LogView_t1067263371_il2cpp_TypeInfo_var);
		RuntimeObject* L_18 = ((LogView_t1067263371_StaticFields*)il2cpp_codegen_static_fields_for(LogView_t1067263371_il2cpp_TypeInfo_var))->get_events_16();
		StringU5BU5D_t1281789340* L_19 = Enumerable_ToArray_TisString_t_m2924696684(NULL /*static, unused*/, L_18, /*hidden argument*/Enumerable_ToArray_TisString_t_m2924696684_RuntimeMethod_var);
		String_t* L_20 = String_Join_m2050845953(NULL /*static, unused*/, _stringLiteral3452614566, L_19, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_21 = ConsoleBase_get_TextStyle_m2535314378(__this, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2510215842* L_22 = (GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)2);
		GUILayoutOptionU5BU5D_t2510215842* L_23 = L_22;
		GUILayoutOption_t811797299 * L_24 = GUILayout_ExpandHeight_m4126348219(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_24);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t811797299 *)L_24);
		GUILayoutOptionU5BU5D_t2510215842* L_25 = L_23;
		int32_t L_26 = ConsoleBase_get_MainWindowWidth_m4254761416(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t811797299 * L_27 = GUILayout_MaxWidth_m3297347705(NULL /*static, unused*/, (((float)((float)L_26))), /*hidden argument*/NULL);
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, L_27);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(1), (GUILayoutOption_t811797299 *)L_27);
		GUILayout_TextArea_m894334590(NULL /*static, unused*/, L_20, L_21, L_25, /*hidden argument*/NULL);
		GUILayout_EndScrollView_m1561653937(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndVertical_m3051805938(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.LogView::.cctor()
extern "C" IL2CPP_METHOD_ATTR void LogView__cctor_m45639004 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (LogView__cctor_m45639004_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((LogView_t1067263371_StaticFields*)il2cpp_codegen_static_fields_for(LogView_t1067263371_il2cpp_TypeInfo_var))->set_datePatt_15(_stringLiteral116104668);
		List_1_t3319525431 * L_0 = (List_1_t3319525431 *)il2cpp_codegen_object_new(List_1_t3319525431_il2cpp_TypeInfo_var);
		List_1__ctor_m706204246(L_0, /*hidden argument*/List_1__ctor_m706204246_RuntimeMethod_var);
		((LogView_t1067263371_StaticFields*)il2cpp_codegen_static_fields_for(LogView_t1067263371_il2cpp_TypeInfo_var))->set_events_16(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.Unity.Example.MainMenu::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MainMenu__ctor_m3805200730 (MainMenu_t1823058806 * __this, const RuntimeMethod* method)
{
	{
		MenuBase__ctor_m2160361082(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Facebook.Unity.Example.MainMenu::ShowBackButton()
extern "C" IL2CPP_METHOD_ATTR bool MainMenu_ShowBackButton_m4258292914 (MainMenu_t1823058806 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Void Facebook.Unity.Example.MainMenu::GetGui()
extern "C" IL2CPP_METHOD_ATTR void MainMenu_GetGui_m3329753249 (MainMenu_t1823058806 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_GetGui_m3329753249_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	int32_t G_B14_0 = 0;
	{
		GUILayoutOptionU5BU5D_t2510215842* L_0 = Array_Empty_TisGUILayoutOption_t811797299_m4167206926(NULL /*static, unused*/, /*hidden argument*/Array_Empty_TisGUILayoutOption_t811797299_m4167206926_RuntimeMethod_var);
		GUILayout_BeginVertical_m1213265852(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		bool L_1 = GUI_get_enabled_m1448901857(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = ConsoleBase_Button_m1005519490(__this, _stringLiteral243311952, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0053;
		}
	}
	{
		intptr_t L_3 = (intptr_t)MainMenu_OnInitComplete_m4103279806_RuntimeMethod_var;
		InitDelegate_t3081360126 * L_4 = (InitDelegate_t3081360126 *)il2cpp_codegen_object_new(InitDelegate_t3081360126_il2cpp_TypeInfo_var);
		InitDelegate__ctor_m556099265(L_4, __this, (intptr_t)L_3, /*hidden argument*/NULL);
		intptr_t L_5 = (intptr_t)MainMenu_OnHideUnity_m3704770818_RuntimeMethod_var;
		HideUnityDelegate_t1353799728 * L_6 = (HideUnityDelegate_t1353799728 *)il2cpp_codegen_object_new(HideUnityDelegate_t1353799728_il2cpp_TypeInfo_var);
		HideUnityDelegate__ctor_m1747604723(L_6, __this, (intptr_t)L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		FB_Init_m1856465676(NULL /*static, unused*/, L_4, L_6, (String_t*)NULL, /*hidden argument*/NULL);
		String_t* L_7 = FB_get_AppId_m2803391701(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_8 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3070279316, L_7, /*hidden argument*/NULL);
		ConsoleBase_set_Status_m4147399384(__this, L_8, /*hidden argument*/NULL);
	}

IL_0053:
	{
		GUILayoutOptionU5BU5D_t2510215842* L_9 = Array_Empty_TisGUILayoutOption_t811797299_m4167206926(NULL /*static, unused*/, /*hidden argument*/Array_Empty_TisGUILayoutOption_t811797299_m4167206926_RuntimeMethod_var);
		GUILayout_BeginHorizontal_m1655989246(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		bool L_10 = V_0;
		if (!L_10)
		{
			goto IL_006a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		bool L_11 = FB_get_IsInitialized_m3789500881(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_11));
		goto IL_006b;
	}

IL_006a:
	{
		G_B5_0 = 0;
	}

IL_006b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_enabled_m3922032131(NULL /*static, unused*/, (bool)G_B5_0, /*hidden argument*/NULL);
		bool L_12 = ConsoleBase_Button_m1005519490(__this, _stringLiteral2686216175, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0091;
		}
	}
	{
		MainMenu_CallFBLogin_m2730593439(__this, /*hidden argument*/NULL);
		ConsoleBase_set_Status_m4147399384(__this, _stringLiteral3464325523, /*hidden argument*/NULL);
	}

IL_0091:
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		bool L_13 = FB_get_IsLoggedIn_m2875052829(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_enabled_m3922032131(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		bool L_14 = ConsoleBase_Button_m1005519490(__this, _stringLiteral1606485075, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00bc;
		}
	}
	{
		MainMenu_CallFBLoginForPublish_m2374054384(__this, /*hidden argument*/NULL);
		ConsoleBase_set_Status_m4147399384(__this, _stringLiteral1833358689, /*hidden argument*/NULL);
	}

IL_00bc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t3050628031_il2cpp_TypeInfo_var);
		GUIContent_t3050628031 * L_15 = ((GUIContent_t3050628031_StaticFields*)il2cpp_codegen_static_fields_for(GUIContent_t3050628031_il2cpp_TypeInfo_var))->get_none_6();
		GUILayoutOptionU5BU5D_t2510215842* L_16 = (GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)1);
		GUILayoutOptionU5BU5D_t2510215842* L_17 = L_16;
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t1023975560_il2cpp_TypeInfo_var);
		int32_t L_18 = ConsoleBase_get_MarginFix_m3965407477(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t811797299 * L_19 = GUILayout_MinWidth_m3524212012(NULL /*static, unused*/, (((float)((float)L_18))), /*hidden argument*/NULL);
		NullCheck(L_17);
		ArrayElementTypeCheck (L_17, L_19);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t811797299 *)L_19);
		GUILayout_Label_m1547104774(NULL /*static, unused*/, L_15, L_17, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m125407884(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2510215842* L_20 = Array_Empty_TisGUILayoutOption_t811797299_m4167206926(NULL /*static, unused*/, /*hidden argument*/Array_Empty_TisGUILayoutOption_t811797299_m4167206926_RuntimeMethod_var);
		GUILayout_BeginHorizontal_m1655989246(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		GUIContent_t3050628031 * L_21 = ((GUIContent_t3050628031_StaticFields*)il2cpp_codegen_static_fields_for(GUIContent_t3050628031_il2cpp_TypeInfo_var))->get_none_6();
		GUILayoutOptionU5BU5D_t2510215842* L_22 = (GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)1);
		GUILayoutOptionU5BU5D_t2510215842* L_23 = L_22;
		int32_t L_24 = ConsoleBase_get_MarginFix_m3965407477(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t811797299 * L_25 = GUILayout_MinWidth_m3524212012(NULL /*static, unused*/, (((float)((float)L_24))), /*hidden argument*/NULL);
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_25);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t811797299 *)L_25);
		GUILayout_Label_m1547104774(NULL /*static, unused*/, L_21, L_23, /*hidden argument*/NULL);
		GUILayout_EndHorizontal_m125407884(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_26 = ConsoleBase_Button_m1005519490(__this, _stringLiteral1535797236, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_012d;
		}
	}
	{
		MainMenu_CallFBLogout_m4130237067(__this, /*hidden argument*/NULL);
		ConsoleBase_set_Status_m4147399384(__this, _stringLiteral2455296183, /*hidden argument*/NULL);
	}

IL_012d:
	{
		bool L_27 = V_0;
		if (!L_27)
		{
			goto IL_013a;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		bool L_28 = FB_get_IsInitialized_m3789500881(NULL /*static, unused*/, /*hidden argument*/NULL);
		G_B14_0 = ((int32_t)(L_28));
		goto IL_013b;
	}

IL_013a:
	{
		G_B14_0 = 0;
	}

IL_013b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_enabled_m3922032131(NULL /*static, unused*/, (bool)G_B14_0, /*hidden argument*/NULL);
		bool L_29 = ConsoleBase_Button_m1005519490(__this, _stringLiteral841429450, /*hidden argument*/NULL);
		if (!L_29)
		{
			goto IL_0160;
		}
	}
	{
		RuntimeTypeHandle_t3027515415  L_30 = { reinterpret_cast<intptr_t> (DialogShare_t2334043085_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_31 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		ConsoleBase_SwitchMenu_m676980302(__this, L_31, /*hidden argument*/NULL);
	}

IL_0160:
	{
		bool L_32 = ConsoleBase_Button_m1005519490(__this, _stringLiteral4249307141, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0180;
		}
	}
	{
		RuntimeTypeHandle_t3027515415  L_33 = { reinterpret_cast<intptr_t> (AppRequests_t2419817778_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_34 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		ConsoleBase_SwitchMenu_m676980302(__this, L_34, /*hidden argument*/NULL);
	}

IL_0180:
	{
		bool L_35 = ConsoleBase_Button_m1005519490(__this, _stringLiteral1405573206, /*hidden argument*/NULL);
		if (!L_35)
		{
			goto IL_01a0;
		}
	}
	{
		RuntimeTypeHandle_t3027515415  L_36 = { reinterpret_cast<intptr_t> (GraphRequest_t4047451309_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_37 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		ConsoleBase_SwitchMenu_m676980302(__this, L_37, /*hidden argument*/NULL);
	}

IL_01a0:
	{
		bool L_38 = Constants_get_IsWeb_m1986090831(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_38)
		{
			goto IL_01ca;
		}
	}
	{
		bool L_39 = ConsoleBase_Button_m1005519490(__this, _stringLiteral2600534157, /*hidden argument*/NULL);
		if (!L_39)
		{
			goto IL_01ca;
		}
	}
	{
		RuntimeTypeHandle_t3027515415  L_40 = { reinterpret_cast<intptr_t> (Pay_t1264260185_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_41 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_40, /*hidden argument*/NULL);
		ConsoleBase_SwitchMenu_m676980302(__this, L_41, /*hidden argument*/NULL);
	}

IL_01ca:
	{
		bool L_42 = ConsoleBase_Button_m1005519490(__this, _stringLiteral640607579, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_01ea;
		}
	}
	{
		RuntimeTypeHandle_t3027515415  L_43 = { reinterpret_cast<intptr_t> (AppEvents_t3645639549_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_44 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_43, /*hidden argument*/NULL);
		ConsoleBase_SwitchMenu_m676980302(__this, L_44, /*hidden argument*/NULL);
	}

IL_01ea:
	{
		bool L_45 = ConsoleBase_Button_m1005519490(__this, _stringLiteral172829340, /*hidden argument*/NULL);
		if (!L_45)
		{
			goto IL_020a;
		}
	}
	{
		RuntimeTypeHandle_t3027515415  L_46 = { reinterpret_cast<intptr_t> (AppLinks_t2028121612_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_47 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_46, /*hidden argument*/NULL);
		ConsoleBase_SwitchMenu_m676980302(__this, L_47, /*hidden argument*/NULL);
	}

IL_020a:
	{
		bool L_48 = Constants_get_IsMobile_m316667921(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_48)
		{
			goto IL_0234;
		}
	}
	{
		bool L_49 = ConsoleBase_Button_m1005519490(__this, _stringLiteral3045473405, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_0234;
		}
	}
	{
		RuntimeTypeHandle_t3027515415  L_50 = { reinterpret_cast<intptr_t> (AccessTokenMenu_t4028641200_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_51 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_50, /*hidden argument*/NULL);
		ConsoleBase_SwitchMenu_m676980302(__this, L_51, /*hidden argument*/NULL);
	}

IL_0234:
	{
		GUILayout_EndVertical_m3051805938(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_52 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_enabled_m3922032131(NULL /*static, unused*/, L_52, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.MainMenu::CallFBLogin()
extern "C" IL2CPP_METHOD_ATTR void MainMenu_CallFBLogin_m2730593439 (MainMenu_t1823058806 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_CallFBLogin_m2730593439_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3319525431 * V_0 = NULL;
	{
		List_1_t3319525431 * L_0 = (List_1_t3319525431 *)il2cpp_codegen_object_new(List_1_t3319525431_il2cpp_TypeInfo_var);
		List_1__ctor_m706204246(L_0, /*hidden argument*/List_1__ctor_m706204246_RuntimeMethod_var);
		V_0 = L_0;
		List_1_t3319525431 * L_1 = V_0;
		NullCheck(L_1);
		List_1_Add_m1685793073(L_1, _stringLiteral435507638, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
		List_1_t3319525431 * L_2 = V_0;
		NullCheck(L_2);
		List_1_Add_m1685793073(L_2, _stringLiteral3497109170, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
		List_1_t3319525431 * L_3 = V_0;
		NullCheck(L_3);
		List_1_Add_m1685793073(L_3, _stringLiteral4170297164, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
		List_1_t3319525431 * L_4 = V_0;
		intptr_t L_5 = (intptr_t)MenuBase_HandleResult_m3006125243_RuntimeMethod_var;
		FacebookDelegate_1_t1669328765 * L_6 = (FacebookDelegate_1_t1669328765 *)il2cpp_codegen_object_new(FacebookDelegate_1_t1669328765_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2272005295(L_6, __this, (intptr_t)L_5, /*hidden argument*/FacebookDelegate_1__ctor_m2272005295_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		FB_LogInWithReadPermissions_m2288754438(NULL /*static, unused*/, L_4, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.MainMenu::CallFBLoginForPublish()
extern "C" IL2CPP_METHOD_ATTR void MainMenu_CallFBLoginForPublish_m2374054384 (MainMenu_t1823058806 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_CallFBLoginForPublish_m2374054384_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	List_1_t3319525431 * V_0 = NULL;
	{
		List_1_t3319525431 * L_0 = (List_1_t3319525431 *)il2cpp_codegen_object_new(List_1_t3319525431_il2cpp_TypeInfo_var);
		List_1__ctor_m706204246(L_0, /*hidden argument*/List_1__ctor_m706204246_RuntimeMethod_var);
		V_0 = L_0;
		List_1_t3319525431 * L_1 = V_0;
		NullCheck(L_1);
		List_1_Add_m1685793073(L_1, _stringLiteral3357662094, /*hidden argument*/List_1_Add_m1685793073_RuntimeMethod_var);
		List_1_t3319525431 * L_2 = V_0;
		intptr_t L_3 = (intptr_t)MenuBase_HandleResult_m3006125243_RuntimeMethod_var;
		FacebookDelegate_1_t1669328765 * L_4 = (FacebookDelegate_1_t1669328765 *)il2cpp_codegen_object_new(FacebookDelegate_1_t1669328765_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m2272005295(L_4, __this, (intptr_t)L_3, /*hidden argument*/FacebookDelegate_1__ctor_m2272005295_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		FB_LogInWithPublishPermissions_m2760240870(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.MainMenu::CallFBLogout()
extern "C" IL2CPP_METHOD_ATTR void MainMenu_CallFBLogout_m4130237067 (MainMenu_t1823058806 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_CallFBLogout_m4130237067_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		FB_LogOut_m3633383663(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.MainMenu::OnInitComplete()
extern "C" IL2CPP_METHOD_ATTR void MainMenu_OnInitComplete_m4103279806 (MainMenu_t1823058806 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_OnInitComplete_m4103279806_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		ConsoleBase_set_Status_m4147399384(__this, _stringLiteral2611099171, /*hidden argument*/NULL);
		ConsoleBase_set_LastResponse_m3622711220(__this, _stringLiteral565867462, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(FB_t2178373596_il2cpp_TypeInfo_var);
		bool L_0 = FB_get_IsLoggedIn_m2875052829(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = L_0;
		RuntimeObject * L_2 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_1);
		bool L_3 = FB_get_IsInitialized_m3789500881(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_4 = L_3;
		RuntimeObject * L_5 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_4);
		String_t* L_6 = String_Format_m2556382932(NULL /*static, unused*/, _stringLiteral3613892036, L_2, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		String_t* L_7 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(LogView_t1067263371_il2cpp_TypeInfo_var);
		LogView_AddLog_m1637420510(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		AccessToken_t2431487013 * L_8 = AccessToken_get_CurrentAccessToken_m3037387743(NULL /*static, unused*/, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0054;
		}
	}
	{
		AccessToken_t2431487013 * L_9 = AccessToken_get_CurrentAccessToken_m3037387743(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_9);
		IL2CPP_RUNTIME_CLASS_INIT(LogView_t1067263371_il2cpp_TypeInfo_var);
		LogView_AddLog_m1637420510(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
	}

IL_0054:
	{
		return;
	}
}
// System.Void Facebook.Unity.Example.MainMenu::OnHideUnity(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void MainMenu_OnHideUnity_m3704770818 (MainMenu_t1823058806 * __this, bool ___isGameShown0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MainMenu_OnHideUnity_m3704770818_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		ConsoleBase_set_Status_m4147399384(__this, _stringLiteral2611099171, /*hidden argument*/NULL);
		bool L_0 = ___isGameShown0;
		bool L_1 = L_0;
		RuntimeObject * L_2 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_1);
		String_t* L_3 = String_Format_m2844511972(NULL /*static, unused*/, _stringLiteral1236728777, L_2, /*hidden argument*/NULL);
		ConsoleBase_set_LastResponse_m3622711220(__this, L_3, /*hidden argument*/NULL);
		bool L_4 = ___isGameShown0;
		bool L_5 = L_4;
		RuntimeObject * L_6 = Box(Boolean_t97287965_il2cpp_TypeInfo_var, &L_5);
		String_t* L_7 = String_Concat_m904156431(NULL /*static, unused*/, _stringLiteral4175710859, L_6, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LogView_t1067263371_il2cpp_TypeInfo_var);
		LogView_AddLog_m1637420510(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.Unity.Example.MenuBase::.ctor()
extern "C" IL2CPP_METHOD_ATTR void MenuBase__ctor_m2160361082 (MenuBase_t1079888489 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuBase__ctor_m2160361082_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t1023975560_il2cpp_TypeInfo_var);
		ConsoleBase__ctor_m3547073371(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Facebook.Unity.Example.MenuBase::ShowDialogModeSelector()
extern "C" IL2CPP_METHOD_ATTR bool MenuBase_ShowDialogModeSelector_m675238584 (MenuBase_t1079888489 * __this, const RuntimeMethod* method)
{
	{
		return (bool)0;
	}
}
// System.Boolean Facebook.Unity.Example.MenuBase::ShowBackButton()
extern "C" IL2CPP_METHOD_ATTR bool MenuBase_ShowBackButton_m3890375611 (MenuBase_t1079888489 * __this, const RuntimeMethod* method)
{
	{
		return (bool)1;
	}
}
// System.Void Facebook.Unity.Example.MenuBase::HandleResult(Facebook.Unity.IResult)
extern "C" IL2CPP_METHOD_ATTR void MenuBase_HandleResult_m3006125243 (MenuBase_t1079888489 * __this, RuntimeObject* ___result0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuBase_HandleResult_m3006125243_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___result0;
		if (L_0)
		{
			goto IL_001d;
		}
	}
	{
		ConsoleBase_set_LastResponse_m3622711220(__this, _stringLiteral2872441188, /*hidden argument*/NULL);
		String_t* L_1 = ConsoleBase_get_LastResponse_m2081918938(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(LogView_t1067263371_il2cpp_TypeInfo_var);
		LogView_AddLog_m1637420510(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}

IL_001d:
	{
		ConsoleBase_set_LastResponseTexture_m2611436616(__this, (Texture2D_t3840446185 *)NULL, /*hidden argument*/NULL);
		RuntimeObject* L_2 = ___result0;
		NullCheck(L_2);
		String_t* L_3 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Facebook.Unity.IResult::get_Error() */, IResult_t2818782343_il2cpp_TypeInfo_var, L_2);
		bool L_4 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_005a;
		}
	}
	{
		ConsoleBase_set_Status_m4147399384(__this, _stringLiteral2896053709, /*hidden argument*/NULL);
		RuntimeObject* L_5 = ___result0;
		NullCheck(L_5);
		String_t* L_6 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* System.String Facebook.Unity.IResult::get_Error() */, IResult_t2818782343_il2cpp_TypeInfo_var, L_5);
		String_t* L_7 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2405134015, L_6, /*hidden argument*/NULL);
		ConsoleBase_set_LastResponse_m3622711220(__this, L_7, /*hidden argument*/NULL);
		goto IL_00cc;
	}

IL_005a:
	{
		RuntimeObject* L_8 = ___result0;
		NullCheck(L_8);
		bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(3 /* System.Boolean Facebook.Unity.IResult::get_Cancelled() */, IResult_t2818782343_il2cpp_TypeInfo_var, L_8);
		if (!L_9)
		{
			goto IL_008b;
		}
	}
	{
		ConsoleBase_set_Status_m4147399384(__this, _stringLiteral809088009, /*hidden argument*/NULL);
		RuntimeObject* L_10 = ___result0;
		NullCheck(L_10);
		String_t* L_11 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String Facebook.Unity.IResult::get_RawResult() */, IResult_t2818782343_il2cpp_TypeInfo_var, L_10);
		String_t* L_12 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2670760884, L_11, /*hidden argument*/NULL);
		ConsoleBase_set_LastResponse_m3622711220(__this, L_12, /*hidden argument*/NULL);
		goto IL_00cc;
	}

IL_008b:
	{
		RuntimeObject* L_13 = ___result0;
		NullCheck(L_13);
		String_t* L_14 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String Facebook.Unity.IResult::get_RawResult() */, IResult_t2818782343_il2cpp_TypeInfo_var, L_13);
		bool L_15 = String_IsNullOrEmpty_m2969720369(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		if (L_15)
		{
			goto IL_00c1;
		}
	}
	{
		ConsoleBase_set_Status_m4147399384(__this, _stringLiteral2611099171, /*hidden argument*/NULL);
		RuntimeObject* L_16 = ___result0;
		NullCheck(L_16);
		String_t* L_17 = InterfaceFuncInvoker0< String_t* >::Invoke(2 /* System.String Facebook.Unity.IResult::get_RawResult() */, IResult_t2818782343_il2cpp_TypeInfo_var, L_16);
		String_t* L_18 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral2129832496, L_17, /*hidden argument*/NULL);
		ConsoleBase_set_LastResponse_m3622711220(__this, L_18, /*hidden argument*/NULL);
		goto IL_00cc;
	}

IL_00c1:
	{
		ConsoleBase_set_LastResponse_m3622711220(__this, _stringLiteral2500948695, /*hidden argument*/NULL);
	}

IL_00cc:
	{
		RuntimeObject* L_19 = ___result0;
		NullCheck(L_19);
		String_t* L_20 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_19);
		IL2CPP_RUNTIME_CLASS_INIT(LogView_t1067263371_il2cpp_TypeInfo_var);
		LogView_AddLog_m1637420510(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.MenuBase::OnGUI()
extern "C" IL2CPP_METHOD_ATTR void MenuBase_OnGUI_m4282337643 (MenuBase_t1079888489 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuBase_OnGUI_m4282337643_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Touch_t1921856868  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector2_t2156229523  V_1;
	memset(&V_1, 0, sizeof(V_1));
	Touch_t1921856868  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Vector2_t2156229523  V_3;
	memset(&V_3, 0, sizeof(V_3));
	{
		bool L_0 = ConsoleBase_IsHorizontalLayout_m525810539(__this, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_001f;
		}
	}
	{
		GUILayoutOptionU5BU5D_t2510215842* L_1 = Array_Empty_TisGUILayoutOption_t811797299_m4167206926(NULL /*static, unused*/, /*hidden argument*/Array_Empty_TisGUILayoutOption_t811797299_m4167206926_RuntimeMethod_var);
		GUILayout_BeginHorizontal_m1655989246(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2510215842* L_2 = Array_Empty_TisGUILayoutOption_t811797299_m4167206926(NULL /*static, unused*/, /*hidden argument*/Array_Empty_TisGUILayoutOption_t811797299_m4167206926_RuntimeMethod_var);
		GUILayout_BeginVertical_m1213265852(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001f:
	{
		Type_t * L_3 = Object_GetType_m88164663(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(7 /* System.String System.Reflection.MemberInfo::get_Name() */, L_3);
		GUIStyle_t3956901511 * L_5 = ConsoleBase_get_LabelStyle_m3856255500(__this, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2510215842* L_6 = Array_Empty_TisGUILayoutOption_t811797299_m4167206926(NULL /*static, unused*/, /*hidden argument*/Array_Empty_TisGUILayoutOption_t811797299_m4167206926_RuntimeMethod_var);
		GUILayout_Label_m1096010274(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		MenuBase_AddStatus_m319405204(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		int32_t L_7 = Input_get_touchCount_m3403849067(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((int32_t)L_7) <= ((int32_t)0)))
		{
			goto IL_0091;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Touch_t1921856868  L_8 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_0 = L_8;
		int32_t L_9 = Touch_get_phase_m214549210((Touch_t1921856868 *)(&V_0), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_9) == ((uint32_t)1))))
		{
			goto IL_0091;
		}
	}
	{
		Vector2_t2156229523  L_10 = ConsoleBase_get_ScrollPosition_m2128236017(__this, /*hidden argument*/NULL);
		V_1 = L_10;
		Vector2_t2156229523 * L_11 = (&V_1);
		float L_12 = L_11->get_y_1();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t1431474628_il2cpp_TypeInfo_var);
		Touch_t1921856868  L_13 = Input_GetTouch_m2192712756(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		V_2 = L_13;
		Vector2_t2156229523  L_14 = Touch_get_deltaPosition_m2389653382((Touch_t1921856868 *)(&V_2), /*hidden argument*/NULL);
		V_3 = L_14;
		float L_15 = (&V_3)->get_y_1();
		L_11->set_y_1(((float)il2cpp_codegen_add((float)L_12, (float)L_15)));
		Vector2_t2156229523  L_16 = V_1;
		ConsoleBase_set_ScrollPosition_m589751691(__this, L_16, /*hidden argument*/NULL);
	}

IL_0091:
	{
		Vector2_t2156229523  L_17 = ConsoleBase_get_ScrollPosition_m2128236017(__this, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2510215842* L_18 = (GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)1);
		GUILayoutOptionU5BU5D_t2510215842* L_19 = L_18;
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t1023975560_il2cpp_TypeInfo_var);
		int32_t L_20 = ConsoleBase_get_MainWindowFullWidth_m476492915(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t811797299 * L_21 = GUILayout_MinWidth_m3524212012(NULL /*static, unused*/, (((float)((float)L_20))), /*hidden argument*/NULL);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_21);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t811797299 *)L_21);
		Vector2_t2156229523  L_22 = GUILayout_BeginScrollView_m3542895037(NULL /*static, unused*/, L_17, L_19, /*hidden argument*/NULL);
		ConsoleBase_set_ScrollPosition_m589751691(__this, L_22, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2510215842* L_23 = Array_Empty_TisGUILayoutOption_t811797299_m4167206926(NULL /*static, unused*/, /*hidden argument*/Array_Empty_TisGUILayoutOption_t811797299_m4167206926_RuntimeMethod_var);
		GUILayout_BeginHorizontal_m1655989246(NULL /*static, unused*/, L_23, /*hidden argument*/NULL);
		bool L_24 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Facebook.Unity.Example.MenuBase::ShowBackButton() */, __this);
		if (!L_24)
		{
			goto IL_00d1;
		}
	}
	{
		MenuBase_AddBackButton_m214045700(__this, /*hidden argument*/NULL);
	}

IL_00d1:
	{
		MenuBase_AddLogButton_m1613724113(__this, /*hidden argument*/NULL);
		bool L_25 = VirtFuncInvoker0< bool >::Invoke(7 /* System.Boolean Facebook.Unity.Example.MenuBase::ShowBackButton() */, __this);
		if (!L_25)
		{
			goto IL_0100;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUIContent_t3050628031_il2cpp_TypeInfo_var);
		GUIContent_t3050628031 * L_26 = ((GUIContent_t3050628031_StaticFields*)il2cpp_codegen_static_fields_for(GUIContent_t3050628031_il2cpp_TypeInfo_var))->get_none_6();
		GUILayoutOptionU5BU5D_t2510215842* L_27 = (GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)1);
		GUILayoutOptionU5BU5D_t2510215842* L_28 = L_27;
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t1023975560_il2cpp_TypeInfo_var);
		int32_t L_29 = ConsoleBase_get_MarginFix_m3965407477(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t811797299 * L_30 = GUILayout_MinWidth_m3524212012(NULL /*static, unused*/, (((float)((float)L_29))), /*hidden argument*/NULL);
		NullCheck(L_28);
		ArrayElementTypeCheck (L_28, L_30);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t811797299 *)L_30);
		GUILayout_Label_m1547104774(NULL /*static, unused*/, L_26, L_28, /*hidden argument*/NULL);
	}

IL_0100:
	{
		GUILayout_EndHorizontal_m125407884(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_31 = VirtFuncInvoker0< bool >::Invoke(6 /* System.Boolean Facebook.Unity.Example.MenuBase::ShowDialogModeSelector() */, __this);
		if (!L_31)
		{
			goto IL_0116;
		}
	}
	{
		MenuBase_AddDialogModeButtons_m2182366232(__this, /*hidden argument*/NULL);
	}

IL_0116:
	{
		GUILayoutOptionU5BU5D_t2510215842* L_32 = Array_Empty_TisGUILayoutOption_t811797299_m4167206926(NULL /*static, unused*/, /*hidden argument*/Array_Empty_TisGUILayoutOption_t811797299_m4167206926_RuntimeMethod_var);
		GUILayout_BeginVertical_m1213265852(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		VirtActionInvoker0::Invoke(5 /* System.Void Facebook.Unity.Example.MenuBase::GetGui() */, __this);
		GUILayout_Space_m3613068479(NULL /*static, unused*/, (10.0f), /*hidden argument*/NULL);
		GUILayout_EndVertical_m3051805938(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayout_EndScrollView_m1561653937(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.MenuBase::AddStatus()
extern "C" IL2CPP_METHOD_ATTR void MenuBase_AddStatus_m319405204 (MenuBase_t1079888489 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuBase_AddStatus_m319405204_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		GUILayout_Space_m3613068479(NULL /*static, unused*/, (5.0f), /*hidden argument*/NULL);
		String_t* L_0 = ConsoleBase_get_Status_m2983343794(__this, /*hidden argument*/NULL);
		String_t* L_1 = String_Concat_m3937257545(NULL /*static, unused*/, _stringLiteral3928422333, L_0, /*hidden argument*/NULL);
		GUIStyle_t3956901511 * L_2 = ConsoleBase_get_TextStyle_m2535314378(__this, /*hidden argument*/NULL);
		GUILayoutOptionU5BU5D_t2510215842* L_3 = (GUILayoutOptionU5BU5D_t2510215842*)SZArrayNew(GUILayoutOptionU5BU5D_t2510215842_il2cpp_TypeInfo_var, (uint32_t)1);
		GUILayoutOptionU5BU5D_t2510215842* L_4 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t1023975560_il2cpp_TypeInfo_var);
		int32_t L_5 = ConsoleBase_get_MainWindowWidth_m4254761416(NULL /*static, unused*/, /*hidden argument*/NULL);
		GUILayoutOption_t811797299 * L_6 = GUILayout_MinWidth_m3524212012(NULL /*static, unused*/, (((float)((float)L_5))), /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_6);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (GUILayoutOption_t811797299 *)L_6);
		GUILayout_Box_m3847685353(NULL /*static, unused*/, L_1, L_2, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.MenuBase::AddBackButton()
extern "C" IL2CPP_METHOD_ATTR void MenuBase_AddBackButton_m214045700 (MenuBase_t1079888489 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuBase_AddBackButton_m214045700_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(ConsoleBase_t1023975560_il2cpp_TypeInfo_var);
		Stack_1_t2690840144 * L_0 = ConsoleBase_get_MenuStack_m1135196678(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_1 = Enumerable_Any_TisString_t_m3986012742(NULL /*static, unused*/, L_0, /*hidden argument*/Enumerable_Any_TisString_t_m3986012742_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_enabled_m3922032131(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		bool L_2 = ConsoleBase_Button_m1005519490(__this, _stringLiteral1167841337, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0025;
		}
	}
	{
		ConsoleBase_GoBack_m3535004784(__this, /*hidden argument*/NULL);
	}

IL_0025:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_enabled_m3922032131(NULL /*static, unused*/, (bool)1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.MenuBase::AddLogButton()
extern "C" IL2CPP_METHOD_ATTR void MenuBase_AddLogButton_m1613724113 (MenuBase_t1079888489 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuBase_AddLogButton_m1613724113_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ConsoleBase_Button_m1005519490(__this, _stringLiteral2862787243, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0020;
		}
	}
	{
		RuntimeTypeHandle_t3027515415  L_1 = { reinterpret_cast<intptr_t> (LogView_t1067263371_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		ConsoleBase_SwitchMenu_m676980302(__this, L_2, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Facebook.Unity.Example.MenuBase::AddDialogModeButtons()
extern "C" IL2CPP_METHOD_ATTR void MenuBase_AddDialogModeButtons_m2182366232 (MenuBase_t1079888489 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuBase_AddDialogModeButtons_m2182366232_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	RuntimeObject* V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = -1;
	NO_UNUSED_WARNING (__leave_target);
	{
		GUILayoutOptionU5BU5D_t2510215842* L_0 = Array_Empty_TisGUILayoutOption_t811797299_m4167206926(NULL /*static, unused*/, /*hidden argument*/Array_Empty_TisGUILayoutOption_t811797299_m4167206926_RuntimeMethod_var);
		GUILayout_BeginHorizontal_m1655989246(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		RuntimeTypeHandle_t3027515415  L_1 = { reinterpret_cast<intptr_t> (ShareDialogMode_t1679970535_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m1620074514(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t4135868527_il2cpp_TypeInfo_var);
		RuntimeArray * L_3 = Enum_GetValues_m4192343468(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		RuntimeObject* L_4 = Array_GetEnumerator_m4277730612(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0037;
		}

IL_0024:
		{
			RuntimeObject* L_5 = V_1;
			NullCheck(L_5);
			RuntimeObject * L_6 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_5);
			V_0 = L_6;
			RuntimeObject * L_7 = V_0;
			MenuBase_AddDialogModeButton_m2926670472(__this, ((*(int32_t*)((int32_t*)UnBox(L_7, ShareDialogMode_t1679970535_il2cpp_TypeInfo_var)))), /*hidden argument*/NULL);
		}

IL_0037:
		{
			RuntimeObject* L_8 = V_1;
			NullCheck(L_8);
			bool L_9 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t1853284238_il2cpp_TypeInfo_var, L_8);
			if (L_9)
			{
				goto IL_0024;
			}
		}

IL_0042:
		{
			IL2CPP_LEAVE(0x5B, FINALLY_0047);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0047;
	}

FINALLY_0047:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_10 = V_1;
			RuntimeObject* L_11 = ((RuntimeObject*)IsInst((RuntimeObject*)L_10, IDisposable_t3640265483_il2cpp_TypeInfo_var));
			V_2 = L_11;
			if (!L_11)
			{
				goto IL_005a;
			}
		}

IL_0054:
		{
			RuntimeObject* L_12 = V_2;
			NullCheck(L_12);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t3640265483_il2cpp_TypeInfo_var, L_12);
		}

IL_005a:
		{
			IL2CPP_END_FINALLY(71)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(71)
	{
		IL2CPP_JUMP_TBL(0x5B, IL_005b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_005b:
	{
		GUILayout_EndHorizontal_m125407884(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.MenuBase::AddDialogModeButton(Facebook.Unity.ShareDialogMode)
extern "C" IL2CPP_METHOD_ATTR void MenuBase_AddDialogModeButton_m2926670472 (MenuBase_t1079888489 * __this, int32_t ___mode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (MenuBase_AddDialogModeButton_m2926670472_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		bool L_0 = GUI_get_enabled_m1448901857(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_2 = ___mode0;
		int32_t L_3 = ((MenuBase_t1079888489_StaticFields*)il2cpp_codegen_static_fields_for(MenuBase_t1079888489_il2cpp_TypeInfo_var))->get_shareDialogMode_15();
		G_B3_0 = ((((int32_t)((((int32_t)L_2) == ((int32_t)L_3))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_001a;
	}

IL_0019:
	{
		G_B3_0 = 0;
	}

IL_001a:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_enabled_m3922032131(NULL /*static, unused*/, (bool)G_B3_0, /*hidden argument*/NULL);
		RuntimeObject * L_4 = Box(ShareDialogMode_t1679970535_il2cpp_TypeInfo_var, (&___mode0));
		NullCheck(L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_4);
		___mode0 = *(int32_t*)UnBox(L_4);
		bool L_6 = ConsoleBase_Button_m1005519490(__this, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0043;
		}
	}
	{
		int32_t L_7 = ___mode0;
		((MenuBase_t1079888489_StaticFields*)il2cpp_codegen_static_fields_for(MenuBase_t1079888489_il2cpp_TypeInfo_var))->set_shareDialogMode_15(L_7);
		int32_t L_8 = ___mode0;
		Mobile_set_ShareDialogMode_m2548225244(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
	}

IL_0043:
	{
		bool L_9 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t1624858472_il2cpp_TypeInfo_var);
		GUI_set_enabled_m3922032131(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.Unity.Example.Pay::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Pay__ctor_m988135194 (Pay_t1264260185 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pay__ctor_m988135194_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t* L_0 = ((String_t_StaticFields*)il2cpp_codegen_static_fields_for(String_t_il2cpp_TypeInfo_var))->get_Empty_5();
		__this->set_payProduct_16(L_0);
		MenuBase__ctor_m2160361082(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.Pay::GetGui()
extern "C" IL2CPP_METHOD_ATTR void Pay_GetGui_m2631553089 (Pay_t1264260185 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pay_GetGui_m2631553089_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		String_t** L_0 = __this->get_address_of_payProduct_16();
		ConsoleBase_LabelAndTextField_m2312391562(__this, _stringLiteral2829152152, (String_t**)L_0, /*hidden argument*/NULL);
		bool L_1 = ConsoleBase_Button_m1005519490(__this, _stringLiteral346560186, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0027;
		}
	}
	{
		Pay_CallFBPay_m2256746739(__this, /*hidden argument*/NULL);
	}

IL_0027:
	{
		GUILayout_Space_m3613068479(NULL /*static, unused*/, (10.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.Example.Pay::CallFBPay()
extern "C" IL2CPP_METHOD_ATTR void Pay_CallFBPay_m2256746739 (Pay_t1264260185 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Pay_CallFBPay_m2256746739_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Nullable_1_t378540539  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		String_t* L_0 = __this->get_payProduct_16();
		il2cpp_codegen_initobj((&V_0), sizeof(Nullable_1_t378540539 ));
		Nullable_1_t378540539  L_1 = V_0;
		il2cpp_codegen_initobj((&V_0), sizeof(Nullable_1_t378540539 ));
		Nullable_1_t378540539  L_2 = V_0;
		intptr_t L_3 = (intptr_t)MenuBase_HandleResult_m3006125243_RuntimeMethod_var;
		FacebookDelegate_1_t2649775846 * L_4 = (FacebookDelegate_1_t2649775846 *)il2cpp_codegen_object_new(FacebookDelegate_1_t2649775846_il2cpp_TypeInfo_var);
		FacebookDelegate_1__ctor_m3067526517(L_4, __this, (intptr_t)L_3, /*hidden argument*/FacebookDelegate_1__ctor_m3067526517_RuntimeMethod_var);
		Canvas_Pay_m384955350(NULL /*static, unused*/, L_0, _stringLiteral3039347034, 1, L_1, L_2, (String_t*)NULL, (String_t*)NULL, (String_t*)NULL, L_4, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
