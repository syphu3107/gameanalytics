﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Facebook.Unity.IOS.IOSWrapper
struct IOSWrapper_t3090931871;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;


struct StringU5BU5D_t1281789340;


#ifndef U3CMODULEU3E_T692745550_H
#define U3CMODULEU3E_T692745550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745550 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745550_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef IOSWRAPPER_T3090931871_H
#define IOSWRAPPER_T3090931871_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.IOS.IOSWrapper
struct  IOSWrapper_t3090931871  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSWRAPPER_T3090931871_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef DOUBLE_T594665363_H
#define DOUBLE_T594665363_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Double
struct  Double_t594665363 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t594665363, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t594665363_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t594665363_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOUBLE_T594665363_H
#ifndef INT32_T2950945753_H
#define INT32_T2950945753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t2950945753 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t2950945753, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T2950945753_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
// System.String[]
struct StringU5BU5D_t1281789340  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier(m_Items + index, value);
	}
};



// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBInit(System.String,System.Boolean,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBInit_m517512202 (RuntimeObject * __this /* static, unused */, String_t* ___appId0, bool ___frictionlessRequests1, String_t* ___urlSuffix2, String_t* ___unityUserAgentSuffix3, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBLogInWithReadPermissions(System.Int32,System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBLogInWithReadPermissions_m5889434 (RuntimeObject * __this /* static, unused */, int32_t ___requestId0, String_t* ___scope1, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBLogInWithPublishPermissions(System.Int32,System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBLogInWithPublishPermissions_m3165674541 (RuntimeObject * __this /* static, unused */, int32_t ___requestId0, String_t* ___scope1, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBLogOut()
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBLogOut_m3070710233 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBSetPushNotificationsDeviceTokenString(System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBSetPushNotificationsDeviceTokenString_m1821486503 (RuntimeObject * __this /* static, unused */, String_t* ___token0, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBSetShareDialogMode(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBSetShareDialogMode_m1963734632 (RuntimeObject * __this /* static, unused */, int32_t ___mode0, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBShareLink(System.Int32,System.String,System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBShareLink_m3571816570 (RuntimeObject * __this /* static, unused */, int32_t ___requestId0, String_t* ___contentURL1, String_t* ___contentTitle2, String_t* ___contentDescription3, String_t* ___photoURL4, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBFeedShare(System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBFeedShare_m1770922425 (RuntimeObject * __this /* static, unused */, int32_t ___requestId0, String_t* ___toId1, String_t* ___link2, String_t* ___linkName3, String_t* ___linkCaption4, String_t* ___linkDescription5, String_t* ___picture6, String_t* ___mediaSource7, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppRequest(System.Int32,System.String,System.String,System.String,System.String[],System.Int32,System.String,System.String[],System.Int32,System.Boolean,System.Int32,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAppRequest_m139691371 (RuntimeObject * __this /* static, unused */, int32_t ___requestId0, String_t* ___message1, String_t* ___actionType2, String_t* ___objectId3, StringU5BU5D_t1281789340* ___to4, int32_t ___toLength5, String_t* ___filters6, StringU5BU5D_t1281789340* ___excludeIds7, int32_t ___excludeIdsLength8, bool ___hasMaxRecipients9, int32_t ___maxRecipients10, String_t* ___data11, String_t* ___title12, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsActivateApp()
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAppEventsActivateApp_m1296824355 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsLogEvent(System.String,System.Double,System.Int32,System.String[],System.String[])
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAppEventsLogEvent_m3258488930 (RuntimeObject * __this /* static, unused */, String_t* ___logEvent0, double ___valueToSum1, int32_t ___numParams2, StringU5BU5D_t1281789340* ___paramKeys3, StringU5BU5D_t1281789340* ___paramVals4, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsLogPurchase(System.Double,System.String,System.Int32,System.String[],System.String[])
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAppEventsLogPurchase_m1307098216 (RuntimeObject * __this /* static, unused */, double ___logPurchase0, String_t* ___currency1, int32_t ___numParams2, StringU5BU5D_t1281789340* ___paramKeys3, StringU5BU5D_t1281789340* ___paramVals4, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsSetLimitEventUsage(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAppEventsSetLimitEventUsage_m192279605 (RuntimeObject * __this /* static, unused */, bool ___limitEventUsage0, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAutoLogAppEventsEnabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAutoLogAppEventsEnabled_m3410713062 (RuntimeObject * __this /* static, unused */, bool ___autoLogAppEventsEnabled0, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAdvertiserIDCollectionEnabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAdvertiserIDCollectionEnabled_m91735242 (RuntimeObject * __this /* static, unused */, bool ___advertiserIDCollectionEnabledID0, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBGetAppLink(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBGetAppLink_m1620581570 (RuntimeObject * __this /* static, unused */, int32_t ___requestID0, const RuntimeMethod* method);
// System.String Facebook.Unity.IOS.IOSWrapper::IOSFBSdkVersion()
extern "C" IL2CPP_METHOD_ATTR String_t* IOSWrapper_IOSFBSdkVersion_m3479257637 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBSetUserID(System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBSetUserID_m3444715281 (RuntimeObject * __this /* static, unused */, String_t* ___userID0, const RuntimeMethod* method);
// System.String Facebook.Unity.IOS.IOSWrapper::IOSFBGetUserID()
extern "C" IL2CPP_METHOD_ATTR String_t* IOSWrapper_IOSFBGetUserID_m3425065959 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBUpdateUserProperties(System.Int32,System.String[],System.String[])
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBUpdateUserProperties_m41512031 (RuntimeObject * __this /* static, unused */, int32_t ___numParams0, StringU5BU5D_t1281789340* ___paramKeys1, StringU5BU5D_t1281789340* ___paramVals2, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBFetchDeferredAppLink(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBFetchDeferredAppLink_m3051874284 (RuntimeObject * __this /* static, unused */, int32_t ___requestID0, const RuntimeMethod* method);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBRefreshCurrentAccessToken(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBRefreshCurrentAccessToken_m2830751550 (RuntimeObject * __this /* static, unused */, int32_t ___requestID0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m297566312 (RuntimeObject * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Facebook.Unity.IOS.IOSWrapper::Init(System.String,System.Boolean,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_Init_m1911217006 (IOSWrapper_t3090931871 * __this, String_t* ___appId0, bool ___frictionlessRequests1, String_t* ___urlSuffix2, String_t* ___unityUserAgentSuffix3, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___appId0;
		bool L_1 = ___frictionlessRequests1;
		String_t* L_2 = ___urlSuffix2;
		String_t* L_3 = ___unityUserAgentSuffix3;
		IOSWrapper_IOSFBInit_m517512202(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::LogInWithReadPermissions(System.Int32,System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_LogInWithReadPermissions_m685130985 (IOSWrapper_t3090931871 * __this, int32_t ___requestId0, String_t* ___scope1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___requestId0;
		String_t* L_1 = ___scope1;
		IOSWrapper_IOSFBLogInWithReadPermissions_m5889434(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::LogInWithPublishPermissions(System.Int32,System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_LogInWithPublishPermissions_m1922880666 (IOSWrapper_t3090931871 * __this, int32_t ___requestId0, String_t* ___scope1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___requestId0;
		String_t* L_1 = ___scope1;
		IOSWrapper_IOSFBLogInWithPublishPermissions_m3165674541(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::LogOut()
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_LogOut_m3305324810 (IOSWrapper_t3090931871 * __this, const RuntimeMethod* method)
{
	{
		IOSWrapper_IOSFBLogOut_m3070710233(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::SetPushNotificationsDeviceTokenString(System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_SetPushNotificationsDeviceTokenString_m3617491995 (IOSWrapper_t3090931871 * __this, String_t* ___token0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___token0;
		IOSWrapper_IOSFBSetPushNotificationsDeviceTokenString_m1821486503(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::SetShareDialogMode(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_SetShareDialogMode_m3715766396 (IOSWrapper_t3090931871 * __this, int32_t ___mode0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___mode0;
		IOSWrapper_IOSFBSetShareDialogMode_m1963734632(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::ShareLink(System.Int32,System.String,System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_ShareLink_m2955390049 (IOSWrapper_t3090931871 * __this, int32_t ___requestId0, String_t* ___contentURL1, String_t* ___contentTitle2, String_t* ___contentDescription3, String_t* ___photoURL4, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___requestId0;
		String_t* L_1 = ___contentURL1;
		String_t* L_2 = ___contentTitle2;
		String_t* L_3 = ___contentDescription3;
		String_t* L_4 = ___photoURL4;
		IOSWrapper_IOSFBShareLink_m3571816570(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::FeedShare(System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_FeedShare_m123824257 (IOSWrapper_t3090931871 * __this, int32_t ___requestId0, String_t* ___toId1, String_t* ___link2, String_t* ___linkName3, String_t* ___linkCaption4, String_t* ___linkDescription5, String_t* ___picture6, String_t* ___mediaSource7, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___requestId0;
		String_t* L_1 = ___toId1;
		String_t* L_2 = ___link2;
		String_t* L_3 = ___linkName3;
		String_t* L_4 = ___linkCaption4;
		String_t* L_5 = ___linkDescription5;
		String_t* L_6 = ___picture6;
		String_t* L_7 = ___mediaSource7;
		IOSWrapper_IOSFBFeedShare_m1770922425(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::AppRequest(System.Int32,System.String,System.String,System.String,System.String[],System.Int32,System.String,System.String[],System.Int32,System.Boolean,System.Int32,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_AppRequest_m2206751701 (IOSWrapper_t3090931871 * __this, int32_t ___requestId0, String_t* ___message1, String_t* ___actionType2, String_t* ___objectId3, StringU5BU5D_t1281789340* ___to4, int32_t ___toLength5, String_t* ___filters6, StringU5BU5D_t1281789340* ___excludeIds7, int32_t ___excludeIdsLength8, bool ___hasMaxRecipients9, int32_t ___maxRecipients10, String_t* ___data11, String_t* ___title12, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___requestId0;
		String_t* L_1 = ___message1;
		String_t* L_2 = ___actionType2;
		String_t* L_3 = ___objectId3;
		StringU5BU5D_t1281789340* L_4 = ___to4;
		int32_t L_5 = ___toLength5;
		String_t* L_6 = ___filters6;
		StringU5BU5D_t1281789340* L_7 = ___excludeIds7;
		int32_t L_8 = ___excludeIdsLength8;
		bool L_9 = ___hasMaxRecipients9;
		int32_t L_10 = ___maxRecipients10;
		String_t* L_11 = ___data11;
		String_t* L_12 = ___title12;
		IOSWrapper_IOSFBAppRequest_m139691371(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::FBAppEventsActivateApp()
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_FBAppEventsActivateApp_m4009261202 (IOSWrapper_t3090931871 * __this, const RuntimeMethod* method)
{
	{
		IOSWrapper_IOSFBAppEventsActivateApp_m1296824355(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::LogAppEvent(System.String,System.Double,System.Int32,System.String[],System.String[])
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_LogAppEvent_m3099921062 (IOSWrapper_t3090931871 * __this, String_t* ___logEvent0, double ___valueToSum1, int32_t ___numParams2, StringU5BU5D_t1281789340* ___paramKeys3, StringU5BU5D_t1281789340* ___paramVals4, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___logEvent0;
		double L_1 = ___valueToSum1;
		int32_t L_2 = ___numParams2;
		StringU5BU5D_t1281789340* L_3 = ___paramKeys3;
		StringU5BU5D_t1281789340* L_4 = ___paramVals4;
		IOSWrapper_IOSFBAppEventsLogEvent_m3258488930(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::LogPurchaseAppEvent(System.Double,System.String,System.Int32,System.String[],System.String[])
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_LogPurchaseAppEvent_m3210837050 (IOSWrapper_t3090931871 * __this, double ___logPurchase0, String_t* ___currency1, int32_t ___numParams2, StringU5BU5D_t1281789340* ___paramKeys3, StringU5BU5D_t1281789340* ___paramVals4, const RuntimeMethod* method)
{
	{
		double L_0 = ___logPurchase0;
		String_t* L_1 = ___currency1;
		int32_t L_2 = ___numParams2;
		StringU5BU5D_t1281789340* L_3 = ___paramKeys3;
		StringU5BU5D_t1281789340* L_4 = ___paramVals4;
		IOSWrapper_IOSFBAppEventsLogPurchase_m1307098216(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::FBAppEventsSetLimitEventUsage(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_FBAppEventsSetLimitEventUsage_m2728992360 (IOSWrapper_t3090931871 * __this, bool ___limitEventUsage0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___limitEventUsage0;
		IOSWrapper_IOSFBAppEventsSetLimitEventUsage_m192279605(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::FBAutoLogAppEventsEnabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_FBAutoLogAppEventsEnabled_m3792919513 (IOSWrapper_t3090931871 * __this, bool ___autoLogAppEventsEnabled0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___autoLogAppEventsEnabled0;
		IOSWrapper_IOSFBAutoLogAppEventsEnabled_m3410713062(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::FBAdvertiserIDCollectionEnabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_FBAdvertiserIDCollectionEnabled_m2273512212 (IOSWrapper_t3090931871 * __this, bool ___advertiserIDCollectionEnabled0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___advertiserIDCollectionEnabled0;
		IOSWrapper_IOSFBAdvertiserIDCollectionEnabled_m91735242(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::GetAppLink(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_GetAppLink_m541742853 (IOSWrapper_t3090931871 * __this, int32_t ___requestId0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___requestId0;
		IOSWrapper_IOSFBGetAppLink_m1620581570(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String Facebook.Unity.IOS.IOSWrapper::FBSdkVersion()
extern "C" IL2CPP_METHOD_ATTR String_t* IOSWrapper_FBSdkVersion_m3404066453 (IOSWrapper_t3090931871 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = IOSWrapper_IOSFBSdkVersion_m3479257637(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::FBSetUserID(System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_FBSetUserID_m1595328735 (IOSWrapper_t3090931871 * __this, String_t* ___userID0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___userID0;
		IOSWrapper_IOSFBSetUserID_m3444715281(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.String Facebook.Unity.IOS.IOSWrapper::FBGetUserID()
extern "C" IL2CPP_METHOD_ATTR String_t* IOSWrapper_FBGetUserID_m1198005179 (IOSWrapper_t3090931871 * __this, const RuntimeMethod* method)
{
	{
		String_t* L_0 = IOSWrapper_IOSFBGetUserID_m3425065959(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::UpdateUserProperties(System.Int32,System.String[],System.String[])
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_UpdateUserProperties_m2419146274 (IOSWrapper_t3090931871 * __this, int32_t ___numParams0, StringU5BU5D_t1281789340* ___paramKeys1, StringU5BU5D_t1281789340* ___paramVals2, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___numParams0;
		StringU5BU5D_t1281789340* L_1 = ___paramKeys1;
		StringU5BU5D_t1281789340* L_2 = ___paramVals2;
		IOSWrapper_IOSFBUpdateUserProperties_m41512031(NULL /*static, unused*/, L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::FetchDeferredAppLink(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_FetchDeferredAppLink_m4119635210 (IOSWrapper_t3090931871 * __this, int32_t ___requestId0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___requestId0;
		IOSWrapper_IOSFBFetchDeferredAppLink_m3051874284(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Facebook.Unity.IOS.IOSWrapper::RefreshCurrentAccessToken(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_RefreshCurrentAccessToken_m2477519176 (IOSWrapper_t3090931871 * __this, int32_t ___requestId0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___requestId0;
		IOSWrapper_IOSFBRefreshCurrentAccessToken_m2830751550(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
extern "C" void DEFAULT_CALL IOSFBInit(char*, int32_t, char*, char*);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBInit(System.String,System.Boolean,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBInit_m517512202 (RuntimeObject * __this /* static, unused */, String_t* ___appId0, bool ___frictionlessRequests1, String_t* ___urlSuffix2, String_t* ___unityUserAgentSuffix3, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, int32_t, char*, char*);

	// Marshaling of parameter '___appId0' to native representation
	char* ____appId0_marshaled = NULL;
	____appId0_marshaled = il2cpp_codegen_marshal_string(___appId0);

	// Marshaling of parameter '___urlSuffix2' to native representation
	char* ____urlSuffix2_marshaled = NULL;
	____urlSuffix2_marshaled = il2cpp_codegen_marshal_string(___urlSuffix2);

	// Marshaling of parameter '___unityUserAgentSuffix3' to native representation
	char* ____unityUserAgentSuffix3_marshaled = NULL;
	____unityUserAgentSuffix3_marshaled = il2cpp_codegen_marshal_string(___unityUserAgentSuffix3);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBInit)(____appId0_marshaled, static_cast<int32_t>(___frictionlessRequests1), ____urlSuffix2_marshaled, ____unityUserAgentSuffix3_marshaled);

	// Marshaling cleanup of parameter '___appId0' native representation
	il2cpp_codegen_marshal_free(____appId0_marshaled);
	____appId0_marshaled = NULL;

	// Marshaling cleanup of parameter '___urlSuffix2' native representation
	il2cpp_codegen_marshal_free(____urlSuffix2_marshaled);
	____urlSuffix2_marshaled = NULL;

	// Marshaling cleanup of parameter '___unityUserAgentSuffix3' native representation
	il2cpp_codegen_marshal_free(____unityUserAgentSuffix3_marshaled);
	____unityUserAgentSuffix3_marshaled = NULL;

}
extern "C" void DEFAULT_CALL IOSFBLogInWithReadPermissions(int32_t, char*);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBLogInWithReadPermissions(System.Int32,System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBLogInWithReadPermissions_m5889434 (RuntimeObject * __this /* static, unused */, int32_t ___requestId0, String_t* ___scope1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, char*);

	// Marshaling of parameter '___scope1' to native representation
	char* ____scope1_marshaled = NULL;
	____scope1_marshaled = il2cpp_codegen_marshal_string(___scope1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBLogInWithReadPermissions)(___requestId0, ____scope1_marshaled);

	// Marshaling cleanup of parameter '___scope1' native representation
	il2cpp_codegen_marshal_free(____scope1_marshaled);
	____scope1_marshaled = NULL;

}
extern "C" void DEFAULT_CALL IOSFBLogInWithPublishPermissions(int32_t, char*);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBLogInWithPublishPermissions(System.Int32,System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBLogInWithPublishPermissions_m3165674541 (RuntimeObject * __this /* static, unused */, int32_t ___requestId0, String_t* ___scope1, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, char*);

	// Marshaling of parameter '___scope1' to native representation
	char* ____scope1_marshaled = NULL;
	____scope1_marshaled = il2cpp_codegen_marshal_string(___scope1);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBLogInWithPublishPermissions)(___requestId0, ____scope1_marshaled);

	// Marshaling cleanup of parameter '___scope1' native representation
	il2cpp_codegen_marshal_free(____scope1_marshaled);
	____scope1_marshaled = NULL;

}
extern "C" void DEFAULT_CALL IOSFBLogOut();
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBLogOut()
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBLogOut_m3070710233 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBLogOut)();

}
extern "C" void DEFAULT_CALL IOSFBSetPushNotificationsDeviceTokenString(char*);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBSetPushNotificationsDeviceTokenString(System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBSetPushNotificationsDeviceTokenString_m1821486503 (RuntimeObject * __this /* static, unused */, String_t* ___token0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___token0' to native representation
	char* ____token0_marshaled = NULL;
	____token0_marshaled = il2cpp_codegen_marshal_string(___token0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBSetPushNotificationsDeviceTokenString)(____token0_marshaled);

	// Marshaling cleanup of parameter '___token0' native representation
	il2cpp_codegen_marshal_free(____token0_marshaled);
	____token0_marshaled = NULL;

}
extern "C" void DEFAULT_CALL IOSFBSetShareDialogMode(int32_t);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBSetShareDialogMode(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBSetShareDialogMode_m1963734632 (RuntimeObject * __this /* static, unused */, int32_t ___mode0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBSetShareDialogMode)(___mode0);

}
extern "C" void DEFAULT_CALL IOSFBShareLink(int32_t, char*, char*, char*, char*);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBShareLink(System.Int32,System.String,System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBShareLink_m3571816570 (RuntimeObject * __this /* static, unused */, int32_t ___requestId0, String_t* ___contentURL1, String_t* ___contentTitle2, String_t* ___contentDescription3, String_t* ___photoURL4, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, char*, char*, char*, char*);

	// Marshaling of parameter '___contentURL1' to native representation
	char* ____contentURL1_marshaled = NULL;
	____contentURL1_marshaled = il2cpp_codegen_marshal_string(___contentURL1);

	// Marshaling of parameter '___contentTitle2' to native representation
	char* ____contentTitle2_marshaled = NULL;
	____contentTitle2_marshaled = il2cpp_codegen_marshal_string(___contentTitle2);

	// Marshaling of parameter '___contentDescription3' to native representation
	char* ____contentDescription3_marshaled = NULL;
	____contentDescription3_marshaled = il2cpp_codegen_marshal_string(___contentDescription3);

	// Marshaling of parameter '___photoURL4' to native representation
	char* ____photoURL4_marshaled = NULL;
	____photoURL4_marshaled = il2cpp_codegen_marshal_string(___photoURL4);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBShareLink)(___requestId0, ____contentURL1_marshaled, ____contentTitle2_marshaled, ____contentDescription3_marshaled, ____photoURL4_marshaled);

	// Marshaling cleanup of parameter '___contentURL1' native representation
	il2cpp_codegen_marshal_free(____contentURL1_marshaled);
	____contentURL1_marshaled = NULL;

	// Marshaling cleanup of parameter '___contentTitle2' native representation
	il2cpp_codegen_marshal_free(____contentTitle2_marshaled);
	____contentTitle2_marshaled = NULL;

	// Marshaling cleanup of parameter '___contentDescription3' native representation
	il2cpp_codegen_marshal_free(____contentDescription3_marshaled);
	____contentDescription3_marshaled = NULL;

	// Marshaling cleanup of parameter '___photoURL4' native representation
	il2cpp_codegen_marshal_free(____photoURL4_marshaled);
	____photoURL4_marshaled = NULL;

}
extern "C" void DEFAULT_CALL IOSFBFeedShare(int32_t, char*, char*, char*, char*, char*, char*, char*);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBFeedShare(System.Int32,System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBFeedShare_m1770922425 (RuntimeObject * __this /* static, unused */, int32_t ___requestId0, String_t* ___toId1, String_t* ___link2, String_t* ___linkName3, String_t* ___linkCaption4, String_t* ___linkDescription5, String_t* ___picture6, String_t* ___mediaSource7, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, char*, char*, char*, char*, char*, char*, char*);

	// Marshaling of parameter '___toId1' to native representation
	char* ____toId1_marshaled = NULL;
	____toId1_marshaled = il2cpp_codegen_marshal_string(___toId1);

	// Marshaling of parameter '___link2' to native representation
	char* ____link2_marshaled = NULL;
	____link2_marshaled = il2cpp_codegen_marshal_string(___link2);

	// Marshaling of parameter '___linkName3' to native representation
	char* ____linkName3_marshaled = NULL;
	____linkName3_marshaled = il2cpp_codegen_marshal_string(___linkName3);

	// Marshaling of parameter '___linkCaption4' to native representation
	char* ____linkCaption4_marshaled = NULL;
	____linkCaption4_marshaled = il2cpp_codegen_marshal_string(___linkCaption4);

	// Marshaling of parameter '___linkDescription5' to native representation
	char* ____linkDescription5_marshaled = NULL;
	____linkDescription5_marshaled = il2cpp_codegen_marshal_string(___linkDescription5);

	// Marshaling of parameter '___picture6' to native representation
	char* ____picture6_marshaled = NULL;
	____picture6_marshaled = il2cpp_codegen_marshal_string(___picture6);

	// Marshaling of parameter '___mediaSource7' to native representation
	char* ____mediaSource7_marshaled = NULL;
	____mediaSource7_marshaled = il2cpp_codegen_marshal_string(___mediaSource7);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBFeedShare)(___requestId0, ____toId1_marshaled, ____link2_marshaled, ____linkName3_marshaled, ____linkCaption4_marshaled, ____linkDescription5_marshaled, ____picture6_marshaled, ____mediaSource7_marshaled);

	// Marshaling cleanup of parameter '___toId1' native representation
	il2cpp_codegen_marshal_free(____toId1_marshaled);
	____toId1_marshaled = NULL;

	// Marshaling cleanup of parameter '___link2' native representation
	il2cpp_codegen_marshal_free(____link2_marshaled);
	____link2_marshaled = NULL;

	// Marshaling cleanup of parameter '___linkName3' native representation
	il2cpp_codegen_marshal_free(____linkName3_marshaled);
	____linkName3_marshaled = NULL;

	// Marshaling cleanup of parameter '___linkCaption4' native representation
	il2cpp_codegen_marshal_free(____linkCaption4_marshaled);
	____linkCaption4_marshaled = NULL;

	// Marshaling cleanup of parameter '___linkDescription5' native representation
	il2cpp_codegen_marshal_free(____linkDescription5_marshaled);
	____linkDescription5_marshaled = NULL;

	// Marshaling cleanup of parameter '___picture6' native representation
	il2cpp_codegen_marshal_free(____picture6_marshaled);
	____picture6_marshaled = NULL;

	// Marshaling cleanup of parameter '___mediaSource7' native representation
	il2cpp_codegen_marshal_free(____mediaSource7_marshaled);
	____mediaSource7_marshaled = NULL;

}
extern "C" void DEFAULT_CALL IOSFBAppRequest(int32_t, char*, char*, char*, char**, int32_t, char*, char**, int32_t, int32_t, int32_t, char*, char*);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppRequest(System.Int32,System.String,System.String,System.String,System.String[],System.Int32,System.String,System.String[],System.Int32,System.Boolean,System.Int32,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAppRequest_m139691371 (RuntimeObject * __this /* static, unused */, int32_t ___requestId0, String_t* ___message1, String_t* ___actionType2, String_t* ___objectId3, StringU5BU5D_t1281789340* ___to4, int32_t ___toLength5, String_t* ___filters6, StringU5BU5D_t1281789340* ___excludeIds7, int32_t ___excludeIdsLength8, bool ___hasMaxRecipients9, int32_t ___maxRecipients10, String_t* ___data11, String_t* ___title12, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, char*, char*, char*, char**, int32_t, char*, char**, int32_t, int32_t, int32_t, char*, char*);

	// Marshaling of parameter '___message1' to native representation
	char* ____message1_marshaled = NULL;
	____message1_marshaled = il2cpp_codegen_marshal_string(___message1);

	// Marshaling of parameter '___actionType2' to native representation
	char* ____actionType2_marshaled = NULL;
	____actionType2_marshaled = il2cpp_codegen_marshal_string(___actionType2);

	// Marshaling of parameter '___objectId3' to native representation
	char* ____objectId3_marshaled = NULL;
	____objectId3_marshaled = il2cpp_codegen_marshal_string(___objectId3);

	// Marshaling of parameter '___to4' to native representation
	char** ____to4_marshaled = NULL;
	if (___to4 != NULL)
	{
		il2cpp_array_size_t ____to4_Length = (___to4)->max_length;
		____to4_marshaled = il2cpp_codegen_marshal_allocate_array<char*>(____to4_Length + 1);
		(____to4_marshaled)[____to4_Length] = NULL;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____to4_Length); i++)
		{
			(____to4_marshaled)[i] = il2cpp_codegen_marshal_string((___to4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____to4_marshaled = NULL;
	}

	// Marshaling of parameter '___filters6' to native representation
	char* ____filters6_marshaled = NULL;
	____filters6_marshaled = il2cpp_codegen_marshal_string(___filters6);

	// Marshaling of parameter '___excludeIds7' to native representation
	char** ____excludeIds7_marshaled = NULL;
	if (___excludeIds7 != NULL)
	{
		il2cpp_array_size_t ____excludeIds7_Length = (___excludeIds7)->max_length;
		____excludeIds7_marshaled = il2cpp_codegen_marshal_allocate_array<char*>(____excludeIds7_Length + 1);
		(____excludeIds7_marshaled)[____excludeIds7_Length] = NULL;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____excludeIds7_Length); i++)
		{
			(____excludeIds7_marshaled)[i] = il2cpp_codegen_marshal_string((___excludeIds7)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____excludeIds7_marshaled = NULL;
	}

	// Marshaling of parameter '___data11' to native representation
	char* ____data11_marshaled = NULL;
	____data11_marshaled = il2cpp_codegen_marshal_string(___data11);

	// Marshaling of parameter '___title12' to native representation
	char* ____title12_marshaled = NULL;
	____title12_marshaled = il2cpp_codegen_marshal_string(___title12);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBAppRequest)(___requestId0, ____message1_marshaled, ____actionType2_marshaled, ____objectId3_marshaled, ____to4_marshaled, ___toLength5, ____filters6_marshaled, ____excludeIds7_marshaled, ___excludeIdsLength8, static_cast<int32_t>(___hasMaxRecipients9), ___maxRecipients10, ____data11_marshaled, ____title12_marshaled);

	// Marshaling cleanup of parameter '___message1' native representation
	il2cpp_codegen_marshal_free(____message1_marshaled);
	____message1_marshaled = NULL;

	// Marshaling cleanup of parameter '___actionType2' native representation
	il2cpp_codegen_marshal_free(____actionType2_marshaled);
	____actionType2_marshaled = NULL;

	// Marshaling cleanup of parameter '___objectId3' native representation
	il2cpp_codegen_marshal_free(____objectId3_marshaled);
	____objectId3_marshaled = NULL;

	// Marshaling cleanup of parameter '___to4' native representation
	if (____to4_marshaled != NULL)
	{
		const il2cpp_array_size_t ____to4_marshaled_CleanupLoopCount = (___to4 != NULL) ? (___to4)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____to4_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free((____to4_marshaled)[i]);
			(____to4_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____to4_marshaled);
		____to4_marshaled = NULL;
	}

	// Marshaling cleanup of parameter '___filters6' native representation
	il2cpp_codegen_marshal_free(____filters6_marshaled);
	____filters6_marshaled = NULL;

	// Marshaling cleanup of parameter '___excludeIds7' native representation
	if (____excludeIds7_marshaled != NULL)
	{
		const il2cpp_array_size_t ____excludeIds7_marshaled_CleanupLoopCount = (___excludeIds7 != NULL) ? (___excludeIds7)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____excludeIds7_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free((____excludeIds7_marshaled)[i]);
			(____excludeIds7_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____excludeIds7_marshaled);
		____excludeIds7_marshaled = NULL;
	}

	// Marshaling cleanup of parameter '___data11' native representation
	il2cpp_codegen_marshal_free(____data11_marshaled);
	____data11_marshaled = NULL;

	// Marshaling cleanup of parameter '___title12' native representation
	il2cpp_codegen_marshal_free(____title12_marshaled);
	____title12_marshaled = NULL;

}
extern "C" void DEFAULT_CALL IOSFBAppEventsActivateApp();
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsActivateApp()
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAppEventsActivateApp_m1296824355 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBAppEventsActivateApp)();

}
extern "C" void DEFAULT_CALL IOSFBAppEventsLogEvent(char*, double, int32_t, char**, char**);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsLogEvent(System.String,System.Double,System.Int32,System.String[],System.String[])
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAppEventsLogEvent_m3258488930 (RuntimeObject * __this /* static, unused */, String_t* ___logEvent0, double ___valueToSum1, int32_t ___numParams2, StringU5BU5D_t1281789340* ___paramKeys3, StringU5BU5D_t1281789340* ___paramVals4, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*, double, int32_t, char**, char**);

	// Marshaling of parameter '___logEvent0' to native representation
	char* ____logEvent0_marshaled = NULL;
	____logEvent0_marshaled = il2cpp_codegen_marshal_string(___logEvent0);

	// Marshaling of parameter '___paramKeys3' to native representation
	char** ____paramKeys3_marshaled = NULL;
	if (___paramKeys3 != NULL)
	{
		il2cpp_array_size_t ____paramKeys3_Length = (___paramKeys3)->max_length;
		____paramKeys3_marshaled = il2cpp_codegen_marshal_allocate_array<char*>(____paramKeys3_Length + 1);
		(____paramKeys3_marshaled)[____paramKeys3_Length] = NULL;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramKeys3_Length); i++)
		{
			(____paramKeys3_marshaled)[i] = il2cpp_codegen_marshal_string((___paramKeys3)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____paramKeys3_marshaled = NULL;
	}

	// Marshaling of parameter '___paramVals4' to native representation
	char** ____paramVals4_marshaled = NULL;
	if (___paramVals4 != NULL)
	{
		il2cpp_array_size_t ____paramVals4_Length = (___paramVals4)->max_length;
		____paramVals4_marshaled = il2cpp_codegen_marshal_allocate_array<char*>(____paramVals4_Length + 1);
		(____paramVals4_marshaled)[____paramVals4_Length] = NULL;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramVals4_Length); i++)
		{
			(____paramVals4_marshaled)[i] = il2cpp_codegen_marshal_string((___paramVals4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____paramVals4_marshaled = NULL;
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBAppEventsLogEvent)(____logEvent0_marshaled, ___valueToSum1, ___numParams2, ____paramKeys3_marshaled, ____paramVals4_marshaled);

	// Marshaling cleanup of parameter '___logEvent0' native representation
	il2cpp_codegen_marshal_free(____logEvent0_marshaled);
	____logEvent0_marshaled = NULL;

	// Marshaling cleanup of parameter '___paramKeys3' native representation
	if (____paramKeys3_marshaled != NULL)
	{
		const il2cpp_array_size_t ____paramKeys3_marshaled_CleanupLoopCount = (___paramKeys3 != NULL) ? (___paramKeys3)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramKeys3_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free((____paramKeys3_marshaled)[i]);
			(____paramKeys3_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____paramKeys3_marshaled);
		____paramKeys3_marshaled = NULL;
	}

	// Marshaling cleanup of parameter '___paramVals4' native representation
	if (____paramVals4_marshaled != NULL)
	{
		const il2cpp_array_size_t ____paramVals4_marshaled_CleanupLoopCount = (___paramVals4 != NULL) ? (___paramVals4)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramVals4_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free((____paramVals4_marshaled)[i]);
			(____paramVals4_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____paramVals4_marshaled);
		____paramVals4_marshaled = NULL;
	}

}
extern "C" void DEFAULT_CALL IOSFBAppEventsLogPurchase(double, char*, int32_t, char**, char**);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsLogPurchase(System.Double,System.String,System.Int32,System.String[],System.String[])
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAppEventsLogPurchase_m1307098216 (RuntimeObject * __this /* static, unused */, double ___logPurchase0, String_t* ___currency1, int32_t ___numParams2, StringU5BU5D_t1281789340* ___paramKeys3, StringU5BU5D_t1281789340* ___paramVals4, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (double, char*, int32_t, char**, char**);

	// Marshaling of parameter '___currency1' to native representation
	char* ____currency1_marshaled = NULL;
	____currency1_marshaled = il2cpp_codegen_marshal_string(___currency1);

	// Marshaling of parameter '___paramKeys3' to native representation
	char** ____paramKeys3_marshaled = NULL;
	if (___paramKeys3 != NULL)
	{
		il2cpp_array_size_t ____paramKeys3_Length = (___paramKeys3)->max_length;
		____paramKeys3_marshaled = il2cpp_codegen_marshal_allocate_array<char*>(____paramKeys3_Length + 1);
		(____paramKeys3_marshaled)[____paramKeys3_Length] = NULL;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramKeys3_Length); i++)
		{
			(____paramKeys3_marshaled)[i] = il2cpp_codegen_marshal_string((___paramKeys3)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____paramKeys3_marshaled = NULL;
	}

	// Marshaling of parameter '___paramVals4' to native representation
	char** ____paramVals4_marshaled = NULL;
	if (___paramVals4 != NULL)
	{
		il2cpp_array_size_t ____paramVals4_Length = (___paramVals4)->max_length;
		____paramVals4_marshaled = il2cpp_codegen_marshal_allocate_array<char*>(____paramVals4_Length + 1);
		(____paramVals4_marshaled)[____paramVals4_Length] = NULL;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramVals4_Length); i++)
		{
			(____paramVals4_marshaled)[i] = il2cpp_codegen_marshal_string((___paramVals4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____paramVals4_marshaled = NULL;
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBAppEventsLogPurchase)(___logPurchase0, ____currency1_marshaled, ___numParams2, ____paramKeys3_marshaled, ____paramVals4_marshaled);

	// Marshaling cleanup of parameter '___currency1' native representation
	il2cpp_codegen_marshal_free(____currency1_marshaled);
	____currency1_marshaled = NULL;

	// Marshaling cleanup of parameter '___paramKeys3' native representation
	if (____paramKeys3_marshaled != NULL)
	{
		const il2cpp_array_size_t ____paramKeys3_marshaled_CleanupLoopCount = (___paramKeys3 != NULL) ? (___paramKeys3)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramKeys3_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free((____paramKeys3_marshaled)[i]);
			(____paramKeys3_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____paramKeys3_marshaled);
		____paramKeys3_marshaled = NULL;
	}

	// Marshaling cleanup of parameter '___paramVals4' native representation
	if (____paramVals4_marshaled != NULL)
	{
		const il2cpp_array_size_t ____paramVals4_marshaled_CleanupLoopCount = (___paramVals4 != NULL) ? (___paramVals4)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramVals4_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free((____paramVals4_marshaled)[i]);
			(____paramVals4_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____paramVals4_marshaled);
		____paramVals4_marshaled = NULL;
	}

}
extern "C" void DEFAULT_CALL IOSFBAppEventsSetLimitEventUsage(int32_t);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAppEventsSetLimitEventUsage(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAppEventsSetLimitEventUsage_m192279605 (RuntimeObject * __this /* static, unused */, bool ___limitEventUsage0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBAppEventsSetLimitEventUsage)(static_cast<int32_t>(___limitEventUsage0));

}
extern "C" void DEFAULT_CALL IOSFBAutoLogAppEventsEnabled(int32_t);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAutoLogAppEventsEnabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAutoLogAppEventsEnabled_m3410713062 (RuntimeObject * __this /* static, unused */, bool ___autoLogAppEventsEnabled0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBAutoLogAppEventsEnabled)(static_cast<int32_t>(___autoLogAppEventsEnabled0));

}
extern "C" void DEFAULT_CALL IOSFBAdvertiserIDCollectionEnabled(int32_t);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBAdvertiserIDCollectionEnabled(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBAdvertiserIDCollectionEnabled_m91735242 (RuntimeObject * __this /* static, unused */, bool ___advertiserIDCollectionEnabledID0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBAdvertiserIDCollectionEnabled)(static_cast<int32_t>(___advertiserIDCollectionEnabledID0));

}
extern "C" void DEFAULT_CALL IOSFBGetAppLink(int32_t);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBGetAppLink(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBGetAppLink_m1620581570 (RuntimeObject * __this /* static, unused */, int32_t ___requestID0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBGetAppLink)(___requestID0);

}
extern "C" char* DEFAULT_CALL IOSFBSdkVersion();
// System.String Facebook.Unity.IOS.IOSWrapper::IOSFBSdkVersion()
extern "C" IL2CPP_METHOD_ATTR String_t* IOSWrapper_IOSFBSdkVersion_m3479257637 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(IOSFBSdkVersion)();

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL IOSFBFetchDeferredAppLink(int32_t);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBFetchDeferredAppLink(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBFetchDeferredAppLink_m3051874284 (RuntimeObject * __this /* static, unused */, int32_t ___requestID0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBFetchDeferredAppLink)(___requestID0);

}
extern "C" void DEFAULT_CALL IOSFBRefreshCurrentAccessToken(int32_t);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBRefreshCurrentAccessToken(System.Int32)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBRefreshCurrentAccessToken_m2830751550 (RuntimeObject * __this /* static, unused */, int32_t ___requestID0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBRefreshCurrentAccessToken)(___requestID0);

}
extern "C" void DEFAULT_CALL IOSFBSetUserID(char*);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBSetUserID(System.String)
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBSetUserID_m3444715281 (RuntimeObject * __this /* static, unused */, String_t* ___userID0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (char*);

	// Marshaling of parameter '___userID0' to native representation
	char* ____userID0_marshaled = NULL;
	____userID0_marshaled = il2cpp_codegen_marshal_string(___userID0);

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBSetUserID)(____userID0_marshaled);

	// Marshaling cleanup of parameter '___userID0' native representation
	il2cpp_codegen_marshal_free(____userID0_marshaled);
	____userID0_marshaled = NULL;

}
extern "C" char* DEFAULT_CALL IOSFBGetUserID();
// System.String Facebook.Unity.IOS.IOSWrapper::IOSFBGetUserID()
extern "C" IL2CPP_METHOD_ATTR String_t* IOSWrapper_IOSFBGetUserID_m3425065959 (RuntimeObject * __this /* static, unused */, const RuntimeMethod* method)
{
	typedef char* (DEFAULT_CALL *PInvokeFunc) ();

	// Native function invocation
	char* returnValue = reinterpret_cast<PInvokeFunc>(IOSFBGetUserID)();

	// Marshaling of return value back from native representation
	String_t* _returnValue_unmarshaled = NULL;
	_returnValue_unmarshaled = il2cpp_codegen_marshal_string_result(returnValue);

	// Marshaling cleanup of return value native representation
	il2cpp_codegen_marshal_free(returnValue);
	returnValue = NULL;

	return _returnValue_unmarshaled;
}
extern "C" void DEFAULT_CALL IOSFBUpdateUserProperties(int32_t, char**, char**);
// System.Void Facebook.Unity.IOS.IOSWrapper::IOSFBUpdateUserProperties(System.Int32,System.String[],System.String[])
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper_IOSFBUpdateUserProperties_m41512031 (RuntimeObject * __this /* static, unused */, int32_t ___numParams0, StringU5BU5D_t1281789340* ___paramKeys1, StringU5BU5D_t1281789340* ___paramVals2, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (int32_t, char**, char**);

	// Marshaling of parameter '___paramKeys1' to native representation
	char** ____paramKeys1_marshaled = NULL;
	if (___paramKeys1 != NULL)
	{
		il2cpp_array_size_t ____paramKeys1_Length = (___paramKeys1)->max_length;
		____paramKeys1_marshaled = il2cpp_codegen_marshal_allocate_array<char*>(____paramKeys1_Length + 1);
		(____paramKeys1_marshaled)[____paramKeys1_Length] = NULL;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramKeys1_Length); i++)
		{
			(____paramKeys1_marshaled)[i] = il2cpp_codegen_marshal_string((___paramKeys1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____paramKeys1_marshaled = NULL;
	}

	// Marshaling of parameter '___paramVals2' to native representation
	char** ____paramVals2_marshaled = NULL;
	if (___paramVals2 != NULL)
	{
		il2cpp_array_size_t ____paramVals2_Length = (___paramVals2)->max_length;
		____paramVals2_marshaled = il2cpp_codegen_marshal_allocate_array<char*>(____paramVals2_Length + 1);
		(____paramVals2_marshaled)[____paramVals2_Length] = NULL;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramVals2_Length); i++)
		{
			(____paramVals2_marshaled)[i] = il2cpp_codegen_marshal_string((___paramVals2)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(i)));
		}
	}
	else
	{
		____paramVals2_marshaled = NULL;
	}

	// Native function invocation
	reinterpret_cast<PInvokeFunc>(IOSFBUpdateUserProperties)(___numParams0, ____paramKeys1_marshaled, ____paramVals2_marshaled);

	// Marshaling cleanup of parameter '___paramKeys1' native representation
	if (____paramKeys1_marshaled != NULL)
	{
		const il2cpp_array_size_t ____paramKeys1_marshaled_CleanupLoopCount = (___paramKeys1 != NULL) ? (___paramKeys1)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramKeys1_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free((____paramKeys1_marshaled)[i]);
			(____paramKeys1_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____paramKeys1_marshaled);
		____paramKeys1_marshaled = NULL;
	}

	// Marshaling cleanup of parameter '___paramVals2' native representation
	if (____paramVals2_marshaled != NULL)
	{
		const il2cpp_array_size_t ____paramVals2_marshaled_CleanupLoopCount = (___paramVals2 != NULL) ? (___paramVals2)->max_length : 0;
		for (int32_t i = 0; i < ARRAY_LENGTH_AS_INT32(____paramVals2_marshaled_CleanupLoopCount); i++)
		{
			il2cpp_codegen_marshal_free((____paramVals2_marshaled)[i]);
			(____paramVals2_marshaled)[i] = NULL;
		}
		il2cpp_codegen_marshal_free(____paramVals2_marshaled);
		____paramVals2_marshaled = NULL;
	}

}
// System.Void Facebook.Unity.IOS.IOSWrapper::.ctor()
extern "C" IL2CPP_METHOD_ATTR void IOSWrapper__ctor_m4245412039 (IOSWrapper_t3090931871 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m297566312(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
