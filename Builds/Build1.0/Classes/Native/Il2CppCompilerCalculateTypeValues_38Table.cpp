﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Facebook.Unity.AccessToken
struct AccessToken_t2431487013;
// Facebook.Unity.AsyncRequestString
struct AsyncRequestString_t3669574124;
// Facebook.Unity.CallbackManager
struct CallbackManager_t2446104503;
// Facebook.Unity.Editor.Dialogs.MockLoginDialog
struct MockLoginDialog_t1686232399;
// Facebook.Unity.Editor.IEditorWrapper
struct IEditorWrapper_t1620787942;
// Facebook.Unity.FB/OnDLLLoaded
struct OnDLLLoaded_t2963647862;
// Facebook.Unity.FBSDKEventBindingManager
struct FBSDKEventBindingManager_t2486575668;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>
struct FacebookDelegate_1_t138390958;
// Facebook.Unity.Gameroom.GameroomFacebook/OnComplete
struct OnComplete_t3786498944;
// Facebook.Unity.Gameroom.GameroomFacebookGameObject
struct GameroomFacebookGameObject_t2310521816;
// Facebook.Unity.Gameroom.IGameroomWrapper
struct IGameroomWrapper_t1796962761;
// Facebook.Unity.HideUnityDelegate
struct HideUnityDelegate_t1353799728;
// Facebook.Unity.IAsyncRequestStringWrapper
struct IAsyncRequestStringWrapper_t1063345135;
// Facebook.Unity.IFacebook
struct IFacebook_t588365452;
// Facebook.Unity.IFacebookCallbackHandler
struct IFacebookCallbackHandler_t762977247;
// Facebook.Unity.IFacebookImplementation
struct IFacebookImplementation_t2218504286;
// Facebook.Unity.IFacebookLogger
struct IFacebookLogger_t2421194823;
// Facebook.Unity.InitDelegate
struct InitDelegate_t3081360126;
// Facebook.Unity.Mobile.Android.IAndroidWrapper
struct IAndroidWrapper_t3623207840;
// Facebook.Unity.Mobile.IOS.IIOSWrapper
struct IIOSWrapper_t1975150683;
// Facebook.Unity.ResultContainer
struct ResultContainer_t4150301447;
// Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>
struct Callback_1_t3507265931;
// System.Action
struct Action_t1264377477;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t1329213854;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t96558379;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t827303578;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t600458651;
// System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent>
struct List_1_t567476090;
// System.Collections.Generic.List`1<Facebook.Unity.FBSDKEventBinding>
struct List_1_t196722230;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.DelegateData
struct DelegateData_t1677132599;
// System.Delegate[]
struct DelegateU5BU5D_t1703627840;
// System.Func`2<System.Object,System.String>
struct Func_2_t1214474899;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t2197129486;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Uri
struct Uri_t100236324;
// System.Void
struct Void_t1185182177;
// UnityEngine.Camera
struct Camera_t4157153871;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// UnityEngine.WWW
struct WWW_t3688466362;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CSTARTU3ED__9_T2855687532_H
#define U3CSTARTU3ED__9_T2855687532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AsyncRequestString/<Start>d__9
struct  U3CStartU3Ed__9_t2855687532  : public RuntimeObject
{
public:
	// System.Int32 Facebook.Unity.AsyncRequestString/<Start>d__9::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Facebook.Unity.AsyncRequestString/<Start>d__9::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString/<Start>d__9::<>4__this
	AsyncRequestString_t3669574124 * ___U3CU3E4__this_2;
	// UnityEngine.WWW Facebook.Unity.AsyncRequestString/<Start>d__9::<www>5__2
	WWW_t3688466362 * ___U3CwwwU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__9_t2855687532, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__9_t2855687532, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__9_t2855687532, ___U3CU3E4__this_2)); }
	inline AsyncRequestString_t3669574124 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AsyncRequestString_t3669574124 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AsyncRequestString_t3669574124 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__9_t2855687532, ___U3CwwwU3E5__2_3)); }
	inline WWW_t3688466362 * get_U3CwwwU3E5__2_3() const { return ___U3CwwwU3E5__2_3; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E5__2_3() { return &___U3CwwwU3E5__2_3; }
	inline void set_U3CwwwU3E5__2_3(WWW_t3688466362 * value)
	{
		___U3CwwwU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__9_T2855687532_H
#ifndef ASYNCREQUESTSTRINGWRAPPER_T707311955_H
#define ASYNCREQUESTSTRINGWRAPPER_T707311955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AsyncRequestStringWrapper
struct  AsyncRequestStringWrapper_t707311955  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCREQUESTSTRINGWRAPPER_T707311955_H
#ifndef U3CGENSNAPSHOTU3ED__4_T77578824_H
#define U3CGENSNAPSHOTU3ED__4_T77578824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.CodelessCrawler/<GenSnapshot>d__4
struct  U3CGenSnapshotU3Ed__4_t77578824  : public RuntimeObject
{
public:
	// System.Int32 Facebook.Unity.CodelessCrawler/<GenSnapshot>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Facebook.Unity.CodelessCrawler/<GenSnapshot>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGenSnapshotU3Ed__4_t77578824, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGenSnapshotU3Ed__4_t77578824, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGENSNAPSHOTU3ED__4_T77578824_H
#ifndef CODELESSIAPAUTOLOG_T3776592774_H
#define CODELESSIAPAUTOLOG_T3776592774_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.CodelessIAPAutoLog
struct  CodelessIAPAutoLog_t3776592774  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODELESSIAPAUTOLOG_T3776592774_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T95552448_H
#define U3CU3EC__DISPLAYCLASS4_0_T95552448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.Dialogs.MockLoginDialog/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t95552448  : public RuntimeObject
{
public:
	// System.String Facebook.Unity.Editor.Dialogs.MockLoginDialog/<>c__DisplayClass4_0::facebookID
	String_t* ___facebookID_0;
	// Facebook.Unity.Editor.Dialogs.MockLoginDialog Facebook.Unity.Editor.Dialogs.MockLoginDialog/<>c__DisplayClass4_0::<>4__this
	MockLoginDialog_t1686232399 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_facebookID_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t95552448, ___facebookID_0)); }
	inline String_t* get_facebookID_0() const { return ___facebookID_0; }
	inline String_t** get_address_of_facebookID_0() { return &___facebookID_0; }
	inline void set_facebookID_0(String_t* value)
	{
		___facebookID_0 = value;
		Il2CppCodeGenWriteBarrier((&___facebookID_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t95552448, ___U3CU3E4__this_1)); }
	inline MockLoginDialog_t1686232399 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline MockLoginDialog_t1686232399 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(MockLoginDialog_t1686232399 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T95552448_H
#ifndef EDITORWRAPPER_T3420512207_H
#define EDITORWRAPPER_T3420512207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.EditorWrapper
struct  EditorWrapper_t3420512207  : public RuntimeObject
{
public:
	// Facebook.Unity.IFacebookCallbackHandler Facebook.Unity.Editor.EditorWrapper::callbackHandler
	RuntimeObject* ___callbackHandler_0;

public:
	inline static int32_t get_offset_of_callbackHandler_0() { return static_cast<int32_t>(offsetof(EditorWrapper_t3420512207, ___callbackHandler_0)); }
	inline RuntimeObject* get_callbackHandler_0() const { return ___callbackHandler_0; }
	inline RuntimeObject** get_address_of_callbackHandler_0() { return &___callbackHandler_0; }
	inline void set_callbackHandler_0(RuntimeObject* value)
	{
		___callbackHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&___callbackHandler_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORWRAPPER_T3420512207_H
#ifndef U3CU3EC__DISPLAYCLASS35_0_T171294370_H
#define U3CU3EC__DISPLAYCLASS35_0_T171294370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FB/<>c__DisplayClass35_0
struct  U3CU3Ec__DisplayClass35_0_t171294370  : public RuntimeObject
{
public:
	// Facebook.Unity.InitDelegate Facebook.Unity.FB/<>c__DisplayClass35_0::onInitComplete
	InitDelegate_t3081360126 * ___onInitComplete_0;
	// System.String Facebook.Unity.FB/<>c__DisplayClass35_0::appId
	String_t* ___appId_1;
	// System.Boolean Facebook.Unity.FB/<>c__DisplayClass35_0::cookie
	bool ___cookie_2;
	// System.Boolean Facebook.Unity.FB/<>c__DisplayClass35_0::logging
	bool ___logging_3;
	// System.Boolean Facebook.Unity.FB/<>c__DisplayClass35_0::status
	bool ___status_4;
	// System.Boolean Facebook.Unity.FB/<>c__DisplayClass35_0::xfbml
	bool ___xfbml_5;
	// System.String Facebook.Unity.FB/<>c__DisplayClass35_0::authResponse
	String_t* ___authResponse_6;
	// System.Boolean Facebook.Unity.FB/<>c__DisplayClass35_0::frictionlessRequests
	bool ___frictionlessRequests_7;
	// System.String Facebook.Unity.FB/<>c__DisplayClass35_0::javascriptSDKLocale
	String_t* ___javascriptSDKLocale_8;
	// Facebook.Unity.HideUnityDelegate Facebook.Unity.FB/<>c__DisplayClass35_0::onHideUnity
	HideUnityDelegate_t1353799728 * ___onHideUnity_9;

public:
	inline static int32_t get_offset_of_onInitComplete_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t171294370, ___onInitComplete_0)); }
	inline InitDelegate_t3081360126 * get_onInitComplete_0() const { return ___onInitComplete_0; }
	inline InitDelegate_t3081360126 ** get_address_of_onInitComplete_0() { return &___onInitComplete_0; }
	inline void set_onInitComplete_0(InitDelegate_t3081360126 * value)
	{
		___onInitComplete_0 = value;
		Il2CppCodeGenWriteBarrier((&___onInitComplete_0), value);
	}

	inline static int32_t get_offset_of_appId_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t171294370, ___appId_1)); }
	inline String_t* get_appId_1() const { return ___appId_1; }
	inline String_t** get_address_of_appId_1() { return &___appId_1; }
	inline void set_appId_1(String_t* value)
	{
		___appId_1 = value;
		Il2CppCodeGenWriteBarrier((&___appId_1), value);
	}

	inline static int32_t get_offset_of_cookie_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t171294370, ___cookie_2)); }
	inline bool get_cookie_2() const { return ___cookie_2; }
	inline bool* get_address_of_cookie_2() { return &___cookie_2; }
	inline void set_cookie_2(bool value)
	{
		___cookie_2 = value;
	}

	inline static int32_t get_offset_of_logging_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t171294370, ___logging_3)); }
	inline bool get_logging_3() const { return ___logging_3; }
	inline bool* get_address_of_logging_3() { return &___logging_3; }
	inline void set_logging_3(bool value)
	{
		___logging_3 = value;
	}

	inline static int32_t get_offset_of_status_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t171294370, ___status_4)); }
	inline bool get_status_4() const { return ___status_4; }
	inline bool* get_address_of_status_4() { return &___status_4; }
	inline void set_status_4(bool value)
	{
		___status_4 = value;
	}

	inline static int32_t get_offset_of_xfbml_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t171294370, ___xfbml_5)); }
	inline bool get_xfbml_5() const { return ___xfbml_5; }
	inline bool* get_address_of_xfbml_5() { return &___xfbml_5; }
	inline void set_xfbml_5(bool value)
	{
		___xfbml_5 = value;
	}

	inline static int32_t get_offset_of_authResponse_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t171294370, ___authResponse_6)); }
	inline String_t* get_authResponse_6() const { return ___authResponse_6; }
	inline String_t** get_address_of_authResponse_6() { return &___authResponse_6; }
	inline void set_authResponse_6(String_t* value)
	{
		___authResponse_6 = value;
		Il2CppCodeGenWriteBarrier((&___authResponse_6), value);
	}

	inline static int32_t get_offset_of_frictionlessRequests_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t171294370, ___frictionlessRequests_7)); }
	inline bool get_frictionlessRequests_7() const { return ___frictionlessRequests_7; }
	inline bool* get_address_of_frictionlessRequests_7() { return &___frictionlessRequests_7; }
	inline void set_frictionlessRequests_7(bool value)
	{
		___frictionlessRequests_7 = value;
	}

	inline static int32_t get_offset_of_javascriptSDKLocale_8() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t171294370, ___javascriptSDKLocale_8)); }
	inline String_t* get_javascriptSDKLocale_8() const { return ___javascriptSDKLocale_8; }
	inline String_t** get_address_of_javascriptSDKLocale_8() { return &___javascriptSDKLocale_8; }
	inline void set_javascriptSDKLocale_8(String_t* value)
	{
		___javascriptSDKLocale_8 = value;
		Il2CppCodeGenWriteBarrier((&___javascriptSDKLocale_8), value);
	}

	inline static int32_t get_offset_of_onHideUnity_9() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t171294370, ___onHideUnity_9)); }
	inline HideUnityDelegate_t1353799728 * get_onHideUnity_9() const { return ___onHideUnity_9; }
	inline HideUnityDelegate_t1353799728 ** get_address_of_onHideUnity_9() { return &___onHideUnity_9; }
	inline void set_onHideUnity_9(HideUnityDelegate_t1353799728 * value)
	{
		___onHideUnity_9 = value;
		Il2CppCodeGenWriteBarrier((&___onHideUnity_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_0_T171294370_H
#ifndef CANVAS_T3146753929_H
#define CANVAS_T3146753929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FB/Canvas
struct  Canvas_t3146753929  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVAS_T3146753929_H
#ifndef MOBILE_T77204530_H
#define MOBILE_T77204530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FB/Mobile
struct  Mobile_t77204530  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILE_T77204530_H
#ifndef FBSDKCODELESSPATHCOMPONENT_T3390368644_H
#define FBSDKCODELESSPATHCOMPONENT_T3390368644_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FBSDKCodelessPathComponent
struct  FBSDKCodelessPathComponent_t3390368644  : public RuntimeObject
{
public:
	// System.String Facebook.Unity.FBSDKCodelessPathComponent::<className>k__BackingField
	String_t* ___U3CclassNameU3Ek__BackingField_0;
	// System.String Facebook.Unity.FBSDKCodelessPathComponent::<text>k__BackingField
	String_t* ___U3CtextU3Ek__BackingField_1;
	// System.String Facebook.Unity.FBSDKCodelessPathComponent::<hint>k__BackingField
	String_t* ___U3ChintU3Ek__BackingField_2;
	// System.String Facebook.Unity.FBSDKCodelessPathComponent::<desc>k__BackingField
	String_t* ___U3CdescU3Ek__BackingField_3;
	// System.String Facebook.Unity.FBSDKCodelessPathComponent::<tag>k__BackingField
	String_t* ___U3CtagU3Ek__BackingField_4;
	// System.Int64 Facebook.Unity.FBSDKCodelessPathComponent::<index>k__BackingField
	int64_t ___U3CindexU3Ek__BackingField_5;
	// System.Int64 Facebook.Unity.FBSDKCodelessPathComponent::<section>k__BackingField
	int64_t ___U3CsectionU3Ek__BackingField_6;
	// System.Int64 Facebook.Unity.FBSDKCodelessPathComponent::<row>k__BackingField
	int64_t ___U3CrowU3Ek__BackingField_7;
	// System.Int64 Facebook.Unity.FBSDKCodelessPathComponent::<matchBitmask>k__BackingField
	int64_t ___U3CmatchBitmaskU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CclassNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FBSDKCodelessPathComponent_t3390368644, ___U3CclassNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CclassNameU3Ek__BackingField_0() const { return ___U3CclassNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CclassNameU3Ek__BackingField_0() { return &___U3CclassNameU3Ek__BackingField_0; }
	inline void set_U3CclassNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CclassNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CclassNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CtextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FBSDKCodelessPathComponent_t3390368644, ___U3CtextU3Ek__BackingField_1)); }
	inline String_t* get_U3CtextU3Ek__BackingField_1() const { return ___U3CtextU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CtextU3Ek__BackingField_1() { return &___U3CtextU3Ek__BackingField_1; }
	inline void set_U3CtextU3Ek__BackingField_1(String_t* value)
	{
		___U3CtextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtextU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3ChintU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FBSDKCodelessPathComponent_t3390368644, ___U3ChintU3Ek__BackingField_2)); }
	inline String_t* get_U3ChintU3Ek__BackingField_2() const { return ___U3ChintU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3ChintU3Ek__BackingField_2() { return &___U3ChintU3Ek__BackingField_2; }
	inline void set_U3ChintU3Ek__BackingField_2(String_t* value)
	{
		___U3ChintU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3ChintU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CdescU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FBSDKCodelessPathComponent_t3390368644, ___U3CdescU3Ek__BackingField_3)); }
	inline String_t* get_U3CdescU3Ek__BackingField_3() const { return ___U3CdescU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CdescU3Ek__BackingField_3() { return &___U3CdescU3Ek__BackingField_3; }
	inline void set_U3CdescU3Ek__BackingField_3(String_t* value)
	{
		___U3CdescU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdescU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CtagU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FBSDKCodelessPathComponent_t3390368644, ___U3CtagU3Ek__BackingField_4)); }
	inline String_t* get_U3CtagU3Ek__BackingField_4() const { return ___U3CtagU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CtagU3Ek__BackingField_4() { return &___U3CtagU3Ek__BackingField_4; }
	inline void set_U3CtagU3Ek__BackingField_4(String_t* value)
	{
		___U3CtagU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtagU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CindexU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FBSDKCodelessPathComponent_t3390368644, ___U3CindexU3Ek__BackingField_5)); }
	inline int64_t get_U3CindexU3Ek__BackingField_5() const { return ___U3CindexU3Ek__BackingField_5; }
	inline int64_t* get_address_of_U3CindexU3Ek__BackingField_5() { return &___U3CindexU3Ek__BackingField_5; }
	inline void set_U3CindexU3Ek__BackingField_5(int64_t value)
	{
		___U3CindexU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CsectionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FBSDKCodelessPathComponent_t3390368644, ___U3CsectionU3Ek__BackingField_6)); }
	inline int64_t get_U3CsectionU3Ek__BackingField_6() const { return ___U3CsectionU3Ek__BackingField_6; }
	inline int64_t* get_address_of_U3CsectionU3Ek__BackingField_6() { return &___U3CsectionU3Ek__BackingField_6; }
	inline void set_U3CsectionU3Ek__BackingField_6(int64_t value)
	{
		___U3CsectionU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CrowU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FBSDKCodelessPathComponent_t3390368644, ___U3CrowU3Ek__BackingField_7)); }
	inline int64_t get_U3CrowU3Ek__BackingField_7() const { return ___U3CrowU3Ek__BackingField_7; }
	inline int64_t* get_address_of_U3CrowU3Ek__BackingField_7() { return &___U3CrowU3Ek__BackingField_7; }
	inline void set_U3CrowU3Ek__BackingField_7(int64_t value)
	{
		___U3CrowU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CmatchBitmaskU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FBSDKCodelessPathComponent_t3390368644, ___U3CmatchBitmaskU3Ek__BackingField_8)); }
	inline int64_t get_U3CmatchBitmaskU3Ek__BackingField_8() const { return ___U3CmatchBitmaskU3Ek__BackingField_8; }
	inline int64_t* get_address_of_U3CmatchBitmaskU3Ek__BackingField_8() { return &___U3CmatchBitmaskU3Ek__BackingField_8; }
	inline void set_U3CmatchBitmaskU3Ek__BackingField_8(int64_t value)
	{
		___U3CmatchBitmaskU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FBSDKCODELESSPATHCOMPONENT_T3390368644_H
#ifndef FBSDKEVENTBINDING_T3019614784_H
#define FBSDKEVENTBINDING_T3019614784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FBSDKEventBinding
struct  FBSDKEventBinding_t3019614784  : public RuntimeObject
{
public:
	// System.String Facebook.Unity.FBSDKEventBinding::<eventName>k__BackingField
	String_t* ___U3CeventNameU3Ek__BackingField_0;
	// System.String Facebook.Unity.FBSDKEventBinding::<eventType>k__BackingField
	String_t* ___U3CeventTypeU3Ek__BackingField_1;
	// System.String Facebook.Unity.FBSDKEventBinding::<appVersion>k__BackingField
	String_t* ___U3CappVersionU3Ek__BackingField_2;
	// System.String Facebook.Unity.FBSDKEventBinding::<pathType>k__BackingField
	String_t* ___U3CpathTypeU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<Facebook.Unity.FBSDKCodelessPathComponent> Facebook.Unity.FBSDKEventBinding::<path>k__BackingField
	List_1_t567476090 * ___U3CpathU3Ek__BackingField_4;
	// System.Collections.Generic.List`1<System.String> Facebook.Unity.FBSDKEventBinding::<parameters>k__BackingField
	List_1_t3319525431 * ___U3CparametersU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CeventNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FBSDKEventBinding_t3019614784, ___U3CeventNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CeventNameU3Ek__BackingField_0() const { return ___U3CeventNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CeventNameU3Ek__BackingField_0() { return &___U3CeventNameU3Ek__BackingField_0; }
	inline void set_U3CeventNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CeventNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CeventNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CeventTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FBSDKEventBinding_t3019614784, ___U3CeventTypeU3Ek__BackingField_1)); }
	inline String_t* get_U3CeventTypeU3Ek__BackingField_1() const { return ___U3CeventTypeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CeventTypeU3Ek__BackingField_1() { return &___U3CeventTypeU3Ek__BackingField_1; }
	inline void set_U3CeventTypeU3Ek__BackingField_1(String_t* value)
	{
		___U3CeventTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CeventTypeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CappVersionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FBSDKEventBinding_t3019614784, ___U3CappVersionU3Ek__BackingField_2)); }
	inline String_t* get_U3CappVersionU3Ek__BackingField_2() const { return ___U3CappVersionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CappVersionU3Ek__BackingField_2() { return &___U3CappVersionU3Ek__BackingField_2; }
	inline void set_U3CappVersionU3Ek__BackingField_2(String_t* value)
	{
		___U3CappVersionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CappVersionU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CpathTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FBSDKEventBinding_t3019614784, ___U3CpathTypeU3Ek__BackingField_3)); }
	inline String_t* get_U3CpathTypeU3Ek__BackingField_3() const { return ___U3CpathTypeU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CpathTypeU3Ek__BackingField_3() { return &___U3CpathTypeU3Ek__BackingField_3; }
	inline void set_U3CpathTypeU3Ek__BackingField_3(String_t* value)
	{
		___U3CpathTypeU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpathTypeU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CpathU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FBSDKEventBinding_t3019614784, ___U3CpathU3Ek__BackingField_4)); }
	inline List_1_t567476090 * get_U3CpathU3Ek__BackingField_4() const { return ___U3CpathU3Ek__BackingField_4; }
	inline List_1_t567476090 ** get_address_of_U3CpathU3Ek__BackingField_4() { return &___U3CpathU3Ek__BackingField_4; }
	inline void set_U3CpathU3Ek__BackingField_4(List_1_t567476090 * value)
	{
		___U3CpathU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpathU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CparametersU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FBSDKEventBinding_t3019614784, ___U3CparametersU3Ek__BackingField_5)); }
	inline List_1_t3319525431 * get_U3CparametersU3Ek__BackingField_5() const { return ___U3CparametersU3Ek__BackingField_5; }
	inline List_1_t3319525431 ** get_address_of_U3CparametersU3Ek__BackingField_5() { return &___U3CparametersU3Ek__BackingField_5; }
	inline void set_U3CparametersU3Ek__BackingField_5(List_1_t3319525431 * value)
	{
		___U3CparametersU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CparametersU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FBSDKEVENTBINDING_T3019614784_H
#ifndef FBSDKEVENTBINDINGMANAGER_T2486575668_H
#define FBSDKEVENTBINDINGMANAGER_T2486575668_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FBSDKEventBindingManager
struct  FBSDKEventBindingManager_t2486575668  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Facebook.Unity.FBSDKEventBinding> Facebook.Unity.FBSDKEventBindingManager::<eventBindings>k__BackingField
	List_1_t196722230 * ___U3CeventBindingsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CeventBindingsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FBSDKEventBindingManager_t2486575668, ___U3CeventBindingsU3Ek__BackingField_0)); }
	inline List_1_t196722230 * get_U3CeventBindingsU3Ek__BackingField_0() const { return ___U3CeventBindingsU3Ek__BackingField_0; }
	inline List_1_t196722230 ** get_address_of_U3CeventBindingsU3Ek__BackingField_0() { return &___U3CeventBindingsU3Ek__BackingField_0; }
	inline void set_U3CeventBindingsU3Ek__BackingField_0(List_1_t196722230 * value)
	{
		___U3CeventBindingsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CeventBindingsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FBSDKEVENTBINDINGMANAGER_T2486575668_H
#ifndef FBSDKVIEWHIEARCHY_T250320177_H
#define FBSDKVIEWHIEARCHY_T250320177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FBSDKViewHiearchy
struct  FBSDKViewHiearchy_t250320177  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FBSDKVIEWHIEARCHY_T250320177_H
#ifndef FBUNITYUTILITY_T2214217213_H
#define FBUNITYUTILITY_T2214217213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FBUnityUtility
struct  FBUnityUtility_t2214217213  : public RuntimeObject
{
public:

public:
};

struct FBUnityUtility_t2214217213_StaticFields
{
public:
	// Facebook.Unity.IAsyncRequestStringWrapper Facebook.Unity.FBUnityUtility::asyncRequestStringWrapper
	RuntimeObject* ___asyncRequestStringWrapper_0;

public:
	inline static int32_t get_offset_of_asyncRequestStringWrapper_0() { return static_cast<int32_t>(offsetof(FBUnityUtility_t2214217213_StaticFields, ___asyncRequestStringWrapper_0)); }
	inline RuntimeObject* get_asyncRequestStringWrapper_0() const { return ___asyncRequestStringWrapper_0; }
	inline RuntimeObject** get_address_of_asyncRequestStringWrapper_0() { return &___asyncRequestStringWrapper_0; }
	inline void set_asyncRequestStringWrapper_0(RuntimeObject* value)
	{
		___asyncRequestStringWrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&___asyncRequestStringWrapper_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FBUNITYUTILITY_T2214217213_H
#ifndef FACEBOOKBASE_T1615169142_H
#define FACEBOOKBASE_T1615169142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookBase
struct  FacebookBase_t1615169142  : public RuntimeObject
{
public:
	// Facebook.Unity.InitDelegate Facebook.Unity.FacebookBase::onInitCompleteDelegate
	InitDelegate_t3081360126 * ___onInitCompleteDelegate_0;
	// System.Boolean Facebook.Unity.FacebookBase::<Initialized>k__BackingField
	bool ___U3CInitializedU3Ek__BackingField_1;
	// Facebook.Unity.CallbackManager Facebook.Unity.FacebookBase::<CallbackManager>k__BackingField
	CallbackManager_t2446104503 * ___U3CCallbackManagerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_onInitCompleteDelegate_0() { return static_cast<int32_t>(offsetof(FacebookBase_t1615169142, ___onInitCompleteDelegate_0)); }
	inline InitDelegate_t3081360126 * get_onInitCompleteDelegate_0() const { return ___onInitCompleteDelegate_0; }
	inline InitDelegate_t3081360126 ** get_address_of_onInitCompleteDelegate_0() { return &___onInitCompleteDelegate_0; }
	inline void set_onInitCompleteDelegate_0(InitDelegate_t3081360126 * value)
	{
		___onInitCompleteDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___onInitCompleteDelegate_0), value);
	}

	inline static int32_t get_offset_of_U3CInitializedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FacebookBase_t1615169142, ___U3CInitializedU3Ek__BackingField_1)); }
	inline bool get_U3CInitializedU3Ek__BackingField_1() const { return ___U3CInitializedU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CInitializedU3Ek__BackingField_1() { return &___U3CInitializedU3Ek__BackingField_1; }
	inline void set_U3CInitializedU3Ek__BackingField_1(bool value)
	{
		___U3CInitializedU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CCallbackManagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FacebookBase_t1615169142, ___U3CCallbackManagerU3Ek__BackingField_2)); }
	inline CallbackManager_t2446104503 * get_U3CCallbackManagerU3Ek__BackingField_2() const { return ___U3CCallbackManagerU3Ek__BackingField_2; }
	inline CallbackManager_t2446104503 ** get_address_of_U3CCallbackManagerU3Ek__BackingField_2() { return &___U3CCallbackManagerU3Ek__BackingField_2; }
	inline void set_U3CCallbackManagerU3Ek__BackingField_2(CallbackManager_t2446104503 * value)
	{
		___U3CCallbackManagerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackManagerU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKBASE_T1615169142_H
#ifndef U3CU3EC_T3332642991_H
#define U3CU3EC_T3332642991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookBase/<>c
struct  U3CU3Ec_t3332642991  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3332642991_StaticFields
{
public:
	// Facebook.Unity.FacebookBase/<>c Facebook.Unity.FacebookBase/<>c::<>9
	U3CU3Ec_t3332642991 * ___U3CU3E9_0;
	// System.Func`2<System.String,System.Boolean> Facebook.Unity.FacebookBase/<>c::<>9__41_0
	Func_2_t2197129486 * ___U3CU3E9__41_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3332642991_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3332642991 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3332642991 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3332642991 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__41_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3332642991_StaticFields, ___U3CU3E9__41_0_1)); }
	inline Func_2_t2197129486 * get_U3CU3E9__41_0_1() const { return ___U3CU3E9__41_0_1; }
	inline Func_2_t2197129486 ** get_address_of_U3CU3E9__41_0_1() { return &___U3CU3E9__41_0_1; }
	inline void set_U3CU3E9__41_0_1(Func_2_t2197129486 * value)
	{
		___U3CU3E9__41_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__41_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3332642991_H
#ifndef FACEBOOKLOGGER_T1173398901_H
#define FACEBOOKLOGGER_T1173398901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookLogger
struct  FacebookLogger_t1173398901  : public RuntimeObject
{
public:

public:
};

struct FacebookLogger_t1173398901_StaticFields
{
public:
	// Facebook.Unity.IFacebookLogger Facebook.Unity.FacebookLogger::<Instance>k__BackingField
	RuntimeObject* ___U3CInstanceU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FacebookLogger_t1173398901_StaticFields, ___U3CInstanceU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CInstanceU3Ek__BackingField_0() const { return ___U3CInstanceU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CInstanceU3Ek__BackingField_0() { return &___U3CInstanceU3Ek__BackingField_0; }
	inline void set_U3CInstanceU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CInstanceU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKLOGGER_T1173398901_H
#ifndef DEBUGLOGGER_T2300716689_H
#define DEBUGLOGGER_T2300716689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookLogger/DebugLogger
struct  DebugLogger_t2300716689  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGGER_T2300716689_H
#ifndef U3CDELAYEVENTU3ED__1_T2601426355_H
#define U3CDELAYEVENTU3ED__1_T2601426355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookScheduler/<DelayEvent>d__1
struct  U3CDelayEventU3Ed__1_t2601426355  : public RuntimeObject
{
public:
	// System.Int32 Facebook.Unity.FacebookScheduler/<DelayEvent>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Facebook.Unity.FacebookScheduler/<DelayEvent>d__1::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int64 Facebook.Unity.FacebookScheduler/<DelayEvent>d__1::delay
	int64_t ___delay_2;
	// System.Action Facebook.Unity.FacebookScheduler/<DelayEvent>d__1::action
	Action_t1264377477 * ___action_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDelayEventU3Ed__1_t2601426355, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDelayEventU3Ed__1_t2601426355, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CDelayEventU3Ed__1_t2601426355, ___delay_2)); }
	inline int64_t get_delay_2() const { return ___delay_2; }
	inline int64_t* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(int64_t value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_action_3() { return static_cast<int32_t>(offsetof(U3CDelayEventU3Ed__1_t2601426355, ___action_3)); }
	inline Action_t1264377477 * get_action_3() const { return ___action_3; }
	inline Action_t1264377477 ** get_address_of_action_3() { return &___action_3; }
	inline void set_action_3(Action_t1264377477 * value)
	{
		___action_3 = value;
		Il2CppCodeGenWriteBarrier((&___action_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEVENTU3ED__1_T2601426355_H
#ifndef FACEBOOKSDKVERSION_T4164812230_H
#define FACEBOOKSDKVERSION_T4164812230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookSdkVersion
struct  FacebookSdkVersion_t4164812230  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKSDKVERSION_T4164812230_H
#ifndef U3CWAITFORPIPERESPONSEU3ED__4_T3567704217_H
#define U3CWAITFORPIPERESPONSEU3ED__4_T3567704217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Gameroom.GameroomFacebookGameObject/<WaitForPipeResponse>d__4
struct  U3CWaitForPipeResponseU3Ed__4_t3567704217  : public RuntimeObject
{
public:
	// System.Int32 Facebook.Unity.Gameroom.GameroomFacebookGameObject/<WaitForPipeResponse>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Facebook.Unity.Gameroom.GameroomFacebookGameObject/<WaitForPipeResponse>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Facebook.Unity.Gameroom.GameroomFacebookGameObject Facebook.Unity.Gameroom.GameroomFacebookGameObject/<WaitForPipeResponse>d__4::<>4__this
	GameroomFacebookGameObject_t2310521816 * ___U3CU3E4__this_2;
	// Facebook.Unity.Gameroom.GameroomFacebook/OnComplete Facebook.Unity.Gameroom.GameroomFacebookGameObject/<WaitForPipeResponse>d__4::onCompleteDelegate
	OnComplete_t3786498944 * ___onCompleteDelegate_3;
	// System.String Facebook.Unity.Gameroom.GameroomFacebookGameObject/<WaitForPipeResponse>d__4::callbackId
	String_t* ___callbackId_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForPipeResponseU3Ed__4_t3567704217, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForPipeResponseU3Ed__4_t3567704217, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForPipeResponseU3Ed__4_t3567704217, ___U3CU3E4__this_2)); }
	inline GameroomFacebookGameObject_t2310521816 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GameroomFacebookGameObject_t2310521816 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GameroomFacebookGameObject_t2310521816 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_onCompleteDelegate_3() { return static_cast<int32_t>(offsetof(U3CWaitForPipeResponseU3Ed__4_t3567704217, ___onCompleteDelegate_3)); }
	inline OnComplete_t3786498944 * get_onCompleteDelegate_3() const { return ___onCompleteDelegate_3; }
	inline OnComplete_t3786498944 ** get_address_of_onCompleteDelegate_3() { return &___onCompleteDelegate_3; }
	inline void set_onCompleteDelegate_3(OnComplete_t3786498944 * value)
	{
		___onCompleteDelegate_3 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleteDelegate_3), value);
	}

	inline static int32_t get_offset_of_callbackId_4() { return static_cast<int32_t>(offsetof(U3CWaitForPipeResponseU3Ed__4_t3567704217, ___callbackId_4)); }
	inline String_t* get_callbackId_4() const { return ___callbackId_4; }
	inline String_t** get_address_of_callbackId_4() { return &___callbackId_4; }
	inline void set_callbackId_4(String_t* value)
	{
		___callbackId_4 = value;
		Il2CppCodeGenWriteBarrier((&___callbackId_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORPIPERESPONSEU3ED__4_T3567704217_H
#ifndef METHODARGUMENTS_T1563002313_H
#define METHODARGUMENTS_T1563002313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.MethodArguments
struct  MethodArguments_t1563002313  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.MethodArguments::arguments
	RuntimeObject* ___arguments_0;

public:
	inline static int32_t get_offset_of_arguments_0() { return static_cast<int32_t>(offsetof(MethodArguments_t1563002313, ___arguments_0)); }
	inline RuntimeObject* get_arguments_0() const { return ___arguments_0; }
	inline RuntimeObject** get_address_of_arguments_0() { return &___arguments_0; }
	inline void set_arguments_0(RuntimeObject* value)
	{
		___arguments_0 = value;
		Il2CppCodeGenWriteBarrier((&___arguments_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODARGUMENTS_T1563002313_H
#ifndef NATIVEDICT_T1844390233_H
#define NATIVEDICT_T1844390233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict
struct  NativeDict_t1844390233  : public RuntimeObject
{
public:
	// System.Int32 Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict::<NumEntries>k__BackingField
	int32_t ___U3CNumEntriesU3Ek__BackingField_0;
	// System.String[] Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict::<Keys>k__BackingField
	StringU5BU5D_t1281789340* ___U3CKeysU3Ek__BackingField_1;
	// System.String[] Facebook.Unity.Mobile.IOS.IOSFacebook/NativeDict::<Values>k__BackingField
	StringU5BU5D_t1281789340* ___U3CValuesU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CNumEntriesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NativeDict_t1844390233, ___U3CNumEntriesU3Ek__BackingField_0)); }
	inline int32_t get_U3CNumEntriesU3Ek__BackingField_0() const { return ___U3CNumEntriesU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CNumEntriesU3Ek__BackingField_0() { return &___U3CNumEntriesU3Ek__BackingField_0; }
	inline void set_U3CNumEntriesU3Ek__BackingField_0(int32_t value)
	{
		___U3CNumEntriesU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CKeysU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NativeDict_t1844390233, ___U3CKeysU3Ek__BackingField_1)); }
	inline StringU5BU5D_t1281789340* get_U3CKeysU3Ek__BackingField_1() const { return ___U3CKeysU3Ek__BackingField_1; }
	inline StringU5BU5D_t1281789340** get_address_of_U3CKeysU3Ek__BackingField_1() { return &___U3CKeysU3Ek__BackingField_1; }
	inline void set_U3CKeysU3Ek__BackingField_1(StringU5BU5D_t1281789340* value)
	{
		___U3CKeysU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeysU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CValuesU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(NativeDict_t1844390233, ___U3CValuesU3Ek__BackingField_2)); }
	inline StringU5BU5D_t1281789340* get_U3CValuesU3Ek__BackingField_2() const { return ___U3CValuesU3Ek__BackingField_2; }
	inline StringU5BU5D_t1281789340** get_address_of_U3CValuesU3Ek__BackingField_2() { return &___U3CValuesU3Ek__BackingField_2; }
	inline void set_U3CValuesU3Ek__BackingField_2(StringU5BU5D_t1281789340* value)
	{
		___U3CValuesU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValuesU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NATIVEDICT_T1844390233_H
#ifndef RESULTCONTAINER_T4150301447_H
#define RESULTCONTAINER_T4150301447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ResultContainer
struct  ResultContainer_t4150301447  : public RuntimeObject
{
public:
	// System.String Facebook.Unity.ResultContainer::<RawResult>k__BackingField
	String_t* ___U3CRawResultU3Ek__BackingField_1;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.ResultContainer::<ResultDictionary>k__BackingField
	RuntimeObject* ___U3CResultDictionaryU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CRawResultU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ResultContainer_t4150301447, ___U3CRawResultU3Ek__BackingField_1)); }
	inline String_t* get_U3CRawResultU3Ek__BackingField_1() const { return ___U3CRawResultU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CRawResultU3Ek__BackingField_1() { return &___U3CRawResultU3Ek__BackingField_1; }
	inline void set_U3CRawResultU3Ek__BackingField_1(String_t* value)
	{
		___U3CRawResultU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRawResultU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CResultDictionaryU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ResultContainer_t4150301447, ___U3CResultDictionaryU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CResultDictionaryU3Ek__BackingField_2() const { return ___U3CResultDictionaryU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CResultDictionaryU3Ek__BackingField_2() { return &___U3CResultDictionaryU3Ek__BackingField_2; }
	inline void set_U3CResultDictionaryU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CResultDictionaryU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResultDictionaryU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTCONTAINER_T4150301447_H
#ifndef UTILITIES_T2011615223_H
#define UTILITIES_T2011615223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Utilities
struct  Utilities_t2011615223  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILITIES_T2011615223_H
#ifndef U3CU3EC_T3449414030_H
#define U3CU3EC_T3449414030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Utilities/<>c
struct  U3CU3Ec_t3449414030  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3449414030_StaticFields
{
public:
	// Facebook.Unity.Utilities/<>c Facebook.Unity.Utilities/<>c::<>9
	U3CU3Ec_t3449414030 * ___U3CU3E9_0;
	// System.Func`2<System.Object,System.String> Facebook.Unity.Utilities/<>c::<>9__18_0
	Func_2_t1214474899 * ___U3CU3E9__18_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3449414030_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3449414030 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3449414030 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3449414030 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3449414030_StaticFields, ___U3CU3E9__18_0_1)); }
	inline Func_2_t1214474899 * get_U3CU3E9__18_0_1() const { return ___U3CU3E9__18_0_1; }
	inline Func_2_t1214474899 ** get_address_of_U3CU3E9__18_0_1() { return &___U3CU3E9__18_0_1; }
	inline void set_U3CU3E9__18_0_1(Func_2_t1214474899 * value)
	{
		___U3CU3E9__18_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__18_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3449414030_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef GAMEROOMFACEBOOK_T453300599_H
#define GAMEROOMFACEBOOK_T453300599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Gameroom.GameroomFacebook
struct  GameroomFacebook_t453300599  : public FacebookBase_t1615169142
{
public:
	// System.String Facebook.Unity.Gameroom.GameroomFacebook::appId
	String_t* ___appId_3;
	// Facebook.Unity.Gameroom.IGameroomWrapper Facebook.Unity.Gameroom.GameroomFacebook::gameroomWrapper
	RuntimeObject* ___gameroomWrapper_4;
	// System.Boolean Facebook.Unity.Gameroom.GameroomFacebook::<LimitEventUsage>k__BackingField
	bool ___U3CLimitEventUsageU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_appId_3() { return static_cast<int32_t>(offsetof(GameroomFacebook_t453300599, ___appId_3)); }
	inline String_t* get_appId_3() const { return ___appId_3; }
	inline String_t** get_address_of_appId_3() { return &___appId_3; }
	inline void set_appId_3(String_t* value)
	{
		___appId_3 = value;
		Il2CppCodeGenWriteBarrier((&___appId_3), value);
	}

	inline static int32_t get_offset_of_gameroomWrapper_4() { return static_cast<int32_t>(offsetof(GameroomFacebook_t453300599, ___gameroomWrapper_4)); }
	inline RuntimeObject* get_gameroomWrapper_4() const { return ___gameroomWrapper_4; }
	inline RuntimeObject** get_address_of_gameroomWrapper_4() { return &___gameroomWrapper_4; }
	inline void set_gameroomWrapper_4(RuntimeObject* value)
	{
		___gameroomWrapper_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameroomWrapper_4), value);
	}

	inline static int32_t get_offset_of_U3CLimitEventUsageU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GameroomFacebook_t453300599, ___U3CLimitEventUsageU3Ek__BackingField_5)); }
	inline bool get_U3CLimitEventUsageU3Ek__BackingField_5() const { return ___U3CLimitEventUsageU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CLimitEventUsageU3Ek__BackingField_5() { return &___U3CLimitEventUsageU3Ek__BackingField_5; }
	inline void set_U3CLimitEventUsageU3Ek__BackingField_5(bool value)
	{
		___U3CLimitEventUsageU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEROOMFACEBOOK_T453300599_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t3528271667* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t3528271667* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t3528271667** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t3528271667* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T1164162090_H
#define NULLABLE_1_T1164162090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int64>
struct  Nullable_1_t1164162090 
{
public:
	// T System.Nullable`1::value
	int64_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1164162090, ___value_0)); }
	inline int64_t get_value_0() const { return ___value_0; }
	inline int64_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int64_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1164162090, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1164162090_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t1185182177__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef FACEBOOKUNITYPLATFORM_T2098070734_H
#define FACEBOOKUNITYPLATFORM_T2098070734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookUnityPlatform
struct  FacebookUnityPlatform_t2098070734 
{
public:
	// System.Int32 Facebook.Unity.FacebookUnityPlatform::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FacebookUnityPlatform_t2098070734, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKUNITYPLATFORM_T2098070734_H
#ifndef HTTPMETHOD_T313594167_H
#define HTTPMETHOD_T313594167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.HttpMethod
struct  HttpMethod_t313594167 
{
public:
	// System.Int32 Facebook.Unity.HttpMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HttpMethod_t313594167, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPMETHOD_T313594167_H
#ifndef OGACTIONTYPE_T397354381_H
#define OGACTIONTYPE_T397354381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.OGActionType
struct  OGActionType_t397354381 
{
public:
	// System.Int32 Facebook.Unity.OGActionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(OGActionType_t397354381, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OGACTIONTYPE_T397354381_H
#ifndef RESULTBASE_T777682874_H
#define RESULTBASE_T777682874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ResultBase
struct  ResultBase_t777682874  : public RuntimeObject
{
public:
	// System.String Facebook.Unity.ResultBase::<Error>k__BackingField
	String_t* ___U3CErrorU3Ek__BackingField_0;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.ResultBase::<ResultDictionary>k__BackingField
	RuntimeObject* ___U3CResultDictionaryU3Ek__BackingField_1;
	// System.String Facebook.Unity.ResultBase::<RawResult>k__BackingField
	String_t* ___U3CRawResultU3Ek__BackingField_2;
	// System.Boolean Facebook.Unity.ResultBase::<Cancelled>k__BackingField
	bool ___U3CCancelledU3Ek__BackingField_3;
	// System.String Facebook.Unity.ResultBase::<CallbackId>k__BackingField
	String_t* ___U3CCallbackIdU3Ek__BackingField_4;
	// System.Nullable`1<System.Int64> Facebook.Unity.ResultBase::<CanvasErrorCode>k__BackingField
	Nullable_1_t1164162090  ___U3CCanvasErrorCodeU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CErrorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ResultBase_t777682874, ___U3CErrorU3Ek__BackingField_0)); }
	inline String_t* get_U3CErrorU3Ek__BackingField_0() const { return ___U3CErrorU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CErrorU3Ek__BackingField_0() { return &___U3CErrorU3Ek__BackingField_0; }
	inline void set_U3CErrorU3Ek__BackingField_0(String_t* value)
	{
		___U3CErrorU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CResultDictionaryU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ResultBase_t777682874, ___U3CResultDictionaryU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CResultDictionaryU3Ek__BackingField_1() const { return ___U3CResultDictionaryU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CResultDictionaryU3Ek__BackingField_1() { return &___U3CResultDictionaryU3Ek__BackingField_1; }
	inline void set_U3CResultDictionaryU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CResultDictionaryU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResultDictionaryU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CRawResultU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ResultBase_t777682874, ___U3CRawResultU3Ek__BackingField_2)); }
	inline String_t* get_U3CRawResultU3Ek__BackingField_2() const { return ___U3CRawResultU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CRawResultU3Ek__BackingField_2() { return &___U3CRawResultU3Ek__BackingField_2; }
	inline void set_U3CRawResultU3Ek__BackingField_2(String_t* value)
	{
		___U3CRawResultU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRawResultU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CCancelledU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ResultBase_t777682874, ___U3CCancelledU3Ek__BackingField_3)); }
	inline bool get_U3CCancelledU3Ek__BackingField_3() const { return ___U3CCancelledU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CCancelledU3Ek__BackingField_3() { return &___U3CCancelledU3Ek__BackingField_3; }
	inline void set_U3CCancelledU3Ek__BackingField_3(bool value)
	{
		___U3CCancelledU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CCallbackIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ResultBase_t777682874, ___U3CCallbackIdU3Ek__BackingField_4)); }
	inline String_t* get_U3CCallbackIdU3Ek__BackingField_4() const { return ___U3CCallbackIdU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CCallbackIdU3Ek__BackingField_4() { return &___U3CCallbackIdU3Ek__BackingField_4; }
	inline void set_U3CCallbackIdU3Ek__BackingField_4(String_t* value)
	{
		___U3CCallbackIdU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackIdU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CCanvasErrorCodeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ResultBase_t777682874, ___U3CCanvasErrorCodeU3Ek__BackingField_5)); }
	inline Nullable_1_t1164162090  get_U3CCanvasErrorCodeU3Ek__BackingField_5() const { return ___U3CCanvasErrorCodeU3Ek__BackingField_5; }
	inline Nullable_1_t1164162090 * get_address_of_U3CCanvasErrorCodeU3Ek__BackingField_5() { return &___U3CCanvasErrorCodeU3Ek__BackingField_5; }
	inline void set_U3CCanvasErrorCodeU3Ek__BackingField_5(Nullable_1_t1164162090  value)
	{
		___U3CCanvasErrorCodeU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTBASE_T777682874_H
#ifndef SHAREDIALOGMODE_T1679970535_H
#define SHAREDIALOGMODE_T1679970535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ShareDialogMode
struct  ShareDialogMode_t1679970535 
{
public:
	// System.Int32 Facebook.Unity.ShareDialogMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShareDialogMode_t1679970535, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREDIALOGMODE_T1679970535_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_9)); }
	inline DelegateData_t1677132599 * get_data_9() const { return ___data_9; }
	inline DelegateData_t1677132599 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1677132599 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t1188392813_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1677132599 * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T1188392813_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef ACCESSTOKENREFRESHRESULT_T1866521153_H
#define ACCESSTOKENREFRESHRESULT_T1866521153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AccessTokenRefreshResult
struct  AccessTokenRefreshResult_t1866521153  : public ResultBase_t777682874
{
public:
	// Facebook.Unity.AccessToken Facebook.Unity.AccessTokenRefreshResult::<AccessToken>k__BackingField
	AccessToken_t2431487013 * ___U3CAccessTokenU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CAccessTokenU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AccessTokenRefreshResult_t1866521153, ___U3CAccessTokenU3Ek__BackingField_6)); }
	inline AccessToken_t2431487013 * get_U3CAccessTokenU3Ek__BackingField_6() const { return ___U3CAccessTokenU3Ek__BackingField_6; }
	inline AccessToken_t2431487013 ** get_address_of_U3CAccessTokenU3Ek__BackingField_6() { return &___U3CAccessTokenU3Ek__BackingField_6; }
	inline void set_U3CAccessTokenU3Ek__BackingField_6(AccessToken_t2431487013 * value)
	{
		___U3CAccessTokenU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAccessTokenU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESSTOKENREFRESHRESULT_T1866521153_H
#ifndef APPLINKRESULT_T495655548_H
#define APPLINKRESULT_T495655548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AppLinkResult
struct  AppLinkResult_t495655548  : public ResultBase_t777682874
{
public:
	// System.String Facebook.Unity.AppLinkResult::<Url>k__BackingField
	String_t* ___U3CUrlU3Ek__BackingField_6;
	// System.String Facebook.Unity.AppLinkResult::<TargetUrl>k__BackingField
	String_t* ___U3CTargetUrlU3Ek__BackingField_7;
	// System.String Facebook.Unity.AppLinkResult::<Ref>k__BackingField
	String_t* ___U3CRefU3Ek__BackingField_8;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.AppLinkResult::<Extras>k__BackingField
	RuntimeObject* ___U3CExtrasU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CUrlU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AppLinkResult_t495655548, ___U3CUrlU3Ek__BackingField_6)); }
	inline String_t* get_U3CUrlU3Ek__BackingField_6() const { return ___U3CUrlU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CUrlU3Ek__BackingField_6() { return &___U3CUrlU3Ek__BackingField_6; }
	inline void set_U3CUrlU3Ek__BackingField_6(String_t* value)
	{
		___U3CUrlU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUrlU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CTargetUrlU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AppLinkResult_t495655548, ___U3CTargetUrlU3Ek__BackingField_7)); }
	inline String_t* get_U3CTargetUrlU3Ek__BackingField_7() const { return ___U3CTargetUrlU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CTargetUrlU3Ek__BackingField_7() { return &___U3CTargetUrlU3Ek__BackingField_7; }
	inline void set_U3CTargetUrlU3Ek__BackingField_7(String_t* value)
	{
		___U3CTargetUrlU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTargetUrlU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CRefU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AppLinkResult_t495655548, ___U3CRefU3Ek__BackingField_8)); }
	inline String_t* get_U3CRefU3Ek__BackingField_8() const { return ___U3CRefU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CRefU3Ek__BackingField_8() { return &___U3CRefU3Ek__BackingField_8; }
	inline void set_U3CRefU3Ek__BackingField_8(String_t* value)
	{
		___U3CRefU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRefU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CExtrasU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(AppLinkResult_t495655548, ___U3CExtrasU3Ek__BackingField_9)); }
	inline RuntimeObject* get_U3CExtrasU3Ek__BackingField_9() const { return ___U3CExtrasU3Ek__BackingField_9; }
	inline RuntimeObject** get_address_of_U3CExtrasU3Ek__BackingField_9() { return &___U3CExtrasU3Ek__BackingField_9; }
	inline void set_U3CExtrasU3Ek__BackingField_9(RuntimeObject* value)
	{
		___U3CExtrasU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtrasU3Ek__BackingField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLINKRESULT_T495655548_H
#ifndef APPREQUESTRESULT_T635749912_H
#define APPREQUESTRESULT_T635749912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AppRequestResult
struct  AppRequestResult_t635749912  : public ResultBase_t777682874
{
public:
	// System.String Facebook.Unity.AppRequestResult::<RequestID>k__BackingField
	String_t* ___U3CRequestIDU3Ek__BackingField_6;
	// System.Collections.Generic.IEnumerable`1<System.String> Facebook.Unity.AppRequestResult::<To>k__BackingField
	RuntimeObject* ___U3CToU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CRequestIDU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AppRequestResult_t635749912, ___U3CRequestIDU3Ek__BackingField_6)); }
	inline String_t* get_U3CRequestIDU3Ek__BackingField_6() const { return ___U3CRequestIDU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CRequestIDU3Ek__BackingField_6() { return &___U3CRequestIDU3Ek__BackingField_6; }
	inline void set_U3CRequestIDU3Ek__BackingField_6(String_t* value)
	{
		___U3CRequestIDU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRequestIDU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CToU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AppRequestResult_t635749912, ___U3CToU3Ek__BackingField_7)); }
	inline RuntimeObject* get_U3CToU3Ek__BackingField_7() const { return ___U3CToU3Ek__BackingField_7; }
	inline RuntimeObject** get_address_of_U3CToU3Ek__BackingField_7() { return &___U3CToU3Ek__BackingField_7; }
	inline void set_U3CToU3Ek__BackingField_7(RuntimeObject* value)
	{
		___U3CToU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CToU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPREQUESTRESULT_T635749912_H
#ifndef EDITORFACEBOOK_T2350935743_H
#define EDITORFACEBOOK_T2350935743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.EditorFacebook
struct  EditorFacebook_t2350935743  : public FacebookBase_t1615169142
{
public:
	// Facebook.Unity.Editor.IEditorWrapper Facebook.Unity.Editor.EditorFacebook::editorWrapper
	RuntimeObject* ___editorWrapper_3;
	// System.Boolean Facebook.Unity.Editor.EditorFacebook::<LimitEventUsage>k__BackingField
	bool ___U3CLimitEventUsageU3Ek__BackingField_4;
	// Facebook.Unity.ShareDialogMode Facebook.Unity.Editor.EditorFacebook::<ShareDialogMode>k__BackingField
	int32_t ___U3CShareDialogModeU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_editorWrapper_3() { return static_cast<int32_t>(offsetof(EditorFacebook_t2350935743, ___editorWrapper_3)); }
	inline RuntimeObject* get_editorWrapper_3() const { return ___editorWrapper_3; }
	inline RuntimeObject** get_address_of_editorWrapper_3() { return &___editorWrapper_3; }
	inline void set_editorWrapper_3(RuntimeObject* value)
	{
		___editorWrapper_3 = value;
		Il2CppCodeGenWriteBarrier((&___editorWrapper_3), value);
	}

	inline static int32_t get_offset_of_U3CLimitEventUsageU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(EditorFacebook_t2350935743, ___U3CLimitEventUsageU3Ek__BackingField_4)); }
	inline bool get_U3CLimitEventUsageU3Ek__BackingField_4() const { return ___U3CLimitEventUsageU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CLimitEventUsageU3Ek__BackingField_4() { return &___U3CLimitEventUsageU3Ek__BackingField_4; }
	inline void set_U3CLimitEventUsageU3Ek__BackingField_4(bool value)
	{
		___U3CLimitEventUsageU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CShareDialogModeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(EditorFacebook_t2350935743, ___U3CShareDialogModeU3Ek__BackingField_5)); }
	inline int32_t get_U3CShareDialogModeU3Ek__BackingField_5() const { return ___U3CShareDialogModeU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CShareDialogModeU3Ek__BackingField_5() { return &___U3CShareDialogModeU3Ek__BackingField_5; }
	inline void set_U3CShareDialogModeU3Ek__BackingField_5(int32_t value)
	{
		___U3CShareDialogModeU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORFACEBOOK_T2350935743_H
#ifndef GRAPHRESULT_T1918224463_H
#define GRAPHRESULT_T1918224463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.GraphResult
struct  GraphResult_t1918224463  : public ResultBase_t777682874
{
public:
	// System.Collections.Generic.IList`1<System.Object> Facebook.Unity.GraphResult::<ResultList>k__BackingField
	RuntimeObject* ___U3CResultListU3Ek__BackingField_6;
	// UnityEngine.Texture2D Facebook.Unity.GraphResult::<Texture>k__BackingField
	Texture2D_t3840446185 * ___U3CTextureU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CResultListU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GraphResult_t1918224463, ___U3CResultListU3Ek__BackingField_6)); }
	inline RuntimeObject* get_U3CResultListU3Ek__BackingField_6() const { return ___U3CResultListU3Ek__BackingField_6; }
	inline RuntimeObject** get_address_of_U3CResultListU3Ek__BackingField_6() { return &___U3CResultListU3Ek__BackingField_6; }
	inline void set_U3CResultListU3Ek__BackingField_6(RuntimeObject* value)
	{
		___U3CResultListU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResultListU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CTextureU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GraphResult_t1918224463, ___U3CTextureU3Ek__BackingField_7)); }
	inline Texture2D_t3840446185 * get_U3CTextureU3Ek__BackingField_7() const { return ___U3CTextureU3Ek__BackingField_7; }
	inline Texture2D_t3840446185 ** get_address_of_U3CTextureU3Ek__BackingField_7() { return &___U3CTextureU3Ek__BackingField_7; }
	inline void set_U3CTextureU3Ek__BackingField_7(Texture2D_t3840446185 * value)
	{
		___U3CTextureU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextureU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHRESULT_T1918224463_H
#ifndef LOGINRESULT_T3585129065_H
#define LOGINRESULT_T3585129065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.LoginResult
struct  LoginResult_t3585129065  : public ResultBase_t777682874
{
public:
	// Facebook.Unity.AccessToken Facebook.Unity.LoginResult::<AccessToken>k__BackingField
	AccessToken_t2431487013 * ___U3CAccessTokenU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CAccessTokenU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(LoginResult_t3585129065, ___U3CAccessTokenU3Ek__BackingField_10)); }
	inline AccessToken_t2431487013 * get_U3CAccessTokenU3Ek__BackingField_10() const { return ___U3CAccessTokenU3Ek__BackingField_10; }
	inline AccessToken_t2431487013 ** get_address_of_U3CAccessTokenU3Ek__BackingField_10() { return &___U3CAccessTokenU3Ek__BackingField_10; }
	inline void set_U3CAccessTokenU3Ek__BackingField_10(AccessToken_t2431487013 * value)
	{
		___U3CAccessTokenU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAccessTokenU3Ek__BackingField_10), value);
	}
};

struct LoginResult_t3585129065_StaticFields
{
public:
	// System.String Facebook.Unity.LoginResult::UserIdKey
	String_t* ___UserIdKey_6;
	// System.String Facebook.Unity.LoginResult::ExpirationTimestampKey
	String_t* ___ExpirationTimestampKey_7;
	// System.String Facebook.Unity.LoginResult::PermissionsKey
	String_t* ___PermissionsKey_8;
	// System.String Facebook.Unity.LoginResult::AccessTokenKey
	String_t* ___AccessTokenKey_9;

public:
	inline static int32_t get_offset_of_UserIdKey_6() { return static_cast<int32_t>(offsetof(LoginResult_t3585129065_StaticFields, ___UserIdKey_6)); }
	inline String_t* get_UserIdKey_6() const { return ___UserIdKey_6; }
	inline String_t** get_address_of_UserIdKey_6() { return &___UserIdKey_6; }
	inline void set_UserIdKey_6(String_t* value)
	{
		___UserIdKey_6 = value;
		Il2CppCodeGenWriteBarrier((&___UserIdKey_6), value);
	}

	inline static int32_t get_offset_of_ExpirationTimestampKey_7() { return static_cast<int32_t>(offsetof(LoginResult_t3585129065_StaticFields, ___ExpirationTimestampKey_7)); }
	inline String_t* get_ExpirationTimestampKey_7() const { return ___ExpirationTimestampKey_7; }
	inline String_t** get_address_of_ExpirationTimestampKey_7() { return &___ExpirationTimestampKey_7; }
	inline void set_ExpirationTimestampKey_7(String_t* value)
	{
		___ExpirationTimestampKey_7 = value;
		Il2CppCodeGenWriteBarrier((&___ExpirationTimestampKey_7), value);
	}

	inline static int32_t get_offset_of_PermissionsKey_8() { return static_cast<int32_t>(offsetof(LoginResult_t3585129065_StaticFields, ___PermissionsKey_8)); }
	inline String_t* get_PermissionsKey_8() const { return ___PermissionsKey_8; }
	inline String_t** get_address_of_PermissionsKey_8() { return &___PermissionsKey_8; }
	inline void set_PermissionsKey_8(String_t* value)
	{
		___PermissionsKey_8 = value;
		Il2CppCodeGenWriteBarrier((&___PermissionsKey_8), value);
	}

	inline static int32_t get_offset_of_AccessTokenKey_9() { return static_cast<int32_t>(offsetof(LoginResult_t3585129065_StaticFields, ___AccessTokenKey_9)); }
	inline String_t* get_AccessTokenKey_9() const { return ___AccessTokenKey_9; }
	inline String_t** get_address_of_AccessTokenKey_9() { return &___AccessTokenKey_9; }
	inline void set_AccessTokenKey_9(String_t* value)
	{
		___AccessTokenKey_9 = value;
		Il2CppCodeGenWriteBarrier((&___AccessTokenKey_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINRESULT_T3585129065_H
#ifndef MOBILEFACEBOOK_T4202726836_H
#define MOBILEFACEBOOK_T4202726836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.MobileFacebook
struct  MobileFacebook_t4202726836  : public FacebookBase_t1615169142
{
public:
	// Facebook.Unity.ShareDialogMode Facebook.Unity.Mobile.MobileFacebook::shareDialogMode
	int32_t ___shareDialogMode_3;

public:
	inline static int32_t get_offset_of_shareDialogMode_3() { return static_cast<int32_t>(offsetof(MobileFacebook_t4202726836, ___shareDialogMode_3)); }
	inline int32_t get_shareDialogMode_3() const { return ___shareDialogMode_3; }
	inline int32_t* get_address_of_shareDialogMode_3() { return &___shareDialogMode_3; }
	inline void set_shareDialogMode_3(int32_t value)
	{
		___shareDialogMode_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEFACEBOOK_T4202726836_H
#ifndef PAYRESULT_T2637031204_H
#define PAYRESULT_T2637031204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.PayResult
struct  PayResult_t2637031204  : public ResultBase_t777682874
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAYRESULT_T2637031204_H
#ifndef SHARERESULT_T3324833527_H
#define SHARERESULT_T3324833527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ShareResult
struct  ShareResult_t3324833527  : public ResultBase_t777682874
{
public:
	// System.String Facebook.Unity.ShareResult::<PostId>k__BackingField
	String_t* ___U3CPostIdU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CPostIdU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ShareResult_t3324833527, ___U3CPostIdU3Ek__BackingField_6)); }
	inline String_t* get_U3CPostIdU3Ek__BackingField_6() const { return ___U3CPostIdU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CPostIdU3Ek__BackingField_6() { return &___U3CPostIdU3Ek__BackingField_6; }
	inline void set_U3CPostIdU3Ek__BackingField_6(String_t* value)
	{
		___U3CPostIdU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPostIdU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARERESULT_T3324833527_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t1703627840* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t1703627840* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t1703627840** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t1703627840* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t1188392813_marshaled_pinvoke
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t1188392813_marshaled_com
{
	DelegateU5BU5D_t1703627840* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef FB_T2178373596_H
#define FB_T2178373596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FB
struct  FB_t2178373596  : public ScriptableObject_t2528358522
{
public:

public:
};

struct FB_t2178373596_StaticFields
{
public:
	// Facebook.Unity.IFacebook Facebook.Unity.FB::facebook
	RuntimeObject* ___facebook_5;
	// System.Boolean Facebook.Unity.FB::isInitCalled
	bool ___isInitCalled_6;
	// System.String Facebook.Unity.FB::facebookDomain
	String_t* ___facebookDomain_7;
	// System.String Facebook.Unity.FB::graphApiVersion
	String_t* ___graphApiVersion_8;
	// System.String Facebook.Unity.FB::<AppId>k__BackingField
	String_t* ___U3CAppIdU3Ek__BackingField_9;
	// System.String Facebook.Unity.FB::<ClientToken>k__BackingField
	String_t* ___U3CClientTokenU3Ek__BackingField_10;
	// Facebook.Unity.FB/OnDLLLoaded Facebook.Unity.FB::<OnDLLLoadedDelegate>k__BackingField
	OnDLLLoaded_t2963647862 * ___U3COnDLLLoadedDelegateU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_facebook_5() { return static_cast<int32_t>(offsetof(FB_t2178373596_StaticFields, ___facebook_5)); }
	inline RuntimeObject* get_facebook_5() const { return ___facebook_5; }
	inline RuntimeObject** get_address_of_facebook_5() { return &___facebook_5; }
	inline void set_facebook_5(RuntimeObject* value)
	{
		___facebook_5 = value;
		Il2CppCodeGenWriteBarrier((&___facebook_5), value);
	}

	inline static int32_t get_offset_of_isInitCalled_6() { return static_cast<int32_t>(offsetof(FB_t2178373596_StaticFields, ___isInitCalled_6)); }
	inline bool get_isInitCalled_6() const { return ___isInitCalled_6; }
	inline bool* get_address_of_isInitCalled_6() { return &___isInitCalled_6; }
	inline void set_isInitCalled_6(bool value)
	{
		___isInitCalled_6 = value;
	}

	inline static int32_t get_offset_of_facebookDomain_7() { return static_cast<int32_t>(offsetof(FB_t2178373596_StaticFields, ___facebookDomain_7)); }
	inline String_t* get_facebookDomain_7() const { return ___facebookDomain_7; }
	inline String_t** get_address_of_facebookDomain_7() { return &___facebookDomain_7; }
	inline void set_facebookDomain_7(String_t* value)
	{
		___facebookDomain_7 = value;
		Il2CppCodeGenWriteBarrier((&___facebookDomain_7), value);
	}

	inline static int32_t get_offset_of_graphApiVersion_8() { return static_cast<int32_t>(offsetof(FB_t2178373596_StaticFields, ___graphApiVersion_8)); }
	inline String_t* get_graphApiVersion_8() const { return ___graphApiVersion_8; }
	inline String_t** get_address_of_graphApiVersion_8() { return &___graphApiVersion_8; }
	inline void set_graphApiVersion_8(String_t* value)
	{
		___graphApiVersion_8 = value;
		Il2CppCodeGenWriteBarrier((&___graphApiVersion_8), value);
	}

	inline static int32_t get_offset_of_U3CAppIdU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(FB_t2178373596_StaticFields, ___U3CAppIdU3Ek__BackingField_9)); }
	inline String_t* get_U3CAppIdU3Ek__BackingField_9() const { return ___U3CAppIdU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CAppIdU3Ek__BackingField_9() { return &___U3CAppIdU3Ek__BackingField_9; }
	inline void set_U3CAppIdU3Ek__BackingField_9(String_t* value)
	{
		___U3CAppIdU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAppIdU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CClientTokenU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(FB_t2178373596_StaticFields, ___U3CClientTokenU3Ek__BackingField_10)); }
	inline String_t* get_U3CClientTokenU3Ek__BackingField_10() const { return ___U3CClientTokenU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CClientTokenU3Ek__BackingField_10() { return &___U3CClientTokenU3Ek__BackingField_10; }
	inline void set_U3CClientTokenU3Ek__BackingField_10(String_t* value)
	{
		___U3CClientTokenU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClientTokenU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3COnDLLLoadedDelegateU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(FB_t2178373596_StaticFields, ___U3COnDLLLoadedDelegateU3Ek__BackingField_11)); }
	inline OnDLLLoaded_t2963647862 * get_U3COnDLLLoadedDelegateU3Ek__BackingField_11() const { return ___U3COnDLLLoadedDelegateU3Ek__BackingField_11; }
	inline OnDLLLoaded_t2963647862 ** get_address_of_U3COnDLLLoadedDelegateU3Ek__BackingField_11() { return &___U3COnDLLLoadedDelegateU3Ek__BackingField_11; }
	inline void set_U3COnDLLLoadedDelegateU3Ek__BackingField_11(OnDLLLoaded_t2963647862 * value)
	{
		___U3COnDLLLoadedDelegateU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnDLLLoadedDelegateU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FB_T2178373596_H
#ifndef ONDLLLOADED_T2963647862_H
#define ONDLLLOADED_T2963647862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FB/OnDLLLoaded
struct  OnDLLLoaded_t2963647862  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDLLLOADED_T2963647862_H
#ifndef ONCOMPLETE_T3786498944_H
#define ONCOMPLETE_T3786498944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Gameroom.GameroomFacebook/OnComplete
struct  OnComplete_t3786498944  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCOMPLETE_T3786498944_H
#ifndef HIDEUNITYDELEGATE_T1353799728_H
#define HIDEUNITYDELEGATE_T1353799728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.HideUnityDelegate
struct  HideUnityDelegate_t1353799728  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEUNITYDELEGATE_T1353799728_H
#ifndef INITDELEGATE_T3081360126_H
#define INITDELEGATE_T3081360126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.InitDelegate
struct  InitDelegate_t3081360126  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITDELEGATE_T3081360126_H
#ifndef ANDROIDFACEBOOK_T3975172769_H
#define ANDROIDFACEBOOK_T3975172769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.Android.AndroidFacebook
struct  AndroidFacebook_t3975172769  : public MobileFacebook_t4202726836
{
public:
	// System.Boolean Facebook.Unity.Mobile.Android.AndroidFacebook::limitEventUsage
	bool ___limitEventUsage_4;
	// Facebook.Unity.Mobile.Android.IAndroidWrapper Facebook.Unity.Mobile.Android.AndroidFacebook::androidWrapper
	RuntimeObject* ___androidWrapper_5;
	// System.String Facebook.Unity.Mobile.Android.AndroidFacebook::userID
	String_t* ___userID_6;
	// System.String Facebook.Unity.Mobile.Android.AndroidFacebook::<KeyHash>k__BackingField
	String_t* ___U3CKeyHashU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_limitEventUsage_4() { return static_cast<int32_t>(offsetof(AndroidFacebook_t3975172769, ___limitEventUsage_4)); }
	inline bool get_limitEventUsage_4() const { return ___limitEventUsage_4; }
	inline bool* get_address_of_limitEventUsage_4() { return &___limitEventUsage_4; }
	inline void set_limitEventUsage_4(bool value)
	{
		___limitEventUsage_4 = value;
	}

	inline static int32_t get_offset_of_androidWrapper_5() { return static_cast<int32_t>(offsetof(AndroidFacebook_t3975172769, ___androidWrapper_5)); }
	inline RuntimeObject* get_androidWrapper_5() const { return ___androidWrapper_5; }
	inline RuntimeObject** get_address_of_androidWrapper_5() { return &___androidWrapper_5; }
	inline void set_androidWrapper_5(RuntimeObject* value)
	{
		___androidWrapper_5 = value;
		Il2CppCodeGenWriteBarrier((&___androidWrapper_5), value);
	}

	inline static int32_t get_offset_of_userID_6() { return static_cast<int32_t>(offsetof(AndroidFacebook_t3975172769, ___userID_6)); }
	inline String_t* get_userID_6() const { return ___userID_6; }
	inline String_t** get_address_of_userID_6() { return &___userID_6; }
	inline void set_userID_6(String_t* value)
	{
		___userID_6 = value;
		Il2CppCodeGenWriteBarrier((&___userID_6), value);
	}

	inline static int32_t get_offset_of_U3CKeyHashU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AndroidFacebook_t3975172769, ___U3CKeyHashU3Ek__BackingField_7)); }
	inline String_t* get_U3CKeyHashU3Ek__BackingField_7() const { return ___U3CKeyHashU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CKeyHashU3Ek__BackingField_7() { return &___U3CKeyHashU3Ek__BackingField_7; }
	inline void set_U3CKeyHashU3Ek__BackingField_7(String_t* value)
	{
		___U3CKeyHashU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeyHashU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDFACEBOOK_T3975172769_H
#ifndef IOSFACEBOOK_T793364667_H
#define IOSFACEBOOK_T793364667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.IOS.IOSFacebook
struct  IOSFacebook_t793364667  : public MobileFacebook_t4202726836
{
public:
	// System.Boolean Facebook.Unity.Mobile.IOS.IOSFacebook::limitEventUsage
	bool ___limitEventUsage_4;
	// Facebook.Unity.Mobile.IOS.IIOSWrapper Facebook.Unity.Mobile.IOS.IOSFacebook::iosWrapper
	RuntimeObject* ___iosWrapper_5;
	// System.String Facebook.Unity.Mobile.IOS.IOSFacebook::userID
	String_t* ___userID_6;

public:
	inline static int32_t get_offset_of_limitEventUsage_4() { return static_cast<int32_t>(offsetof(IOSFacebook_t793364667, ___limitEventUsage_4)); }
	inline bool get_limitEventUsage_4() const { return ___limitEventUsage_4; }
	inline bool* get_address_of_limitEventUsage_4() { return &___limitEventUsage_4; }
	inline void set_limitEventUsage_4(bool value)
	{
		___limitEventUsage_4 = value;
	}

	inline static int32_t get_offset_of_iosWrapper_5() { return static_cast<int32_t>(offsetof(IOSFacebook_t793364667, ___iosWrapper_5)); }
	inline RuntimeObject* get_iosWrapper_5() const { return ___iosWrapper_5; }
	inline RuntimeObject** get_address_of_iosWrapper_5() { return &___iosWrapper_5; }
	inline void set_iosWrapper_5(RuntimeObject* value)
	{
		___iosWrapper_5 = value;
		Il2CppCodeGenWriteBarrier((&___iosWrapper_5), value);
	}

	inline static int32_t get_offset_of_userID_6() { return static_cast<int32_t>(offsetof(IOSFacebook_t793364667, ___userID_6)); }
	inline String_t* get_userID_6() const { return ___userID_6; }
	inline String_t** get_address_of_userID_6() { return &___userID_6; }
	inline void set_userID_6(String_t* value)
	{
		___userID_6 = value;
		Il2CppCodeGenWriteBarrier((&___userID_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSFACEBOOK_T793364667_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ASYNCREQUESTSTRING_T3669574124_H
#define ASYNCREQUESTSTRING_T3669574124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AsyncRequestString
struct  AsyncRequestString_t3669574124  : public MonoBehaviour_t3962482529
{
public:
	// System.Uri Facebook.Unity.AsyncRequestString::url
	Uri_t100236324 * ___url_4;
	// Facebook.Unity.HttpMethod Facebook.Unity.AsyncRequestString::method
	int32_t ___method_5;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Facebook.Unity.AsyncRequestString::formData
	RuntimeObject* ___formData_6;
	// UnityEngine.WWWForm Facebook.Unity.AsyncRequestString::query
	WWWForm_t4064702195 * ___query_7;
	// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult> Facebook.Unity.AsyncRequestString::callback
	FacebookDelegate_1_t138390958 * ___callback_8;

public:
	inline static int32_t get_offset_of_url_4() { return static_cast<int32_t>(offsetof(AsyncRequestString_t3669574124, ___url_4)); }
	inline Uri_t100236324 * get_url_4() const { return ___url_4; }
	inline Uri_t100236324 ** get_address_of_url_4() { return &___url_4; }
	inline void set_url_4(Uri_t100236324 * value)
	{
		___url_4 = value;
		Il2CppCodeGenWriteBarrier((&___url_4), value);
	}

	inline static int32_t get_offset_of_method_5() { return static_cast<int32_t>(offsetof(AsyncRequestString_t3669574124, ___method_5)); }
	inline int32_t get_method_5() const { return ___method_5; }
	inline int32_t* get_address_of_method_5() { return &___method_5; }
	inline void set_method_5(int32_t value)
	{
		___method_5 = value;
	}

	inline static int32_t get_offset_of_formData_6() { return static_cast<int32_t>(offsetof(AsyncRequestString_t3669574124, ___formData_6)); }
	inline RuntimeObject* get_formData_6() const { return ___formData_6; }
	inline RuntimeObject** get_address_of_formData_6() { return &___formData_6; }
	inline void set_formData_6(RuntimeObject* value)
	{
		___formData_6 = value;
		Il2CppCodeGenWriteBarrier((&___formData_6), value);
	}

	inline static int32_t get_offset_of_query_7() { return static_cast<int32_t>(offsetof(AsyncRequestString_t3669574124, ___query_7)); }
	inline WWWForm_t4064702195 * get_query_7() const { return ___query_7; }
	inline WWWForm_t4064702195 ** get_address_of_query_7() { return &___query_7; }
	inline void set_query_7(WWWForm_t4064702195 * value)
	{
		___query_7 = value;
		Il2CppCodeGenWriteBarrier((&___query_7), value);
	}

	inline static int32_t get_offset_of_callback_8() { return static_cast<int32_t>(offsetof(AsyncRequestString_t3669574124, ___callback_8)); }
	inline FacebookDelegate_1_t138390958 * get_callback_8() const { return ___callback_8; }
	inline FacebookDelegate_1_t138390958 ** get_address_of_callback_8() { return &___callback_8; }
	inline void set_callback_8(FacebookDelegate_1_t138390958 * value)
	{
		___callback_8 = value;
		Il2CppCodeGenWriteBarrier((&___callback_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCREQUESTSTRING_T3669574124_H
#ifndef CODELESSCRAWLER_T2306925468_H
#define CODELESSCRAWLER_T2306925468_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.CodelessCrawler
struct  CodelessCrawler_t2306925468  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct CodelessCrawler_t2306925468_StaticFields
{
public:
	// System.Boolean Facebook.Unity.CodelessCrawler::isGeneratingSnapshot
	bool ___isGeneratingSnapshot_4;
	// UnityEngine.Camera Facebook.Unity.CodelessCrawler::mainCamera
	Camera_t4157153871 * ___mainCamera_5;

public:
	inline static int32_t get_offset_of_isGeneratingSnapshot_4() { return static_cast<int32_t>(offsetof(CodelessCrawler_t2306925468_StaticFields, ___isGeneratingSnapshot_4)); }
	inline bool get_isGeneratingSnapshot_4() const { return ___isGeneratingSnapshot_4; }
	inline bool* get_address_of_isGeneratingSnapshot_4() { return &___isGeneratingSnapshot_4; }
	inline void set_isGeneratingSnapshot_4(bool value)
	{
		___isGeneratingSnapshot_4 = value;
	}

	inline static int32_t get_offset_of_mainCamera_5() { return static_cast<int32_t>(offsetof(CodelessCrawler_t2306925468_StaticFields, ___mainCamera_5)); }
	inline Camera_t4157153871 * get_mainCamera_5() const { return ___mainCamera_5; }
	inline Camera_t4157153871 ** get_address_of_mainCamera_5() { return &___mainCamera_5; }
	inline void set_mainCamera_5(Camera_t4157153871 * value)
	{
		___mainCamera_5 = value;
		Il2CppCodeGenWriteBarrier((&___mainCamera_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODELESSCRAWLER_T2306925468_H
#ifndef CODELESSUIINTERACTEVENT_T546423177_H
#define CODELESSUIINTERACTEVENT_T546423177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.CodelessUIInteractEvent
struct  CodelessUIInteractEvent_t546423177  : public MonoBehaviour_t3962482529
{
public:
	// Facebook.Unity.FBSDKEventBindingManager Facebook.Unity.CodelessUIInteractEvent::<eventBindingManager>k__BackingField
	FBSDKEventBindingManager_t2486575668 * ___U3CeventBindingManagerU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CeventBindingManagerU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CodelessUIInteractEvent_t546423177, ___U3CeventBindingManagerU3Ek__BackingField_4)); }
	inline FBSDKEventBindingManager_t2486575668 * get_U3CeventBindingManagerU3Ek__BackingField_4() const { return ___U3CeventBindingManagerU3Ek__BackingField_4; }
	inline FBSDKEventBindingManager_t2486575668 ** get_address_of_U3CeventBindingManagerU3Ek__BackingField_4() { return &___U3CeventBindingManagerU3Ek__BackingField_4; }
	inline void set_U3CeventBindingManagerU3Ek__BackingField_4(FBSDKEventBindingManager_t2486575668 * value)
	{
		___U3CeventBindingManagerU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CeventBindingManagerU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODELESSUIINTERACTEVENT_T546423177_H
#ifndef EDITORFACEBOOKMOCKDIALOG_T3691766720_H
#define EDITORFACEBOOKMOCKDIALOG_T3691766720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.EditorFacebookMockDialog
struct  EditorFacebookMockDialog_t3691766720  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rect Facebook.Unity.Editor.EditorFacebookMockDialog::modalRect
	Rect_t2360479859  ___modalRect_4;
	// UnityEngine.GUIStyle Facebook.Unity.Editor.EditorFacebookMockDialog::modalStyle
	GUIStyle_t3956901511 * ___modalStyle_5;
	// Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer> Facebook.Unity.Editor.EditorFacebookMockDialog::<Callback>k__BackingField
	Callback_1_t3507265931 * ___U3CCallbackU3Ek__BackingField_6;
	// System.String Facebook.Unity.Editor.EditorFacebookMockDialog::<CallbackID>k__BackingField
	String_t* ___U3CCallbackIDU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_modalRect_4() { return static_cast<int32_t>(offsetof(EditorFacebookMockDialog_t3691766720, ___modalRect_4)); }
	inline Rect_t2360479859  get_modalRect_4() const { return ___modalRect_4; }
	inline Rect_t2360479859 * get_address_of_modalRect_4() { return &___modalRect_4; }
	inline void set_modalRect_4(Rect_t2360479859  value)
	{
		___modalRect_4 = value;
	}

	inline static int32_t get_offset_of_modalStyle_5() { return static_cast<int32_t>(offsetof(EditorFacebookMockDialog_t3691766720, ___modalStyle_5)); }
	inline GUIStyle_t3956901511 * get_modalStyle_5() const { return ___modalStyle_5; }
	inline GUIStyle_t3956901511 ** get_address_of_modalStyle_5() { return &___modalStyle_5; }
	inline void set_modalStyle_5(GUIStyle_t3956901511 * value)
	{
		___modalStyle_5 = value;
		Il2CppCodeGenWriteBarrier((&___modalStyle_5), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(EditorFacebookMockDialog_t3691766720, ___U3CCallbackU3Ek__BackingField_6)); }
	inline Callback_1_t3507265931 * get_U3CCallbackU3Ek__BackingField_6() const { return ___U3CCallbackU3Ek__BackingField_6; }
	inline Callback_1_t3507265931 ** get_address_of_U3CCallbackU3Ek__BackingField_6() { return &___U3CCallbackU3Ek__BackingField_6; }
	inline void set_U3CCallbackU3Ek__BackingField_6(Callback_1_t3507265931 * value)
	{
		___U3CCallbackU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CCallbackIDU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(EditorFacebookMockDialog_t3691766720, ___U3CCallbackIDU3Ek__BackingField_7)); }
	inline String_t* get_U3CCallbackIDU3Ek__BackingField_7() const { return ___U3CCallbackIDU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CCallbackIDU3Ek__BackingField_7() { return &___U3CCallbackIDU3Ek__BackingField_7; }
	inline void set_U3CCallbackIDU3Ek__BackingField_7(String_t* value)
	{
		___U3CCallbackIDU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackIDU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORFACEBOOKMOCKDIALOG_T3691766720_H
#ifndef COMPILEDFACEBOOKLOADER_T350999866_H
#define COMPILEDFACEBOOKLOADER_T350999866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FB/CompiledFacebookLoader
struct  CompiledFacebookLoader_t350999866  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPILEDFACEBOOKLOADER_T350999866_H
#ifndef FACEBOOKGAMEOBJECT_T3770693708_H
#define FACEBOOKGAMEOBJECT_T3770693708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookGameObject
struct  FacebookGameObject_t3770693708  : public MonoBehaviour_t3962482529
{
public:
	// Facebook.Unity.IFacebookImplementation Facebook.Unity.FacebookGameObject::<Facebook>k__BackingField
	RuntimeObject* ___U3CFacebookU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CFacebookU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FacebookGameObject_t3770693708, ___U3CFacebookU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CFacebookU3Ek__BackingField_4() const { return ___U3CFacebookU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CFacebookU3Ek__BackingField_4() { return &___U3CFacebookU3Ek__BackingField_4; }
	inline void set_U3CFacebookU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CFacebookU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFacebookU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKGAMEOBJECT_T3770693708_H
#ifndef FACEBOOKSCHEDULER_T1007614953_H
#define FACEBOOKSCHEDULER_T1007614953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookScheduler
struct  FacebookScheduler_t1007614953  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKSCHEDULER_T1007614953_H
#ifndef EMPTYMOCKDIALOG_T3184315303_H
#define EMPTYMOCKDIALOG_T3184315303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.Dialogs.EmptyMockDialog
struct  EmptyMockDialog_t3184315303  : public EditorFacebookMockDialog_t3691766720
{
public:
	// System.String Facebook.Unity.Editor.Dialogs.EmptyMockDialog::<EmptyDialogTitle>k__BackingField
	String_t* ___U3CEmptyDialogTitleU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CEmptyDialogTitleU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(EmptyMockDialog_t3184315303, ___U3CEmptyDialogTitleU3Ek__BackingField_8)); }
	inline String_t* get_U3CEmptyDialogTitleU3Ek__BackingField_8() const { return ___U3CEmptyDialogTitleU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CEmptyDialogTitleU3Ek__BackingField_8() { return &___U3CEmptyDialogTitleU3Ek__BackingField_8; }
	inline void set_U3CEmptyDialogTitleU3Ek__BackingField_8(String_t* value)
	{
		___U3CEmptyDialogTitleU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEmptyDialogTitleU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYMOCKDIALOG_T3184315303_H
#ifndef MOCKLOGINDIALOG_T1686232399_H
#define MOCKLOGINDIALOG_T1686232399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.Dialogs.MockLoginDialog
struct  MockLoginDialog_t1686232399  : public EditorFacebookMockDialog_t3691766720
{
public:
	// System.String Facebook.Unity.Editor.Dialogs.MockLoginDialog::accessToken
	String_t* ___accessToken_8;

public:
	inline static int32_t get_offset_of_accessToken_8() { return static_cast<int32_t>(offsetof(MockLoginDialog_t1686232399, ___accessToken_8)); }
	inline String_t* get_accessToken_8() const { return ___accessToken_8; }
	inline String_t** get_address_of_accessToken_8() { return &___accessToken_8; }
	inline void set_accessToken_8(String_t* value)
	{
		___accessToken_8 = value;
		Il2CppCodeGenWriteBarrier((&___accessToken_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOCKLOGINDIALOG_T1686232399_H
#ifndef MOCKSHAREDIALOG_T1183999029_H
#define MOCKSHAREDIALOG_T1183999029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.Dialogs.MockShareDialog
struct  MockShareDialog_t1183999029  : public EditorFacebookMockDialog_t3691766720
{
public:
	// System.String Facebook.Unity.Editor.Dialogs.MockShareDialog::<SubTitle>k__BackingField
	String_t* ___U3CSubTitleU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CSubTitleU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(MockShareDialog_t1183999029, ___U3CSubTitleU3Ek__BackingField_8)); }
	inline String_t* get_U3CSubTitleU3Ek__BackingField_8() const { return ___U3CSubTitleU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CSubTitleU3Ek__BackingField_8() { return &___U3CSubTitleU3Ek__BackingField_8; }
	inline void set_U3CSubTitleU3Ek__BackingField_8(String_t* value)
	{
		___U3CSubTitleU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSubTitleU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOCKSHAREDIALOG_T1183999029_H
#ifndef EDITORFACEBOOKGAMEOBJECT_T4122771246_H
#define EDITORFACEBOOKGAMEOBJECT_T4122771246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.EditorFacebookGameObject
struct  EditorFacebookGameObject_t4122771246  : public FacebookGameObject_t3770693708
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORFACEBOOKGAMEOBJECT_T4122771246_H
#ifndef EDITORFACEBOOKLOADER_T147050272_H
#define EDITORFACEBOOKLOADER_T147050272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.EditorFacebookLoader
struct  EditorFacebookLoader_t147050272  : public CompiledFacebookLoader_t350999866
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORFACEBOOKLOADER_T147050272_H
#ifndef GAMEROOMFACEBOOKGAMEOBJECT_T2310521816_H
#define GAMEROOMFACEBOOKGAMEOBJECT_T2310521816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Gameroom.GameroomFacebookGameObject
struct  GameroomFacebookGameObject_t2310521816  : public FacebookGameObject_t3770693708
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEROOMFACEBOOKGAMEOBJECT_T2310521816_H
#ifndef GAMEROOMFACEBOOKLOADER_T1800644555_H
#define GAMEROOMFACEBOOKLOADER_T1800644555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Gameroom.GameroomFacebookLoader
struct  GameroomFacebookLoader_t1800644555  : public CompiledFacebookLoader_t350999866
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEROOMFACEBOOKLOADER_T1800644555_H
#ifndef IOSFACEBOOKLOADER_T2962337469_H
#define IOSFACEBOOKLOADER_T2962337469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.IOS.IOSFacebookLoader
struct  IOSFacebookLoader_t2962337469  : public CompiledFacebookLoader_t350999866
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSFACEBOOKLOADER_T2962337469_H
#ifndef MOBILEFACEBOOKGAMEOBJECT_T508240675_H
#define MOBILEFACEBOOKGAMEOBJECT_T508240675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.MobileFacebookGameObject
struct  MobileFacebookGameObject_t508240675  : public FacebookGameObject_t3770693708
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEFACEBOOKGAMEOBJECT_T508240675_H
#ifndef ANDROIDFACEBOOKGAMEOBJECT_T4170725345_H
#define ANDROIDFACEBOOKGAMEOBJECT_T4170725345_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.Android.AndroidFacebookGameObject
struct  AndroidFacebookGameObject_t4170725345  : public MobileFacebookGameObject_t508240675
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANDROIDFACEBOOKGAMEOBJECT_T4170725345_H
#ifndef IOSFACEBOOKGAMEOBJECT_T1787084451_H
#define IOSFACEBOOKGAMEOBJECT_T1787084451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.IOS.IOSFacebookGameObject
struct  IOSFacebookGameObject_t1787084451  : public MobileFacebookGameObject_t508240675
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSFACEBOOKGAMEOBJECT_T1787084451_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3800 = { sizeof (FB_t2178373596), -1, sizeof(FB_t2178373596_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3800[8] = 
{
	0,
	FB_t2178373596_StaticFields::get_offset_of_facebook_5(),
	FB_t2178373596_StaticFields::get_offset_of_isInitCalled_6(),
	FB_t2178373596_StaticFields::get_offset_of_facebookDomain_7(),
	FB_t2178373596_StaticFields::get_offset_of_graphApiVersion_8(),
	FB_t2178373596_StaticFields::get_offset_of_U3CAppIdU3Ek__BackingField_9(),
	FB_t2178373596_StaticFields::get_offset_of_U3CClientTokenU3Ek__BackingField_10(),
	FB_t2178373596_StaticFields::get_offset_of_U3COnDLLLoadedDelegateU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3801 = { sizeof (OnDLLLoaded_t2963647862), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3802 = { sizeof (Canvas_t3146753929), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3803 = { sizeof (Mobile_t77204530), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3804 = { sizeof (CompiledFacebookLoader_t350999866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3805 = { sizeof (U3CU3Ec__DisplayClass35_0_t171294370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3805[10] = 
{
	U3CU3Ec__DisplayClass35_0_t171294370::get_offset_of_onInitComplete_0(),
	U3CU3Ec__DisplayClass35_0_t171294370::get_offset_of_appId_1(),
	U3CU3Ec__DisplayClass35_0_t171294370::get_offset_of_cookie_2(),
	U3CU3Ec__DisplayClass35_0_t171294370::get_offset_of_logging_3(),
	U3CU3Ec__DisplayClass35_0_t171294370::get_offset_of_status_4(),
	U3CU3Ec__DisplayClass35_0_t171294370::get_offset_of_xfbml_5(),
	U3CU3Ec__DisplayClass35_0_t171294370::get_offset_of_authResponse_6(),
	U3CU3Ec__DisplayClass35_0_t171294370::get_offset_of_frictionlessRequests_7(),
	U3CU3Ec__DisplayClass35_0_t171294370::get_offset_of_javascriptSDKLocale_8(),
	U3CU3Ec__DisplayClass35_0_t171294370::get_offset_of_onHideUnity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3806 = { sizeof (FacebookBase_t1615169142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3806[3] = 
{
	FacebookBase_t1615169142::get_offset_of_onInitCompleteDelegate_0(),
	FacebookBase_t1615169142::get_offset_of_U3CInitializedU3Ek__BackingField_1(),
	FacebookBase_t1615169142::get_offset_of_U3CCallbackManagerU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3807 = { sizeof (U3CU3Ec_t3332642991), -1, sizeof(U3CU3Ec_t3332642991_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3807[2] = 
{
	U3CU3Ec_t3332642991_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3332642991_StaticFields::get_offset_of_U3CU3E9__41_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3808 = { sizeof (InitDelegate_t3081360126), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3809 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3810 = { sizeof (HideUnityDelegate_t1353799728), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3811 = { sizeof (FacebookGameObject_t3770693708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3811[1] = 
{
	FacebookGameObject_t3770693708::get_offset_of_U3CFacebookU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3812 = { sizeof (FacebookSdkVersion_t4164812230), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3813 = { sizeof (FacebookUnityPlatform_t2098070734)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3813[6] = 
{
	FacebookUnityPlatform_t2098070734::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3814 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3815 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3816 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3817 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3818 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3819 = { sizeof (MethodArguments_t1563002313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3819[1] = 
{
	MethodArguments_t1563002313::get_offset_of_arguments_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3820 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3820[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3821 = { sizeof (ShareDialogMode_t1679970535)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3821[5] = 
{
	ShareDialogMode_t1679970535::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3822 = { sizeof (OGActionType_t397354381)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3822[4] = 
{
	OGActionType_t397354381::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3823 = { sizeof (AccessTokenRefreshResult_t1866521153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3823[1] = 
{
	AccessTokenRefreshResult_t1866521153::get_offset_of_U3CAccessTokenU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3824 = { sizeof (AppLinkResult_t495655548), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3824[4] = 
{
	AppLinkResult_t495655548::get_offset_of_U3CUrlU3Ek__BackingField_6(),
	AppLinkResult_t495655548::get_offset_of_U3CTargetUrlU3Ek__BackingField_7(),
	AppLinkResult_t495655548::get_offset_of_U3CRefU3Ek__BackingField_8(),
	AppLinkResult_t495655548::get_offset_of_U3CExtrasU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3825 = { sizeof (AppRequestResult_t635749912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3825[2] = 
{
	AppRequestResult_t635749912::get_offset_of_U3CRequestIDU3Ek__BackingField_6(),
	AppRequestResult_t635749912::get_offset_of_U3CToU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3826 = { sizeof (GraphResult_t1918224463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3826[2] = 
{
	GraphResult_t1918224463::get_offset_of_U3CResultListU3Ek__BackingField_6(),
	GraphResult_t1918224463::get_offset_of_U3CTextureU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3827 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3828 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3829 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3830 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3831 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3832 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3833 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3834 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3835 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3836 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3837 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3838 = { sizeof (LoginResult_t3585129065), -1, sizeof(LoginResult_t3585129065_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3838[5] = 
{
	LoginResult_t3585129065_StaticFields::get_offset_of_UserIdKey_6(),
	LoginResult_t3585129065_StaticFields::get_offset_of_ExpirationTimestampKey_7(),
	LoginResult_t3585129065_StaticFields::get_offset_of_PermissionsKey_8(),
	LoginResult_t3585129065_StaticFields::get_offset_of_AccessTokenKey_9(),
	LoginResult_t3585129065::get_offset_of_U3CAccessTokenU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3839 = { sizeof (PayResult_t2637031204), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3840 = { sizeof (ResultBase_t777682874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3840[6] = 
{
	ResultBase_t777682874::get_offset_of_U3CErrorU3Ek__BackingField_0(),
	ResultBase_t777682874::get_offset_of_U3CResultDictionaryU3Ek__BackingField_1(),
	ResultBase_t777682874::get_offset_of_U3CRawResultU3Ek__BackingField_2(),
	ResultBase_t777682874::get_offset_of_U3CCancelledU3Ek__BackingField_3(),
	ResultBase_t777682874::get_offset_of_U3CCallbackIdU3Ek__BackingField_4(),
	ResultBase_t777682874::get_offset_of_U3CCanvasErrorCodeU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3841 = { sizeof (ResultContainer_t4150301447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3841[3] = 
{
	0,
	ResultContainer_t4150301447::get_offset_of_U3CRawResultU3Ek__BackingField_1(),
	ResultContainer_t4150301447::get_offset_of_U3CResultDictionaryU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3842 = { sizeof (ShareResult_t3324833527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3842[1] = 
{
	ShareResult_t3324833527::get_offset_of_U3CPostIdU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3843 = { sizeof (AsyncRequestString_t3669574124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3843[5] = 
{
	AsyncRequestString_t3669574124::get_offset_of_url_4(),
	AsyncRequestString_t3669574124::get_offset_of_method_5(),
	AsyncRequestString_t3669574124::get_offset_of_formData_6(),
	AsyncRequestString_t3669574124::get_offset_of_query_7(),
	AsyncRequestString_t3669574124::get_offset_of_callback_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3844 = { sizeof (U3CStartU3Ed__9_t2855687532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3844[4] = 
{
	U3CStartU3Ed__9_t2855687532::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__9_t2855687532::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__9_t2855687532::get_offset_of_U3CU3E4__this_2(),
	U3CStartU3Ed__9_t2855687532::get_offset_of_U3CwwwU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3845 = { sizeof (FacebookLogger_t1173398901), -1, sizeof(FacebookLogger_t1173398901_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3845[1] = 
{
	FacebookLogger_t1173398901_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3846 = { sizeof (DebugLogger_t2300716689), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3847 = { sizeof (HttpMethod_t313594167)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable3847[4] = 
{
	HttpMethod_t313594167::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3848 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3849 = { sizeof (Utilities_t2011615223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3850 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3851 = { sizeof (U3CU3Ec_t3449414030), -1, sizeof(U3CU3Ec_t3449414030_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3851[2] = 
{
	U3CU3Ec_t3449414030_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3449414030_StaticFields::get_offset_of_U3CU3E9__18_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3852 = { sizeof (FBUnityUtility_t2214217213), -1, sizeof(FBUnityUtility_t2214217213_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3852[1] = 
{
	FBUnityUtility_t2214217213_StaticFields::get_offset_of_asyncRequestStringWrapper_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3853 = { sizeof (AsyncRequestStringWrapper_t707311955), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3854 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3855 = { sizeof (FacebookScheduler_t1007614953), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3856 = { sizeof (U3CDelayEventU3Ed__1_t2601426355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3856[4] = 
{
	U3CDelayEventU3Ed__1_t2601426355::get_offset_of_U3CU3E1__state_0(),
	U3CDelayEventU3Ed__1_t2601426355::get_offset_of_U3CU3E2__current_1(),
	U3CDelayEventU3Ed__1_t2601426355::get_offset_of_delay_2(),
	U3CDelayEventU3Ed__1_t2601426355::get_offset_of_action_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3857 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3858 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3859 = { sizeof (CodelessIAPAutoLog_t3776592774), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3860 = { sizeof (CodelessCrawler_t2306925468), -1, sizeof(CodelessCrawler_t2306925468_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable3860[2] = 
{
	CodelessCrawler_t2306925468_StaticFields::get_offset_of_isGeneratingSnapshot_4(),
	CodelessCrawler_t2306925468_StaticFields::get_offset_of_mainCamera_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3861 = { sizeof (U3CGenSnapshotU3Ed__4_t77578824), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3861[2] = 
{
	U3CGenSnapshotU3Ed__4_t77578824::get_offset_of_U3CU3E1__state_0(),
	U3CGenSnapshotU3Ed__4_t77578824::get_offset_of_U3CU3E2__current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3862 = { sizeof (CodelessUIInteractEvent_t546423177), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3862[1] = 
{
	CodelessUIInteractEvent_t546423177::get_offset_of_U3CeventBindingManagerU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3863 = { sizeof (FBSDKViewHiearchy_t250320177), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3864 = { sizeof (FBSDKCodelessPathComponent_t3390368644), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3864[9] = 
{
	FBSDKCodelessPathComponent_t3390368644::get_offset_of_U3CclassNameU3Ek__BackingField_0(),
	FBSDKCodelessPathComponent_t3390368644::get_offset_of_U3CtextU3Ek__BackingField_1(),
	FBSDKCodelessPathComponent_t3390368644::get_offset_of_U3ChintU3Ek__BackingField_2(),
	FBSDKCodelessPathComponent_t3390368644::get_offset_of_U3CdescU3Ek__BackingField_3(),
	FBSDKCodelessPathComponent_t3390368644::get_offset_of_U3CtagU3Ek__BackingField_4(),
	FBSDKCodelessPathComponent_t3390368644::get_offset_of_U3CindexU3Ek__BackingField_5(),
	FBSDKCodelessPathComponent_t3390368644::get_offset_of_U3CsectionU3Ek__BackingField_6(),
	FBSDKCodelessPathComponent_t3390368644::get_offset_of_U3CrowU3Ek__BackingField_7(),
	FBSDKCodelessPathComponent_t3390368644::get_offset_of_U3CmatchBitmaskU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3865 = { sizeof (FBSDKEventBinding_t3019614784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3865[6] = 
{
	FBSDKEventBinding_t3019614784::get_offset_of_U3CeventNameU3Ek__BackingField_0(),
	FBSDKEventBinding_t3019614784::get_offset_of_U3CeventTypeU3Ek__BackingField_1(),
	FBSDKEventBinding_t3019614784::get_offset_of_U3CappVersionU3Ek__BackingField_2(),
	FBSDKEventBinding_t3019614784::get_offset_of_U3CpathTypeU3Ek__BackingField_3(),
	FBSDKEventBinding_t3019614784::get_offset_of_U3CpathU3Ek__BackingField_4(),
	FBSDKEventBinding_t3019614784::get_offset_of_U3CparametersU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3866 = { sizeof (FBSDKEventBindingManager_t2486575668), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3866[1] = 
{
	FBSDKEventBindingManager_t2486575668::get_offset_of_U3CeventBindingsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3867 = { sizeof (GameroomFacebook_t453300599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3867[3] = 
{
	GameroomFacebook_t453300599::get_offset_of_appId_3(),
	GameroomFacebook_t453300599::get_offset_of_gameroomWrapper_4(),
	GameroomFacebook_t453300599::get_offset_of_U3CLimitEventUsageU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3868 = { sizeof (OnComplete_t3786498944), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3869 = { sizeof (GameroomFacebookGameObject_t2310521816), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3870 = { sizeof (U3CWaitForPipeResponseU3Ed__4_t3567704217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3870[5] = 
{
	U3CWaitForPipeResponseU3Ed__4_t3567704217::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForPipeResponseU3Ed__4_t3567704217::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForPipeResponseU3Ed__4_t3567704217::get_offset_of_U3CU3E4__this_2(),
	U3CWaitForPipeResponseU3Ed__4_t3567704217::get_offset_of_onCompleteDelegate_3(),
	U3CWaitForPipeResponseU3Ed__4_t3567704217::get_offset_of_callbackId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3871 = { sizeof (GameroomFacebookLoader_t1800644555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3872 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3873 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3874 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3875 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3876 = { sizeof (EditorFacebook_t2350935743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3876[3] = 
{
	EditorFacebook_t2350935743::get_offset_of_editorWrapper_3(),
	EditorFacebook_t2350935743::get_offset_of_U3CLimitEventUsageU3Ek__BackingField_4(),
	EditorFacebook_t2350935743::get_offset_of_U3CShareDialogModeU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3877 = { sizeof (EditorFacebookGameObject_t4122771246), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3878 = { sizeof (EditorFacebookLoader_t147050272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3879 = { sizeof (EditorFacebookMockDialog_t3691766720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3879[4] = 
{
	EditorFacebookMockDialog_t3691766720::get_offset_of_modalRect_4(),
	EditorFacebookMockDialog_t3691766720::get_offset_of_modalStyle_5(),
	EditorFacebookMockDialog_t3691766720::get_offset_of_U3CCallbackU3Ek__BackingField_6(),
	EditorFacebookMockDialog_t3691766720::get_offset_of_U3CCallbackIDU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3880 = { sizeof (EditorWrapper_t3420512207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3880[1] = 
{
	EditorWrapper_t3420512207::get_offset_of_callbackHandler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3881 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3882 = { sizeof (EmptyMockDialog_t3184315303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3882[1] = 
{
	EmptyMockDialog_t3184315303::get_offset_of_U3CEmptyDialogTitleU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3883 = { sizeof (MockLoginDialog_t1686232399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3883[1] = 
{
	MockLoginDialog_t1686232399::get_offset_of_accessToken_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3884 = { sizeof (U3CU3Ec__DisplayClass4_0_t95552448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3884[2] = 
{
	U3CU3Ec__DisplayClass4_0_t95552448::get_offset_of_facebookID_0(),
	U3CU3Ec__DisplayClass4_0_t95552448::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3885 = { sizeof (MockShareDialog_t1183999029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3885[1] = 
{
	MockShareDialog_t1183999029::get_offset_of_U3CSubTitleU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3886 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3887 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3888 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3889 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3890 = { sizeof (MobileFacebook_t4202726836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3890[1] = 
{
	MobileFacebook_t4202726836::get_offset_of_shareDialogMode_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3891 = { sizeof (MobileFacebookGameObject_t508240675), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3892 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3893 = { sizeof (IOSFacebook_t793364667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3893[3] = 
{
	IOSFacebook_t793364667::get_offset_of_limitEventUsage_4(),
	IOSFacebook_t793364667::get_offset_of_iosWrapper_5(),
	IOSFacebook_t793364667::get_offset_of_userID_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3894 = { sizeof (NativeDict_t1844390233), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3894[3] = 
{
	NativeDict_t1844390233::get_offset_of_U3CNumEntriesU3Ek__BackingField_0(),
	NativeDict_t1844390233::get_offset_of_U3CKeysU3Ek__BackingField_1(),
	NativeDict_t1844390233::get_offset_of_U3CValuesU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3895 = { sizeof (IOSFacebookGameObject_t1787084451), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3896 = { sizeof (IOSFacebookLoader_t2962337469), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3897 = { sizeof (AndroidFacebook_t3975172769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable3897[4] = 
{
	AndroidFacebook_t3975172769::get_offset_of_limitEventUsage_4(),
	AndroidFacebook_t3975172769::get_offset_of_androidWrapper_5(),
	AndroidFacebook_t3975172769::get_offset_of_userID_6(),
	AndroidFacebook_t3975172769::get_offset_of_U3CKeyHashU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3898 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable3898[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize3899 = { sizeof (AndroidFacebookGameObject_t4170725345), -1, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
